#ifndef TREATMECA_H
#define TREATMECA_H

#include <QFile>
#include <QTextStream>
#include <QDebug>

#include "Settings.h"
#include "InfoFile.h"
#include <cmath>

#include <iostream>
using namespace std;

class TreatMeca
{
    Settings* _sets = 0;
    InfoFile* _info = 0;
    QString _path = "";
    int index_TIME = -1;
    int index_DEP = -1;
    int index_FORCE = -1;
    int index_DEF1 = -1;
    int index_DEF2 = -1;
    int index_DEF3 = -1;
    int index_CTR = -1;
    int index_RE = -1;
    int index_EA = -1;
    QVector<QVector<QString>> header;
    QVector<QVector<double>> data;
    QVector<QVector<double>> treated_data;
    QVector<QVector<double>> treated_rawdata;

public:
    TreatMeca( InfoFile* info_file = 0, Settings* sets = 0 )
    {
        _sets = sets;
        _info = info_file;
        if ( _info != 0)
            _path = _info->path();
    }
    void treat()
    {
        bool mecaFileExist = readMecaFile();
        if ( mecaFileExist )
        {
            QString program = getProgram();
            QString header_data;
            QString header_rawdata;
            if ( program.contains("R8562.BAS") )
            {
                if ( _info->donneesDispo().isEmpty() )
                    _info->setDonneesDispo("R.E.");
                index_TIME = 0;
                index_FORCE = 1;
                index_DEF1 = 2;
                index_DEF2 = 3;
                index_RE = 4;
                header_data = "t(s);e(perc);s(MPa)";
                if ( _info->donneesDispo().contains("R.E.") )
                {
                    header_data += ";RE(mOhm);dR_R0(perc)";
                }
                double R0 = data[0][index_RE];
                for ( int it = 0; it < data.size(); ++it )
                {
                    QVector<double> line = data[it];
                    double def = 0.5*( line[index_DEF1] + line[index_DEF2] );
                    if ( def >= 0 &&
                         (line[index_TIME] < _info->tcut() || _info->tcut() == -1 ) )
                    {
                        // NEED SECTION !!!!
                        double section = _info->largeur() * _info->epaisseur();
                        double ctr = line[index_FORCE]/section;
                        if ( _info->epaisseur() == -1 )
                            ctr = line[index_FORCE]/_info->largeur();
                        double dRdR0 = 100*( line[index_RE] - R0 )/R0;
                        if ( R0 == 0 )
                            dRdR0 = 0;
                        QVector<double> dataLine = { line[index_TIME], def, ctr };
                        if ( _info->donneesDispo().contains("R.E.") )
                            dataLine.append( {line[index_RE], dRdR0} );
                        treated_data.append( dataLine );
                    }
                }
            }
            else if ( program.contains("RESIST.BAS") )
            {
                if ( _info->donneesDispo().isEmpty() )
                    _info->setDonneesDispo("R.E. - E.A.");
                index_TIME = 5;
                index_CTR = 0;
                index_DEF1 = 1;
                index_DEF2 = 2;
                index_RE = 4;
                index_EA = 3;
                header_data = "t(s);e(perc);s(MPa)";
                if ( _info->donneesDispo().contains("R.E.") )
                {
                    header_data += ";RE(mOhm);dR_R0(perc)";
                }
                if ( _info->donneesDispo().contains("E.A.") )
                {
                    header_data += ";EA(coups)";
                }
                double R0 = data[0][index_RE];
                for ( int it = 0; it < data.size(); ++it )
                {
                    QVector<double> line = data[it];
                    double def = 0.5*( line[index_DEF1] + line[index_DEF2] );
                    if ( def >= 0 &&
                         (line[index_TIME] < _info->tcut() || _info->tcut() == -1 ) )
                    {
                        double ctr = line[index_CTR];
                        double dRdR0 = 100*( line[index_RE] - R0 )/R0;
                        if ( R0 == 0 )
                            dRdR0 = 0;
                        QVector<double> dataLine = { line[index_TIME], def, ctr };
                        if ( _info->donneesDispo().contains("R.E.") )
                            dataLine.append( {line[index_RE], dRdR0} );
                        if ( _info->donneesDispo().contains("E.A.") )
                            dataLine.append( line[index_EA] );
                        treated_data.append( dataLine );
                    }
                }
            }
            else if ( program.contains("TRACMONO.BAS") )
            {
                if ( _info->donneesDispo().isEmpty() )
                    _info->setDonneesDispo("E.A.");
                index_TIME = 5;
                index_DEP = 3;
                index_FORCE = 0;
                index_DEF1 = 1;
                index_DEF2 = 2;
                index_CTR = 6;
                index_EA = 4;
                header_data = "t(s);e(perc);s(MPa)";
                if ( _info->donneesDispo().contains("E.A.") )
                {
                    header_data += ";EA(coups)";
                }
                for ( int it = 0; it < data.size(); ++it )
                {
                    QVector<double> line = data[it];
                    double def = 0.5*( line[index_DEF1] + line[index_DEF2] );
                    if ( def >= -0.001 &&
                         (line[index_TIME] < _info->tcut() || _info->tcut() == -1 ) )
                    {
                        double ctr = line[index_CTR];
                        QVector<double> dataLine = { line[index_TIME], def, ctr };
                        if ( _info->donneesDispo().contains("E.A.") )
                            dataLine.append( line[index_EA] );
                        treated_data.append( dataLine );
                    }
                }
            }
            else if ( program.contains("FLEXION.BAS") )
            {
                if ( _info->donneesDispo().isEmpty() )
                    _info->setDonneesDispo("E.A.");
                index_TIME = 3;
                index_DEP = 1;
                index_FORCE = 0;
                index_DEF1 = 6;
                index_CTR = 5;
                index_EA = 2;
                header_data = "t(s);e(perc);s(MPa)";
                header_rawdata = "t(s);fle(mm);forc(N)";
                if ( _info->donneesDispo().contains("E.A.") )
                {
                    header_data += ";EA(coups)";
                    header_rawdata += ";EA(coups)";
                }
                for ( int it = 0; it < data.size(); ++it )
                {
                    QVector<double> line = data[it];
                    double fle = line[index_DEP];
                    if ( fle >= -0.01 &&
                         (line[index_TIME] < _info->tcut() || _info->tcut() == -1 ) )
                    {
                        double force = line[index_FORCE];
                        double def = line[index_DEF1];
                        double ctr = line[index_CTR];
                        QVector<double> dataLine = { line[index_TIME], def, ctr };
                        QVector<double> rawdataLine = { line[index_TIME], fle, force };
                        if ( _info->donneesDispo().contains("E.A.") )
                        {
                            dataLine.append( line[index_EA] );
                            rawdataLine.append( line[index_EA] );
                        }
                        treated_data.append( dataLine );
                        treated_rawdata.append( rawdataLine );
                    }
                }
            }
            else if ( program.contains("FLEXION1.BAS") || program.contains("FLEXION2.BAS") )
            {
                if ( _info->donneesDispo().isEmpty() )
                    _info->setDonneesDispo("R.E. - E.A.");
                index_TIME = 4;
                index_DEP = 1;
                index_FORCE = 0;
                index_RE = 3;
                index_EA = 2;
                header_data = "t(s);e(perc);s(MPa)";
                header_rawdata = "t(s);fle(mm);forc(N)";
                if ( _info->donneesDispo().contains("R.E.") )
                {
                    header_data += ";RE(mOhm);dR_R0(perc)";
                    header_rawdata += ";RE(mOhm);dR_R0(perc)";
                }
                if ( _info->donneesDispo().contains("E.A.") )
                {
                    header_data += ";EA(coups)";
                    header_rawdata += ";EA(coups)";
                }
                double R0 = data[0][index_RE];
                for ( int it = 0; it < data.size(); ++it )
                {
                    QVector<double> line = data[it];
                    double fle = line[index_DEP];
                    if ( fle >= -0.01 &&
                         (line[index_TIME] < _info->tcut() || _info->tcut() == -1 ) )
                    {
                        double force = line[index_FORCE];
                        // NEED L b et e !!!!
                        double L = _info->longueur();
                        double b = _info->largeur();
                        double e = _info->epaisseur();
                        double def = 100*(6*e/(L*L))*line[index_DEP];
                        double ctr = 1.5*(L/(b*e*e))*line[index_FORCE];
                        double dRdR0 = 100*( line[index_RE] - R0 )/R0;
                        if ( R0 == 0 )
                            dRdR0 = 0;
                        QVector<double> dataLine = { line[index_TIME], def, ctr };
                        QVector<double> rawdataLine = { line[index_TIME], fle, force };
                        if ( _info->donneesDispo().contains("R.E.") )
                        {
                            dataLine.append( {line[index_RE], dRdR0} );
                            rawdataLine.append( {line[index_RE], dRdR0} );
                        }
                        if ( _info->donneesDispo().contains("E.A.") )
                        {
                            dataLine.append( line[index_EA] );
                            rawdataLine.append( line[index_EA] );
                        }
                        treated_data.append( dataLine );
                        treated_rawdata.append( rawdataLine );
                    }
                }
            }
            else if ( program.contains("IOSI3J.BAS") )
            {
                if ( _info->donneesDispo().isEmpty() )
                    _info->setDonneesDispo("R.E. - E.A.");
                index_TIME = 3;
                index_DEP = 1;
                index_FORCE = 0;
                index_DEF1 = 4;
                index_DEF2 = 5;
                index_DEF3 = 6;
                index_RE = 7;
                index_EA = 2;
                header_data = "t(s);epsilon12(perc);sigma12(MPa)";
                if ( _info->donneesDispo().contains("R.E.") )
                    header_data += ";RE(mOhm);dR_R0(perc)";
                if ( _info->donneesDispo().contains("E.A.") )
                    header_data += ";EA(coups)";
                double R0 = data[0][index_RE];
                for ( int it = 0; it < data.size(); ++it )
                {
                    QVector<double> line = data[it];
                    if ( (line[index_TIME] < _info->tcut() || _info->tcut() == -1 ) )
                    {
                        double force = line[index_FORCE];
                        // NEED L b et e !!!!
                        double b = _info->largeur();
                        double e = _info->epaisseur();
                        double def = line[index_DEF1] - line[index_DEF2];
                        double ctr = force/(b*e);
                        if ( e == -1 )
                            ctr = force/b;
                        double dRdR0 = 100*( line[index_RE] - R0 )/R0;
                        if ( R0 == 0 )
                            dRdR0 = 0;
                        QVector<double> dataLine = { line[index_TIME], def, ctr };
                        if ( _info->donneesDispo().contains("R.E.") )
                            dataLine.append( {line[index_RE], dRdR0} );
                        if ( _info->donneesDispo().contains("E.A.") )
                            dataLine.append( line[index_EA] );
                        treated_data.append( dataLine );
                    }
                }
            }
            else if ( program.contains("VIEIL") )
            {
                if ( _info->donneesDispo().isEmpty() )
                    _info->setDonneesDispo("");
                index_TIME = 0;
                index_DEP = 8;
                index_FORCE = 7;
                int ind_TEMP = 9;
                header_data = "t(s);e(perc);s(MPa);T(C)";
                bool _tempFour = false;
                QFile file( _path+"/Remarques.txt");
                QString remarques;
                if ( file.open(QFile::ReadOnly) )
                {
                    QTextStream in( &file );
                    in.setCodec("ISO 8859-1");
                    while ( !in.atEnd() )
                    {
                        QString line = in.readLine().toUtf8();
                        if ( (line.contains("sservissement") || line.contains("égulation")) && line.contains("thermocouple") && line.contains("four") && !line.contains("erreur") )
                            _tempFour = true;
                        remarques += line+"\n";
                    }
                    file.close();
                }
                if ( !_info->commentaire().contains(remarques) )
                {
                    if ( !_info->commentaire().isEmpty() )
                        _info->setCommentaire( _info->commentaire() + "\n" + remarques );
                    else
                        _info->setCommentaire( remarques );
                }
                for ( int it = 0; it < data.size(); ++it )
                {
                    QVector<double> line = data[it];
                    if ( (line[index_TIME] < _info->tcut() || _info->tcut() == -1 ) )
                    {
                        double force = line[index_FORCE];
                        // NEED L b et e !!!!
                        double b = _info->largeur();
                        double e = _info->epaisseur();
                        double def = -1;//line[index_DEP];
                        double ctr = force/(b*e);
                        double temp = line[ind_TEMP];
                        if ( _tempFour )
                            temp = - 0.0000005127092*pow(line[ind_TEMP], 3)
                                    + 0.0007545365*pow(line[ind_TEMP], 2)
                                    + 0.9624921*line[ind_TEMP]
                                    + 5.306989;
                        if ( e == -1 )
                            ctr = force/b;
                        treated_data.append( { line[index_TIME], def, ctr, temp } );
                    }
                }
            }
            _info->save();

            lissageParMoyenneGlissee();

            QFile treatFile( _path + "/MECA_DEF_CONT.dat" );
            if ( treatFile.open(QFile::WriteOnly) )
            {
                QTextStream out( &treatFile );
                //out.setCodec("ISO 8859-1");
                out << header_data + "\n";
                out << convert( treated_data );
                treatFile.close();
            }
            if ( treated_rawdata.size() > 0 )
            {
                QFile treatFile( _path + "/MECA_DEP_FORCE.dat" );
                if ( treatFile.open(QFile::WriteOnly) )
                {
                    QTextStream out( &treatFile );
                    //out.setCodec("ISO 8859-1");
                    out << header_rawdata + "\n";
                    out << convert( treated_rawdata );
                    treatFile.close();
                }
            }
        }
    }
    QString convert( const QVector<QVector<double>>& rawData )
    {
        QString data = "";
        for ( QVector<double> line : rawData )
        {
            int size = line.size();
            for ( int it = 0; it < size; ++it )
            {
                data += QString::number(line.at(it));
                if ( it == size-1 )
                    data += "\n";
                else
                    data += ";";
            }
        }
        return data;
    }
    bool readMecaFile()
    {
        QFile file( _path + "/MECA.dat" );
        if ( file.open(QFile::ReadOnly) )
        {
            header.clear();
            data.clear();
            // get the content of the file
            QTextStream in( &file );
            in.setCodec("ISO 8859-1");
            bool isNumber = false;
            while ( !in.atEnd() )
            {
                QString line = in.readLine();
                if ( line.count('\t') == line.size() )
                    continue;
                QStringList temp = line.split("\t");
                QString tempNb = temp.at(0);
                tempNb.replace(',', '.').toDouble( &isNumber );
                if ( isNumber )
                {
                    QVector<double> lineNumber;
                    for ( QString col : temp )
                    {
                        lineNumber.append( col.replace(',', '.').toDouble() );
                    }
                    data.append( lineNumber );
                }
                else
                {
                    header.append( temp.toVector() );
                }
            }
            file.close();
            return true;
        }
        else
        {
            QFile file( _path + "/MECA.csv" );
            if ( file.open(QFile::ReadOnly) )
            {
                header.clear();
                data.clear();
                // get the content of the file
                QTextStream in( &file );
                in.setCodec("ISO 8859-1");
                bool isNumber = false;
                while ( !in.atEnd() )
                {
                    QString line = in.readLine();
                    QStringList temp = line.split(";");
                    if ( temp.size() == 1 )
                        temp = temp.first().split("\t");
                    if ( !isNumber )
                    {
                        header.append( temp.toVector() );
                        isNumber = true;
                    }
                    else
                    {
                        QVector<double> lineNumber;
                        for ( QString col : temp )
                        {
                            lineNumber.append( col.replace(',', '.').toDouble() );
                        }
                        data.append( lineNumber );
                    }
                }
                file.close();
                return true;
            }
            return false;
        }
    }
    QString getProgram()
    {
        QString prog;
        for ( int it = 0; it < header.size(); ++it )
        {
            for ( int jt = 0; jt < header.at(it).size(); ++jt )
            {
                prog = header[it][jt];
                if ( prog.contains(">>") && prog.contains("PROGRAMME") )
                {
                    return prog;
                }
            }
        }
        if ( QFile(_path).fileName().split("/").last().left(5).contains("VIEIL") )
            prog = "VIEIL";
        return prog;
    }
    void lissageParMoyenneGlissee()
    {
        int nbPts = _sets->getInt( SettingsKeys::Lissage, SettingsKeys::nbPoints );
        int hNbPts = 0.5*nbPts;
        int size = treated_data.size();
        //        qDebug() << hNbPts;
        QVector<QVector<double>> temp = treated_data;
        for ( int it = 0; it < size; ++it )
        {
            if ( it < hNbPts || it > size - hNbPts - 1 )
            {
                for ( int jt = 0; jt < treated_data[it].size(); ++jt )
                {
                    temp[it][jt] = treated_data[it][jt];
                }
            }
            else
            {
                temp[it][0] = treated_data[it][0];
                for ( int jt = 1; jt < treated_data[it].size(); ++jt )
                {
                    double somme = 0;
                    int count = 0;
                    for ( int lt = it - hNbPts; lt <= it + hNbPts; ++lt )
                    {
                        somme += treated_data[lt][jt];
                        ++count;
                    }
                    temp[it][jt] = somme/count;
                    //                    qDebug() << treated_data[it][jt] << temp[it][jt];
                }
                treated_data = temp;
            }
        }
        //        data = temp;
    }
};

#endif // TREATMECA_H
