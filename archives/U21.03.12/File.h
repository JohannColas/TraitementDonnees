#ifndef FILE_H
#define FILE_H

#include <QFile>
#include <QTextStream>
#include <QVector>

class File
{

public:
    static inline QVector<QVector<double>> getData( const QString& filepath )
    {
        QVector<QVector<double>> data;
        QFile file( filepath  );
        if ( file.open(QFile::ReadOnly) )
        {
            // get the content of the file
            QTextStream in( &file );
            bool isNumber = false;
            while ( !in.atEnd() )
            {
                QString line = in.readLine();
                QStringList temp = line.split(";");
                if ( temp.size() == 1 )
                    temp = temp.first().split("\t");
                temp.first().toDouble(&isNumber);
                if ( isNumber )
                {
                    QVector<double> lineNumber;
                    for ( QString col : temp )
                    {
                        lineNumber.append( col.replace(',', '.').toDouble() );
                    }
                    data.append( lineNumber );
                }
            }
            file.close();
        }
        return data;
    }
    static inline void save( const QString& path, const QString& content )
    {
        QFile saveFile( path );
        saveFile.open( QIODevice::WriteOnly | QIODevice::Text );
        QTextStream out( &saveFile );
        out << content;
        saveFile.close();
    }
    static inline void save( const QString& path, const QStringList& content )
    {
        QFile saveFile( path );
        saveFile.open( QIODevice::WriteOnly | QIODevice::Text );
        QTextStream out( &saveFile );
        for ( QString line : content )
            out << line << "\n";
        saveFile.close();
    }
};

#endif // FILE_H
