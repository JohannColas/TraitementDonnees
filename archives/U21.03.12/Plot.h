#ifndef PLOT_H
#define PLOT_H

#include <QFile>
#include <QProcess>
#include <QDebug>
#include "Settings.h"
#include "InfoFile.h"

namespace PlotKeys {
    static QString xlabel = "%%%XLABEL%%%";
    static QString ylabel = "%%%YLABEL%%%";
    static QString yblabel = "%%%YBLABEL%%%";
    static QString ytlabel = "%%%YTLABEL%%%";
    static QString xindex = "%%%XIND%%%";
    static QString yindex = "%%%YIND%%%";
    static QString ybindex = "%%%YBIND%%%";
    static QString ytindex = "%%%YTIND%%%";
    static QString ycolor = "%%%YCLR%%%";
    static QString ybcolor = "%%%YBCLR%%%";
    static QString ytcolor = "%%%YTCLR%%%";
    static QString ylegend = "%%%YLEGEND%%%";
    static QString yblegend = "%%%YBLEGEND%%%";
    static QString ytlegend = "%%%YTLEGEND%%%";

    static QString TIME = "Temps (s)";
    static QString DEF_LABEL = "Déformation (\\%)";
    static QString CTR_LABEL = "Contrainte (MPa)";
    static QString EA_LABEL = "E.A. (coups)";
    static QString RE_LABEL = "$\\mathbfsf{\\frac{\\Updelta R}{R_0}}$ (\\%)";
    static QString T_LABEL = "Température (°C)";
    static QString TIME_LEGEND = "0";
    static QString DEF_LEGEND = "Déformation";
    static QString CTR_LEGEND = "Contrainte";
    static QString EA_LEGEND = "Émission acoustique";
    static QString RE_LEGEND = "Résistance électrique";
    static QString T_LEGEND = "Température";
    static QString DEF_COLOR = "CLR3";
    static QString CTR_COLOR = "CLR2";
    static QString EA_COLOR = "CLR3";
    static QString RE_COLOR = "CLR4";
    static QString T_COLOR = "CLR3";
}

class Plot
{
    QString _path;
    Settings* _sets = 0;

public:
    Plot( QString path, Settings* sets = 0 )
    {
        _path = path;
        _sets = sets;
    }
    void generate( const QString& path = "" )
    {
        if ( path != "" )
            _path = path;

        QDir dir( _path + "/plots/" );
        dir.removeRecursively();
        dir.mkpath( "LaTeX" );

        InfoFile infoF( _path );
        QString prefix;
        if ( infoF.essai().contains("TRAC_") )
        {
            prefix = "TRAC-";
        }
        else if ( infoF.essai().contains("FLEX_") )
        {
            prefix = "FLEX-";
        }
        else if ( infoF.essai().contains("IOSI_") ||
                  infoF.essai().contains("CISA_") )
        {
            prefix = "CISA-";
        }
        else if ( infoF.essai().left(6) == "VIEIL_" )
        {
            prefix = "VIEIL-";
        }
        if ( !prefix.isEmpty() )
        {
            QStringList plots;
            if ( prefix != "VIEIL-" )
            {
                plots.append( { "t-s-e", "e-s" } );
                if ( infoF.donneesDispo().contains("R.E.") &&
                     infoF.donneesDispo().contains("E.A.") )
                    plots.append( {"t-s-EA-RE","e-s-EA-RE"} );
                else
                    if ( infoF.donneesDispo().contains("R.E.") )
                        plots.append( {"t-s-RE", "e-s-RE"} );
                    else if ( infoF.donneesDispo().contains("E.A.") )
                        plots.append( {"t-s-EA", "e-s-EA"} );
            }
            else
            {
                plots.append( "t-s-T" );
            }

            QDir(_path).mkdir( "plots" );
            // Pour chaque graphe à construire
            for ( QString plotName : plots )
            {
//                qDebug() << plotName;
                // Copie le fichier TEX vers le dossier de l'essai en cours (essai/plots)
                copy( prefix, plotName );
                // Inserer Donnees
                insertData( plotName );
                // Compilation le fichier TEX
                compile( plotName );
                compile( plotName );
                // Convertion du PDF vers PNG
                convertPDFtoPNG( plotName );
            }
            // Suppression des fichiers de compilation
            deleteCompilFiles();
        }
    }
    void copy( const QString& prefix, const QString& plotName )
    {
        QString plotTplDir = _sets->getString( SettingsKeys::Plots + "/" + SettingsKeys::TplDir);
//        QFile::copy( plotTplDir + "/" + prefix + plotName + ".tex",
//                     _path + "/plots/" + plotName + ".tex" );
        QString plotTpl = "x-y";
        if ( plotName.count('-') == 2 )
            plotTpl = "x-y-y";
        else if ( plotName.count('-') == 3 )
            plotTpl = "x-y-y-y";
        QFile::copy( plotTplDir + "/" + plotTpl + ".tex",
                     _path + "/plots/LaTeX/" + plotName + ".tex" );
    }
    void insertData( const QString& plotName )
    {
        QFile file( _path + "/plots/LaTeX/" + plotName + ".tex" );
        file.open( QIODevice::ReadOnly );
        QTextStream in( &file );

        QString contents = in.readAll();
        contents.replace("%%%FILENAME%%%", _path+"/MECA_DEF_CONT.dat");
        QStringList strl = plotName.split("-");
        if ( strl.at(0) == "t" )
        {
            contents.replace( PlotKeys::xlabel, PlotKeys::TIME );
            contents.replace( PlotKeys::xindex, "0" );
        }
        else if ( strl.at(0) == "e" )
        {
            contents.replace( PlotKeys::xlabel, PlotKeys::DEF_LABEL );
            contents.replace( PlotKeys::xindex, "1" );
        }
        if ( strl.at(1) == "s" )
        {
            contents.replace( PlotKeys::ylabel, PlotKeys::CTR_LABEL );
            contents.replace( PlotKeys::yindex, "2" );
            contents.replace( PlotKeys::ylegend, PlotKeys::CTR_LEGEND );
            contents.replace( PlotKeys::ycolor, PlotKeys::CTR_COLOR );
        }
        if ( strl.size() == 3 )
        {
            if ( strl.at(2) == "EA" )
            {
                contents.replace( PlotKeys::yblabel, PlotKeys::EA_LABEL );
                contents.replace( PlotKeys::ybindex, "3" );
                contents.replace( PlotKeys::yblegend, PlotKeys::EA_LEGEND );
                contents.replace( PlotKeys::ybcolor, PlotKeys::EA_COLOR );
            }
            else if ( strl.at(2) == "RE" )
            {
                contents.replace( PlotKeys::yblabel, PlotKeys::RE_LABEL );
                contents.replace( PlotKeys::ybindex, "4" );
                contents.replace( PlotKeys::yblegend, PlotKeys::RE_LEGEND );
                contents.replace( PlotKeys::ybcolor, PlotKeys::RE_COLOR );
            }
            else if ( strl.at(2) == "T" )
            {
                contents.replace( PlotKeys::yblabel, PlotKeys::T_LABEL );
                contents.replace( PlotKeys::ybindex, "3" );
                contents.replace( PlotKeys::yblegend, PlotKeys::T_LEGEND );
                contents.replace( PlotKeys::ybcolor, PlotKeys::T_COLOR );
            }
            else if ( strl.at(2) == "e" )
            {
                contents.replace( PlotKeys::yblabel, PlotKeys::DEF_LABEL );
                contents.replace( PlotKeys::ybindex, "1" );
                contents.replace( PlotKeys::yblegend, PlotKeys::DEF_LEGEND );
                contents.replace( PlotKeys::ybcolor, PlotKeys::DEF_COLOR );
            }
        }
        else if ( strl.size() == 4 )
        {
            if ( strl.at(2) == "EA" && strl.at(3) == "RE" )
            {
                contents.replace( PlotKeys::yblabel, PlotKeys::EA_LABEL );
                contents.replace( PlotKeys::ybindex, "5" );
                contents.replace( PlotKeys::yblegend, PlotKeys::EA_LEGEND );
                contents.replace( PlotKeys::ybcolor, PlotKeys::EA_COLOR );
                contents.replace( PlotKeys::ytlabel, PlotKeys::RE_LABEL );
                contents.replace( PlotKeys::ytindex, "4" );
                contents.replace( PlotKeys::ytlegend, PlotKeys::RE_LEGEND );
                contents.replace( PlotKeys::ytcolor, PlotKeys::RE_COLOR );
            }
        }

        file.close();
        file.remove();

        file.open( QIODevice::WriteOnly );
        QTextStream out( &file );
        out << contents;
        file.close();
    }
    void compile( const QString& plotName )
    {
        command( "pdflatex -synctex=1 -interaction=nonstopmode \"" + _path + "/plots/LaTeX/" + plotName + ".tex\"",
                 _path + "/plots/LaTeX" );
    }
    void convertPDFtoPNG( const QString& plotName )
    {
        QFile::copy( _path + "/plots/LaTeX/" + plotName + ".pdf",
                     _path + "/plots/" + plotName + ".pdf" );
        QFile::remove( _path + "/plots/LaTeX/" + plotName + ".pdf" );

        command( "convert -density 600 \"" + _path + "/plots/" + plotName + ".pdf\" \"" + _path + "/plots/" + plotName + ".png\"" );
    }
    void deleteCompilFiles()
    {
        QDir dir( QDir::currentPath() + "/PDFLaTeX_Compilation" );
        for ( QString fileName : dir.entryList( QDir::NoDotAndDotDot | QDir::Files ) )
        {
            QFile file( QDir::currentPath() + "/PDFLaTeX_Compilation/" + fileName );
            file.remove();
        }
    }
    void command( const QString& cmd, const QString& workingPath = "" )
    {
            QProcess process;
            QDir( QDir::currentPath() ).mkdir( "PDFLaTeX_Compilation" );
            process.setWorkingDirectory( QDir::currentPath() + "/PDFLaTeX_Compilation" );
            if ( workingPath != "" )
                process.setWorkingDirectory( workingPath );

//            QStringList args;   //Contains arguments of the command
//            for ( int it = 1; it < cmd.size(); ++it )
//                args.append( cmd.at(it) );
            process.start( cmd/*.at(0), args*/, QIODevice::ReadOnly ); //Starts execution of command
            process.waitForFinished();

//            for ( QString str : cmd )
//                qDebug() << str;
//            qDebug() << "Failed:" << process.errorString();
//            qDebug() << "Output:" << process.readAll();
//            qDebug() << "";
//            qDebug() << "";
    }
};

#endif // PLOT_H
