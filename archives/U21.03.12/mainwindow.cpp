#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QUrl>
#include <QDesktopServices>
#include <QFileDialog>
#include <QDebug>
#include <QProgressBar>
#include <QElapsedTimer>
#include <QTime>
#include <QGraphicsScene>

#include "TreatMeca.h"
#include "Treatment.h"
#include "Widgets/ProgressWidget.h"

MainWindow::MainWindow( QWidget *parent )
    : QMainWindow( parent )
    , ui( new Ui::MainWindow )
{
    ui->setupUi( this );
//    QString essairDir = _settings->essaisDir();
    QString essairDir = _settings->getString( SettingsKeys::EssaiDir );
    ui->pb_dirEssai->setText( "Chemin : " + essairDir );
    ui->pb_templatesPath->setText( _settings->getString( SettingsKeys::Plots + "/" + SettingsKeys::TplDir ) );
    model->setRootPath( essairDir );
    //    model->setFilter( QDir::NoDotAndDotDot | QDir::Dirs );
    ui->treeView->setModel( model );
    ui->treeView->setRootIndex( model->index(essairDir) );
    ui->treeView->setColumnHidden( 1, true );
    ui->treeView->setColumnHidden( 2, true );
    ui->treeView->setColumnHidden( 3, true );
    ui->treeView->header()->hide();

    ui->tabWidget->setTabVisible( 2, false );
    ui->tabWidget->setTabVisible( 3, false );
    ui->tabWidget->setTabVisible( 4, false );
    ui->pb_BuildReport->hide();
    //
    QRegExp doubleRE = QRegExp("^(-?)(0|([1-9][0-9]*))(\\.[0-9]+)?$");
    ui->champ_longueur->setValidator( new QRegExpValidator( doubleRE, ui->champ_longueur ));
    ui->champ_largeur->setValidator( new QRegExpValidator( doubleRE, ui->champ_largeur ));
    ui->champ_epaisseur->setValidator( new QRegExpValidator( doubleRE, ui->champ_epaisseur ));
    ui->champ_tcut->setValidator( new QRegExpValidator( doubleRE, ui->champ_tcut ));

    connect( ui->champ_longueur, &QLineEdit::editingFinished,
             this, &MainWindow::saveInfo );
    connect( ui->champ_largeur, &QLineEdit::editingFinished,
             this, &MainWindow::saveInfo );
    connect( ui->champ_epaisseur, &QLineEdit::editingFinished,
             this, &MainWindow::saveInfo );
    connect( ui->champ_tcut, &QLineEdit::editingFinished,
             this, &MainWindow::saveInfo );
    connect( ui->champ_suivi, &QLineEdit::editingFinished,
             this, &MainWindow::saveInfo );
    connect( ui->champ_commentaire, &PlainTextEdit::editingFinished,
             this, &MainWindow::saveInfo );

    //
    ui->splitter->widget(0)->setMaximumWidth(350);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pb_dirEssai_clicked()
{
    QString path = _settings->getString( SettingsKeys::EssaiDir );
    if ( path.isEmpty() )
        path = QDir::homePath();
    QString newpath = QFileDialog::getExistingDirectory( this,
                                                         "Veuillez choisir le dossier contenant les résusltats de vos essais !",
                                                         path,
                                                         QFileDialog::ShowDirsOnly );
    if ( !newpath.isEmpty() )
    {
        ui->pb_dirEssai->setText( "Chemin : " + newpath );
        _settings->save( SettingsKeys::EssaiDir, newpath );
        model->setRootPath( newpath );
        ui->treeView->setRootIndex( model->index(newpath) );
    }
}

void MainWindow::on_treeView_clicked( const QModelIndex &index )
{
    QString path = model->filePath( index );
    info_file->clear();
    QFileInfo meca( path + "/MECA.dat" );
    QFileInfo meca2( path + "/MECA.csv" );
    if ( meca.exists() || meca2.exists() )
    {
//        ui->wid_container->show();
        info_file->setPath( path );
        updateInfoView();
        updatePlotSelector();

        if ( ui->tabWidget->currentIndex() == 0 )
        {
        }
        else if ( ui->tabWidget->currentIndex() == 1 )
        {
            ui->meca_rawdata->setChecked( true );
            emit ui->meca_rawdata->clicked( true );
        }
        else if ( ui->tabWidget->currentIndex() == 2 )
        {

        }
        else if ( ui->tabWidget->currentIndex() == 3 )
        {

        }
        else if ( ui->tabWidget->currentIndex() == 4 )
        {

        }
    }
    else
    {
//        ui->wid_container->hide();
        clearContainer();
    }
}

void MainWindow::on_treeView_doubleClicked( const QModelIndex &index )
{
    QString path = model->filePath( index );
    QDesktopServices::openUrl( QUrl( "file:"+path ) );
}

void MainWindow::on_meca_rawdata_clicked( bool checked )
{
    ui->meca_traite->setChecked( !checked );
    if ( checked )
        showMecaRawData();
    else
        showMecaTraite();
}

void MainWindow::on_meca_traite_clicked( bool checked )
{
//    ui->meca_rawdata->blockSignals(true);
    ui->meca_rawdata->setChecked( !checked );
    if ( checked )
        showMecaTraite();
    else
        showMecaRawData();
}

void MainWindow::showMecaRawData()
{
    QString path = model->filePath( ui->treeView->currentIndex() );
    QFile file(path + "/MECA.dat" );
    if ( file.open(QFile::ReadOnly) )
    {
        QTextStream in( &file );
        in.setCodec("ISO 8859-1");
        QString str = in.readAll();
        ui->meca_text_file->setPlainText( str );
        file.close();
    }
    else
    {
        QFile file(path + "/MECA.csv" );
        if ( file.open(QFile::ReadOnly) )
        {
            QTextStream in( &file );
//            in.setCodec("ISO 8859-1");
            QString str = in.readAll();
            ui->meca_text_file->setPlainText( str );
            file.close();
        }
        else
            ui->meca_text_file->setPlainText( "No MECA File." );
    }

}

void MainWindow::showMecaTraite()
{
    QString path = model->filePath( ui->treeView->currentIndex() );
    QFile file( path + "/MECA_DEF_CONT.dat" );
    if ( file.open(QFile::ReadOnly) )
    {
        QTextStream in( &file );
        QString str = in.readAll();
        ui->meca_text_file->setPlainText( str );
        file.close();
    }
    else
        ui->meca_text_file->setPlainText( "No MECA File." );
}

void MainWindow::updateInfoView()
{
    ui->info_materiau->setText( info_file->materiau() );
    ui->info_eprouvette->setText( info_file->eprouvette() );
    ui->info_essai->setText( info_file->essai() );
    // Dimensions éprouvettes
    ui->champ_longueur->setText( QString::number( info_file->longueur() ) );
    ui->champ_largeur->setText( QString::number( info_file->largeur() ) );
    ui->champ_epaisseur->setText( QString::number( info_file->epaisseur() ) );
    ui->champ_tcut->setText( QString::number( info_file->tcut() ) );
    ui->champ_suivi->setText( info_file->donneesDispo() );
    // Commentaires
    ui->champ_commentaire->setPlainText( info_file->commentaire() );
}

void MainWindow::clearContainer()
{
    info_file->fullClear();
    ui->info_materiau->setText( "" );
    ui->info_eprouvette->setText( "" );
    ui->info_essai->setText( "" );
    // Dimensions éprouvettes
    ui->champ_longueur->setText( "" );
    ui->champ_largeur->setText( "" );
    ui->champ_epaisseur->setText( "" );
    ui->champ_tcut->setText( "" );
    ui->champ_suivi->setText( "" );
    // Commentaires
    ui->champ_commentaire->setPlainText( "" );
    //
    ui->meca_text_file->setPlainText( "No MECA File." );
    updatePlotSelector();
}

void MainWindow::on_tabWidget_currentChanged( int index )
{
    if ( index == 0 )
    {
        QString path = model->filePath( ui->treeView->currentIndex() );
        QFile file( path+"/INFO" );
        if ( file.open( QFile::ReadOnly ) )
        {
            //            QTextStream in( &file );
            //            QString str = in.readAll();
            //            ui->meca_text_file->setPlainText( str );
            file.close();
        }
        else
        {
            //            ui->meca_text_file->setPlainText( "No MECA File" );
        }
    }
    else if ( index == 1 )
    {
        ui->meca_rawdata->setChecked( true );
        emit ui->meca_rawdata->clicked( true );
    }
}

void MainWindow::saveInfo()
{
    QString path = model->filePath( ui->treeView->currentIndex() );
    if ( path.contains(" - ") )
    {
        //
        info_file->setMateriau( ui->info_materiau->text() );
        info_file->setEprouvette( ui->info_eprouvette->text());
        info_file->setEssai( ui->info_essai->text() );
        // Dimensions éprouvettes
        info_file->setLongueur( ui->champ_longueur->text().toDouble() );
        info_file->setLargeur( ui->champ_largeur->text().toDouble() );
        info_file->setEpaisseur( ui->champ_epaisseur->text().toDouble() );
        info_file->setTcut( ui->champ_tcut->text().toDouble() );
        info_file->setDonneesDispo( ui->champ_suivi->text() );
        // Commentaires
        info_file->setCommentaire( ui->champ_commentaire->toPlainText() );
        info_file->save();
    }
}

void MainWindow::on_pb_templatesPath_clicked()
{
    QString path = _settings->getString( SettingsKeys::Plots + "/" + SettingsKeys::TplDir );
    if ( path.isEmpty() )
        path = QDir::homePath();
    QString newpath = QFileDialog::getExistingDirectory( this,
                                                         "Veuillez choisir le dossier contenant les templates LaTeX des graphes !",
                                                         path,
                                                         QFileDialog::ShowDirsOnly );
    if ( !newpath.isEmpty() )
    {
        ui->pb_templatesPath->setText( newpath );
        _settings->save( SettingsKeys::Plots + "/" + SettingsKeys::TplDir, newpath );
    }
}

void MainWindow::treatDirectory( const QString& path )
{
    QLabel* statusLabel = 0;
    if ( !_treatAll )
    {
        statusLabel = new QLabel("Processing...");
        statusBar()->addWidget( statusLabel );
    }
    QEventLoop loop;
    Treatment treat;
    connect( &treat, SIGNAL(finished()),
             &loop, SLOT(quit()) );
    treat.go( path, _settings );
    loop.exec();
    if ( !_treatAll )
        statusBar()->removeWidget( statusLabel );
    if ( statusLabel )
        delete statusLabel;
}

QStringList MainWindow::listDirectories( const QString& path )
{
    QStringList dirList = QDir(path).entryList(QDir::NoDotAndDotDot | QDir::Dirs );
    QStringList temp;
    bool recursiveSearch = _settings->getBool( SettingsKeys::RecursiveSearch );
    for ( QString dir : dirList )
    {
        QFileInfo info( path + "/" + dir + "/ESSAI.info" );
        QFileInfo meca( path + "/" + dir + "/MECA.dat" );
        QFileInfo meca2( path + "/" + dir + "/MECA.csv" );
        if ( info.exists() && ( meca.exists() || meca2.exists() ) )
            temp.append( path + "/" + dir );
        // Recursive Search
        if ( recursiveSearch )
            temp.append( listDirectories( path + "/" + dir ) );
    }
    return temp;
}


void MainWindow::updatePlotSelector()
{
    QDir dir( info_file->path() + "/plots" );
    QStringList pngs = dir.entryList( {"*.png", "*.tiff", "*.jpg"}, QDir::NoDotAndDotDot | QDir::Files );
    ui->plotSelector->clear();
    ui->plotSelector->addItems(pngs);
}
void MainWindow::on_plotSelector_currentTextChanged( const QString &arg1 )
{
    ui->imageViewer->loadFile( info_file->path() + "/plots/" + arg1 );
}


void MainWindow::on_pb_ptsCaract_clicked()
{
    PointsCaract ptsCaract;
    ptsCaract.run( info_file->path(), _settings );
}


void MainWindow::on_pb_traiterCetEssai_released()
{
    QString path = model->filePath( ui->treeView->currentIndex() );
    QFileInfo info( path + "/ESSAI.info" );
    QFileInfo meca( path + "/MECA.dat" );
    QFileInfo meca2( path + "/MECA.csv" );
    if ( info.exists() && ( meca.exists() || meca2.exists() ) )
    {
        //
        saveInfo();
        //
        treatDirectory( path );
        //
        updateInfoView();
        updatePlotSelector();
        ui->meca_traite->setChecked( true );
        emit ui->meca_traite->clicked( true );
    }
}
void MainWindow::on_pb_toutTraiter_released()
{
    _treatAll = true;
    ProgressWidget* progress = new ProgressWidget;
    connect( progress, SIGNAL(stopT()),
             this, SLOT(stopTreatment()) );
    statusBar()->addWidget( progress );


    QStringList list = listDirectories( _settings->getString( SettingsKeys::EssaiDir ) );
    int size = list.size(), it = 0;
    QElapsedTimer timer;
    timer.start();
    for ( QString dir : list )
    {
        if ( _stopTreatement == true )
            break;
        progress->setProgress( (100*it)/size );

        if ( it != 0 )
        {
            QTime time = QTime(0,0,0).addSecs( (size-it)*timer.elapsed()/(1000*it) );
            progress->setRemaintime( time );
        }
//        QEventLoop loop;
//        Treatment treat;
//        connect( &treat, SIGNAL(finished()),
//                 &loop, SLOT(quit()) );
//        treat.go( dir, _settings );
//        loop.exec();
        treatDirectory( dir );

        ++it;
    }

    statusBar()->removeWidget( progress );
    delete progress;
    _stopTreatement = false;
    _treatAll = false;
    updatePlotSelector();
}
void MainWindow::stopTreatment()
{
    _stopTreatement = true;
}
