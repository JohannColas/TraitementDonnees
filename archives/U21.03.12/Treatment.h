#ifndef TREATMENT_H
#define TREATMENT_H
/**********************************************/
#include <QApplication>
#include <QObject>
#include <QThread>
#include "InfoFile.h"
#include "TreatMeca.h"
#include "PointsCaract.h"
#include "Plot.h"
#include "Report.h"
#include "Settings.h"
#include "CNI.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
struct TreatOptions
{
    bool treatMeca = true;
    bool treatPtsCaract = true;
    bool treatPlots = true;
    bool treatReport = true;
    bool treatCNI = false;
    bool treatRE = false;
    bool treatEA = false;
    void treatAll()
    {
        treatMeca = true;
        treatPtsCaract = true;
        treatPlots = true;
        treatReport = true;
        treatCNI = true;
        treatRE = true;
        treatEA = true;
    }
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Thread
        : public QThread
{
    Q_OBJECT
    QString _path;
    Settings* _sets = 0;
    TreatOptions* _opts = new TreatOptions();
    void run() override
    {
        InfoFile* infoF = new InfoFile;
        infoF->setPath( _path );

        if ( _opts->treatMeca )
        {
            TreatMeca meca( infoF, _sets );
            meca.treat();
        }
        if ( _opts->treatPtsCaract )
        {
            PointsCaract ptsCaract;
            ptsCaract.run( _path, _sets );
        }
        if ( _opts->treatPlots )
        {
            Plot plot( _path, _sets );
            plot.generate();
        }
        if ( _opts->treatReport )
        {
            Report report( infoF, _sets );
            report.generate();
        }

        delete infoF;
        emit finished();
    }

public:
    Thread( const QString& path, Settings* sets )
    {
        _path = path;
        _sets = sets;
    }
signals:
    void finished();
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Treatment
        : public QObject
{
    Q_OBJECT

public:
    void go( const QString& path, Settings* sets )
    {
        Thread *thread = new Thread( path, sets );
        connect( thread, &Thread::finished,
                 this, &Treatment::finished );
        thread->start();
    }

public slots:

signals:
    void finished();
};

// obj* ob = new obj;
// od->download(...); // ou treat(...) ?
/**********************************************/
/**********************************************/
/**********************************************/

#endif // TREATMENT_H
