#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QFileSystemModel>
#include <QLabel>
#include <QMainWindow>
#include "InfoFile.h"
#include "Plot.h"
#include "Settings.h"
#include "PointsCaract.h"
#include "Treatment.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
protected:
    void treatDirectory( const QString& path );
    QStringList listDirectories( const QString& path );

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void showMecaRawData();
    void showMecaTraite();
    void saveInfo();
    void updateInfoView();
    void clearContainer();

private slots:
    void closeEvent( QCloseEvent* event ) override
    {
        QMainWindow::closeEvent( event );
    }
    void on_pb_dirEssai_clicked();
    void on_treeView_clicked( const QModelIndex &index );
    void on_treeView_doubleClicked( const QModelIndex &index );
    void on_meca_rawdata_clicked( bool checked );
    void on_meca_traite_clicked( bool checked );
    void on_tabWidget_currentChanged( int index );
    void on_pb_templatesPath_clicked();
    void stopTreatment();
    void updatePlotSelector();
    void on_plotSelector_currentTextChanged(const QString &arg1);
    void on_pb_ptsCaract_clicked();
    void on_pb_traiterCetEssai_released();
    void on_pb_toutTraiter_released();


private:
    Ui::MainWindow *ui;
    QFileSystemModel *model = new QFileSystemModel;
    Settings* _settings = new Settings;
    InfoFile* info_file = new InfoFile;
    bool _stopTreatement = false;
    bool _treatAll = false;
    Treatment treat;
};
#endif // MAINWINDOW_H
