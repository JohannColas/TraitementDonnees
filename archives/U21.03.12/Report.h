#ifndef REPORT_H
#define REPORT_H

#include <QFile>
#include <QProcess>
#include <QDebug>
#include "Settings.h"
#include "InfoFile.h"

class Report
{
    InfoFile* _info = 0;
    Settings* _sets = 0;

public:
    Report( InfoFile* info = 0, Settings* sets = 0 )
    {
        _info = info;
        _sets = sets;
    }
    void generate()
    {
        if ( _info != 0 && _sets != 0 )
        {
//            QDir( _info->path() + "/plots/" ).removeRecursively();

            QString prefix;
            if ( _info->essai().contains("TRAC_") )
            {
                prefix = "TRAC";
                if ( _info->epaisseur() == -1 )
                    prefix = "TRAC_SECTION";
            }
            else if ( _info->essai().contains("FLEX_") )
            {
                prefix = "FLEX";
            }
            else if ( _info->essai().contains("IOSI_") ||
                      _info->essai().contains("CISA_") )
            {
                prefix = "CISA";
            }
            else if ( _info->essai().left(6) == "VIEIL_" )
            {
                prefix = "VIEIL";
                if ( _info->epaisseur() == -1 )
                    prefix = "VIEIL_SECTION";
            }
            if ( !prefix.isEmpty() )
            {
                QString reportName = "synthese";
                // Copie le fichier TEX vers le dossier de l'essai en cours
                copyTemplate( prefix, reportName );
                // Inserer Donnees
                insertData( reportName );
                // Compilation le fichier TEX
                compile( reportName );

                // Suppression des fichiers de compilation
                deleteCompilFiles();
            }
        }
    }
    void copyTemplate( const QString& prefix, const QString& reportName )
    {
        QString tplPath = _sets->getString( SettingsKeys::Reports + "/" + SettingsKeys::TplDir);
        QFile::remove( _info->path() + "/" + reportName + ".tex" );
        QFile::copy( tplPath + "/" + prefix + ".tex",
                     _info->path() + "/" + reportName + ".tex" );
//        qDebug() << tplPath + "/" + prefix + ".tex";
//        qDebug() << _info->path() + "/" + reportName + ".tex";
//        QFile file( _sets->templatesReportDir() + "/" + prefix + ".tex" );
//        if ( file.open( QIODevice::ReadOnly ) )
//            qDebug() <<"read OK";
//        file.close();
    }
    void insertData( const QString& reportName )
    {
        QFile file( _info->path() + "/" + reportName + ".tex" );
        file.open( QIODevice::ReadOnly );
        QTextStream in( &file );

        QString contents = in.readAll();
        contents.replace("%%DIRESSAI%%", _info->path());
        contents.replace("%%EPROUV%%", _info->eprouvette());
        QString essai = _info->essai();
        QString type;
        if ( essai.contains("TRAC") )
            type = "Traction ";
        else if ( essai.contains("FLEX") )
            type = "Flexion ";
        else if ( essai.contains("IOSI") )
            type = "Iosipescu ";
        else if ( essai.contains("RAIL") )
            type = "Shear Rail ";
        else if ( essai.left(5) == "VIEIL" )
            type = "Vieillissement";
        if ( essai.contains("MONO") )
            type += "monotone";
        else if ( essai.contains("CYCL") )
        {
            type += "cyclé";
            if ( essai.contains("TRAC") ||
                 essai.contains("FLEX") )
                type += "e";
        }
        contents.replace("%%TYPE%%", type);
        QString vieil = "Non-vieilli";
        if ( essai.contains("VIEIL_") )
        {
            int ind = essai.indexOf("VIEIL_") + 6;
            QString mid = essai.mid( ind, 4);
            vieil = "Vieilli à " + mid + "\\,\\textcelsius";
        }
        contents.replace("%%VIEIL%%", vieil);
        contents.replace("%%LONGUEUR%%", QString::number(_info->longueur()));
        contents.replace("%%LARGEUR%%", QString::number(_info->largeur()));
        contents.replace("%%EPAISSEUR%%", QString::number(_info->epaisseur()));
        if ( !essai.left(6).contains("VIEIL_") )
        {
            QString temp = "20";
            if ( essai.contains("_1300_VIEIL") || essai.contains("_1300 ") )
            {
                temp = "1300";
            }
            else if ( essai.contains("_1000_VIEIL") || essai.contains("_1000 ") )
            {
                temp = "1000";
            }
            contents.replace("%%TEMP%%", temp);
        }
        contents.replace("%%VITES%%", _info->vitesseEssai());
        QString suivi = "Aucun";
        QString suiviAB = "";
        if ( _info->donneesDispo().contains("R.E.") )
        {
            suivi = "Résistance électrique";
            suiviAB = "-RE";
        }
//        qDebug() << _info->donneesDispo();
        if ( _info->donneesDispo().contains("E.A.") )
        {
            suiviAB = "-EA" + suiviAB;
            if ( _info->donneesDispo().contains("R.E.") )
                suivi += " - Émission acoustique";
            else
                suivi = "Émission acoustique";
        }
        contents.replace("%%SUIVI%%", suivi);
        contents.replace("%%SUIVIAB%%", suiviAB);
        contents.replace("%%COMMENT%%", _info->commentaire().replace("\n","\\\\\n"));
        file.close();
        file.remove();

        file.open( QIODevice::WriteOnly );
        QTextStream out( &file );
        out << contents;

        QString prop_path = _info->path() + "/meca/PROP.tex";
        QFileInfo prop( prop_path );
        if ( prop.exists() )
        {
            out << "\n\n";
            out << QString("\t\\subsubsection{Propriétés}\n");
            out << "\n";
            out << QString("\t\t\\begin{jtable}{\\textwidth}{Éprouvette \\eprouvette; Propriétés initiales}{\\eprouvette-PROP}\n");
            out << QString("\t\t\t\\input{" + prop_path + "}\n");
            out << "\t\t\\end{jtable}\n";
        }
        QString prop_cycl_path = _info->path() + "/meca/PROP_CYCL.tex";
        QFileInfo prop_cycl( prop_cycl_path );
        if ( prop_cycl.exists() )
        {
            out << "\n";
            out << QString("\t\t\\begin{jtable}{\\textwidth}{Éprouvette \\eprouvette; Paramètres des boucles d'hystérésis}{\\eprouvette-PROP-CYCL}\n");
            out << QString("\t\t\t\\input{" + prop_cycl_path + "}\n");
            out << "\t\t\\end{jtable}\n";
        }

        file.close();

    }
    void compile( const QString& /*reportName*/ )
    {
    }
    void deleteCompilFiles()
    {
    }
    void command( const QString& cmd, const QString& workingPath = "" )
    {
        //        QProcess::startDetached( cmd );
        //        if ( cmd.size() > 0 )
        {
            QProcess process;
            QDir( QDir::currentPath() ).mkdir( "PDFLaTeX_Compilation" );
            process.setWorkingDirectory( QDir::currentPath() + "/PDFLaTeX_Compilation" );
            if ( workingPath != "" )
                process.setWorkingDirectory( workingPath );

            //            QStringList args;   //Contains arguments of the command
            //            for ( int it = 1; it < cmd.size(); ++it )
            //                args.append( cmd.at(it) );
            process.start( cmd/*.at(0), args*/, QIODevice::ReadOnly ); //Starts execution of command
            process.waitForFinished();

            //            for ( QString str : cmd )
            //                qDebug() << str;
            //            qDebug() << "Failed:" << process.errorString();
            //            qDebug() << "Output:" << process.readAll();
            //            qDebug() << "";
            //            qDebug() << "";
        }
    }
};

#endif // REPORT_H
