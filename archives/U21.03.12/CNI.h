#ifndef CNI_H
#define CNI_H

#include <QObject>
#include <QFile>
#include <QProcess>
#include <QDebug>
#include <QElapsedTimer>
#include "Settings.h"
#include "InfoFile.h"


class CNI
{
    InfoFile* _info = 0;
    Settings* _sets = 0;
    QStringList _imagePaths;
    int width = 1000;
    int height = 800;

public:
    virtual ~CNI()
    {

    }
    CNI( InfoFile* info = 0, Settings* sets = 0 )
    {
        _info = info;
        _sets = sets;
    }
    void treat()
    {
        if ( _info )
        {
                treatTIF();
//                dir.cd( _info->path() + "/CNI/traitees" );

        }
    }
    void treatTIF()
    {
        QDir dir( _info->path() + "/CNI/originales" );
        if ( dir.exists() )
        {
            QDir( _info->path() + "/CNI" ).mkdir("traitees");
            QElapsedTimer timer;
            timer.start();
            QStringList imagePaths = dir.entryList( {"*.tif"} );
            int scale = _info->lighterScale();
            for ( QString path : imagePaths )
            {
                QImage im( _info->path() + "/CNI/originales/" + path );
                width = im.width();
                height = im.height();
                for ( int it = 0; it < width; ++it )
                    for ( int jt = 0; jt < height; ++jt )
                    {
                        QColor col = im.pixelColor( it, jt );
                        im.setPixelColor( it, jt, col.lighter(scale));
                    }
                QString newpath = path.remove(".tif").remove("image");
                int nbImage = newpath.toInt();

                im.save( _info->path() + "/CNI/traitees/" + QString().asprintf("%03d",  nbImage ) );
            }
            qDebug() << timer.elapsed();
        }
    }
    void makeVideo()
    {
        QProcess process;
        process.setWorkingDirectory( _info->path() + "/CNI" );
        QString cmd = "ffmpeg";
        QStringList args;
        args << "-r 5";
        args << "-f image2";
        args << "-s " + QString::number(width)+"x"+QString::number(height);
        args << "-i traitees/%03d.png";
        args << "-vcodec libx264";
        args << "-crf 20";
        args << "-pix_fmt yuv420p";
        args << "CNI.mp4";
        process.start( cmd, args, QIODevice::ReadOnly ); //Starts execution of command
        process.waitForFinished();
    }
};

#endif // CNI_H
