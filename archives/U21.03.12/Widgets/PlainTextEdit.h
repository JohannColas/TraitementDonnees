#ifndef PLAINTEXTEDIT_H
#define PLAINTEXTEDIT_H

#include <QPlainTextEdit>

class PlainTextEdit
        : public QPlainTextEdit
{
    Q_OBJECT
public:
    PlainTextEdit( QWidget* parent = 0 )
        : QPlainTextEdit( parent )
    {

    }


protected slots:
    void focusOutEvent( QFocusEvent* event )
    {
        emit editingFinished();
        QPlainTextEdit::focusOutEvent( event );
    }

signals:
    void editingFinished();
};

#endif // PLAINTEXTEDIT_H
