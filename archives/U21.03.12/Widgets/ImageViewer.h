#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include <QMenu>
#include <QAction>
#include <QImageReader>
#include <QImage>
#include <QPixmap>

#include <QColorSpace>
#include <QHBoxLayout>
#include <QImageReader>
#include <QImageWriter>
#include <QKeyEvent>
#include <QLabel>
#include <QScrollArea>
#include <QScrollBar>
#include <QWheelEvent>
#include <QShortcut>

class ImageViewer
        : public QWidget
{
    Q_OBJECT
private:
    QImage image;
    QLabel *imageLabel;
    QScrollArea *scrollArea;
    double scaleFactor = 1;
    QAction *zoomInAct;
    QAction *zoomOutAct;
    QAction *normalSizeAct;
    QAction *fitToWindowAct;
    QMenu *viewMenu;
    QShortcut* sc_zoomIn;
    QShortcut* sc_zoomOut;

public:
    ImageViewer( QWidget* parent = 0 )
        : QWidget(parent), imageLabel(new QLabel)
        , scrollArea(new QScrollArea)
     {
        sc_zoomIn = new QShortcut( this );
        sc_zoomIn->setKey( QKeySequence( Qt::CTRL + Qt::Key_Z ) );
        connect( sc_zoomIn, &QShortcut::activated,
                 this, &ImageViewer::zoomIn );
        sc_zoomOut = new QShortcut( this );
        sc_zoomOut->setKey( QKeySequence( Qt::CTRL + Qt::Key_S ) );
        connect( sc_zoomOut, &QShortcut::activated,
                 this, &ImageViewer::zoomOut );

         imageLabel->setBackgroundRole(QPalette::Base);
         imageLabel->setStyleSheet( "background:white;" );
         imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
         imageLabel->setScaledContents(true);

         scrollArea->setBackgroundRole(QPalette::Base);
         scrollArea->setWidget(imageLabel);
         scrollArea->setVisible(false);
         this->setLayout( new QHBoxLayout );
         layout()->addWidget( scrollArea );

         setContextMenuPolicy( Qt::CustomContextMenu );
         connect( this, &ImageViewer::customContextMenuRequested,
                  this, &ImageViewer::showContextMenu );
         createActions();

         //         resize(QGuiApplication::primaryScreen()->availableSize() * 3 / 5);
     }

     bool loadFile(const QString &fileName)
     {
         QImageReader reader(fileName);
         reader.setAutoTransform(true);
         const QImage newImage = reader.read();
         if (newImage.isNull()) {
             scrollArea->setVisible(false);
             return false;
         }
         setImage(newImage);

         setWindowFilePath(fileName);
         normalSize();
         return true;
     }
private:
     void setImage(const QImage &newImage)
     {
         image = newImage;
         if (image.colorSpace().isValid())
             image.convertToColorSpace(QColorSpace::SRgb);
         imageLabel->setPixmap(QPixmap::fromImage(image));
         scaleFactor = 1.0;

         scrollArea->setVisible(true);
         fitToWindowAct->setEnabled(true);
         updateActions();

         normalSize();
     }

private slots:
     void showContextMenu( const QPoint& pos )
     {
         viewMenu->exec( this->mapToGlobal(pos) );
     }
     void zoomIn()
     {
         scaleImage(1.25);
     }
     void zoomOut()
     {
         scaleImage(0.8);
     }
     void normalSize()
     {
//         imageLabel->adjustSize();
         int width = imageLabel->pixmap(Qt::ReturnByValue).size().width();
         int height = imageLabel->pixmap(Qt::ReturnByValue).size().height();
         double scaleFx = ((double)this->width()-20)/width;
         double scaleFy = ((double)this->height()-20)/height;
         double scaleF = scaleFy;
         if ( scaleFy > scaleFx )
             scaleF = scaleFx;

//         qDebug() << (double)imageLabel->width() << scaleFx;;
         scaleImage( scaleF/scaleFactor );
//         qDebug() << imageLabel->width();
     }
     void fitToWindow()
     {
         bool fitToWindow = fitToWindowAct->isChecked();
         scrollArea->setWidgetResizable(fitToWindow);
         if (!fitToWindow)
             normalSize();
         updateActions();
     }
     void createActions()
     {

         viewMenu = new QMenu(tr("&View"));

         zoomInAct = viewMenu->addAction(tr("Zoom &In (25%)"), this, &ImageViewer::zoomIn);
         zoomInAct->setShortcut(QKeySequence::ZoomIn);
         zoomInAct->setEnabled(false);

         zoomOutAct = viewMenu->addAction(tr("Zoom &Out (25%)"), this, &ImageViewer::zoomOut);
         zoomOutAct->setShortcut(QKeySequence::ZoomOut);
         zoomOutAct->setEnabled(false);

         normalSizeAct = viewMenu->addAction(tr("&Normal Size"), this, &ImageViewer::normalSize);
         normalSizeAct->setShortcut(tr("Ctrl+S"));
         normalSizeAct->setEnabled(false);

         viewMenu->addSeparator();

         fitToWindowAct = viewMenu->addAction(tr("&Fit to Window"), this, &ImageViewer::fitToWindow);
         fitToWindowAct->setEnabled(false);
         fitToWindowAct->setCheckable(true);
         fitToWindowAct->setShortcut(tr("Ctrl+F"));
     }
     void updateActions()
     {
         zoomInAct->setEnabled(!fitToWindowAct->isChecked());
         zoomOutAct->setEnabled(!fitToWindowAct->isChecked());
         normalSizeAct->setEnabled(!fitToWindowAct->isChecked());
     }
     void scaleImage( double factor )
     {
         scaleFactor *= factor;
         if ( 0.05 < scaleFactor && scaleFactor < 20.0)
         {
             imageLabel->resize(scaleFactor * imageLabel->pixmap(Qt::ReturnByValue).size());

             adjustScrollBar(scrollArea->horizontalScrollBar(), factor);
             adjustScrollBar(scrollArea->verticalScrollBar(), factor);
         }
         zoomInAct->setEnabled(scaleFactor < 20.0);
         sc_zoomIn->setEnabled(scaleFactor < 20.0);
         zoomOutAct->setEnabled(scaleFactor > 0.05);
         sc_zoomOut->setEnabled(scaleFactor > 0.05);
     }
     void adjustScrollBar(QScrollBar *scrollBar, double factor)
     {
         scrollBar->setValue(int(factor * scrollBar->value()
                                 + ((factor - 1) * scrollBar->pageStep()/2)));
     }
     void wheelEvent( QWheelEvent* event )
     {
         if ( event->modifiers() == Qt::ShiftModifier )
         {
//             if ( event->angleDelta().y() > 0 )
//                 zoomIn();
//             else
//                 zoomOut();
         }
//         else if ( event->modifiers() == Qt::ShiftModifier )
//             QWidget::wheelEvent( event );
     }
     void resizeEvent( QResizeEvent* event )
     {
         normalSize();
         QWidget::resizeEvent( event );
     }
};

#endif // IMAGEVIEWER_H
