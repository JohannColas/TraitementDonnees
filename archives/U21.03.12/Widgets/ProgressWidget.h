#ifndef PROGRESSWIDGET_H
#define PROGRESSWIDGET_H

#include <QHBoxLayout>
#include <QLabel>
#include <QProgressBar>
#include <QPushButton>
#include <QTime>
#include <QWidget>

class ProgressWidget
        : public QWidget
{
    Q_OBJECT
    QProgressBar* progress = new QProgressBar;
    QLabel* label = new QLabel( "Traitement en cours" );
    QLabel* remainTimer = new QLabel( "" );
    QPushButton* stop = new QPushButton( "STOP" );
public:
    ProgressWidget( QWidget* parent = 0 )
        : QWidget( parent )
    {
        connect( stop, SIGNAL(released()),
                 this, SLOT(stopping()) );
        QHBoxLayout* layout = new QHBoxLayout;
        layout->setMargin( 0 );
        layout->addWidget( progress );
        layout->addWidget( stop );
        layout->addWidget( label );
        layout->addWidget( remainTimer );

        setLayout( layout );
    }
    void setProgress( int prog )
    {
        progress->setValue( prog );
    }
    void setRemaintime( QTime time )
    {
        QString format = "h'h' m'm' s's'";
        if ( time.hour() == 0  )
            format = "m'm' s's'";
        remainTimer->setText( "(" + time.toString(format) + ")" );
    }

protected slots:
    void stopping()
    {
        stop->setText("Stopping...");
        emit stopT();
    }

signals:
    void stopT();
};

#endif // PROGRESSWIDGET_H
