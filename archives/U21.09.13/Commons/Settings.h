#ifndef SETTINGS_H
#define SETTINGS_H
/**********************************************/
#include <QDir>
#include <QDebug>
#include <QImage>
#include <QColor>
#include "SettingsFile.h"
#include "System.h"
/**********************************************/
/**********************************************/
namespace Keys {
    static QString EssaiDir = "EssaiDir";
    static QString LaTeXDir = "LaTeXDir";
    static QString PythonDir = "PythonDir";
    static QString ComparisonDir = "ComparisonDir";
    static QString RecursiveSearch = "RecursiveSearch";
    static QString Lissage = "Lissage";
    static QString Type = "Type";
    static QString nbPoints = "nbPoints";
    static QString Plots = "Plots";
    static QString Reports = "Reports";
    static QString TplDir = "TemplateDir";
    static QString TreatMeca = "TreatMeca";
    static QString TreatPlots = "TreatPlots";
    static QString TreatPtsCaract = "TreatPtsCaract";
    static QString TreatEA = "TreatEA";
    static QString TreatRE = "TreatRE";
    static QString TreatCNI = "TreatCNI";
    static QString TreatSynthese = "TreatSynthese";
    static QString CNI = "Reports";
    static QString lighterFactor = "lighterFactor";
    static QString Regression = "Regression";
    static QString sampling = "sampling";
    static QString MECA_DEF_CONT = "MECA_DEF_CONT";
    static QString MECA_DEP_FORCE = "MECA_DEP_FORCE";
    static QString MECA_MOD_TANG = "MECA_MOD_TANG";
    static QString ImageViewer = "ImageViewer";
    static QString Scale = "scale";
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Settings
{
private:
    static inline SettingsFile* _settings = new SettingsFile( QDir::currentPath() + "/settings.ini" );
    /**********************************************/
    /**********************************************/
public:
    static inline QVariant value( const QString& key,  const QString& subkey = "" )
    {
        if ( _settings )
        {
            QString _key = key;
            if ( subkey != "" )
                _key += "/" + subkey;
            QVariant var = _settings->value( _key );
            if ( var.isValid() )
                return var;
        }
        return QVariant();
    }
    /**********************************************/
    /**********************************************/
    static inline bool getBool( const QString& key,  const QString& subkey = "" )
    {
        QVariant var = _settings->value( key, subkey );
        if ( var.isValid() )
            return var.toBool();
        return false;
    }
    /**********************************************/
    /**********************************************/
    static inline QString getString( const QString& key, const QString& subkey = "" )
    {
        QVariant var = _settings->value( key, subkey );
        if ( var.isValid() )
            return var.toString();
        return "";
    }
    /**********************************************/
    /**********************************************/
    static inline int getInt( const QString& key, const QString& subkey = "" )
    {
        QVariant var = _settings->value( key, subkey );
        if ( var.isValid() )
            return var.toInt();
        return -1;
    }
    /**********************************************/
    /**********************************************/
    static inline double getDouble( const QString& key, const QString& subkey = "" )
    {
        QVariant var = _settings->value( key, subkey );
        if ( var.isValid() )
            return var.toDouble();
        return -1;
    }
    /**********************************************/
    /**********************************************/
    static inline void save( const QVariant& value, const QString& key, const QString& subkey = ""  )
    {
        QString _key = key;
        if ( subkey != "" )
            _key += "/" + subkey;
        _settings->save( value, _key );
    }
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SETTINGS_H
