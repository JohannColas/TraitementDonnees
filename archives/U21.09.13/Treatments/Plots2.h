#ifndef PLOTS2_H
#define PLOTS2_H
/**********************************************/
#include "Commons/Files.h"
#include "Commons/Settings.h"
#include "Meca.h"
#include "Commons/LaTeX.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
namespace Plot2Keys {
	static inline QStringList AxisLabels = {"x", "y", "yb", "yt"};
	static QString xlabel = "%%%XLABEL%%%";
	static QString ylabel = "%%%YLABEL%%%";
	static QString yblabel = "%%%YBLABEL%%%";
	static QString ytlabel = "%%%YTLABEL%%%";
	static QString data = "%%%DATA%%%";
	static QString legend = "%%%LEGEND%%%";
	static QString plaxis1 = "%%%PLOTS-AXIS1%%%";
	static QString plaxis2 = "%%%PLOTS-AXIS2%%%";
	static QString plaxis3 = "%%%PLOTS-AXIS3%%%";
	static QString fLimits = "%%%FLIMITS%%%";
	//
	static int FRENCH = 0;
	static int ENGLISH = 1;
	static QStringList TIME = {"Temps (s)", "Time (s)"};
	static QStringList TIME_LEGEND = {"0", "0"};
	//
	static QStringList DEF_LABEL = {"Déformation (\\si{\\percent})", "Stain (\\si{\\percent})"};
	static QStringList DEF_LEGEND = {"Déformation", "Stain"};
	static QString DEF_COLOR = "DEF";
	//
	static QStringList CTR_LABEL = {"Contrainte (\\si{\\mega\\pascal})", "Stress (\\si{\\mega\\pascal})"};
	static QStringList CTR_LEGEND = {"Contrainte", "Stress"};
	static QString CTR_COLOR = "CTR";
	//
	static QStringList MT_LABEL = {"Module tangent (\\si{\\giga\\pascal})", "Tangent module (\\si{\\giga\\pascal})"};
	static QStringList MT_LEGEND = {"Module tangent", "Tangent module"};
	static QString MT_COLOR = "MT";
	//
	static QStringList MS_LABEL = {"Module sécant (\\si{\\giga\\pascal})", "Secant module (\\si{\\giga\\pascal})"};
	static QStringList MS_LEGEND = {"Module sécant", "Secant module"};
	static QString MS_COLOR = "MS";
	//
	static QStringList EA_LABEL = {"E.A. (coups)", "E.A. (counts)"};
	static QStringList EA_LEGEND = {"Émission acoustique", "Acoustic emission"};
	static QString EA_COLOR = "EA";
	//
	static QStringList RE_LABEL = {"$\\mathbfsf{\\frac{\\Updelta R}{R_0}}$ (\\si{\\percent})", "$\\mathbfsf{\\frac{\\Updelta R}{R_0}}$ (\\si{\\percent})"};
	static QStringList RE_LEGEND = {"Résistance électrique", "Electrical resistance"};
	static QString RE_COLOR = "RE";
	//
	static QStringList T_LABEL = {"Température (\\si{\\celsius})", "Temperature (\\si{\\celsius})"};
	static QStringList T_LEGEND = {"Température", "Temperature"};
	static QString T_COLOR = "TEMP";
}
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Plots2 {
private:
	static inline int _lang = Plot2Keys::FRENCH;
	static inline QString _imageEXT = "png";
	/**********************************************/
	/**********************************************/
public:
	static inline void GenerateMECA( TestInfo* info )
	{
		QString path = info->path();
		Files::createDir( path, "plots" );
		Files::createDir( path + "/plots", "LaTeX" );

		QStringList plots;
		QString essai = info->getString( TestInfoKeys::TestName );
		QString suivi = info->getString( TestInfoKeys::AvailableTracking );
		if ( essai.left(6) != "VIEIL_" )
		{
			plots.append( "e-s" );
			if ( suivi.contains("R.E.") && suivi.contains("E.A.") )
				plots.append( "e-s-EA-RE" );
			else if ( suivi.contains("R.E.") && !suivi.contains("E.A.") )
				    plots.append( "e-s-RE" );
			else if ( !suivi.contains("R.E.") && suivi.contains("E.A.") )
				plots.append( "e-s-EA" );
			if ( !essai.contains("AMBI") )
				plots.append( { "t-s-T", "t-s-e-T"} );
			Plots2::Treat( path,"e-s-ms", {PlotData(path + "/MECA_MOD_TANG.dat", "\\dataA")} );
		}
		else
			plots.append( { "t-s-T", "t-s-e-T", "e-s" } );
		Plots2::Treat( path, plots, {PlotData(path + "/MECA_DEF_CONT.dat", "\\dataA")} );
	}
	/**********************************************/
	/**********************************************/
	static inline void GenerateRE( TestInfo* info )
	{
		if ( info == 0 )
			return;
		QString path = info->path() + "/RE";
		Files::createDir( path, "plots" );
		Files::createDir( path + "/plots", "LaTeX" );

		Plots2::Treat( path, "e-s-RE", {PlotData(path + "/RE_DEF_CONT.dat", "\\dataA")} );
	}
	/**********************************************/
	/**********************************************/
	static inline void Treat( const QString& workpath, const QStringList& plots, QList<PlotData> lst_data )
	{
		for ( QString plotName : plots )
		{
			Plots2::Treat( workpath, plotName, lst_data );
		}
	}
	/**********************************************/
	/**********************************************/
	static inline void Treat( const QString& workpath, const QString& plot, QList<PlotData> lst_data, const QString& filename = "", const QString& subpath = "/plots" )
	{
		QStringList plot_mks = plot.split('-');
		// Get axis limits
		QStringList axislim = Plots2::GetAxesLimits( lst_data, plot_mks);
		//
		QString contents;
		// Add the plot template
		contents += "\\input{/home/colas/Documents/LaTeX/plots/a-plot.tpl.tex}\n\n";
		// Add axis labels
		for ( int it = 0; it < plot_mks.size(); ++it )
		{
			QString mks = plot_mks[it];
			contents += "\\def\\"+Plot2Keys::AxisLabels[it]+"axislabel{";
			if ( mks == "t" ) contents += Plot2Keys::TIME[_lang];
			else if ( mks == "e" ) contents += Plot2Keys::DEF_LABEL[_lang];
			else if ( mks == "s" ) contents += Plot2Keys::CTR_LABEL[_lang];
			else if ( mks == "RE" ) contents += Plot2Keys::RE_LABEL[_lang];
			else if ( mks == "EA"  ) contents += Plot2Keys::EA_LABEL[_lang];
			else if ( mks == "T" ) contents += Plot2Keys::T_LABEL[_lang];
			else if ( mks == "mt" )  contents += Plot2Keys::MT_LABEL[_lang];
			else if ( mks == "ms" ) contents += Plot2Keys::MS_LABEL[_lang];
			contents += "}\n";
		}
		contents += "\n";
		// Add data
		for ( int it = 0; it < lst_data.size(); ++it )
		{
			PlotData& data = lst_data[it];
			if ( data.dataLB().isEmpty() ) data.setDataLB( QString("\\data%1").arg(QChar((char)(it+65))) );
			contents += "\\getData{" + data.path() + "}" + data.dataLB()  + "\n";
		}
		contents += "\n";
		// Add data indexes labels
//		contents += "\\def\\xInd{}";
//		contents += "\\def\\yaxislabel{}";
//		contents += "\\def\\ybaxislabel{}";
//		contents += "\\def\\ytaxislabel{}";
		// Begin document
		contents += "\\begin{document}\n";
		// Begin plot
		contents += "\t\\begin{plot}\n";
		// Begin master axis
		contents += "\t\t\\begin{axis}[master axis, xmin=0" + axislim.at(0) +", ymin=0" + axislim.at(1) +"]\n\n";
		// Add data legend
		if ( !(plot_mks.size() == 2 && lst_data.size() == 1 ) )
		{
			for ( int it = 0; it < lst_data.size(); ++it )
			{
				PlotData& data = lst_data[it];
				data.setLegendLB( QString("PL%1").arg(it+1) );
				for ( int jt = 1; jt < plot_mks.size(); jt++ )
				{
					QString plot_mk = plot_mks[jt];
					QString legend;
					if ( !data.legend().isEmpty() ) legend = data.legend();
					else if ( plot_mk == "e" ) legend = Plot2Keys::DEF_LEGEND[_lang];
					else if ( plot_mk == "s" ) legend = Plot2Keys::CTR_LEGEND[_lang];
					else if ( plot_mk == "RE" ) legend = Plot2Keys::RE_LEGEND[_lang];
					else if ( plot_mk == "EA"  ) legend = Plot2Keys::EA_LEGEND[_lang];
					else if ( plot_mk == "T" ) legend = Plot2Keys::T_LEGEND[_lang];
					else if ( plot_mk == "mt" ) legend = Plot2Keys::MT_LEGEND[_lang];
					else if ( plot_mk == "ms" ) legend = Plot2Keys::MS_LEGEND[_lang];
					contents += "\t\t\t\\addtolegend{" + data.legendLB() + plot_mk + "}{" + legend  + "}\n";
				}
			}
			contents += "\n";
		}
		// Add data plot
		contents += Plots2::AddDataLines( lst_data, plot_mks, 1 );
		contents += "\n";
		// End master axis
		contents += "\t\t\\end{axis}\n";
		if ( plot_mks.size() > 2 )
		{
			if ( plot_mks[2] == "EA" ) contents += "\t\t\\NumbersInScientificMode\n";
			// Begin slave axis
			contents += "\t\t\\begin{axis}[slave axis, ymin=0" + axislim.at(2) +"]\n\n";
			// Add data plot
			contents += Plots2::AddDataLines( lst_data, plot_mks, 2 );
			contents += "\n";
			// End slave axis
			contents += "\t\t\\end{axis}\n";
			if ( plot_mks[2] == "EA" ) contents += "\t\t\\NumbersInNormalMode\n";
		}
		if ( plot_mks.size() > 3 )
		{
			if ( plot_mks[3] == "EA" ) contents += "\t\t\\NumbersInScientificMode\n";
			// Begin slave2 axis
			contents += "\t\t\\begin{axis}[slave2 axis, ymin=0" + axislim.at(3) +"]\n\n";
			// Add data plot
			contents += Plots2::AddDataLines( lst_data, plot_mks, 3 );
			contents += "\n";
			// End slave2 axis
			contents += "\t\t\\end{axis}\n";
			if ( plot_mks[3] == "EA" ) contents += "\t\t\\NumbersInNormalMode\n";
		}
		// End plot
		contents += "\t\\end{plot}\n";
		// End document
		contents += "\\end{document}\n";

		// Save contents in the TeX File
		QString fileN = filename;
		if ( fileN.isEmpty() ) fileN = plot;
		if ( _lang == Plot2Keys::ENGLISH ) fileN += "_EN";
		Files::removeAll( workpath + subpath, fileN, true );
		QString TeX_Path = workpath + subpath + "/LaTeX/" + fileN + ".tex";
		Files::save( TeX_Path, contents );
		// Compilation et Conversion en PNG
		LaTeX::CompileAndConvertToPNG( workpath + subpath, fileN, _imageEXT );
		_lang = Plot2Keys::FRENCH;
		_imageEXT = "png";
	}
	/**********************************************/
	/**********************************************/
	static inline QStringList GetDataIndexes( const QStringList& plot_mks, const QString& dataPath )
	{
		QStringList dataindexes;
		QString line;
		if ( !Files::readFirstLine( dataPath, line ) ) return QStringList();
		QStringList cols = line.split(";", Qt::SkipEmptyParts);
		for ( QString plot_mk : plot_mks )
			for ( int it = 0; it < cols.size(); ++it )
			{
				QString col = cols.at(it);
				if ( (plot_mk == "t"  && col == DataKeys::TIME) ||
				     (plot_mk == "e"  && col == DataKeys::DEF)  ||
				     (plot_mk == "s"  && col == DataKeys::CTR)  ||
				     (plot_mk == "RE" && col == DataKeys::DRR0) ||
				     (plot_mk == "EA" && col == DataKeys::EA)   ||
				     (plot_mk == "T"  && col == DataKeys::T)    ||
				     (plot_mk == "mt" && col == DataKeys::MT)   ||
				     (plot_mk == "ms" && col == DataKeys::MS) )
					dataindexes.append( QString::number(it) );
			}
		return dataindexes;
	}
	/**********************************************/
	/**********************************************/
	static inline QString AddDataLines( QList<PlotData>& lst_data, const QStringList& plot_mks, int mk_ind )
	{
		QString temp;
		for ( PlotData& data : lst_data )
		{
			QStringList indexes = Plots2::GetDataIndexes( {plot_mks[0],plot_mks[mk_ind]}, data.path() );
			if ( indexes.size() != 2 ) continue;
			QString style;
			if ( !data.style().isEmpty() ) style = data.style();
			else if ( plot_mks[mk_ind] == "e" ) style = Plot2Keys::DEF_COLOR;
			else if ( plot_mks[mk_ind] == "s" ) style = Plot2Keys::CTR_COLOR;
			else if ( plot_mks[mk_ind] == "RE" ) style = Plot2Keys::RE_COLOR;
			else if ( plot_mks[mk_ind] == "EA"  ) style = Plot2Keys::EA_COLOR;
			else if ( plot_mks[mk_ind] == "T" ) style = Plot2Keys::T_COLOR;
			else if ( plot_mks[mk_ind] == "mt" ) style = Plot2Keys::MT_COLOR;
			else if ( plot_mks[mk_ind] == "ms" ) style = Plot2Keys::MS_COLOR;
			temp += "\t\t\t\\addplot+ ["+ style + "] " +
			               "table [x index=" + indexes[0] + ", " +
			               "y index=" + indexes[1] + "] " +
			               "{" + data.dataLB() + "};";
			if ( !(plot_mks.size() == 2 && lst_data.size() == 1 ) )
				temp += "\\label{" + data.legendLB() + plot_mks[mk_ind] + "}\n";
			else
				temp += "\n";
		}
		return temp;
	}
	/**********************************************/
	/**********************************************/
	static inline QStringList GetAxesLimits(  const QList<PlotData>& lst_data, const QStringList& plot_mks )
	{
		QStringList axeslimits;
		double time_max = -1;
		double def_max = -1;
		double ctr_max = -1;
		double re_max = -1;
		double ea_max = -1;
		double temp_max = -1;
		double mt_max = -1;
		double ms_max = -1;
		for ( PlotData data : lst_data )
		{
			QFileInfo info(data.path());
			TestInfo tinfo; tinfo.setPath( info.absolutePath() );
			if ( time_max < tinfo.getDouble( TestInfoKeys::TIME_MAX ) )
				time_max = Math::RoundToUpperMultiple( tinfo.getDouble( TestInfoKeys::TIME_MAX ), 500);
			if ( def_max < tinfo.getDouble( TestInfoKeys::DEF_MAX ) )
			{
				if ( tinfo.getDouble( TestInfoKeys::DEF_MAX ) > 0.3 )
					def_max = Math::RoundToUpperMultiple( tinfo.getDouble( TestInfoKeys::DEF_MAX ), 0.2);
				else
					def_max = Math::RoundToUpperMultiple( tinfo.getDouble( TestInfoKeys::DEF_MAX ), 0.04);
			}
			if ( ctr_max < tinfo.getDouble( TestInfoKeys::CTR_MAX ) )
			{
				if ( tinfo.getDouble( TestInfoKeys::CTR_MAX ) > 180 )
					ctr_max = Math::RoundToUpperMultiple( tinfo.getDouble( TestInfoKeys::CTR_MAX ), 50);
				else
					ctr_max = Math::RoundToUpperMultiple( tinfo.getDouble( TestInfoKeys::CTR_MAX ), 20);
			}
			if ( re_max < tinfo.getDouble( TestInfoKeys::RE_MAX ) )
				re_max = Math::RoundToUpperMultiple( tinfo.getDouble( TestInfoKeys::RE_MAX ), 0.5);
			if ( ea_max < tinfo.getDouble( TestInfoKeys::EA_MAX ) )
				ea_max = Math::RoundToUpperMultiple( tinfo.getDouble( TestInfoKeys::EA_MAX ), 100000);
			if ( temp_max < tinfo.getDouble( TestInfoKeys::T_MAX ) )
				temp_max = Math::RoundToUpperMultiple( tinfo.getDouble( TestInfoKeys::T_MAX ), 200);
			if ( mt_max < tinfo.getDouble( TestInfoKeys::MT_MAX ) )
				mt_max = Math::RoundToUpperMultiple( tinfo.getDouble( TestInfoKeys::MT_MAX ), 10);
			if ( ms_max < tinfo.getDouble( TestInfoKeys::MS_MAX ) )
				ms_max = Math::RoundToUpperMultiple( tinfo.getDouble( TestInfoKeys::MS_MAX ), 10);
		}
		for ( int it = 0; it < plot_mks.size(); ++it )
		{
			QString mks = plot_mks[it];
			QString lim = ", xmax=";
			if ( it ) lim = ", ymax=";
			double val = -1;
			if ( mks == "t" ) val = time_max;
			else if ( mks == "e" ) val = def_max;
			else if ( mks == "s" ) val = ctr_max;
			else if ( mks == "RE" ) val = re_max;
			else if ( mks == "EA"  ) val = ea_max;
			else if ( mks == "T" ) val = temp_max;
			else if ( mks == "mt" )  val =mt_max;
			else if ( mks == "ms" ) val = ms_max;
			if ( val == -1 )
				lim = "";
			else
				lim += QString::number(val);
			if ( it == 0 && val != -1 )
				lim += ",";
			axeslimits.append(lim);
		}
		return axeslimits;
	}
	/**********************************************/
	/**********************************************/
	static inline void EnglishVersion()
	{
		_lang = Plot2Keys::ENGLISH;
	}
	static inline void FrenchVersion()
	{
		_lang = Plot2Keys::FRENCH;
	}
	/**********************************************/
	/**********************************************/
	static inline void SetImageEXT( const QString& ext )
	{
		_imageEXT = ext;
	}
	/**********************************************/
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLOTS2_H
