#ifndef PLOTS_H
#define PLOTS_H
/**********************************************/
#include "Commons/Files.h"
#include "Commons/Settings.h"
#include "Meca.h"
#include "Commons/LaTeX.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
namespace PlotKeys {
	static QString xlabel = "%%%XLABEL%%%";
	static QString ylabel = "%%%YLABEL%%%";
	static QString yblabel = "%%%YBLABEL%%%";
	static QString ytlabel = "%%%YTLABEL%%%";
	static QString data = "%%%DATA%%%";
	static QString legend = "%%%LEGEND%%%";
	static QString plaxis1 = "%%%PLOTS-AXIS1%%%";
	static QString plaxis2 = "%%%PLOTS-AXIS2%%%";
	static QString plaxis3 = "%%%PLOTS-AXIS3%%%";
	static QString fLimits = "%%%FLIMITS%%%";
	//
	static int FRENCH = 0;
	static int ENGLISH = 1;
	static QStringList TIME = {"Temps (s)", "Time (s)"};
	static QStringList TIME_LEGEND = {"0", "0"};
	//
	static QStringList DEF_LABEL = {"Déformation (\\si{\\percent})", "Stain (\\si{\\percent})"};
	static QStringList DEF_LEGEND = {"Déformation", "Stain"};
	static QString DEF_COLOR = "DEF";
	//
	static QStringList CTR_LABEL = {"Contrainte (\\si{\\mega\\pascal})", "Stress (\\si{\\mega\\pascal})"};
	static QStringList CTR_LEGEND = {"Contrainte", "Stress"};
	static QString CTR_COLOR = "CTR";
	//
	static QStringList MT_LABEL = {"Module tangent (\\si{\\giga\\pascal})", "Tangent module (\\si{\\giga\\pascal})"};
	static QStringList MT_LEGEND = {"Module tangent", "Tangent module"};
	static QString MT_COLOR = "MT";
	//
	static QStringList MS_LABEL = {"Module sécant (\\si{\\giga\\pascal})", "Secant module (\\si{\\giga\\pascal})"};
	static QStringList MS_LEGEND = {"Module sécant", "Secant module"};
	static QString MS_COLOR = "MS";
	//
	static QStringList EA_LABEL = {"E.A. (coups)", "E.A. (counts)"};
	static QStringList EA_LEGEND = {"Émission acoustique", "Acoustic emission"};
	static QString EA_COLOR = "EA";
	//
	static QStringList RE_LABEL = {"$\\mathbfsf{\\frac{\\Updelta R}{R_0}}$ (\\si{\\percent})", "$\\mathbfsf{\\frac{\\Updelta R}{R_0}}$ (\\si{\\percent})"};
	static QStringList RE_LEGEND = {"Résistance électrique", "Electrical resistance"};
	static QString RE_COLOR = "RE";
	//
	static QStringList T_LABEL = {"Température (\\si{\\celsius})", "Temperature (\\si{\\celsius})"};
	static QStringList T_LEGEND = {"Température", "Temperature"};
	static QString T_COLOR = "TEMP";
}
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Plots {
private:
	static inline int _lang = PlotKeys::FRENCH;
	static inline QString _imageEXT = "png";
	/**********************************************/
	/**********************************************/
public:
	static inline void EnglishVersion()
	{
		_lang = PlotKeys::ENGLISH;
	}
	static inline void FrenchVersion()
	{
		_lang = PlotKeys::FRENCH;
	}
	/**********************************************/
	/**********************************************/
	static inline void SetImageEXT( const QString& ext )
	{
		_imageEXT = ext;
	}
	/**********************************************/
	/**********************************************/
	static inline void Treat( const QString& workpath, const QStringList& plots, const QList<PlotData>& lst_data )
	{
		for ( QString plotName : plots )
		{
			Plots::Treat( workpath, plotName, lst_data );
		}
	}
	/**********************************************/
	/**********************************************/
	static inline void Treat( const QString& workpath, const QString& plot, const QList<PlotData>& lst_data, const QString& filename = "", const QString& subpath = "/plots" )
	{
		if (  !QDir(workpath).exists() )
			return;

		QStringList plot_mks = plot.split('-');
		// Get contents in the TeX Template File
		QString contents;
		if ( !LaTeX::ReadPlotTemplate( plot_mks, contents ) )
			return;

		Plots::ReplaceAxisLabels( plot_mks, contents );

		QString legend;
		QStringList axes = {"","",""};
		QString datafile;
		bool needLegend = true;
		if ( plot_mks.size() == 2 && lst_data.size() == 1 )
			needLegend = false;
		for ( int it = 0; it < lst_data.size(); it++ )
		{
			PlotData data = lst_data[it];
			if ( data.dataLB().isEmpty() )
				data.setDataLB( QString("\\data%1").arg(QChar((char)(it+65))) );
			datafile += Plots::BuildDataLine( data );
			for ( int jt = 1; jt < plot_mks.size(); jt++ )
			{
				// Data legend label
				data.setLegendLB( QString("PL" + plot_mks[jt]+"%1").arg(it+1) );
				QString line = Plots::BuildPlotLine( {plot_mks[0], plot_mks[jt]}, data, needLegend );
				axes[jt-1].append( line );
				if ( needLegend )
				{
					QString line2 = Plots::BuildLegendLine( {plot_mks[0], plot_mks[jt]}, data );
					legend += line2;
				}
			}
		}

		contents.replace( PlotKeys::data, datafile );
		contents.replace( PlotKeys::legend, legend );
		contents.replace( PlotKeys::plaxis1, axes[0] );
		contents.replace( PlotKeys::plaxis2, axes[1] );
		contents.replace( PlotKeys::plaxis3, axes[2] );

		// Save contents in the TeX File
		QString fileN = "";
		if ( _lang == PlotKeys::ENGLISH )
			fileN = "EN_";
		if ( filename.isEmpty() )
			fileN += plot;
		else
			fileN += filename;
		if ( lst_data[0].path().contains("_focused") )
			fileN += "_focused";
		QString TeX_Path = workpath + subpath + "/LaTeX/" + fileN + ".tex";
		Files::save( TeX_Path, contents );
		// Compilation et Conversion en PNG
		LaTeX::CompileAndConvertToPNG( workpath + subpath, fileN, _imageEXT );
		_lang = PlotKeys::FRENCH;
		_imageEXT = "png";
	}
	/**********************************************/
	/**********************************************/
	static inline void GenerateMECA( TestInfo* info )
	{
		QString path = info->path();
		Files::createDir( path, "plots" );
		Files::createDir( path + "/plots", "LaTeX" );

		QStringList plots;
		QString essai = info->getString( TestInfoKeys::TestName );
		QString suivi = info->getString( TestInfoKeys::AvailableTracking );
		if ( essai.left(6) != "VIEIL_" )
		{
			plots.append( /*{ "t-s-e",*/ "e-s" /*}*/ );
			if ( suivi.contains("R.E.") && suivi.contains("E.A.") )
				plots.append( /*{ "t-s-EA-RE",*/ "e-s-EA-RE" /*}*/ );
			else if ( suivi.contains("R.E.") && !suivi.contains("E.A.") )
				    plots.append( /*{ "t-s-RE",*/ "e-s-RE" /*}*/ );
			else if ( !suivi.contains("R.E.") && suivi.contains("E.A.") )
				plots.append( /*{ "t-s-EA",*/ "e-s-EA" /*}*/ );
			if ( !essai.contains("AMBI") )
				plots.append( { "t-s-T", "t-s-e-T"} );
			Plots::Treat( path, /*{*/"e-s-ms"/*, "t-e-ms"}*/, {PlotData(path + "/MECA_MOD_TANG.dat", "\\dataA", "","","")} );
		}
		else
			plots.append( { "t-s-T", "t-s-e-T", "e-s" } );
		Plots::Treat( path, plots, {PlotData(path + "/MECA_DEF_CONT.dat", "\\dataA", "","","")} );
	}
	/**********************************************/
	/**********************************************/
	static inline void GenerateRE( TestInfo* info )
	{
		if ( info == 0 )
			return;
		QString path = info->path() + "/RE";
		Files::createDir( path, "plots" );
		Files::createDir( path + "/plots", "LaTeX" );

		QStringList plots;
		plots.append( /*{ "t-s-e-RE", "t-s-T-RE",*/ "e-s-RE" /*}*/ );

		Plots::Treat( path, plots, {PlotData(path + "/RE_DEF_CONT.dat", "\\dataA", "","","")} );
	}
	/**********************************************/
	/**********************************************/
	static inline QStringList GetDataIndexes( const QStringList& plot_mks, const QString& dataPath )
	{
		QStringList dataindexes;
		QString line;
		if ( !Files::readFirstLine( dataPath, line ) )
			return QStringList();
		QStringList cols = line.split(";");
		for ( QString plot_mk : plot_mks )
		{
			for ( int it = 0; it < cols.size(); ++it )
			{
				QString col = cols.at(it);
				if ( plot_mk == "t" && col == DataKeys::TIME )
					dataindexes.append( QString::number(it) );
				else if ( plot_mk == "e" && col == DataKeys::DEF )
					dataindexes.append( QString::number(it) );
				else if ( plot_mk == "s" && col == DataKeys::CTR )
					dataindexes.append( QString::number(it) );
				else if ( plot_mk == "RE" && col == DataKeys::DRR0 )
					dataindexes.append( QString::number(it) );
				else if ( plot_mk == "EA" && col == DataKeys::EA )
					dataindexes.append( QString::number(it) );
				else if ( plot_mk == "T" && col == DataKeys::T )
					dataindexes.append( QString::number(it) );
				else if ( plot_mk == "mt" && col == DataKeys::MT )
					dataindexes.append( QString::number(it) );
				else if ( plot_mk == "ms" && col == DataKeys::MS )
					dataindexes.append( QString::number(it) );
			}
		}
		return dataindexes;
	}
	/**********************************************/
	/**********************************************/
	static inline QString BuildDataLine( const PlotData& data )
	{
		return "\\getData{" + data.path() + "}" + data.dataLB()  + "\n";
	}
	/**********************************************/
	/**********************************************/
	static inline QString BuildPlotLine( const QStringList& plot_mks, const PlotData& data, bool needLegend = true )
	{
		QStringList indexes = Plots::GetDataIndexes( plot_mks, data.path() );
		if ( indexes.size() != 2 )
			return "";
		QString style = data.style();
		if ( style.isEmpty() )
		{
			if ( plot_mks[1] == "e" ) style = PlotKeys::DEF_COLOR;
			else if ( plot_mks[1] == "s" ) style = PlotKeys::CTR_COLOR;
			else if ( plot_mks[1] == "RE" ) style = PlotKeys::RE_COLOR;
			else if ( plot_mks[1] == "EA"  ) style = PlotKeys::EA_COLOR;
			else if ( plot_mks[1] == "T" ) style = PlotKeys::T_COLOR;
			else if ( plot_mks[1] == "mt" ) style = PlotKeys::MT_COLOR;
			else if ( plot_mks[1] == "ms" ) style = PlotKeys::MS_COLOR;
		}
		QString temp = "\t\t\t\\addplot+["+ style + "] " +
		               "table[x index=" + indexes[0] + "," +
		               "y index=" + indexes[1] + "] " +
		               "{" + data.dataLB() + "};";
		if ( needLegend )
			temp += "\\label{" + data.legendLB() + "}\n";
		else
			temp += "\n";
		return temp;
	}
	/**********************************************/
	/**********************************************/
	static inline QString BuildLegendLine( const QStringList& plot_mks, const PlotData& data )
	{
		QString legend = data.legend();
		if ( legend.isEmpty() )
		{
			if ( plot_mks[1] == "e" ) legend = PlotKeys::DEF_LEGEND[_lang];
			else if ( plot_mks[1] == "s" ) legend = PlotKeys::CTR_LEGEND[_lang];
			else if ( plot_mks[1] == "RE" ) legend = PlotKeys::RE_LEGEND[_lang];
			else if ( plot_mks[1] == "EA"  ) legend = PlotKeys::EA_LEGEND[_lang];
			else if ( plot_mks[1] == "T" ) legend = PlotKeys::T_LEGEND[_lang];
			else if ( plot_mks[1] == "mt" ) legend = PlotKeys::MT_LEGEND[_lang];
			else if ( plot_mks[1] == "ms" ) legend = PlotKeys::MS_LEGEND[_lang];
		}
		return "\t\t\t\\addtolegend{" + data.legendLB() + "}{" + legend + "}\n";;
	}
	/**********************************************/
	/**********************************************/
	static inline void ReplaceAxisLabels( const QStringList& plot_mks, QString& content )
	{
		QStringList markers = {"%%%XLABEL%%%", "%%%YLABEL%%%", "%%%YBLABEL%%%", "%%%YTLABEL%%%"};
		for ( int it = 0; it < plot_mks.size(); ++it )
		{
			QString mks = plot_mks[it];
			if ( mks == "t" ) content.replace( markers[it], PlotKeys::TIME[_lang] );
			else if ( mks == "e" ) content.replace( markers[it], PlotKeys::DEF_LABEL[_lang] );
			else if ( mks == "s" ) content.replace( markers[it], PlotKeys::CTR_LABEL[_lang] );
			else if ( mks == "RE" ) content.replace( markers[it], PlotKeys::RE_LABEL[_lang] );
			else if ( mks == "EA"  ) content.replace( markers[it], PlotKeys::EA_LABEL[_lang] );
			else if ( mks == "T" ) content.replace( markers[it], PlotKeys::T_LABEL[_lang] );
			else if ( mks == "mt" ) content.replace( markers[it], PlotKeys::MT_LABEL[_lang] );
			else if ( mks == "ms" ) content.replace( markers[it], PlotKeys::MS_LABEL[_lang] );
		}
	}
	/**********************************************/
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLOTS_H
