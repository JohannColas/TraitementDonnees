#ifndef MECA_H
#define MECA_H
/**********************************************/
#include "Commons/Files.h"
#include "Treatments/Data.h"
#include "Commons/Settings.h"
#include "Commons/TestInfo.h"
#include "Commons/Math.h"
#include "Fitting.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Meca
{
public:
	static inline bool Treat( TestInfo* testinfo )
	{
		if ( !testinfo )
			return false;
		QString path = testinfo->path();
		// Get Program Info
		QString firstLine;
		if ( !Files::readFirstLine( path + "/MECA.dat", firstLine, QStringConverter::Latin1) )
			if ( !Files::readFirstLine( path + "/MECA.csv", firstLine, QStringConverter::Latin1) )
				return false;

		QVector<int> progfrt = DataProg::NONE;
		if ( firstLine.contains("R8562.BAS") )
			progfrt = DataProg::R8562;
		else if ( firstLine.contains("RESIST.BAS") )
			progfrt = DataProg::RESIST;
		else if ( firstLine.contains("RESIST2.BAS") )
			progfrt = DataProg::RESIST2;
		else if ( firstLine.contains("TRACTR.BAS") )
			progfrt = DataProg::TRACTR;
		else if ( firstLine.contains("TRACMONO.BAS") )
			progfrt = DataProg::TRACMONO;
		else if ( firstLine.contains("TRACTION.BAS") )
			progfrt = DataProg::TRAC;
		else if ( firstLine.contains("FLEXION.BAS") )
			progfrt = DataProg::FLEX;
		else if ( firstLine.contains("FLEXION1.BAS") || firstLine.contains("FLEXION2.BAS") )
			progfrt = DataProg::FLEX12;
		else if ( firstLine.contains("IOSI3J.BAS") )
			progfrt = DataProg::IOSI3J;
		else if ( firstLine.contains("Temps total (s)") )
		{
			QString testname = testinfo->getString( TestInfoKeys::TestName );
			if ( testname.left(8).contains("VIEIL") )
				progfrt = DataProg::VIEIL;
			else
				progfrt = DataProg::ESSAIHT;
		}

		if ( progfrt == DataProg::NONE ) return false;

		// Read Raw Data
		QVector<QVector<double>> raw_data;
		if ( !Files::readData( path + "/MECA.dat", raw_data, QStringConverter::Latin1) )
			if ( !Files::readData( path + "/MECA.csv", raw_data, QStringConverter::Latin1) )
				return false;

		int iTIME  = progfrt[DataProg::idTIME];
		int iDEP   = progfrt[DataProg::idDEP];
		int iFORCE = progfrt[DataProg::idFORCE];
		int iDEF1  = progfrt[DataProg::idDEF1];
		int iDEF2  = progfrt[DataProg::idDEF2];
		int iDEF3  = progfrt[DataProg::idDEF3];
		Q_UNUSED(iDEF3)
		int iCTR   = progfrt[DataProg::idCTR];
		int iRE    = progfrt[DataProg::idRE];
		int iEA    = progfrt[DataProg::idEA];
		int iT     = progfrt[DataProg::idT];

		bool tempFour = false;
//		int tempFocused = 980;
		if ( iT != -1 )
		{
//			if ( path.contains("VIEIL_1300") )
//				tempFocused = 1280;
			QString rmqs;
			if ( Files::read( path+"/Remarques.txt", rmqs, QStringConverter::Latin1 ) )
			{
				rmqs.remove("\r");
				QStringList coms = rmqs.split("\n");
				for ( QString com : coms )
				{
					if ( (com.contains("sservissement") || com.contains("égulation")) &&
					     com.contains("thermocouple") && com.contains("four") && !com.contains("erreur") )
						tempFour = true;
					QString comments = testinfo->getString( TestInfoKeys::Comments );
					if ( !comments.contains(com) )
						testinfo->save( comments + com + "\n", TestInfoKeys::Comments );
				}
			}
		}

		QString suiviDispo;
		if ( iRE != -1 )
			suiviDispo = "R.E.";
		if ( iEA != -1 )
			suiviDispo = (iRE == -1 ? "" : suiviDispo + " - ") + "E.A.";
		QString suivi = testinfo->getString( TestInfoKeys::AvailableTracking );
		if ( suivi.isEmpty() )
			testinfo->save( suiviDispo, TestInfoKeys::AvailableTracking );
		if ( !suivi.contains( "R.E." ) )
			iRE = -1;
		if ( !suivi.contains( "E.A." ) )
			iEA = -1;

		// Build headers of the treated file
		QString header_data;
		QString header_rawdata;
		header_data = DataKeys::TIME + ";" + DataKeys::DEF + ";" + DataKeys::CTR;
		if ( iT != -1 )
			header_data += ";" + DataKeys::T;
		if ( iRE != -1 )
			header_data += ";" + DataKeys::RE + ";" + DataKeys::DRR0;
		if ( iEA != -1 )
			header_data += ";" + DataKeys::EA;
		if ( iDEP != -1 && iFORCE != -1 )
		{
			header_rawdata = DataKeys::TIME + ";" + DataKeys::DEP + ";" + DataKeys::FORCE;
			if ( iT != -1 )
				header_rawdata += ";" + DataKeys::T;
			if ( iRE != -1 )
				header_rawdata += ";" + DataKeys::RE + ";" + DataKeys::DRR0;
			if ( iEA != -1 )
				header_rawdata += ";" + DataKeys::EA;
		}

		// Treat data
		QVector<QVector<double>> trd_data;
		QVector<QVector<double>> trd_data2;
		// Get RawData
		double L = testinfo->getDouble( TestInfoKeys::Length );
		double b = testinfo->getDouble( TestInfoKeys::Width );
		double e = testinfo->getDouble( TestInfoKeys::Thickness );
		double tcut = testinfo->getDouble( TestInfoKeys::TimeCut );
		double tim = -1;
		double dep = -1;
		double dep0 = -1;
		if ( iDEP != -1)
			dep0 = raw_data[0][iDEP];
		double frc = -1;
		double def = -1;
		double ctr = -1;
		double re = -1;
		double R0 = -1;
		double dRdR0 = 0;
		if ( iRE != -1)
		{
			R0 = raw_data[0][iRE];
			if ( R0 < 1 )
				R0 *= 1000;
		}
		double ea = -1;
		double temp = -1;
		for ( int it = 0; it < raw_data.size(); ++it )
		{
			QVector<double> line = raw_data[it];
			tim = line[iTIME];
			// Get deplacement
			if ( iDEP != -1)
				dep = line[iDEP];
			// Get Force
			if ( iFORCE != -1)
				frc = line[iFORCE];
			// Get Strain
			if ( progfrt == DataProg::FLEX12 )
				def = 100*(6*e/(L*L))*line[iDEP];
			else if ( progfrt == DataProg::IOSI3J )
				def = line[iDEF1] - line[iDEF2];
			else if ( progfrt == DataProg::VIEIL || progfrt == DataProg::ESSAIHT )
			{
				def = 100*(dep - dep0)/L;
				if ( L == -1 )
					def = -1;
			}
			else if ( iDEF1 != -1 )
			{
				if ( iDEF2 == -1 )
					def = line[iDEF1];
				else
					def = 0.5*( line[iDEF1] + line[iDEF2] );
			}
			// Get Stress
			if ( progfrt == DataProg::FLEX12 )
				ctr = 1.5*(L/(b*e*e))*line[iFORCE];
			else if ( progfrt == DataProg::IOSI3J ||
			          progfrt == DataProg::R8562 ||
			          progfrt == DataProg::VIEIL ||
			          progfrt == DataProg::ESSAIHT )
			{
				ctr = frc/(b*e);
				if ( e == -1 )
					ctr *= -1;
			}
			else if ( iCTR != -1)
				ctr = line[iCTR];
			// get RE
			if ( iRE != -1 )
			{
				re = line[iRE];
				if ( re < 1 )
					re *= 1000; // convert in mOhm
				dRdR0 = 100*( re - R0 )/R0;
			}
			// get EA
			if ( iEA != -1 )
				ea = line[iEA];
			// get Temperature
			if ( iT != -1 )
			{
				temp = line[iT];
				if ( tempFour )
					temp = - 0.0000005127092*pow(temp, 3)
					       + 0.0007545365*pow(temp, 2)
					       + 0.9624921*temp
					       + 5.306989;
			}
			// Save data in local variable
			if ( /*def >= -0.001 &&*/
			     (line[iTIME] < tcut || tcut == -1 ) )
			{
				QVector<double> dataLine = { tim, def, ctr };
				if ( iT != -1 )
					dataLine.append( temp );
				if ( iRE != -1 )
					dataLine.append( {re, dRdR0} );
				if ( iEA != -1 )
					dataLine.append( ea );
				trd_data.append( dataLine );

				if ( iDEP != -1 && iFORCE != -1 )
				{
					QVector<double> rawdataLine = { tim, dep, frc };
					if ( iT != -1 )
						rawdataLine.append( temp );
					if ( iRE != -1 )
						rawdataLine.append( {re, dRdR0} );
					if ( iEA != -1 )
						rawdataLine.append( ea );
					trd_data2.append( rawdataLine );
				}
			}
		}

		// Save Data
		Math::MoyenneGlissee( trd_data );
		Files::save( path + "/MECA_DEF_CONT.dat", trd_data, header_data );

		header_data = DataKeys::TIME + ";" + DataKeys::DEF + ";" + DataKeys::CTR;
		QVector<double> maxes = Math::max( trd_data );
		testinfo->save( maxes[0], TestInfoKeys::TIME_MAX );
		testinfo->save( maxes[1], TestInfoKeys::DEF_MAX );
		testinfo->save( maxes[2], TestInfoKeys::CTR_MAX );
		int it = 3;
		if ( iT != -1 )
		{
			testinfo->save( maxes[it], TestInfoKeys::T_MAX );
			++it;
		}
		if ( iRE != -1 )
		{
			++it;
			testinfo->save( maxes[it], TestInfoKeys::RE_MAX );
			++it;
		}
		if ( iEA != -1 )
			testinfo->save( maxes[it], TestInfoKeys::EA_MAX );

		// Save Raw Data
		if ( trd_data2.size() > 0 )
		{
			Math::MoyenneGlissee( trd_data2 );
			Files::save( path + "/MECA_DEP_FORCE.dat", trd_data2, header_rawdata );
		}
		// Get and Save Tangent Module
		if ( progfrt != DataProg::VIEIL )
		{
			QVector<QVector<double>> tang_mod = Math::RegressionLineaire( trd_data, 1, 2 );
			for ( QVector<double>& line : tang_mod )
				if (line.at(1) == 0)
					line.append( 0);
				else
					line.append( 0.1*line.at(2)/line.at(1) );
			Math::MoyenneGlissee( tang_mod );
			QString header = DataKeys::TIME + ";" + DataKeys::DEF + ";" + DataKeys::CTR + ";" + DataKeys::MT + ";" + DataKeys::MTB+ ";" + DataKeys::MS;
			QVector<double> maxes = Math::max( tang_mod );
			testinfo->save( maxes[3], TestInfoKeys::MT_MAX );
			testinfo->save( maxes[5], TestInfoKeys::MS_MAX );
			Files::save( path + "/MECA_MOD_TANG.dat", tang_mod, header );
			if ( testinfo->getString( TestInfoKeys::TestName ).contains("CYCL") )
				Meca::TreatCycles( path, tang_mod );
		}
		return true;
	}
	/**********************************************/
	/**********************************************/
	static inline void TreatCycles( const QString& path, const QVector<QVector<double>>& data )
	{
		System::createDir(path, Files::CyclesFolder);
		QString cpath = path + "/" + Files::CyclesFolder;
		// Headers
		QString header = DataKeys::TIME + ";" + DataKeys::Ncycl + ";" + DataKeys::Scycl + ";" + DataKeys::DEF + ";" + DataKeys::CTR + ";" + DataKeys::CTRn + ";" + DataKeys::CTRd + ";" + DataKeys::CTRc + ";" + DataKeys::MS + ";" + DataKeys::MSn + ";" + DataKeys::MSd + ";" + DataKeys::MSc;
		QString header2 = DataKeys::DEF + ";" + DataKeys::CTR + ";" + DataKeys::MS;
		// Treated data
		QVector<QStringList> trd_data;
		trd_data.append( {QString::number(data.at(0).at(0)), // time
		                  "0", "n",
		                  QString::number(data.at(0).at(1)), // e
		                  QString::number(data.at(0).at(2)), QString::number(data.at(0).at(2)), "", "", // s, s_n, s_d, s_c
		                  "0", "0", "", "" // Esc, Esc_n, Esc_d, Esc_c
		                 } );
		// Local variables
		int n_cycle = 0;
		bool charge = true;
		bool decharge = false;
		double lim_def = -1;
		int lim_it = -1;
		QVector<QStringList> norm_data;
		QVector<QStringList> dech_data;
		QVector<QStringList> char_data;
		for ( int it = 1; it < data.size(); ++it )
		{
			QVector<double> row = data.at(it);
			double def = row[1];
			double LASTdef = data.at(it-1).at(1);
			QString t = QString::number(row[0]), e = QString::number(def), ncycl = QString::number(n_cycle);
			QString s   = QString::number(row[2]),   s_n = "",   s_d = "",   s_c = "";
			QString Esc = QString::number(row[5]), Esc_n = "", Esc_d = "", Esc_c = "";
			QString step = "";
			if ( /*it - lim_def > 15 &&*/ lim_it > 10 ) // Permit to avoid small variations
			{
				if ( def < LASTdef && !decharge )
				{
					decharge = true;
					charge = false;
					lim_def = def;
//					lim_it = it-1;
					if ( char_data.size() > 0 )
						Files::save( cpath + "/c"+ncycl+".dat", char_data, header2 );
					char_data.clear();
					++n_cycle;
				}
				else if ( def > LASTdef && !charge )
				{
					decharge = false;
					charge = true;
					if ( dech_data.size() > 0 )
						Files::save( cpath + "/d"+ncycl+".dat", dech_data, header2 );
					dech_data.clear();
				}
				lim_it = 0;
			}
			if ( charge )
			{
				if ( def > lim_def )
				{
					s_n = s;
					Esc_n = Esc;
					step = "n";
					norm_data.append( {e, s, Esc} );
				}
				else
				{
					s_c = s;
					Esc_c = Esc;
					step = "c";
					char_data.append( {e, s, Esc} );
				}
			}
			else
			{
				s_d = s;
				Esc_d = Esc;
				step = "d";
				dech_data.append( {e, s, Esc} );
			}
			trd_data.append( { t, ncycl, step, e,
			                   s, s_n, s_d, s_c,
			                   Esc, Esc_n, Esc_d, Esc_c } );
			++lim_it;
			// Save Last Charge
			if ( it+1 == data.size() )
				Files::save( cpath + "/c"+ncycl+".dat", char_data, header2 );
		}
		Files::save( cpath + "/n.dat", norm_data, header2 );
		Files::save( cpath + "/cycles.dat", trd_data, header );
	}
	/**********************************************/
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // MECA_H
