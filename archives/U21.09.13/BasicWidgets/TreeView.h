#ifndef TREEVIEW_H
#define TREEVIEW_H

#include <QAbstractItemModel>
#include <QKeyEvent>
#include <QTreeView>
#include <QScrollBar>
#include <QDebug>

class TreeView
    : public QTreeView
{
    Q_OBJECT

public:
    TreeView( QWidget* parent = 0 )
        : QTreeView( parent )
    {
    }
    /**********************************************/
    int nbVisibleItems( const QModelIndex& ind )
    {
        int nb = model()->rowCount( ind );
        for ( int it = 0; it < nb; ++it )
        {
            if ( model()->hasChildren( model()->index( it, 0, ind ) )  && this->isExpanded( model()->index( it, 0, ind ) ) )
            {
                nb += nbVisibleItems( model()->index( it, 0, ind ) );
            }
        }
        return nb;
    }
    /**********************************************/
    int itemPosition( const QModelIndex& ind )
    {
        int pos = -1;
        QModelIndex checker = ind;
        while ( checker != this->rootIndex() )
        {
            while ( this->indexAbove( checker ).isValid() )
            {
                checker = this->indexAbove( checker );
                ++pos;
            }
            checker = checker.parent();
            ++pos;
        }
        return pos;
    }
    /**********************************************/
    void focusSrollbar( const QModelIndex& ind )
    {
        int itempos = itemPosition( ind );
        int nbitems = nbVisibleItems( this->rootIndex() );
        int max = this->verticalScrollBar()->maximum();
        int dif = nbitems - max;
        int currentValue = this->verticalScrollBar()->value();
        if ( !( itempos < currentValue + dif && itempos > currentValue ))
        {
            int newValue = itempos - dif;
            if ( itempos > dif )
                newValue = max;
            if ( newValue < 0 )
                newValue = 0;
            this->verticalScrollBar()->setValue( newValue );
        }
    }
    /**********************************************/
public slots:
    void autofocusScrollbar()
    {
        focusSrollbar( this->currentIndex() );
    }

protected slots:
    void keyPressEvent( QKeyEvent* event ){

        QTreeView::keyPressEvent(event);

        if ( event->key() == Qt::Key_Down )
        {
            QModelIndex qmi = this->currentIndex();
            emit clicked( qmi );
        }
        else if ( event->key() == Qt::Key_Up )
        {
            QModelIndex qmi = this->currentIndex();
            emit clicked( qmi );
        }
        else
        {
        }
    }
};

#endif // TREEVIEW_H
