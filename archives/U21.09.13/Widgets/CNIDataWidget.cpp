#include "CNIDataWidget.h"
#include "ui_CNIDataWidget.h"
/**********************************************/
/**********************************************/
/**********************************************/
CNIDataWidget::CNIDataWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CNIDataWidget)
{
    ui->setupUi(this);
    ui->wid_CNIProcessing->hide();
    ui->pb_stopprocess->hide();
}
/**********************************************/
/**********************************************/
/**********************************************/
CNIDataWidget::~CNIDataWidget()
{
    delete ui;
}
/**********************************************/
/**********************************************/
/**********************************************/
void CNIDataWidget::linkToTestInfo( TestInfo *info )
{
    _testinfo = info;
}
/**********************************************/
/**********************************************/
/**********************************************/
void CNIDataWidget::on_traite_CNI_released()
{
	if ( !_testinfo )
		return;
    ui->wid_CNIProcessing->show();
    ui->wid_CNItreatImgs->show();
    ui->lb_CNImakingVideo->hide();
    ui->pb_stopprocess->show();
	CNI* cni = new CNI( _testinfo );
    connect( cni, &CNI::sendCurrentImage,
             ui->lb_curImg, &NumberLabel::setInt );
    connect( cni, &CNI::sendTotImage,
             ui->lb_totImg, &NumberLabel::setInt );
    connect( cni, &CNI::makingVideo,
             ui->lb_CNImakingVideo, &QWidget::show );
    connect( cni, &CNI::makingVideo,
             ui->wid_CNItreatImgs, &QWidget::hide );
    connect( cni, &CNI::finished,
             ui->wid_CNIProcessing, &QWidget::hide );
    connect( cni, &CNI::finished,
             ui->pb_stopprocess, &QWidget::hide );
    cni->treat();
}
/**********************************************/
/**********************************************/
/**********************************************/
void CNIDataWidget::on_pb_CNIVideo_released()
{
	if ( !_testinfo )
		return;
    ui->wid_CNIProcessing->show();
    ui->wid_CNItreatImgs->hide();
    ui->lb_CNImakingVideo->show();
    ui->pb_stopprocess->show();
    CNI* cni = new CNI( _testinfo );
    connect( cni, &CNI::finished,
             ui->wid_CNIProcessing, &QWidget::hide );
    connect( cni, &CNI::finished,
             ui->pb_stopprocess, &QWidget::hide );
    cni->makeVideo();
}
/**********************************************/
/**********************************************/
/**********************************************/
void CNIDataWidget::on_pb_stopprocess_released()
{
    emit stopProcess();
}
/**********************************************/
/**********************************************/
/**********************************************/
void CNIDataWidget::on_sp_lighterFactor_valueChanged( int value )
{
	if ( _testinfo )
		_testinfo->save( value, TestInfoKeys::LighterFactor );
}
/**********************************************/
/**********************************************/
/**********************************************/

