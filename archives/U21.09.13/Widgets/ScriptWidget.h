#ifndef SCRIPTWIDGET_H
#define SCRIPTWIDGET_H
/**********************************************/
#include <QWidget>
#include <QShortcut>
#include "Treatments/Script.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
namespace Ui {
    class ScriptWidget;
}
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ScriptWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ScriptWidget(QWidget *parent = nullptr);
    ~ScriptWidget();
    /**********************************************/
    /**********************************************/
private slots:
    void on_pb_launch_released();
    void on_pb_script_help_released();
    /**********************************************/
    /**********************************************/


private:
    Ui::ScriptWidget *ui;
    QShortcut* sc_launch;
    QShortcut* sc_launch2;
    QShortcut* sc_help;
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SCRIPTWIDGET_H
