#ifndef LATEX_H
#define LATEX_H
/**********************************************/
#include <QThread>
#include "Files.h"
#include "Settings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LaTeX
{
public:
    static inline void Compile( const QString& path, const QString& fileName, const QString& tmpPath = "" )
    {
        QString cmd = "lualatex";
        QStringList args;
        args << "-synctex=1"
             << "-interaction=nonstopmode"
             << "\"" + fileName + ".tex\"";
        // Execute compile TeX File
        // Need two compile twice to correct legend labeling !!
        QString compilePath = path;
        if ( tmpPath != "" )
            compilePath = tmpPath;
        System::exec( cmd, args, compilePath + "/LaTeX" );
        System::exec( cmd, args, compilePath + "/LaTeX" );
        //
        Files::move( compilePath + "/LaTeX/" + fileName + ".pdf",
                     path + "/" + fileName + ".pdf", true); // true is to force move
    }
    /**********************************************/
    /**********************************************/
    static inline void convertPDFtoImage( const QString& path, const QString& fileName, const QString& imageExt = "png" )
    {
        QString cmd = "convert";
        QStringList args;
        args << "-density"
             << "600"
             << fileName + ".pdf"
             << fileName + "." + imageExt;
        // Execute conversion PDF to PNG
        System::exec( cmd, args, path );
    }
    /**********************************************/
    /**********************************************/
    static inline void CompileAndConvertToPNG( const QString& path, const QString& fileName, const QString& imageExt = "png"  )
    {
        LaTeX::Compile( path, fileName );
        LaTeX::convertPDFtoImage( path, fileName, imageExt );
    }
    /**********************************************/
    /**********************************************/
    static inline bool ReadPlotTemplate( const QStringList& datatoplot, QString& contents )
    {
        QString plotTplDir = Settings::getString( Keys::Plots, Keys::TplDir );
        QString plotTpl = "";
        if ( datatoplot.size() == 2 )
            plotTpl = "OneYAxis";
        else if ( datatoplot.size() == 3 )
            plotTpl = "TwoYAxes";
        else //if ( datatoplot.size() == 4 )
            plotTpl = "ThreeYAxes";

        // Get contents in the TeX Template File
        QString origPath = plotTplDir + "/" + plotTpl + ".tex";
        return Files::read( origPath, contents );
    }
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LaTeXThread
        : public QThread
{
    Q_OBJECT
    QString _path;
    QString _filename;

    void run() override
    {
        LaTeX::CompileAndConvertToPNG( _path, _filename );
        emit finished();
    }

public:
    LaTeXThread( const QString& path, const QString& filename )
    {
        _path = path;
        _filename = filename;
    }

signals:
    void finished();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LATEX_H
