#include "MainWindow.h"
#include "ui_MainWindow.h"
/**********************************************/
/**********************************************/
/**********************************************/
#include <QUrl>
#include <QDesktopServices>
#include <QFileDialog>
#include <QDebug>
#include <QProgressBar>
#include <QElapsedTimer>
#include <QTabWidget>
#include <QTime>
#include <QGraphicsScene>
#include <QTimer>
#include <QTreeWidgetItemIterator>
/**********************************************/
#include "Commons/LaTeX.h"
#include "Treatments/Meca.h"
#include "Treatments/RE.h"
#include "Treatments/Treatment.h"
#include "BasicWidgets/ProgressWidget.h"
/**********************************************/
/**********************************************/
/**********************************************/
MainWindow::~MainWindow()
{
    delete ui;
}
/**********************************************/
/**********************************************/
/**********************************************/
MainWindow::MainWindow( QWidget *parent )
    : QMainWindow( parent )
    , ui( new Ui::MainWindow )
{
    ui->setupUi( this );
    ui->wid_testinfo->linkToTestInfo( _testinfo );
    ui->wid_mecadata->linkToTestInfo( _testinfo );
    ui->wid_REdata->linkToTestInfo( _testinfo );
    ui->wid_EAdata->linkToTestInfo( _testinfo );
    ui->wid_CNIdata->linkToTestInfo( _testinfo );
    //
    //
    QString essairDir = Settings::getString( Keys::EssaiDir );
    ui->pb_dirEssai->setText( "Chemin : " + essairDir );
    // TreeWidget
    model->setRootPath( essairDir );
    ui->treeView->setModel( model );
    ui->treeView->setRootIndex( model->index(essairDir) );
    //
    ui->treeView->setColumnHidden( 1, true );
    ui->treeView->setColumnHidden( 2, true );
    ui->treeView->setColumnHidden( 3, true );
    ui->treeView->header()->hide();

    QPalette pal = palette();
    pal.setColor( QPalette::Window, pal.alternateBase().color() );
    ui->sel_menu->setAutoFillBackground(true);
    ui->sel_menu->setPalette(pal);
    ui->tabWidget->setTabVisible( 0, false );
    ui->tabWidget->setTabVisible( 1, false );
    ui->tabWidget->setTabVisible( 2, false );
    ui->tabWidget->setTabVisible( 3, false );
    ui->tabWidget->setTabVisible( 4, false );
    ui->tabWidget->setTabVisible( 5, false );
    ui->tabWidget->setTabVisible( 6, false );
    ui->tabWidget->setTabVisible( 7, false );
    ui->wid_treat->hide();
    //
    ui->splitter->widget(0)->setMaximumWidth(350);
    statusBar()->hide();

    connect( Script::Object(), &ScriptObject::treatAll,
             this, &MainWindow::on_pb_toutTraiter_released );
    connect( Script::Object(), &ScriptObject::treatThis,
             this, &MainWindow::on_pb_traiterCetEssai_released );
    connect( Script::Object(), &ScriptObject::treat,
             this, &MainWindow::treatment );
    connect( ui->wid_settings, &SettingsWidget::treat,
             this, &MainWindow::toutTraiterExt );
    connect( ui->wid_graphs, &GraphWidget::sendPlotPath,
             this, &MainWindow::setTreeCurrent );
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::on_pb_dirEssai_released()
{
    QString path = Settings::getString( Keys::EssaiDir );
    if ( path.isEmpty() )
        path = QDir::homePath();
    QString newpath = QFileDialog::getExistingDirectory( this,
                                                         "Veuillez choisir le dossier contenant les résusltats de vos essais !",
                                                         path,
                                                         QFileDialog::ShowDirsOnly );
    if ( !newpath.isEmpty() )
    {
        ui->pb_dirEssai->setText( "Chemin : " + newpath );
        Settings::save( newpath, Keys::EssaiDir );
        model->setRootPath( newpath );
        ui->treeView->setRootIndex( model->index(newpath) );
    }
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::on_treeView_clicked( const QModelIndex &index )
{
    _currentPath = model->filePath( index );
    _testinfo->setPath( _currentPath );
    ui->wid_testinfo->updateTestInfo();
    ui->wid_mecadata->update();
    ui->wid_graphs->setPath( _currentPath );
    if ( _testinfo->isValid() )
    {
        QString path = _testinfo->getString(TestInfoKeys::Material) + "   |   " +
                _testinfo->getString(TestInfoKeys::Sample) + "   |   " +
                _testinfo->getString(TestInfoKeys::TestName);
        ui->lb_path->setText( path );

        ui->tabWidget->setTabVisible( 0, true );
        ui->wid_treat->show();
        ui->sel_essai->click();
        ui->tabWidget->setTabVisible( 1, true );

        if ( ui->tabWidget->currentIndex() == 0 )
        {
        }
        else if ( ui->tabWidget->currentIndex() == 1 )
        {
            ui->wid_mecadata->update();
        }
        ui->sel_essai->click();
    }
    else
    {
        ui->tabWidget->setTabVisible( 0, false );
        ui->wid_treat->hide();
        ui->tabWidget->setTabVisible( 1, false );
        ui->lb_path->setText( "--" );
        clearContainer();
        if ( !Files::ListImages( _currentPath, true ).isEmpty() || Files::isImage( _currentPath ) )
            ui->sel_graphs->click();
    }
    if ( QFileInfo::exists(_currentPath+"/EA/EA.dat") )
        ui->tabWidget->setTabVisible( 2, true );
    else
        ui->tabWidget->setTabVisible( 2, false );
    if ( QFileInfo::exists(_currentPath+"/RE/RE.dat")  )
        ui->tabWidget->setTabVisible( 3, true );
    else
        ui->tabWidget->setTabVisible( 3, false );
    if ( QDir(_currentPath+"/CNI").exists() )
        ui->tabWidget->setTabVisible( 4, true );
    else
        ui->tabWidget->setTabVisible( 4, false );
    if ( QDir(_currentPath+"/MO").exists() )
        ui->tabWidget->setTabVisible( 5, true );
    else
        ui->tabWidget->setTabVisible( 5, false );
    if ( QDir(_currentPath+"/MEB").exists() )
        ui->tabWidget->setTabVisible( 6, true );
    else
        ui->tabWidget->setTabVisible( 6, false );
    if ( QDir(_currentPath+"/DRX").exists() )
        ui->tabWidget->setTabVisible( 7, true );
    else
        ui->tabWidget->setTabVisible( 7, false );
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::on_treeView_doubleClicked( const QModelIndex &index )
{
    QString path = model->filePath( index );
    QDesktopServices::openUrl( QUrl( "file:"+path ) );
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::setTreeCurrent( const QString& path )
{
    QString newpath = path;
    QModelIndex ind = model->index( newpath, 0 );
    ui->treeView->setCurrentIndex( ind );
    on_treeView_clicked( ind );
    connect( model, &QFileSystemModel::layoutChanged,
             ui->treeView, &TreeView::autofocusScrollbar );
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::on_sel_essai_released()
{
    ui->stackedWidget->setCurrentIndex(0);
    ui->sel_essai->setChecked( true );
    ui->sel_graphs->setChecked( false );
    ui->sel_script->setChecked( false );
    ui->sel_parametres->setChecked( false );
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::on_sel_graphs_released()
{
    ui->stackedWidget->setCurrentIndex(1);
    ui->sel_essai->setChecked( false );
    ui->sel_graphs->setChecked( true );
    ui->sel_script->setChecked( false );
    ui->sel_parametres->setChecked( false );
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::on_sel_script_released()
{
    ui->stackedWidget->setCurrentIndex(2);
    ui->sel_essai->setChecked( false );
    ui->sel_graphs->setChecked( false );
    ui->sel_script->setChecked( true );
    ui->sel_parametres->setChecked( false );
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::on_sel_parametres_released()
{
    ui->stackedWidget->setCurrentIndex(3);
    ui->sel_essai->setChecked( false );
    ui->sel_graphs->setChecked( false );
    ui->sel_script->setChecked( false );
    ui->sel_parametres->setChecked( true );
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::on_tabWidget_currentChanged( int index )
{
    if ( index == 0 )
    {
        //        QString path = model->filePath( ui->treeView->currentIndex() );
        //        QFile file( path+"/INFO" );
    }
    else if ( index == 1 )
    {
        //        on_meca_rawdata_released();
    }
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::clearContainer()
{
    ui->tabWidget->setTabVisible( 2, false );
    ui->tabWidget->setTabVisible( 3, false );
    ui->tabWidget->setTabVisible( 4, false );
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::on_pb_traiterCetEssai_released()
{
    if ( _testinfo->isValid() )
        treatment( {_testinfo->path()} );
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::on_pb_toutTraiter_released()
{
    QStringList list = Files::ListMecaDirectories( Settings::getString( Keys::EssaiDir ),
                                                   Settings::getBool( Keys::RecursiveSearch ) );
    treatment( list );
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::toutTraiterExt( const TreatmentInfo& trinfo )
{
    QStringList list = Files::ListMecaDirectories( Settings::getString( Keys::EssaiDir ),
                                                   Settings::getBool( Keys::RecursiveSearch ) );
    treatment( list, trinfo );
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::treatment( const QStringList& paths, const TreatmentInfo& info )
{
    statusBar()->show();
    int size = paths.size(), it = 0;
    ProgressWidget* progress = new ProgressWidget;
    if ( size < 2 )
        progress->hideProgressBar();
    connect( progress, SIGNAL(stopT()),
             this, SLOT(stopTreatment()) );
    statusBar()->addWidget( progress );
    QElapsedTimer timer;
    timer.start();
    for ( const QString &dir : paths )
    {
        if ( _stopTreatement == true )
            break;
        progress->setProgress( (100*it)/size );
        progress->setDir( dir );
        if ( it != 0 )
            progress->setRemaintime( (size-it)*timer.elapsed()/(it) );
        QEventLoop loop;
        Treatment treat;
        connect( &treat, SIGNAL(finished()),
                 &loop, SLOT(quit()) );
        connect( &treat, &Treatment::currentTask,
                 progress, &ProgressWidget::setTask );
        treat.run( dir, info );
        loop.exec();
        ++it;
    }
    statusBar()->removeWidget( progress );
    if ( progress )
        delete progress;
    _stopTreatement = false;
    statusBar()->hide();
    ui->wid_mecadata->update();
    ui->wid_graphs->update();
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::stopTreatment()
{
    _stopTreatement = true;
}
/**********************************************/
/**********************************************/
/**********************************************/
