#ifndef SCRIPT_H
#define SCRIPT_H
/**********************************************/
#include <QProcess>
#include <QDebug>
#include <QMessageBox>
#include "Commons/Files.h"
#include "Commons/Settings.h"
#include "Commons/TestInfo.h"
#include "Treatment.h"
#include "Plots.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ScriptObject : public QObject
{
    Q_OBJECT
public:
    ScriptObject( QObject* obj = 0 ) : QObject( obj )
    {

    }
signals:
    void treatAll();
    void treatThis();
    void treat( const QStringList& list, const TreatmentInfo& trinfo );
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Script
{
    static inline ScriptObject* _object = new ScriptObject;
    QString _path;
    static inline int _nbdata = 0;
    /**********************************************/
    /**********************************************/
public:
    static inline ScriptObject* Object()
    {
        return _object;
    }
    /**********************************************/
    /**********************************************/
    static inline void Launch( const QString& script = "" )
    {
        QStringList scriptLines = script.split("\n", Qt::SkipEmptyParts );
        for ( QString line : scriptLines )
            Script::LaunchLine( line );
    }
    /**********************************************/
    /**********************************************/
    static inline void LaunchLine( const QString& script )
    {
        if ( script == "" )
            return;

        QStringList commandline = Script::SplitLine( script );

        QString command = commandline.takeFirst();
        if ( Script::EqualStrings( command, "compare" ) )
            Script::Compare( commandline );
        else if ( Script::EqualStrings( command, "treat" )  )
            Script::Treat( commandline );
        else
        {
            QMessageBox msgBox;
            msgBox.setText("Commande incomprise !");
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.exec();
            return;
        }
    }
    /**********************************************/
    /**********************************************/
    static inline void Compare( QStringList& commandopt )
    {
        _nbdata = 0;
        QString workpath = Settings::getString( Keys::ComparisonDir );
        QString cmdOpt = commandopt.takeFirst();
        QString filename = "";
        QString lang = "";
        QString imageext = "png";
        QVector<QStringList> data;
        // data format : { QString dataPath, QString dataLabel, QString dataLegend, QString dataColorStyle }
//        QStringList dirlist = Files::ListMecaDirectories( Settings::getString( Keys::EssaiDir ), true );
        for ( int it = 0; it < commandopt.size(); ++it )
        {
            if ( Script::EqualStrings( commandopt[it], "add" ) )
            {
                ++it;
                if ( it >= commandopt.size() )
                    return;
                data.append( Script::DataToAdd(cmdOpt, commandopt[it]) );
            }
            else if ( Script::EqualStrings( commandopt[it], "addAll" ) )
            {
                ++it;
                if ( it >= commandopt.size() )
                    return;
                data.append( Script::DataToAdd(cmdOpt, commandopt[it]) );
            }
            else if ( Script::EqualStrings( commandopt[it], "name" ) )
            {
                ++it;
                if ( it >= commandopt.size() )
                    return;
                filename = commandopt[it];
            }
            else if ( Script::EqualStrings( commandopt[it], "lang" ) )
            {
                ++it;
                if ( it >= commandopt.size() )
                    return;
                lang = commandopt[it];
            }
            else if ( Script::EqualStrings( commandopt[it], "ext" ) )
            {
                ++it;
                if ( it >= commandopt.size() )
                    return;
                imageext = commandopt[it];
            }
        }
        // Set Plot FileName
//        if ( filename.isEmpty() )
//        {
//            filename = cmdOpt;
////            for ( QString file : _toAddFile )
////                filename += "_" + file;
//        }
        // Set Plot Lang
        if ( lang == "EN" )
            MecaPlots::EnglishVersion();
        else
            MecaPlots::FrenchVersion();
        // Set Image Extension
        MecaPlots::SetImageEXT( imageext );

        MecaPlots::Treat( workpath, cmdOpt, data, filename, "" );
        return;
    }
    /**********************************************/
    /**********************************************/
    static inline void Treat( const QStringList& commandopt )
    {
        if ( Script::EqualStrings( commandopt.first(), "all" ) )
        {
            emit _object->treatAll();
            return;
        }
        else if ( Script::EqualStrings( commandopt.first(), "this" ) )
        {
            emit _object->treatThis();
            return;
        }
        QStringList list;
        QStringList dirlist = Files::ListMecaDirectories( Settings::getString( Keys::EssaiDir ), true );
        for ( int it = 0; it < commandopt.size(); ++it )
        {
            if ( Script::EqualStrings( commandopt[it], "add" ) )
            {
                ++it;
                if ( it >= commandopt.size() )
                    return;
                QStringList filters = Script::Filters( commandopt[it] );
                list.append( Script::FilterList( dirlist, filters ) );
            }
            emit _object->treat(list, TreatmentInfo());
        }
    }
    /**********************************************/
    /**********************************************/
    static inline QStringList SplitLine( const QString& line )
    {
        QStringList spliting;
        bool _searchSpace = true;
        bool _searchOPT = false;
        QString tmp;
        for ( int it = 0; it < line.size(); ++it )
        {
            QString tmpC = line.at(it);
            if ( it +1 == line.size() )
            {
                if ( tmpC != "\"" && tmpC != " " )
                    tmp += tmpC;
                spliting.append( tmp );
            }
            else if ( (_searchSpace && tmpC == " ") )
            {
                spliting.append( tmp );
                tmp.clear();
            }
            else if ( tmpC == "\"" )
            {
                _searchOPT = !_searchOPT;
                if ( _searchOPT )
                    _searchSpace = false;
                else
                    _searchSpace = true;
            }
            else
                tmp += tmpC;
        }
        return spliting;
    }
    /**********************************************/
    /**********************************************/
    static inline QStringList Filters( const QString& filters )
    {
        QStringList _filters;
        QString tmp_filter;
        for ( int it = 0; it < filters.size(); ++it )
        {
            if ( it + 1 == filters.size() )
            {
                tmp_filter += filters.at(it);
                if ( !tmp_filter.isEmpty() )
                    _filters.append( tmp_filter );
                break;
            }
            if ( filters.at(it) == QString("&") || filters.at(it) == QString("!") )
            {
                if ( !tmp_filter.isEmpty() )
                    _filters.append( tmp_filter );
                tmp_filter = filters.at(it);
            }
            else
                tmp_filter += filters.at(it);
        }
        return _filters;
    }
    /**********************************************/
    /**********************************************/
    static inline bool Match( const QString& dir, const QStringList& filters )
    {
        bool toTake = true;
        for ( QString filter : filters )
        {
            QString mark = filter.at(0);
            QString tmpfilter = filter.remove(0, 1);
            if ( mark == "&" )
                toTake = toTake && dir.contains(tmpfilter);
            else if ( mark == "!" )
                toTake = toTake && !dir.contains(tmpfilter);
        }
        return toTake;
    }
    /**********************************************/
    /**********************************************/
    static inline QStringList FilterList( const QStringList& dirlist, const QStringList& filters, bool takeFirst = false )
    {
        QStringList list;
        for ( QString dir : dirlist )
            if ( Script::Match( dir, filters ) )
            {
                list.append( dir );
                if ( takeFirst )
                    return list;
            }
        return list;
    }
    /**********************************************/
    /**********************************************/
    static inline QVector<QStringList> DataToAdd( QString& cmdOpt, QString& lineopts )
    {
        QStringList dirlist = Files::ListMecaDirectories( Settings::getString( Keys::EssaiDir ), true );
        QStringList opts = lineopts.split("/", Qt::KeepEmptyParts);
        QVector<QStringList> data;
        QStringList selectList;
        if ( opts.size() == 1 )
            selectList = Script::FilterList( dirlist, Script::Filters( opts[0] ) );
        else
            selectList = Script::FilterList( dirlist, Script::Filters( opts[0] ), true );
        for ( QString datapath : selectList )
        {
            if ( selectList.isEmpty() )
                return data;
            QStringList datarow = {"", "", "", ""};
            datarow[0] = datapath+"/MECA_DEF_CONT.dat";
            if ( cmdOpt.contains("mt") )
                datarow[0] = datapath+"/MECA_MOD_TANG.dat";
            // set data label
            datarow[1] = "";
            // set data legend
            QString legend = "";
            if ( opts.size() > 1 )
                legend = opts[1];
            if ( legend.isEmpty() )
            {
                TestInfo info;
                info.setPath( datapath );
                legend = info.getString( TestInfoKeys::Sample );
            }
            datarow[2] = legend;
            // set data color
            QString color = "";
            if ( opts.size() > 2 )
                color = opts[2];
            if ( color.isEmpty() )
                color = "CLR" + QString("%1").arg(_nbdata+1);
            datarow[3] = color;
            ++_nbdata;
            data.append( datarow );
        }
        return data;
    }
    /**********************************************/
    /**********************************************/
    static inline bool EqualStrings( const QString& str1, const QString& str2 )
    {
        QString stra = str1.toLower();
        QString strb = str2.toLower();
        return ( stra == strb );
    }
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
class ScriptThread : public QThread
{
    Q_OBJECT
    QString _cmd;
    /**********************************************/
    /**********************************************/
    void run() override
    {
        Script::Launch( _cmd );
        emit finished();
    }
    /**********************************************/
    /**********************************************/
public:
    ScriptThread( const QString& cmd )
    {
        _cmd = cmd;
    }
    /**********************************************/
    /**********************************************/
signals:
    void finished();
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SCRIPT_H
