#ifndef DATA_H
#define DATA_H
/**********************************************/
#include <QString>
#include <QVector>
/**********************************************/
/**********************************************/
namespace DataKeys {
    static inline QString TIME  = "      t(s)";
    static inline QString DEP   = "      dep(mm)";
    static inline QString FLE   = "      fle(mm)";
    static inline QString FORCE = "     force(N)";
    static inline QString DEF   = "      e(perc)";
    static inline QString DEF1  = "     e1(perc)";
    static inline QString DEF2  = "     e2(perc)";
    static inline QString DEF3  = "     e3(perc)";
    static inline QString CTR   = "       s(MPa)";
    static inline QString RE    = "     RE(mOhm)";
    static inline QString DRR0  = "  dR_R0(perc)";
    static inline QString EA    = "      EA(cps)";
    static inline QString T     = "         T(C)";
    static inline QString MT    = "    Etg (GPa)";
    static inline QString MTB   = "   beta (MPa)";
    static inline QString Esc   = "    Esc (GPa)";
}
/**********************************************/
/**********************************************/
namespace DataProg { // define in which colunm are the data (-1 if don't exist)
    static inline QVector<int> NONE     = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };
    static inline QVector<int> R8562    = {  0,  0, -1,  1,  2,  3, -1, -1,  4, -1, -1 };
    static inline QVector<int> RESIST   = {  1,  5, -1, -1,  1,  2, -1,  0,  4,  3, -1 };
    static inline QVector<int> TRACMONO = {  2,  5,  3,  0,  1,  2, -1,  6, -1,  4, -1 };
    static inline QVector<int> TRAC     = {  3,  4, -1, -1,  1,  2, -1,  0, -1,  3, -1 };
    static inline QVector<int> FLEX     = {  4,  3,  1,  0,  6, -1, -1,  5, -1,  2, -1 };
    static inline QVector<int> FLEX12   = {  5,  4,  1,  0, -1, -1, -1, -1,  3,  2, -1 };
    static inline QVector<int> IOSI3J   = {  6,  3,  1,  0,  4,  5,  6, -1,  7,  2, -1 };
    static inline QVector<int> VIEIL    = {  7,  0,  8,  7, -1, -1, -1, -1, -1, -1,  9 };
    static inline QVector<int> RESIST2  = {  8,  6, -1, -1,  1,  2, -1,  0,  4,  3, -1 };
    static inline QVector<int> TRACTR   = {  9,  4, -1, -1,  1,  2, -1,  0, -1,  3, -1 };
    static inline QVector<int> ESSAIHT  = { 10,  1,  9,  8, -1, -1, -1, -1, -1, -1, 10 };
    static inline QVector<int> RESCOLAS = { 11,  0,  3,  2, -1, -1, -1, -1,  1, -1, -1 };
    static inline int idTIME  = 1;
    static inline int idDEP   = 2;
    static inline int idFORCE = 3;
    static inline int idDEF1  = 4;
    static inline int idDEF2  = 5;
    static inline int idDEF3  = 6;
    static inline int idCTR   = 7;
    static inline int idRE    = 8;
    static inline int idEA    = 9;
    static inline int idT     = 10;
}
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class DataPlot
{

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // DATA_H
