#ifndef MECA_H
#define MECA_H
/**********************************************/
#include "Commons/Files.h"
#include "Treatments/Data.h"
#include "Commons/Settings.h"
#include "Commons/TestInfo.h"
#include "Commons/Math.h"
#include "Fitting.h"
/**********************************************/
#include <QDebug>
/**********************************************/
/**********************************************/
//namespace DataKeys {
//    static inline QString TIME  = "      t(s)";
//    static inline QString DEP   = "      dep(mm)";
//    static inline QString FLE   = "      fle(mm)";
//    static inline QString FORCE = "     force(N)";
//    static inline QString DEF   = "      e(perc)";
//    static inline QString DEF1  = "     e1(perc)";
//    static inline QString DEF2  = "     e2(perc)";
//    static inline QString DEF3  = "     e3(perc)";
//    static inline QString CTR   = "       s(MPa)";
//    static inline QString RE    = "     RE(mOhm)";
//    static inline QString DRR0  = "  dR_R0(perc)";
//    static inline QString EA    = "      EA(cps)";
//    static inline QString T     = "         T(C)";
//    static inline QString MT    = "        alpha";
//    static inline QString MTB   = "         beta";
//}
/**********************************************/
/**********************************************/
//namespace DataProg { // define in which colunm are the data (-1 if don't exist)
//    static inline QVector<int> NONE     = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };
//    static inline QVector<int> R8562    = {  0,  0, -1,  1,  2,  3, -1, -1,  4, -1, -1 };
//    static inline QVector<int> RESIST   = {  1,  5, -1, -1,  1,  2, -1,  0,  4,  3, -1 };
//    static inline QVector<int> RESIST2  = {  8,  6, -1, -1,  1,  2, -1,  0,  4,  3, -1 };
//    static inline QVector<int> TRACTR   = {  9,  4, -1, -1,  1,  2, -1,  0, -1,  3, -1 };
//    static inline QVector<int> TRACMONO = {  2,  5,  3,  0,  1,  2, -1,  6, -1,  4, -1 };
//    static inline QVector<int> TRAC     = {  3,  4, -1, -1,  1,  2, -1,  0, -1,  3, -1 };
//    static inline QVector<int> FLEX     = {  4,  3,  1,  0,  6, -1, -1,  5, -1,  2, -1 };
//    static inline QVector<int> FLEX12   = {  5,  4,  1,  0, -1, -1, -1, -1,  3,  2, -1 };
//    static inline QVector<int> IOSI3J   = {  6,  3,  1,  0,  4,  5,  6, -1,  7,  2, -1 };
//    static inline QVector<int> VIEIL    = {  7,  0,  8,  7, -1, -1, -1, -1, -1, -1,  9 };
//    static inline QVector<int> ESSAIHT  = { 10,  1,  9,  8, -1, -1, -1, -1, -1, -1, 10 };
//    static inline int idTIME  = 1;
//    static inline int idDEP   = 2;
//    static inline int idFORCE = 3;
//    static inline int idDEF1  = 4;
//    static inline int idDEF2  = 5;
//    static inline int idDEF3  = 6;
//    static inline int idCTR   = 7;
//    static inline int idRE    = 8;
//    static inline int idEA    = 9;
//    static inline int idT     = 10;
//}
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Meca
{
public:
    static inline bool Treat( TestInfo* testinfo )
    {
        if ( !testinfo )
            return false;
        QString path = testinfo->path();
        // Get Program Info
        QString firstLine;
        if ( !Files::readFirstLine( path + "/MECA.dat", firstLine, QStringConverter::Latin1) )
            if ( !Files::readFirstLine( path + "/MECA.csv", firstLine, QStringConverter::Latin1) )
                return false;

        QVector<int> progfrt = DataProg::NONE;
        if ( firstLine.contains("R8562.BAS") )
            progfrt = DataProg::R8562;
        else if ( firstLine.contains("RESIST.BAS") )
            progfrt = DataProg::RESIST;
        else if ( firstLine.contains("RESIST2.BAS") )
            progfrt = DataProg::RESIST2;
        else if ( firstLine.contains("TRACTR.BAS") )
            progfrt = DataProg::TRACTR;
        else if ( firstLine.contains("TRACMONO.BAS") )
            progfrt = DataProg::TRACMONO;
        else if ( firstLine.contains("TRACTION.BAS") )
            progfrt = DataProg::TRAC;
        else if ( firstLine.contains("FLEXION.BAS") )
            progfrt = DataProg::FLEX;
        else if ( firstLine.contains("FLEXION1.BAS") || firstLine.contains("FLEXION2.BAS") )
            progfrt = DataProg::FLEX12;
        else if ( firstLine.contains("IOSI3J.BAS") )
            progfrt = DataProg::IOSI3J;
        else if ( firstLine.contains("Temps total (s)") )
        {
            QString testname = testinfo->getString( TestInfoKeys::TestName );
            if ( testname.left(8).contains("VIEIL") )
                progfrt = DataProg::VIEIL;
            else
                progfrt = DataProg::ESSAIHT;
        }

        if ( progfrt == DataProg::NONE ) return false;

        // Read Raw Data
        QVector<QVector<double>> raw_data;
        if ( !Files::readData( path + "/MECA.dat", raw_data, QStringConverter::Latin1) )
            if ( !Files::readData( path + "/MECA.csv", raw_data, QStringConverter::Latin1) )
                return false;

        int iTIME  = progfrt[DataProg::idTIME];
        int iDEP   = progfrt[DataProg::idDEP];
        int iFORCE = progfrt[DataProg::idFORCE];
        int iDEF1  = progfrt[DataProg::idDEF1];
        int iDEF2  = progfrt[DataProg::idDEF2];
        int iDEF3  = progfrt[DataProg::idDEF3];
        Q_UNUSED(iDEF3)
        int iCTR   = progfrt[DataProg::idCTR];
        int iRE    = progfrt[DataProg::idRE];
        int iEA    = progfrt[DataProg::idEA];
        int iT     = progfrt[DataProg::idT];

        bool tempFour = false;
        int tempFocused = 980;
        if ( iT != -1 )
        {
            if ( path.contains("VIEIL_1300") )
                tempFocused = 1280;
            QString rmqs;
            if ( Files::read( path+"/Remarques.txt", rmqs, QStringConverter::Latin1 ) )
            {
                rmqs.remove("\r");
                QStringList coms = rmqs.split("\n");
                for ( QString com : coms )
                {
                    if ( (com.contains("sservissement") || com.contains("égulation")) &&
                         com.contains("thermocouple") && com.contains("four") && !com.contains("erreur") )
                        tempFour = true;
                    QString comments = testinfo->getString( TestInfoKeys::Comments );
                    if ( !comments.contains(com) )
                        testinfo->save( comments + com + "\n", TestInfoKeys::Comments );
                }
            }
        }

        QString suiviDispo;
        if ( iRE != -1 )
            suiviDispo = "R.E.";
        if ( iEA != -1 )
            suiviDispo = (iRE == -1 ? "" : suiviDispo + " - ") + "E.A.";
        QString suivi = testinfo->getString( TestInfoKeys::AvailableTracking );
        if ( suivi.isEmpty() )
            testinfo->save( suiviDispo, TestInfoKeys::AvailableTracking );
        if ( !suivi.contains( "R.E." ) )
            iRE = -1;
        if ( !suivi.contains( "E.A." ) )
            iEA = -1;

        // Build headers of the treated file
        QString header_data;
        QString header_rawdata;
        header_data = DataKeys::TIME + ";" + DataKeys::DEF + ";" + DataKeys::CTR;
        if ( iT != -1 )
            header_data += ";" + DataKeys::T;
        if ( iRE != -1 )
            header_data += ";" + DataKeys::RE + ";" + DataKeys::DRR0;
        if ( iEA != -1 )
            header_data += ";" + DataKeys::EA;
        if ( iDEP != -1 && iFORCE != -1 )
        {
            header_rawdata = DataKeys::TIME + ";" + DataKeys::DEP + ";" + DataKeys::FORCE;
            if ( iT != -1 )
                header_rawdata += ";" + DataKeys::T;
            if ( iRE != -1 )
                header_rawdata += ";" + DataKeys::RE + ";" + DataKeys::DRR0;
            if ( iEA != -1 )
                header_rawdata += ";" + DataKeys::EA;
        }

        // Treat data
        QVector<QVector<double>> trd_data;
        QVector<QVector<double>> trd_data_fcsd;
        QVector<QVector<double>> trd_data2;
        // Get RawData
        double L = testinfo->getDouble( TestInfoKeys::Length );
        double b = testinfo->getDouble( TestInfoKeys::Width );
        double e = testinfo->getDouble( TestInfoKeys::Thickness );
        double tcut = testinfo->getDouble( TestInfoKeys::TimeCut );
        double tim = -1;
        double dep = -1;
        double dep0 = -1;
        if ( iDEP != -1)
            dep0 = raw_data[0][iDEP];
        double frc = -1;
        double def = -1;
        double ctr = -1;
        double re = -1;
        double R0 = -1;
        double dRdR0 = 0;
        if ( iRE != -1)
        {
            R0 = raw_data[0][iRE];
            if ( R0 < 1 )
                R0 *= 1000;
        }
        double ea = -1;
        double temp = -1;
        for ( int it = 0; it < raw_data.size(); ++it )
        {
            QVector<double> line = raw_data[it];
            tim = line[iTIME];
            // Get deplacement
            if ( iDEP != -1)
                dep = line[iDEP];
            // Get Force
            if ( iFORCE != -1)
                frc = line[iFORCE];
            // Get Strain
            if ( progfrt == DataProg::FLEX12 )
                def = 100*(6*e/(L*L))*line[iDEP];
            else if ( progfrt == DataProg::IOSI3J )
                def = line[iDEF1] - line[iDEF2];
            else if ( progfrt == DataProg::VIEIL || progfrt == DataProg::ESSAIHT )
            {
                def = 100*(dep - dep0)/L;
                if ( L == -1 )
                    def = -1;
            }
            else if ( iDEF1 != -1 )
            {
                if ( iDEF2 == -1 )
                    def = line[iDEF1];
                else
                    def = 0.5*( line[iDEF1] + line[iDEF2] );
            }
            // Get Stress
            if ( progfrt == DataProg::FLEX12 )
                ctr = 1.5*(L/(b*e*e))*line[iFORCE];
            else if ( progfrt == DataProg::IOSI3J ||
                      progfrt == DataProg::R8562 ||
                      progfrt == DataProg::VIEIL ||
                      progfrt == DataProg::ESSAIHT )
            {
                ctr = frc/(b*e);
                if ( e == -1 )
                    ctr *= -1;
            }
            else if ( iCTR != -1)
                ctr = line[iCTR];
            // get RE
            if ( iRE != -1 )
            {
                re = line[iRE];
                if ( re < 1 )
                    re *= 1000; // convert in mOhm
                dRdR0 = 100*( re - R0 )/R0;
            }
            // get EA
            if ( iEA != -1 )
                ea = line[iEA];
            // get Temperature
            if ( iT != -1 )
            {
                temp = line[iT];
                if ( tempFour )
                    temp = - 0.0000005127092*pow(temp, 3)
                            + 0.0007545365*pow(temp, 2)
                            + 0.9624921*temp
                            + 5.306989;
            }
            // Save data in local variable
            if ( /*def >= -0.001 &&*/
                 (line[iTIME] < tcut || tcut == -1 ) )
            {
                QVector<double> dataLine = { tim, def, ctr };
                if ( iT != -1 )
                    dataLine.append( temp );
                if ( iRE != -1 )
                    dataLine.append( {re, dRdR0} );
                if ( iEA != -1 )
                    dataLine.append( ea );
                trd_data.append( dataLine );
//                if ( iT != -1 && temp > tempFocused )
//                    trd_data_fcsd.append( dataLine );

                if ( iDEP != -1 && iFORCE != -1 )
                {
                    QVector<double> rawdataLine = { tim, dep, frc };
                    if ( iT != -1 )
                        rawdataLine.append( temp );
                    if ( iRE != -1 )
                        rawdataLine.append( {re, dRdR0} );
                    if ( iEA != -1 )
                        rawdataLine.append( ea );
                    trd_data2.append( rawdataLine );
                }
            }
        }

        // Save Data
        Math::MoyenneGlissee( trd_data );
        Files::save( path + "/MECA_DEF_CONT.dat", trd_data, header_data );

        // Save Raw Data
        if ( trd_data2.size() > 0 )
        {
            Math::MoyenneGlissee( trd_data2 );
            Files::save( path + "/MECA_DEP_FORCE.dat", trd_data2, header_rawdata );
        }
        // Save Focused Data
//        if ( trd_data_fcsd.size() > 0 )
//        {
//            Math::MoyenneGlissee( trd_data_fcsd );
//            Files::save( path + "/MECA_DEF_CONT_focused.dat", trd_data_fcsd, header_data );
//        }

        // Get and Save Tangent Module
        if ( progfrt != DataProg::VIEIL )
        {
            QVector<QVector<double>> tang_mod = Math::RegressionLineaire( trd_data, 1, 2 );
            for ( QVector<double>& line : tang_mod )
                line.append( 0.1*line.at(2)/line.at(1) );
            QString header = DataKeys::TIME + ";" + DataKeys::DEF + ";" + DataKeys::CTR + ";" + DataKeys::MT + ";" + DataKeys::MTB+ ";" + DataKeys::Esc;
            Files::save( path + "/MECA_MOD_TANG.dat", tang_mod, header );
        }
        if ( testinfo->getString( TestInfoKeys::TestName ).contains("CYCL") )
            Fitting::run( path, trd_data );
        return true;
    }
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // MECA_H
