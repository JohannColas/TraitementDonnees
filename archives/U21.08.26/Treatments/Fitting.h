#ifndef FITTING_H
#define FITTING_H
/**********************************************/
#include <QVector>
/**********************************************/
#include "Commons/Files.h"
#include "Commons/System.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Fitting
{
    /**********************************************/
    /**********************************************/
public:
    static inline void run( const QString& path, const QVector<QVector<double>>& data )
    {
        QVector<QStringList> trd_data;
        int n_cycle = 0;
        QString header = "time(s);nCycle;stepCycle;e(perc);s(MPa);Esc Normal;Esc Decharge;Esc Charge";
        trd_data.append( {QString::number(data.at(0).at(0)),
                               QString::number(n_cycle),
                               "n",
                               QString::number(data.at(0).at(1)),
                               QString::number(data.at(0).at(2)),
                               "0",
                               "",
                               ""
                              } );
        //
        bool charge = true;
        bool decharge = false;
        double lim_def = -1;
        int lim_it = -1;
        QVector<QVector<double>> norm_data;
        QVector<QVector<double>> dech_data;
        QVector<QVector<double>> char_data;
        //
        System::createDir(path, "cycles");
        for ( int it = 1; it < data.size(); ++it )
        {
            QVector<double> row = data.at(it);
            if ( it - lim_def > 10 )
            {
                if ( row.at(1) - data.at(it-1).at(1) < 0 && !decharge )
                {
                    decharge = true;
                    charge = false;
                    lim_def = data.at(it-1).at(1);
                    lim_it = it-1;
                    if ( char_data.size() > 0 )
                        Files::saveB( path + "/cycles/c"+QString::number(n_cycle)+".dat", char_data, "" );
                    char_data.clear();
                    ++n_cycle;
                }
                else if ( row.at(1) - data.at(it-1).at(1) > 0 && !charge )
                {
                    decharge = false;
                    charge = true;
                    if ( dech_data.size() > 0 )
                        Files::saveB( path + "/cycles/d"+QString::number(n_cycle)+".dat", dech_data, "" );
                    dech_data.clear();
                }
            }
            QString Esc = QString::number(0.1*row.at(2)/row.at(1));
            QString Esc_n = "";
            QString Esc_d = "";
            QString Esc_c = "";
            QString step = "";
            if ( charge )
            {
                if ( row.at(1) > lim_def )
                {
                    Esc_n = Esc;
                    step = "n";
                    norm_data.append( {row.at(1), 0.1*row.at(2)/row.at(1)} );
                }
                else
                {
                    Esc_c = Esc;
                    step = "c";
                    char_data.append( {row.at(1), 0.1*row.at(2)/row.at(1)} );
                }
            }
            else
            {
                Esc_d = Esc;
                step = "d";
                dech_data.append( {row.at(1), 0.1*row.at(2)/row.at(1)} );
            }
            trd_data.append( {QString::number(row.at(0)),
                                   QString::number(n_cycle),
                                   step,
                                   QString::number(row.at(1)),
                                   QString::number(row.at(2)),
                                   Esc_n,
                                   Esc_d,
                                   Esc_c
                                  } );
        }
        // Last Charge
        Files::saveB( path + "/cycles/c"+QString::number(n_cycle)+".dat", char_data, "" );
        Files::saveB( path + "/cycles/n.dat", norm_data, "" );
        Files::save( path + "/cycles/cycles.dat", trd_data, header );
    }
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // FITTING_H
