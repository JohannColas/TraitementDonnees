#ifndef PLOTS_H
#define PLOTS_H
/**********************************************/
#include "Commons/Files.h"
#include "Commons/Settings.h"
#include "Meca.h"
#include "Commons/LaTeX.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
namespace PlotKeys {
    static QString xlabel = "%%%XLABEL%%%";
    static QString ylabel = "%%%YLABEL%%%";
    static QString yblabel = "%%%YBLABEL%%%";
    static QString ytlabel = "%%%YTLABEL%%%";
    static QString fLimits = "%%%FLIMITS%%%";
//
    static int FRENCH = 0;
    static int ENGLISH = 1;
    static QStringList TIME = {"Temps (s)", "Time (s)"};
    static QStringList TIME_LEGEND = {"0", "0"};
//
    static QStringList DEF_LABEL = {"Déformation (\\si{\\percent})", "Stain (\\si{\\percent})"};
    static QStringList DEF_LEGEND = {"Déformation", "Stain"};
    static QString DEF_COLOR = "DEF";
    //
    static QStringList CTR_LABEL = {"Contrainte (\\si{\\mega\\pascal})", "Stress (\\si{\\mega\\pascal})"};
    static QStringList CTR_LEGEND = {"Contrainte", "Stress"};
    static QString CTR_COLOR = "CTR";
    //
    static QStringList MT_LABEL = {"Module tangent (\\si{\\giga\\pascal})", "Tangent module (\\si{\\giga\\pascal})"};
    static QStringList MT_LEGEND = {"Module tangent", "Tangent module"};
    static QString MT_COLOR = "CTR";
    //
    static QStringList EA_LABEL = {"E.A. (coups)", "E.A. (counts)"};
    static QStringList EA_LEGEND = {"Émission acoustique", "Acoustic emission"};
    static QString EA_COLOR = "EA";
    //
    static QStringList RE_LABEL = {"$\\mathbfsf{\\frac{\\Updelta R}{R_0}}$ (\\si{\\percent})", "$\\mathbfsf{\\frac{\\Updelta R}{R_0}}$ (\\si{\\percent})"};
    static QStringList RE_LEGEND = {"Résistance électrique", "Electrical resistance"};
    static QString RE_COLOR = "RE";
    //
    static QStringList T_LABEL = {"Température (\\si{\\celsius})", "Temperature (\\si{\\celsius})"};
    static QStringList T_LEGEND = {"Température", "Temperature"};
    static QString T_COLOR = "TEMP";
    //
    static QString PlotLine = "\t\t\t\\addplot+[%COLOR%] table[x index=%XINDEX%,y index=%YINDEX%,col sep=semicolon] {%FILENAME%};\\label{%LABEL%}\n";
    static QString LegendLine = "\t\t\t\\addlegendimage{/pgfplots/refstyle=%LABEL%}\\addlegendentry{%LEGEND%}\n";
}
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class MecaPlots {
private:
    static inline int _lang = PlotKeys::FRENCH;
    static inline QString _imageEXT = "png";
    /**********************************************/
    /**********************************************/
public:
    static inline void EnglishVersion()
    {
        _lang = PlotKeys::ENGLISH;
    }
    static inline void FrenchVersion()
    {
        _lang = PlotKeys::FRENCH;
    }
    /**********************************************/
    /**********************************************/
    static inline void SetImageEXT( const QString& ext )
    {
        _imageEXT = ext;
    }
    /**********************************************/
    /**********************************************/
    static inline void Treat( const QString& workpath, const QStringList& plots, const QVector<QStringList>& data )
    {
        for ( QString plotName : plots )
        {
            MecaPlots::Treat( workpath, plotName, data );
        }
    }
    /**********************************************/
    /**********************************************/
    static inline void Treat( const QString& workpath, const QString& plotName, const QVector<QStringList>& data, const QString& filename = "", const QString& subpath = "/plots" )
    {
            if (  !QDir(workpath).exists() )
                return;

            QStringList plot = plotName.split('-');
            // Get contents in the TeX Template File
            QString contents;
            if ( !LaTeX::ReadPlotTemplate( plot, contents ) )
                return;

            MecaPlots::ReplaceDataLabels( plot, contents );

            QString legend;
            QStringList axes = {"","",""};
            for ( int jt = 1; jt < plot.size(); jt++ )
            {
                for ( int it = 0; it < data.size(); it++ )
                {
                    QStringList datarow = data[it];
                    datarow[1] = QString("PL%1").arg(it+1) + plot[jt];
                    QString line = MecaPlots::BuildPlotLine( {plot[0], plot[jt]}, datarow );
                    axes[jt-1].append( line );
                    if ( plot.size() > 1 || data.size() > 1)
                    {
                        QString line2 = MecaPlots::BuildLegendLine( {plot[0], plot[jt]}, datarow );
                        legend.append( line2 );
                    }
                }
            }

            contents.replace( "%%%LEGEND%%%", legend );
            contents.replace( "%%%PLOTS-AXIS1%%%", axes[0] );
            contents.replace( "%%%PLOTS-AXIS2%%%", axes[1] );
            contents.replace( "%%%PLOTS-AXIS3%%%", axes[2] );

            // Save contents in the TeX File
            QString fileN = "";
            if ( _lang == PlotKeys::ENGLISH )
                fileN = "EN_";
            if ( filename.isEmpty() )
                fileN += plotName;
            else
                fileN += filename;
            if ( data[0][0].contains("_focused") )
                fileN += "_focused";
            QString TeX_Path = workpath + subpath + "/LaTeX/" + fileN + ".tex";
            Files::save( TeX_Path, contents );
            // Compilation et Conversion en PNG
            LaTeX::CompileAndConvertToPNG( workpath + subpath, fileN, _imageEXT );
//            int time = timer.elapsed();
//            qDebug().noquote() << "Time to build "+plotName+" plot :" << System::TimeToString(time);
            _lang = PlotKeys::FRENCH;
            _imageEXT = "png";
    }
    /**********************************************/
    /**********************************************/
    static inline void GenerateMECA( TestInfo* info )
    {
        QString path = info->path();
        Files::createDir( path, "plots" );
        Files::createDir( path + "/plots", "LaTeX" );

        QStringList plots;
        QString essai = info->getString( TestInfoKeys::TestName );
        QString suivi = info->getString( TestInfoKeys::AvailableTracking );
        if ( essai.left(6) != "VIEIL_" )
        {
            plots.append( { "t-s-e", "e-s" } );
            if ( suivi.contains("R.E.") &&
                 suivi.contains("E.A.") )
                plots.append( { "t-s-EA-RE", "e-s-EA-RE" } );
            else
                if ( suivi.contains("R.E.") )
                    plots.append( { "t-s-RE", "e-s-RE" } );
                else if ( suivi.contains("E.A.") )
                    plots.append( { "t-s-EA", "e-s-EA" } );
            if ( !essai.contains("AMBI") )
                plots.append( { "t-s-T", "t-s-e-T"} );
            MecaPlots::Treat( path, {"e-mt", "t-e-mt"}, {{path + "/MECA_MOD_TANG.dat", "","",""}} );
        }
        else
        {
            plots.append( { "t-s-T", "t-s-e-T", "e-s" } );
//            MecaPlots::Treat( path, { "t-s-T", "t-s-e-T", "e-s" } , {{path + "/MECA_DEF_CONT_focused.dat", "","",""}} );
        }
        MecaPlots::Treat( path, plots, {{path + "/MECA_DEF_CONT.dat", "","",""}} );
    }
    /**********************************************/
    /**********************************************/
    static inline void GenerateRE( TestInfo* info )
    {
        if ( info == 0 )
            return;
        QString path = info->path() + "/RE";
        Files::createDir( path, "plots" );
        Files::createDir( path + "/plots", "LaTeX" );

        QStringList plots;
        plots.append( { "t-s-e-RE", "t-s-T-RE", "e-s-RE" } );

        MecaPlots::Treat( path, plots, {{path + "/RE_DEF_CONT.dat", "","",""}} );
//        if ( QFileInfo(path + "/RE_DEF_CONT_focused.dat").exists() )
//            MecaPlots::Treat( path, plots, {{path + "/RE_DEF_CONT_focused.dat", "","",""}} );
    }
    /**********************************************/
    /**********************************************/
    static inline QStringList GetDataIndexes( const QStringList& datatoplot, const QString& dataPath )
    {
        QStringList dataindexes;
        QString line;
        if ( !Files::readFirstLine( dataPath, line ) )
            return QStringList();
        QStringList cols = line.split(";");
        for ( QString data : datatoplot )
        {
            for ( int it = 0; it < cols.size(); ++it )
            {
                QString col = cols.at(it);
                if ( data == "t" && col == DataKeys::TIME )
                    dataindexes.append( QString::number(it) );
                else if ( data == "e" && col == DataKeys::DEF )
                    dataindexes.append( QString::number(it) );
                else if ( data == "s" && col == DataKeys::CTR )
                    dataindexes.append( QString::number(it) );
                else if ( data == "RE" && col == DataKeys::DRR0 )
                    dataindexes.append( QString::number(it) );
                else if ( data == "EA" && col == DataKeys::EA )
                    dataindexes.append( QString::number(it) );
                else if ( data == "T" && col == DataKeys::T )
                    dataindexes.append( QString::number(it) );
                else if ( data == "mt" && col == DataKeys::MT )
                    dataindexes.append( QString::number(it) );
            }
        }
        return dataindexes;
    }
    /**********************************************/
    /**********************************************/
    static inline QString BuildPlotLine( const QStringList& plot, const QStringList& data )
    {
        if ( plot.size() > 1 && data.size() > 4 )
            return "";
        QString temp = PlotKeys::PlotLine;
        temp.replace("%FILENAME%", data[0] );
        QStringList indexes = MecaPlots::GetDataIndexes( plot, data[0] );
        if ( indexes.size() != 2 )
            return "";
        QString color = data[3];
        if ( color == "" )
        {
            if ( plot[1] == "e" )
                color = PlotKeys::DEF_COLOR;
            else if ( plot[1] == "s" )
                color = PlotKeys::CTR_COLOR;
            else if ( plot[1] == "RE" )
                color = PlotKeys::RE_COLOR;
            else if ( plot[1] == "EA"  )
                color = PlotKeys::EA_COLOR;
            else if ( plot[1] == "T" )
                color = PlotKeys::T_COLOR;
            else if ( plot[1] == "mt" )
                color = PlotKeys::MT_COLOR;
        }
        temp.replace("%COLOR%", color );
        temp.replace("%LABEL%", data[1] );
        temp.replace("%XINDEX%", indexes[0] );
        temp.replace("%YINDEX%", indexes[1] );
        return temp;
    }
    /**********************************************/
    /**********************************************/
    static inline QString BuildLegendLine( const QStringList& plot, const QStringList& data )
    {
        if ( plot.size() > 1 && data.size() > 4 )
            return "";
        QString temp = PlotKeys::LegendLine;
        temp.replace("%LABEL%", data[1] );
        QString legend = data[2];
        if ( legend == "" )
        {
            if ( plot[1] == "e" )
                legend = PlotKeys::DEF_LEGEND[_lang];
            else if ( plot[1] == "s" )
                legend = PlotKeys::CTR_LEGEND[_lang];
            else if ( plot[1] == "RE" )
                legend = PlotKeys::RE_LEGEND[_lang];
            else if ( plot[1] == "EA"  )
                legend = PlotKeys::EA_LEGEND[_lang];
            else if ( plot[1] == "T" )
                legend = PlotKeys::T_LEGEND[_lang];
            else if ( plot[1] == "mt" )
                legend = PlotKeys::MT_LEGEND[_lang];
        }
        temp.replace("%LEGEND%", legend );
        return temp;
    }
    /**********************************************/
    /**********************************************/
    static inline void ReplaceDataLabels( const QStringList& datatoplot, QString& content )
    {
        QStringList markers = {"%%%XLABEL%%%", "%%%YLABEL%%%", "%%%YBLABEL%%%", "%%%YTLABEL%%%"};
        for ( int it = 0; it < datatoplot.size(); ++it )
        {
            QString data = datatoplot[it];
            if ( data == "t" )
            {
                content.replace( markers[it], PlotKeys::TIME[_lang] );
            }
            else if ( data == "e" )
            {
                content.replace( markers[it], PlotKeys::DEF_LABEL[_lang] );
            }
            else if ( data == "s" )
            {
                content.replace( markers[it], PlotKeys::CTR_LABEL[_lang] );
            }
            else if ( data == "RE" )
            {
                content.replace( markers[it], PlotKeys::RE_LABEL[_lang] );
            }
            else if ( data == "EA"  )
            {
                content.replace( markers[it], PlotKeys::EA_LABEL[_lang] );
            }
            else if ( data == "T" )
            {
                content.replace( markers[it], PlotKeys::T_LABEL[_lang] );
            }
            else if ( data == "mt" )
            {
                content.replace( markers[it], PlotKeys::MT_LABEL[_lang] );
            }
        }
    }
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLOTS_H
