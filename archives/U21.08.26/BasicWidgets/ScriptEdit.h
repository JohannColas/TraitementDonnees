#ifndef SCRIPTEDIT_H
#define SCRIPTEDIT_H

#include "PlainTextEdit.h"

class ScriptEdit
        : public PlainTextEdit
{
    Q_OBJECT

public:
    ScriptEdit( QWidget* parent = 0 )
        : PlainTextEdit( parent )
    {
        setPlainText( "compare e-s name SGL-FLEX addAll &SGL&FLEX!VIEIL" );
    }
};

#endif // SCRIPTEDIT_H
