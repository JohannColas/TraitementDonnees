#ifndef PLAINTEXTEDIT_H
#define PLAINTEXTEDIT_H

//#include <QTextEdit>

//class PlainTextEdit
//        : public QTextEdit
//{
//    Q_OBJECT
//public:
//    PlainTextEdit( QWidget* parent = 0 )
//        : QTextEdit( parent )
//    {

//    }


//protected slots:
//    void focusOutEvent( QFocusEvent* event )
//    {
//        emit editingFinished();
//        QTextEdit::focusOutEvent( event );
//    }

//signals:
//    void editingFinished();
//};

#include <QPlainTextEdit>

QT_BEGIN_NAMESPACE
class QPaintEvent;
class QResizeEvent;
class QSize;
class QWidget;
QT_END_NAMESPACE

class LineNumberArea;

//![codeeditordefinition]

class PlainTextEdit : public QPlainTextEdit
{
    Q_OBJECT

public:
    PlainTextEdit(QWidget *parent = nullptr);

    void lineNumberAreaPaintEvent(QPaintEvent *event);
    int lineNumberAreaWidth();

protected:
    void resizeEvent(QResizeEvent *event) override;


private slots:
    void updateLineNumberAreaWidth(int newBlockCount);
    void highlightCurrentLine();
    void updateLineNumberArea(const QRect &rect, int dy);

protected slots:
    void focusOutEvent( QFocusEvent* event ) override
    {
        emit editingFinished();
        QPlainTextEdit::focusOutEvent( event );
    }

private:
    QWidget *lineNumberArea;
    int _padding = 8;

signals:
    void editingFinished();
};

//![codeeditordefinition]
//![extraarea]

class LineNumberArea : public QWidget
{
public:
    LineNumberArea(PlainTextEdit *editor) : QWidget(editor), textEdit(editor)
    {}

    QSize sizeHint() const override
    {
        return QSize(textEdit->lineNumberAreaWidth(), 0);
    }

protected:
    void paintEvent(QPaintEvent *event) override
    {
        textEdit->lineNumberAreaPaintEvent(event);
    }

private:
    PlainTextEdit *textEdit;
};

//![extraarea]

#endif // PLAINTEXTEDIT_H
