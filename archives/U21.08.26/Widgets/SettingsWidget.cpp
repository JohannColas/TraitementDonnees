#include "SettingsWidget.h"
#include "ui_SettingsWidget.h"
/**********************************************/
/**********************************************/
/**********************************************/
SettingsWidget::SettingsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingsWidget)
{
    ui->setupUi(this);
    ui->pb_plotsTplPath->setText( Settings::getString( Keys::Plots, Keys::TplDir ) );
    ui->pb_reportsTplPath->setText( Settings::getString( Keys::Reports, Keys::TplDir ) );
    ui->pb_pythonPath->setText( Settings::getString( Keys::PythonDir ) );
    ui->cb_meca->setChecked( Settings::getBool( Keys::TreatMeca ) );
    ui->cb_plots->setChecked( Settings::getBool( Keys::TreatPlots ) );
    ui->cb_ptsCaract->setChecked( Settings::getBool( Keys::TreatPtsCaract ) );
    ui->cb_CNI->setChecked( Settings::getBool( Keys::TreatCNI ) );
    ui->cb_EA->setChecked( Settings::getBool( Keys::TreatEA ) );
    ui->cb_RE->setChecked( Settings::getBool( Keys::TreatRE ) );
    ui->cb_synthese->setChecked( Settings::getBool( Keys::TreatSynthese ) );
    ui->sp_LissageMGnbPts->setValue( Settings::getInt( Keys::Lissage, Keys::nbPoints ) );
    ui->sp_RegressionNbPts->setValue( Settings::getInt( Keys::Regression, Keys::sampling ) );
}
/**********************************************/
/**********************************************/
/**********************************************/
SettingsWidget::~SettingsWidget()
{
    delete ui;
}
/**********************************************/
/**********************************************/
/**********************************************/
void SettingsWidget::on_pb_plotsTplPath_released()
{
    QString path = Settings::getString( Keys::Plots + "/" + Keys::TplDir );
    if ( path.isEmpty() )
        path = QDir::homePath();
    QString newpath = QFileDialog::getExistingDirectory( this,
                                                         "Veuillez choisir le dossier contenant les templates LaTeX des graphes !",
                                                         path,
                                                         QFileDialog::ShowDirsOnly );
    if ( !newpath.isEmpty() ) {
        ui->pb_plotsTplPath->setText( newpath );
        Settings::save( newpath, Keys::Plots + "/" + Keys::TplDir );
    }
}
/**********************************************/
/**********************************************/
/**********************************************/
void SettingsWidget::on_pb_reportsTplPath_released()
{
    QString path = Settings::getString( Keys::Reports, Keys::TplDir );
    if ( path.isEmpty() )
        path = QDir::homePath();
    QString newpath = QFileDialog::getExistingDirectory( this,
                                                         "Veuillez choisir le dossier contenant les templates LaTeX des rapports !",
                                                         path,
                                                         QFileDialog::ShowDirsOnly );
    if ( !newpath.isEmpty() ) {
        ui->pb_reportsTplPath->setText( newpath );
        Settings::save( newpath, Keys::Reports + "/" + Keys::TplDir );
    }
}
/**********************************************/
/**********************************************/
/**********************************************/
void SettingsWidget::on_pb_pythonPath_released()
{
    QString path = Settings::getString( Keys::PythonDir);
    if ( path.isEmpty() )
        path = QDir::homePath();
    QString newpath = QFileDialog::getExistingDirectory( this,
                                                         "Veuillez choisir le dossier contenant les templates LaTeX des rapports !",
                                                         path,
                                                         QFileDialog::ShowDirsOnly );
    if ( !newpath.isEmpty() ) {
        ui->pb_pythonPath->setText( newpath );
        Settings::save( newpath, Keys::PythonDir );
    }
}
/**********************************************/
/**********************************************/
/**********************************************/
void SettingsWidget::on_sp_LissageMGnbPts_valueChanged(int nbPts)
{
    Settings::save( nbPts, Keys::Lissage, Keys::nbPoints );
}
/**********************************************/
/**********************************************/
/**********************************************/
void SettingsWidget::on_sp_RegressionNbPts_valueChanged(int nbPts)
{
    Settings::save( nbPts, Keys::Regression, Keys::sampling );
}
/**********************************************/
/**********************************************/
/**********************************************/
void SettingsWidget::on_cb_meca_toggled(bool checked)
{
    Settings::save( checked, Keys::TreatMeca);
}
/**********************************************/
/**********************************************/
/**********************************************/
void SettingsWidget::on_cb_ptsCaract_toggled(bool checked)
{
    Settings::save( checked, Keys::TreatPtsCaract );
}
/**********************************************/
/**********************************************/
/**********************************************/
void SettingsWidget::on_cb_EA_toggled(bool checked)
{
    Settings::save( checked, Keys::TreatEA );
}
/**********************************************/
/**********************************************/
/**********************************************/
void SettingsWidget::on_cb_RE_toggled(bool checked)
{
    Settings::save( checked, Keys::TreatRE );
}
/**********************************************/
/**********************************************/
/**********************************************/
void SettingsWidget::on_cb_CNI_toggled(bool checked)
{
    Settings::save( checked, Keys::TreatCNI );
}
/**********************************************/
/**********************************************/
/**********************************************/
void SettingsWidget::on_cb_plots_toggled(bool checked)
{
    Settings::save( checked, Keys::TreatPlots );
}
/**********************************************/
/**********************************************/
/**********************************************/
void SettingsWidget::on_cb_synthese_toggled(bool checked)
{
    Settings::save( checked, Keys::TreatSynthese );
}
/**********************************************/
/**********************************************/
/**********************************************/
void SettingsWidget::on_pb_traiterMeca_released()
{
    TreatmentInfo _info = TreatmentInfo();
    _info.onlyMeca();
    emit treat( _info );
}
/**********************************************/
/**********************************************/
/**********************************************/
void SettingsWidget::on_pb_traiterPtsCaract_released()
{
    TreatmentInfo _info = TreatmentInfo();
    _info.onlyPtsCaract();
    _info.isActive = false;
    emit treat( _info );
}
/**********************************************/
/**********************************************/
/**********************************************/
void SettingsWidget::on_pb_traiterEA_released()
{
    TreatmentInfo _info = TreatmentInfo();
    _info.onlyEA();
    emit treat( _info );
}
/**********************************************/
/**********************************************/
/**********************************************/
void SettingsWidget::on_pb_traiterRE_released()
{
    TreatmentInfo _info = TreatmentInfo();
    _info.onlyRE();
    emit treat( _info );
}
/**********************************************/
/**********************************************/
/**********************************************/
void SettingsWidget::on_pb_traiterCNI_released()
{
    TreatmentInfo _info = TreatmentInfo();
    _info.onlyCNI();
    emit treat( _info );
}
/**********************************************/
/**********************************************/
/**********************************************/
void SettingsWidget::on_pb_traiterPlots_released()
{
    TreatmentInfo _info = TreatmentInfo();
    _info.onlyPlots();
    emit treat( _info );
}
/**********************************************/
/**********************************************/
/**********************************************/
void SettingsWidget::on_pb_traiterSynthese_released()
{
    TreatmentInfo _info = TreatmentInfo();
    _info.onlyReport();
    emit treat( _info );
}
/**********************************************/
/**********************************************/
/**********************************************/
