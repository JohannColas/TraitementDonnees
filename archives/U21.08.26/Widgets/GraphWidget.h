#ifndef GRAPHWIDGET_H
#define GRAPHWIDGET_H
/**********************************************/
#include <QWidget>
#include <QShortcut>
#include "Commons/Files.h"
#include "Commons/LaTeX.h"
#include "Commons/TestInfo.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
namespace Ui {
    class GraphWidget;
}
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class GraphWidget : public QWidget
{
    Q_OBJECT
public:
    explicit GraphWidget(QWidget *parent = nullptr);
    ~GraphWidget();
    void setPath( const QString& path )
    {
        _path = path;
        update();
    }
    void update();
    void checkLaTeX();
    void showLaTeX();
    /**********************************************/
    /**********************************************/
private slots:
    void on_plotSelector_currentTextChanged(const QString &arg1);
    void nextPlot();
    void previousPlot();
    void updatePlotSelector();
    void on_pb_modPlot_released();
    void on_pb_compile_released();
    /**********************************************/
    /**********************************************/
    void on_pb_supprimer_released();

private:
    Ui::GraphWidget *ui;
    QString _path = QString();
    QString _plotpath = QString();
    QShortcut* sc_goUp;
    QShortcut* sc_goDown;
    QShortcut* sc_goLeft;
    QShortcut* sc_goRight;
    QShortcut* sc_zoomIn;
    QShortcut* sc_zoomOut;
    QShortcut* sc_normal;
    QShortcut* sc_fit;
    QShortcut* sc_showTeX;
    QShortcut* sc_compile;
    QShortcut* sc_compile2;
    QShortcut* sc_nextPlot;
    QShortcut* sc_previousPlot;
    /**********************************************/
    /**********************************************/
signals:
    void sendPlotPath( const QString& path );
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPHWIDGET_H
