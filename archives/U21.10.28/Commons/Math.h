#ifndef MATH_H
#define MATH_H
/**********************************************/
#include <QVector>
#include <cmath>
#include <math.h>
#include "Settings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Math
{
public:
	static inline void MoyenneGlissee( QVector<QVector<double>>& _data )
	{
		int size = _data.size();
		if ( size == 0 )
			return;

		int nbPts = Settings::getInt( Keys::Lissage, Keys::nbPoints );
		int hNbPts = 0.5*nbPts;
		QVector<QVector<double>> temp = _data;
		for ( int it = 0; it < size; ++it )
		{
			int lmin = it, lmax = it;
			if ( it >= hNbPts && it <= size - 1 - hNbPts )
			{
				lmin = it - hNbPts;
				lmax = it + hNbPts;
			}
			temp[it][0] = _data[it][0];
			for ( int jt = 1; jt < _data[it].size(); ++jt )
			{
				//				double ctr_min=_data[lmin][2], ctr_max=_data[lmin][2];
				double somme = 0;
				//				int count = 0;
				for ( int lt = lmin; lt <= lmax; ++lt )
				{
					//					double ctr = _data[lt][2];
					//					if ( ctr_min > ctr )
					//						ctr_min = ctr;
					//					if ( ctr_max < ctr )
					//						ctr_max = ctr;
					somme += _data[lt][jt];
					//					++count;
				}
				//				if ( ((ctr_max-ctr_min)/ctr_max) < 0.5 )
				temp[it][jt] = somme/(lmax-lmin+1);
			}
		}
		_data = temp;
	}
	/**********************************************/
	/**********************************************/
	static inline QVector<QVector<double>> RegressionLineaire( const QVector<QVector<double>>& _data, int xcol, int ycol )
	{
		if ( _data.size() == 0 )
			return {};

		int nbPts = Settings::getInt( Keys::Regression, Keys::sampling );
		int hNbPts = 0.5*nbPts;
		int size = _data.size();
		QVector<QVector<double>> temp;
		for ( int it = 0; it < size; ++it )
		{
			double alpha = 0, beta = 0;
			double xi = 0, yi = 0, wi = 1;
			double A = 0, B = 0, C = 0, D = 0, E = 0;
			int jmin = 0, jmax = 0;
			// Pas de regression aux extremums
			jmin = it, jmax = it;
			//            if ( it < hNbPts )
			//			{
			//				jmin = 0;
			//				jmax = it + hNbPts;
			//            }
			//            else if ( it > size - hNbPts - 1 )
			//            {
			//                jmin = it - hNbPts;
			//				jmax = size;
			//            }
			//            else
			if ( it >= hNbPts && it <= size - 1 - hNbPts )
			{
				jmin = it - hNbPts;
				jmax = it + hNbPts;
				for ( int jt = jmin; jt <= jmax ; ++jt )
				{
					xi = _data[jt][xcol]/100;
					yi = _data[jt][ycol]/1000;
					A += wi*xi*xi;
					B += wi;
					C += wi*xi;
					D += wi*xi*yi;
					E += wi*yi;
				}
			}
			alpha = (B*D - C*E)/(A*B-C*C);
			beta = (D - A*alpha)/C;
			temp.append( {_data[it][0], _data[it][1], _data[it][2], alpha, beta} );
		}
		return temp;
	}
	/**********************************************/
	/**********************************************/
	static inline double RoundToUpperMultiple( double value, double multiple )
	{
		int it = 0;
		double temp = abs(value);
		double mulp = abs(multiple);
		if ( temp == INFINITY ) return 1;
		while ( temp > 0 )
		{
			temp -= mulp;
			++it;
		}
		return (value > 0 ? it*mulp : -it*mulp);
	}
	/**********************************************/
	/**********************************************/
	static inline double max( QVector<double> lst )
	{
		double temp = lst.first();
		for ( double val : lst )
			if ( val > temp )
				temp = val;
		return temp;
	}
	/**********************************************/
	/**********************************************/
	static inline QVector<double> max( const QVector<QVector<double>> lst )
	{
		QVector<double> maxes = lst.first();
		for ( QVector<double> val : lst )
			for ( int it = 0; it < val.size(); ++it )
			{
				if ( isnan(val[it]) || isinf(val[it]) )
					continue;
				else
					maxes[it] = val[it];
			}
		for ( QVector<double> val : lst )
			for ( int it = 0; it < val.size(); ++it )
			{
				if ( isnan(val[it]) || isinf(val[it]) )
					continue;
				if ( val[it] > maxes[it] )
					maxes[it] = val[it];
			}
		return maxes;
	}
	/**********************************************/
	/**********************************************/
	static inline double max( const QVector<QVector<double>> lst, int index )
	{
		if ( index+1 > lst.first().size() )
			return -1;
		double max = lst.first().at(index);
		for ( QVector<double> val : lst )
			for ( int it = 0; it < val.size(); ++it )
			{
				if ( isnan(val[it]) || isinf(val[it]) )
					continue;
				else
					max = val[it];
			}
		for ( QVector<double> val : lst )
		{
			if ( isnan(val[index]) || isinf(val[index]) )
				continue;
			if ( val[index] > max )
				max = val[index];
		}
		return max;
	}
	/**********************************************/
	/**********************************************/
	static inline QVector<double> min( const QVector<QVector<double>> lst )
	{
		QVector<double> mines = lst.first();
		for ( QVector<double> val : lst )
			for ( int it = 0; it < val.size(); ++it )
			{
				if ( isnan(val[it]) || isinf(val[it]) || val[it] == -100 )
					continue;
				else
					mines[it] = val[it];
			}
		for ( QVector<double> val : lst )
			for ( int it = 0; it < val.size(); ++it )
			{
				if ( isnan(val[it]) || isinf(val[it]) || val[it] == -100 )
					continue;
				if ( val[it] < mines[it] )
					mines[it] = val[it];
			}
		return mines;
	}
	/**********************************************/
	/**********************************************/
	static inline double min( const QVector<QVector<double>> lst, int index )
	{
		if ( index+1 > lst.first().size() )
			return -1;
		double min = lst.first().at(index);
		for ( QVector<double> val : lst )
			for ( int it = 0; it < val.size(); ++it )
			{
				if ( isnan(val[it]) || isinf(val[it]) )
					continue;
				else
					min = val[it];
			}
		for ( QVector<double> val : lst )
		{
			if ( isnan(val[index]) )
				continue;
			if ( val[index] < min )
				min = val[index];
		}
		return min;
	}
	/**********************************************/
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // MATH_H
