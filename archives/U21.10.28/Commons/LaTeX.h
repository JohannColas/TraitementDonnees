#ifndef LATEX_H
#define LATEX_H
/**********************************************/
#include <QThread>
#include "Files.h"
#include "Settings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LaTeX
{
	static inline QString dirname = "LaTeX";
public:
	static inline QString DirName() { return dirname; }
	static inline QString PlotTemplate() { return "\\input{/home/colas/Documents/LaTeX/plots/a-plot.tpl.tex}"; }
	static inline void Compile( const QString& filePath, bool compileTwice = true )
	{
		QString cmd = "lualatex";
		QStringList args;
		args << "-interaction=nonstopmode"
		     << "-shell-escape"
		     << filePath;
		QFileInfo info(filePath);
		QString compilePath = info.absolutePath();
		System::exec( cmd, args, compilePath );
		if ( compileTwice )
			System::exec( cmd, args, compilePath );

		QDir dir(compilePath);
		dir.cd("..");
		QString movePath = dir.absolutePath();
		QString fileName = info.baseName();
		Files::move( compilePath + "/" + fileName + ".pdf",
		             movePath + "/" + fileName + ".pdf", true); // true is to force move
		Files::move( compilePath + "/" + fileName + ".png",
		             movePath + "/" + fileName + ".png", true); // true is to force move
		//qDebug() << "compilePath : " << compilePath;
		//qDebug() << "movePath : " << movePath;
		//qDebug() << "fileName : " << fileName;
		if ( System::os() == System::Linux )
		{

		}
		else if ( System::os() == System::Linux )
		{

		}
		else
			return;
	}
	static inline void Compile( const QString& path, const QString& fileName, const QString& tmpPath = "", bool compileTwice = true, bool movePDF = true )
	{
		QString cmd = "lualatex";
		QStringList args;
		args << "-interaction=nonstopmode"
		     << "-shell-escape"
		     << "\"" + fileName + "\".tex";
		// Execute compile TeX File
		// Need two compile twice to correct legend labeling !!
		QString compilePath = path;
		if ( tmpPath != "" )
			compilePath = tmpPath;
		System::exec( cmd, args, compilePath + "/LaTeX" );
		if ( compileTwice )
			System::exec( cmd, args, compilePath + "/LaTeX" );
		//
		if ( movePDF )
		{
			Files::move( compilePath + "/LaTeX/" + fileName + ".pdf",
			             path + "/" + fileName + ".pdf", true); // true is to force move
			Files::move( compilePath + "/LaTeX/" + fileName + ".png",
			             path + "/" + fileName + ".png", true); // true is to force move
		}
		if ( System::os() == System::Linux )
		{

		}
		else if ( System::os() == System::Linux )
		{

		}
		else
			return;
	}
    static inline void Compile2( const QString& path, const QString& fileName, const QString& tmpPath = "" )
    {
        QString cmd = "lualatex";
        QStringList args;
		args << "-interaction=nonstopmode"
		     << "-shell-escape"
             << "\"" + fileName + ".tex\"";
        // Execute compile TeX File
        // Need two compile twice to correct legend labeling !!
        QString compilePath = path;
        if ( tmpPath != "" )
            compilePath = tmpPath;
        if ( !QDir(compilePath + "/LaTeX").exists() )
        {
            QDir dir(compilePath);
            dir.mkdir("LaTeX");
        }
        Files::move( compilePath + "/" + fileName + ".tex",
                     path + "/LaTeX/" + fileName + ".tex", true);
        System::exec( cmd, args, compilePath + "/LaTeX" );
        System::exec( cmd, args, compilePath + "/LaTeX" );
        //
        Files::move( compilePath + "/LaTeX/" + fileName + ".tex",
                     path + "/" + fileName + ".tex", true); // true is to force move
        Files::move( compilePath + "/LaTeX/" + fileName + ".pdf",
                     path + "/" + fileName + ".pdf", true); // true is to force move
    }
    /**********************************************/
    /**********************************************/
	static inline void convertPDFtoImage( const QString& path, const QString& fileName, const QString& imageExt = "png", const QString& density = "600" )
	{
        QString cmd = "convert";
        QStringList args;
        args << "-density"
		     << density
             << fileName + ".pdf"
             << fileName + "." + imageExt;
        // Execute conversion PDF to PNG
        System::exec( cmd, args, path );
    }
    /**********************************************/
    /**********************************************/
    static inline void CompileAndConvertToPNG( const QString& path, const QString& fileName, const QString& imageExt = "png"  )
    {
        LaTeX::Compile( path, fileName );
        LaTeX::convertPDFtoImage( path, fileName, imageExt );
    }
    /**********************************************/
    /**********************************************/
    static inline bool ReadPlotTemplate( const QStringList& datatoplot, QString& contents )
    {
        QString plotTplDir = Settings::getString( Keys::Plots, Keys::TplDir );
        QString plotTpl = "";
        if ( datatoplot.size() == 2 )
            plotTpl = "OneYAxis";
        else if ( datatoplot.size() == 3 )
            plotTpl = "TwoYAxes";
		else
            plotTpl = "ThreeYAxes";

        // Get contents in the TeX Template File
        QString origPath = plotTplDir + "/" + plotTpl + ".tex";
        return Files::read( origPath, contents );
    }
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LaTeXThread
        : public QThread
{
    Q_OBJECT
    QString _path;
    QString _filename;
    bool _plotCompile = true;

    void run() override
    {
        if ( _plotCompile )
            LaTeX::CompileAndConvertToPNG( _path, _filename );
        else
            LaTeX::Compile2( _path, _filename );
        emit finished();
    }

public:
    LaTeXThread( const QString& path, const QString& filename, bool plotCompile = true )
    {
        _path = path;
        _filename = filename;
        _plotCompile = plotCompile;
    }

signals:
    void finished();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LATEX_H
