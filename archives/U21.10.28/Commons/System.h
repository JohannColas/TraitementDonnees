#ifndef SYSTEM_H
#define SYSTEM_H
/**********************************************/
#include <QDir>
#include <QProcess>
#include <QDebug>
#include <stdlib.h>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class System
{
public:
    enum OS {
        Windows,
        Linux
    };
    static inline bool move( const QString& oldpath, const QString& newpath, bool forced = false )
    {
        if ( forced )
            if ( QFile::exists( newpath ) )
                QFile::remove( newpath );
        if ( !QFile::copy( oldpath, newpath ) )
            return false;
       QFile::remove( oldpath );
       return true;
    }
    /**********************************************/
    /**********************************************/
    static inline QDir createDir( const QString& dirpath, const QString& dirname )
    {
        QDir dir( dirpath );
        if ( !dir.exists( dirname ) )
            dir.mkdir( dirname );
        dir.cd( dirname );
        return dir;
    }
    /**********************************************/
    /**********************************************/
    static inline bool remove( const QString& path)
    {
        if ( QFile::exists( path ) )
            return QFile::remove( path );
       return false;
    }
    /**********************************************/
    /**********************************************/
    static void exec( const QString& cmd, const QStringList& args = {}, const QString& workingPath = "", int killAt = -1 )
    {
		    QProcess process;
			if ( workingPath != "" )
				process.setWorkingDirectory( workingPath );
			else
			{
				QDir( QDir::currentPath() ).mkdir( "PDFLaTeX_Compilation" );
				process.setWorkingDirectory( QDir::currentPath() + "/PDFLaTeX_Compilation" );
			}
			process.start( cmd, args, QIODevice::ReadWrite ); //Starts execution of command
			process.waitForFinished( killAt );
//		    QString ccmd = "cd \"" + workingPath + "\" && "+cmd;
//			for ( QString arg : args )
//				ccmd += " "+arg;
//			system( ccmd.toStdString().c_str() );

//			qDebug() << "Failed:" << process.readAllStandardError();
//			qDebug() << "Output:" << process.readAll();
//			qDebug() << "";
//			qDebug() << "";
    }
    /**********************************************/
    /**********************************************/
    static QString TimeToString( int timems )
    {
        QString str;
        int nbs = timems/1000;
        int hh = nbs/3600;
        int mm = (nbs - 3600*hh)/60;
        int ss = nbs - 3600*hh - 60*mm;
        if ( hh != 0 )
            str += QString("%1h").arg(hh);
        if ( !str.isEmpty() )
            str += " ";
        if ( mm != 0 )
            str += QString("%1m").arg(mm);
        if ( !str.isEmpty() )
            str += " ";
        str += QString("%1s").arg(ss);
        return str;
    }
    /**********************************************/
    /**********************************************/
    static inline OS os()
    {
#if defined(Q_OS_WIN)
        return OS::Windows;
#elif defined(Q_OS_LINUX)
        return OS::Linux;
#elif defined(Q_OS_UNIX)
        return OS::Linux;
#endif
    }
    /**********************************************/
    /**********************************************/
    static inline void print( const QString& path )
    {
        QString cmd = "convert";
        QStringList args;
        args << "-density"
             << "600"
             << path + ".pdf"
             << path + ".png";
        // Execute conversion PDF to PNG
        System::exec( cmd, args, path );
    }
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SYSTEM_H
