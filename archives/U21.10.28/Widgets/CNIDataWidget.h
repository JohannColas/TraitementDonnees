#ifndef CNIDATAWIDGET_H
#define CNIDATAWIDGET_H
/**********************************************/
#include <QWidget>
#include "Commons/TestInfo.h"
#include "Treatments/CNI.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
namespace Ui {
    class CNIDataWidget;
}
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class CNIDataWidget : public QWidget
{
    Q_OBJECT
public:
    explicit CNIDataWidget(QWidget *parent = nullptr);
    ~CNIDataWidget();
    void linkToTestInfo( TestInfo* info );
    /**********************************************/
    /**********************************************/
private slots:
    void on_traite_CNI_released();
    void on_pb_CNIVideo_released();
	void on_pb_stopprocess_released();
    /**********************************************/
	/**********************************************/

private:
    Ui::CNIDataWidget *ui;
    TestInfo* _testinfo = 0;
    /**********************************************/
    /**********************************************/
signals:
    void stopProcess();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // CNIDATAWIDGET_H
