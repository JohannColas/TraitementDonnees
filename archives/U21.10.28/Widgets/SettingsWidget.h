#ifndef SETTINGSWIDGET_H
#define SETTINGSWIDGET_H
/**********************************************/
#include <QWidget>
#include "Treatments/Treatment.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
namespace Ui {
    class SettingsWidget;
}
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SettingsWidget : public QWidget
{
    Q_OBJECT
public:
    explicit SettingsWidget(QWidget *parent = nullptr);
    ~SettingsWidget();
    /**********************************************/
    /**********************************************/
private slots:
    void on_pb_plotsTplPath_released();
    void on_pb_reportsTplPath_released();
    void on_pb_pythonPath_released();
    void on_sp_LissageMGnbPts_valueChanged( int arg1 );
    void on_sp_RegressionNbPts_valueChanged( int arg1 );
    void on_cb_meca_toggled( bool checked );
    void on_cb_ptsCaract_toggled( bool checked );
    void on_cb_EA_toggled( bool checked );
    void on_cb_RE_toggled( bool checked );
    void on_cb_CNI_toggled( bool checked );
    void on_cb_plots_toggled( bool checked );
    void on_cb_synthese_toggled( bool checked );
    void on_pb_traiterMeca_released();
    void on_pb_traiterPtsCaract_released();
    void on_pb_traiterEA_released();
    void on_pb_traiterRE_released();
    void on_pb_traiterCNI_released();
    void on_pb_traiterPlots_released();
    void on_pb_traiterSynthese_released();
    /**********************************************/
    /**********************************************/
private:
    Ui::SettingsWidget *ui;
    /**********************************************/
    /**********************************************/
signals:
    void treat( const TreatmentInfo& trinfo );
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SETTINGSWIDGET_H
