#include "EADataWidget.h"
#include "ui_EADataWidget.h"
/**********************************************/
/**********************************************/
/**********************************************/
EADataWidget::EADataWidget( QWidget* parent ) :
    QWidget(parent),
    ui(new Ui::EADataWidget)
{
    ui->setupUi(this);
}
/**********************************************/
/**********************************************/
/**********************************************/
EADataWidget::~EADataWidget()
{
    delete ui;
}
/**********************************************/
/**********************************************/
/**********************************************/
void EADataWidget::linkToTestInfo( TestInfo* info )
{
    _testinfo = info;
}
/**********************************************/
/**********************************************/
/**********************************************/
void EADataWidget::update()
{
    if ( QFileInfo::exists(_testinfo->path()+"/EA/EA.dat") )
    {
        EA_filterfile->setPath( _testinfo->path() );
		int ampli = EA_filterfile->toInt(EA_KEYS::SeuilAmpli);
        if ( ampli == -1 )
        {
            ampli = 40;
            EA_filterfile->save(EA_KEYS::SeuilAmpli, ampli);
        }
        ui->sp_seuilAmp->setValue( ampli );
        int cps = EA_filterfile->toInt(EA_KEYS::SeuilCps);
        if ( cps == -1 )
        {
            cps = 2;
            EA_filterfile->save(EA_KEYS::SeuilCps, cps);
        }
        ui->sp_seuilCps->setValue( cps );
        int durat = EA_filterfile->toInt(EA_KEYS::SeuilDur);
        if ( durat == -1 )
        {
            durat = 2000;
            EA_filterfile->save(EA_KEYS::SeuilDur, durat);
        }
        ui->sp_seuilDur->setValue( durat );
        int ener = EA_filterfile->toInt(EA_KEYS::SeuilEner);
        if ( ener == -1 )
        {
            ener = 20;
            EA_filterfile->save(EA_KEYS::SeuilEner, ener);
        }
        ui->sp_seuilEner->setValue( ener);
        int rise = EA_filterfile->toInt(EA_KEYS::SeuilRise);
        if ( rise == -1 )
        {
            rise = 100;
            EA_filterfile->save(EA_KEYS::SeuilRise, rise);
        }
        ui->sp_seuilRise->setValue( rise );
        int time = EA_filterfile->toInt(EA_KEYS::SeuilTime);
        if ( time == -1 )
        {
            time = 0;
            EA_filterfile->save(EA_KEYS::SeuilTime, time);
        }
        ui->sp_seuilTime->setValue( time );
    }
    else
        EA_filterfile->setPath();
}
/**********************************************/
/**********************************************/
/**********************************************/
void EADataWidget::on_traite_EA_released()
{
    EA ea( _testinfo->path() );
    ea.treat();
}
/**********************************************/
/**********************************************/
/**********************************************/
void EADataWidget::on_sp_seuilAmp_valueChanged( int value )
{
    EA_filterfile->save( EA_KEYS::SeuilAmpli, value );
}
/**********************************************/
/**********************************************/
/**********************************************/
void EADataWidget::on_sp_seuilTime_valueChanged( int value )
{
    EA_filterfile->save( EA_KEYS::SeuilTime, value );
}
/**********************************************/
/**********************************************/
/**********************************************/
void EADataWidget::on_sp_seuilRise_valueChanged( int value )
{
    EA_filterfile->save( EA_KEYS::SeuilRise, value );
}
/**********************************************/
/**********************************************/
/**********************************************/
void EADataWidget::on_sp_seuilDur_valueChanged( int value )
{
    EA_filterfile->save( EA_KEYS::SeuilDur, value );
}
/**********************************************/
/**********************************************/
/**********************************************/
void EADataWidget::on_sp_seuilCps_valueChanged( int value )
{
    EA_filterfile->save( EA_KEYS::SeuilCps, value );
}
/**********************************************/
/**********************************************/
/**********************************************/
void EADataWidget::on_sp_seuilEner_valueChanged( int value )
{
    EA_filterfile->save( EA_KEYS::SeuilEner, value );
}
/**********************************************/
/**********************************************/
/**********************************************/
