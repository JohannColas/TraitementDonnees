#ifndef REPORTSWIDGET_H
#define REPORTSWIDGET_H
/**********************************************/
#include <QShortcut>
#include <QWidget>
/**********************************************/
/**********************************************/
/**********************************************/
namespace Ui {
class ReportsWidget;
}
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ReportsWidget : public QWidget
{
    Q_OBJECT
    /**********************************************/
    /**********************************************/
public:
    explicit ReportsWidget( QWidget* parent = nullptr );
    ~ReportsWidget();
    void setPath( const QString& path )
    {
        _path = path;
        update();
    }
    void update();
    void updatePlotSelector();
    void showFile( const QString& path );
    /**********************************************/
    /**********************************************/
private slots:
    void on_sel_file_currentIndexChanged( int index );
    void on_pb_new_released();
    void on_pb_externe_released();
	void on_pb_savecompile_released();
    void nextReport();
    void previousReport();
    void deleteFiles();
    /**********************************************/
    /**********************************************/

private:
    Ui::ReportsWidget *ui;
    QString _path = QString();
    QShortcut* sc_compile;
    QShortcut* sc_compile2;
    QShortcut* sc_nextReport;
    QShortcut* sc_previousReport;
    QShortcut* sc_delete;
    /**********************************************/
    /**********************************************/
signals:
    void sendPath( const QString& path );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // REPORTSWIDGET_H
