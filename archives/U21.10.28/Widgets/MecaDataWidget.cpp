#include "MecaDataWidget.h"
#include "ui_MecaDataWidget.h"
#include "Treatments/PointsCaract.h"
#include <QRegularExpression>
/**********************************************/
/**********************************************/
/**********************************************/
MecaDataWidget::MecaDataWidget( QWidget *parent ) :
    QWidget( parent ),
    ui( new Ui::MecaDataWidget )
{
    ui->setupUi(this);
    QFont font("Monospace");
    ui->te_data->setFont(font);
    ui->tbl_ptsCaract->hide();
}
/**********************************************/
/**********************************************/
/**********************************************/
MecaDataWidget::~MecaDataWidget()
{
    delete ui;
}
/**********************************************/
/**********************************************/
/**********************************************/
void MecaDataWidget::linkToTestInfo( TestInfo* info )
{
    _testinfo = info;
}
/**********************************************/
/**********************************************/
/**********************************************/
void MecaDataWidget::update()
{
    QString path = _testinfo->path();
	QStringList files = Files::ListDataFiles( path );
    ui->cb_availabledata->clear();
    ui->cb_availabledata->addItems(files);
    ui->cb_availabledata->setCurrentIndex( 0 );

    QString contents;
    if ( Files::read( path + "/meca/PROP.tex", contents ) )
    {
        QStringList lst = contents.split("\n");
        lst = lst.at(2).split(QRegularExpression("[ \t]"), Qt::SkipEmptyParts);
        if ( lst.size() > 8 )
        {
            ui->tbl_ptsCaract->show();
            setMecaTab( {lst.at(0), lst.at(2), lst.at(4), lst.at(6), lst.at(8)} );
        }
        else
        {
            ui->tbl_ptsCaract->hide();
            setMecaTab( {"", "", "", "", ""} );
        }

    }
    else
    {
        ui->tbl_ptsCaract->hide();
        setMecaTab( {"", "", "", "", ""} );
    }
}
/**********************************************/
/**********************************************/
/**********************************************/
void MecaDataWidget::setMecaTab( const QStringList &lst )
{
    ui->tbl_ptsCaract->item( 1, 0 )->setText(lst.at(0));
    ui->tbl_ptsCaract->item( 1, 1 )->setText(lst.at(1));
    ui->tbl_ptsCaract->item( 1, 2 )->setText(lst.at(2));
    ui->tbl_ptsCaract->item( 1, 3 )->setText(lst.at(3));
    ui->tbl_ptsCaract->item( 1, 4 )->setText(lst.at(4));
}
/**********************************************/
/**********************************************/
/**********************************************/
void MecaDataWidget::on_pb_ptsCaract_released()
{
    PointsCaract ptsCaract;
	ptsCaract.run( _testinfo->path()+"/"+Keys::fn_MecaTreatedData+".dat" );
}
/**********************************************/
/**********************************************/
/**********************************************/
void MecaDataWidget::on_cb_availabledata_currentTextChanged( const QString &file )
{
    QString path = _testinfo->path() + "/" + file;
	QStringConverter::Encoding codec = QStringConverter::Utf8;
	if ( path.endsWith("MECA.dat") || path.endsWith("MECA.csv") )
		codec = QStringConverter::Latin1;
    QString contents;
	if ( !Files::read( path, contents, codec ) )
        contents =  "Le fichier n'a pas pu être lu !!";
    ui->te_data->setPlainText( contents );
}
/**********************************************/
/**********************************************/
/**********************************************/
