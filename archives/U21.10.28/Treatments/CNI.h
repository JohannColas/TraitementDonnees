#ifndef CNI_H
#define CNI_H
/**********************************************/
#include <QObject>
#include <QFile>
#include <QProcess>
#include <QDebug>
#include <QElapsedTimer>
#include <QThread>
#include "Commons/Settings.h"
#include "Commons/TestInfo.h"
#include "Commons/Math.h"
#include "Commons/LaTeX.h"

#include <bits/stdc++.h>
using namespace std;

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class CNIThread
        : public QThread
{
	Q_OBJECT
	TestInfo* _testinfo = 0;
	bool _treatImages = true;
	bool _makeVideo = true;
	bool _kill = false;
	/**********************************************/
	/**********************************************/
	void run() override
	{
		QElapsedTimer timer;
		timer.start();

		if ( !_testinfo )
			return;
		if ( !_testinfo->isValid() )
			return;


		QString essai_path = _testinfo->path();
		if ( QDir(essai_path).exists() )
		{
			QString cni_path = essai_path + "/CNI";
			QString im_orig_path = cni_path + "/images_originales";
			QStringList im_orig_list = QDir(im_orig_path).entryList( {"*.tif"}, QDir::NoDotAndDotDot | QDir::Files );
			QVector<int> im_orig_indexes;
			for ( QString str : im_orig_list )
				im_orig_indexes.append( str.remove("image").remove(".tif").toInt() );
			sort( im_orig_indexes.begin(), im_orig_indexes.end() );

			QString im_treat_path = cni_path + "/images_traitees";
			QStringList im_treat_list = QDir(im_treat_path).entryList( {"*.png"}, QDir::NoDotAndDotDot | QDir::Files );
			QVector<int> im_treat_indexes;
			for ( QString str : im_orig_list )
				im_treat_indexes.append( str.remove("image_").remove(".png").toInt() );
			sort( im_treat_indexes.begin(), im_treat_indexes.end() );
			QString ref_im_path = im_treat_path + "/" + im_treat_list.first();

			QString data_treat_path = cni_path + "/donnees_traitees";
			QStringList data_treat_list = QDir(data_treat_path).entryList( {"*.dat"}, QDir::NoDotAndDotDot | QDir::Files );
			QDir frames_dir = Files::createDir( cni_path, "frames" );
			QDir frames_latex_dir = Files::createDir( cni_path+"/frames", "LaTeX" );

			QDir subframes_dir = Files::createDir( frames_dir.absolutePath(), "subframes" );
			QDir subframes_latex_dir = Files::createDir( frames_dir.absolutePath()+"/subframes", "LaTeX" );

			QString argFormat = "%03d";
			if ( im_orig_indexes.size() > 1000 )
				argFormat = "%04d";

			if ( _treatImages )
			{
				double e11_min = 0;
				double e11_max = 0;
				double e12_min = 0;
				double e12_max = 0;
				double e22_min = 0;
				double e22_max = 0;
				for ( QString path : data_treat_list )
				{
					QVector<QVector<double>> data;
					QString tmp_path = data_treat_path + "/" + path;
					if ( !Files::readData( tmp_path, data ) ) continue;
					if ( data.size() < 1 ) continue;
					if ( data.first().size() < 7 ) continue;
					QVector<double> maxes = Math::max( data );
					QVector<double> mines = Math::min( data );

					if ( mines[4] < e11_min ) e11_min = mines[4];
					if ( maxes[4] > e11_max ) e11_max = maxes[4];

					if ( mines[5] < e12_min ) e12_min = mines[5];
					if ( maxes[5] > e12_max ) e12_max = maxes[5];

					if ( mines[6] < e22_min ) e22_min = mines[6];
					if ( maxes[6] > e22_max ) e22_max = maxes[6];
				}
				e11_min = Math::RoundToUpperMultiple( e11_min, 0.005);
				e11_max = Math::RoundToUpperMultiple( e11_max, 0.005);
				e12_min = Math::RoundToUpperMultiple( e12_min, 0.005);
				e12_max = Math::RoundToUpperMultiple( e12_max, 0.005);
				e22_min = Math::RoundToUpperMultiple( e22_min, 0.005);
				e22_max = Math::RoundToUpperMultiple( e22_max, 0.005);


				// Get Meca Data
				QVector<QVector<double>> meca_data;
				if ( !Files::readData( _testinfo->path() + "/" + Keys::fn_MecaTreatedData + ".dat", meca_data) ) return;
				int it_meca = 1;


				QVector<QVector<double>> forces;
				QString im_force_path = cni_path + "/imageforces.txt";
				if ( !Files::readData( im_force_path, forces ) ) return;

				QString refimage_path = im_treat_path+"/"+im_treat_list.first();
				QString mecadata_path = _testinfo->path() + "/" + Keys::fn_MecaTreatedData + ".dat";

				emit sendTotImage( im_orig_indexes.size() );
				int it_frame = 0;
				for ( int index : im_orig_indexes )
				{
					if ( _kill ) break;
					emit sendCurrentImage( it_frame );
					QVector<double> e_s = {0, 0};
					double b = _testinfo->getDouble( TestInfoKeys::Width );
					double e = _testinfo->getDouble( TestInfoKeys::Thickness );
					double stress = forces[index][0]/(b*e);
					if ( e == -1 )
						stress *= -1;
					if ( stress > 0.2 )
						while ( it_meca < meca_data.size() )
						{
							double stress1 = meca_data[it_meca-1][2];
							double stress2 = meca_data[it_meca][2];
							double def1 = meca_data[it_meca-1][1];
							double def2 = meca_data[it_meca][1];
							if ( stress1 <= stress && stress <= stress2 )
							{
								double def = (def2-def1)*(stress-stress1)/(stress2-stress1)+def1;
								e_s = {def, stress};
								break;
							}
							++it_meca;
						}

					QString frame = "frame_" + QString().asprintf( argFormat.toStdString().c_str(),  it_frame );
					QString curimage_path = im_treat_path+"/"+im_treat_list.at(it_frame);
					QString curdata_path = data_treat_path+"/"+data_treat_list.at(it_frame);

					QString defpath = buildDeformedImage( subframes_dir.absolutePath(), frame,  curimage_path );
					QString plotpath = buildPlot( subframes_dir.absolutePath(), frame, mecadata_path, e_s.at(0), e_s.at(1) );
					QString def11path = buildDef( subframes_dir.absolutePath(), frame, "11", refimage_path, curdata_path, e11_min, e11_max );
					QString def12path = buildDef( subframes_dir.absolutePath(), frame, "12", refimage_path, curdata_path, e12_min, e12_max );
					QString def22path = buildDef( subframes_dir.absolutePath(), frame, "22", refimage_path, curdata_path, e22_min, e22_max );
					buildFrame( frames_dir.absolutePath(), frame, defpath, plotpath, def11path, def12path, def22path );

					++it_frame;
				}
			}
			if ( _makeVideo )
			{
				emit makingVideo();
				QString cmd = "ffmpeg";
				QStringList args;
				args << "-y" // -y overwrite output file
				        /*<< "-report"*/ << "-r" << "2" ;
				args << "-hide_banner";
				args << "-f" << "image2" << "-i" << "frame_"+argFormat+".png" << "-filter_complex" << "[0]split=2[bg][fg];[bg]drawbox=c=white@1:replace=1:t=fill[bg];[bg][fg]overlay=format=auto";
				args << "-crf" << "25";
				args << "-vcodec" << "png";
				args << "-pix_fmt" << "rgba";
				args << "CNI.mp4";

				System::exec( cmd, args, frames_dir.absolutePath() );

				Files::move( frames_dir.absolutePath() +"/CNI.mp4", cni_path+"/CNI.mp4", true );
			}
		}
		_kill = false;

		qDebug() << "Time to build CNI : " << timer.elapsed() << "ms";

		emit finished();
	}
	/**********************************************/
	/**********************************************/
	QString buildDeformedImage( const QString& path, const QString& frame, const QString& imagepath )
	{
		QString contents = LaTeX::PlotTemplate() + "\n\n";
		contents += "\\begin{document}\n";
		contents += "\t\\begin{tikzpicture}\n";
		contents += "\t\t\\begin{axis}[ image axis=imag, height=120mm ]\n\n";
		contents += "\n";
		contents += "\t\t\t\\addGraphics{" + imagepath + "}\n";
		contents += "\n";
		contents += "\t\t\\end{axis}\n";
		contents += "\t\\end{tikzpicture}\n";
		contents += "\\end{document}\n";

		compileSubFrame( path, frame + "_defimg", contents );
		return (path + "/" + frame + "_defimg.pdf");
	}
	/**********************************************/
	/**********************************************/
	QString buildPlot( const QString& path, const QString& frame, const QString& datapath, double x, double y )
	{
		QString contents = LaTeX::PlotTemplate() + "\n\n";
		contents += "\\def\\xaxislabel{$\\mathbfsf{\\upgamma_{12}}$ (\\si{\\percent})}\n";
		contents += "\\def\\yaxislabel{$\\mathbfsf{\\upsigma_{12}}$ (\\si{\\mega\\pascal})}\n\n";
		contents += "\\getData{"+datapath+"}\\dataA\n\n";
		contents += "\\def\\pointPos{("+QString::number(x)+","+QString::number(y)+")}%(x,y)sc\n\n";
		contents += "\\begin{document}\n";
		contents += "\t\\begin{tikzpicture}\n";
		contents += "\t\t\\NumbersInNormalMode\n";
		contents += "\t\t\\begin{axis}[ master axis, width=150mm, height=100mm ]\n\n";
		contents += "\n";
		contents += "\t\t\t\\addplot+ [sml curve=CTR] table [x index=1, y index=2] {\\dataA};\n";
		contents += "\n";
		contents += "\t\t\t\\addplot [point style] coordinates {\\pointPos};\n";
		contents += "\n";
		contents += "\t\t\\end{axis}\n";
		contents += "\t\\end{tikzpicture}\n";
		contents += "\\end{document}\n";

		compileSubFrame( path, frame + "_plot", contents );
		return (path + "/" + frame + "_plot.pdf");
	}
	/**********************************************/
	/**********************************************/
	QString buildDef( const QString& path, const QString& frame, const QString& def, const QString& imgpath, const QString& datapath, double min, double max )
	{
		QString zind = "4";
		if ( def == "12" ) zind = "5";
		else if ( def == "22" ) zind = "6";
		QString contents = LaTeX::PlotTemplate() + "\n\n";
		contents += "\\getData{"+datapath+"}\\dataA\n\n";
		contents += "\\begin{document}\n";
		contents += "\t\\begin{tikzpicture}\n";
		contents += "\t\t\\NumbersInNormalMode\n";
		contents += "\t\t\\def\\pmetamin{"+QString::number(min)+"}\n";
		contents += "\t\t\\def\\pmetamax{"+QString::number(max)+"}\n";
		contents += "\t\t\\begin{axis}[ dic axis, height=70mm, title={\\strainTitle{"+def+"}} ]\n\n";
		contents += "\n";
		contents += "\t\t\t\\addGraphics{"+imgpath+"};\n";
		contents += "\n";
		contents += "\t\t\t\\addplot3 [strain field plot] table [ x index=0, y index=1, z index="+zind+" ] {"+datapath+"};\n";
		contents += "\n";
		contents += "\t\t\\end{axis}\n";
		contents += "\t\\end{tikzpicture}\n";
		contents += "\\end{document}\n";

		compileSubFrame( path, frame + "_e"+def, contents );
		return (path + "/" + frame + "_e" + def + ".pdf");
	}
	/**********************************************/
	/**********************************************/
	void buildFrame( const QString& path, const QString& frame, const QString& defimgpath, const QString& plotpath, const QString& def11path, const QString& def12path, const QString& def22path )
	{
		QString contents = "\\documentclass[border=5pt]{standalone}\n\n";
		contents += "\\usepackage{tikz}\n";
		contents += "\\usepackage{graphicx}\n\n";
		contents += "\\begin{document}\n";
		contents += "\t\\begin{tikzpicture}\n";
		contents += "\t\t\\node (defimg) at (0,0) {\\includegraphics{"+defimgpath+"}};\n";
		contents += "\t\t\\node[anchor=south west] at (defimg.south east) {\\includegraphics{"+plotpath+"}};\n";
		contents += "\t\t\\node[anchor=north west] (e11) at (defimg.south west) {\\includegraphics{"+def11path+"}};\n";
		contents += "\t\t\\node[anchor=north west] (e12) at (e11.north east) {\\includegraphics{"+def12path+"}};\n";
		contents += "\t\t\\node[anchor=north west] at (e12.north east) {\\includegraphics{"+def22path+"}};\n";
		contents += "\t\\end{tikzpicture}\n";
		contents += "\\end{document}\n";

		compileSubFrame( path, frame, contents );
		LaTeX::convertPDFtoImage( path, frame, "png", "300" );
	}
	/**********************************************/
	/**********************************************/
	void compileSubFrame( const QString& path, const QString& file_name, const QString& contents )
	{
		QString tmp_path = path + "/" + LaTeX::DirName() + "/" + file_name + ".tex";
		Files::save( tmp_path, contents );
		LaTeX::Compile( tmp_path, false );
	}
	/**********************************************/
	/**********************************************/
public:
	CNIThread( TestInfo* info = 0 )
	{
		_testinfo = info;
	}
	/**********************************************/
	/**********************************************/
	void avoidTreatImages()
	{
		_treatImages = false;
	}
	/**********************************************/
	/**********************************************/
	void avoidMakeVideo()
	{
		_makeVideo = false;
	}
	/**********************************************/
	/**********************************************/
public slots:
	void terminate()
	{
		_kill = true;
	}
	/**********************************************/
	/**********************************************/
signals:
	void finished();
	void sendCurrentImage( int currentImage );
	void sendTotImage( int totImages );
	void makingVideo();
	/**********************************************/
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class CNI
        : public QObject
{
	Q_OBJECT
	TestInfo* _testinfo = 0;
	Settings* _sets = 0;
	/**********************************************/
	/**********************************************/
public:
	virtual ~CNI()
	{

	}
	/**********************************************/
	/**********************************************/
	CNI( TestInfo* info = 0, Settings* sets = 0 ) : QObject()
	{
		_testinfo = info;
		_sets = sets;
	}
	/**********************************************/
	/**********************************************/
	void treat()
	{
		if ( _testinfo )
		{
			CNIThread *thread = new CNIThread( _testinfo );
			connect( thread, &CNIThread::sendCurrentImage,
			         this, &CNI::sendCurrentImage );
			connect( thread, &CNIThread::sendTotImage,
			         this, &CNI::sendTotImage );
			connect( thread, &CNIThread::makingVideo,
			         this, &CNI::makingVideo );
			connect( this, &CNI::killProcess,
			         thread, &CNIThread::terminate );
			connect( thread, &CNIThread::finished,
			         this, &CNI::finished );
			thread->start();

		}
	}
	/**********************************************/
	/**********************************************/
	void treatTIF()
	{
		CNIThread *thread = new CNIThread( _testinfo );
		connect( thread, &CNIThread::sendCurrentImage,
		         this, &CNI::sendCurrentImage );
		connect( thread, &CNIThread::sendTotImage,
		         this, &CNI::sendTotImage );
		connect( this, &CNI::killProcess,
		         thread, &CNIThread::terminate );
		connect( thread, &CNIThread::finished,
		         this, &CNI::finished );
		thread->avoidMakeVideo();
		thread->start();
	}
	/**********************************************/
	/**********************************************/
	void makeVideo()
	{
		CNIThread *thread = new CNIThread( _testinfo );
		connect( thread, &CNIThread::makingVideo,
		         this, &CNI::makingVideo );
		connect( thread, &CNIThread::finished,
		         this, &CNI::finished );
		thread->avoidTreatImages();
		thread->start();
	}
	/**********************************************/
	/**********************************************/
signals:
	void sendCurrentImage( int currentImage );
	void sendTotImage( int totImages );
	void finished();
	void makingVideo();
	void killProcess();
	/**********************************************/
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // CNI_H
