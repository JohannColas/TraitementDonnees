#ifndef EA_H
#define EA_H
/**********************************************/
#include <QFile>
#include <QFileInfo>
#include <QSettings>
#include <QDebug>
#include <QDir>
#include <QThread>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
namespace EA_KEYS {
	static inline QString SeuilAmpli = "SeuilAmpli";
	static inline QString SeuilTime = "SeuilTemps";
	static inline QString SeuilRise = "SeuilRise";
	static inline QString SeuilDur = "SeuilDuree";
	static inline QString SeuilCps = "SeuilCoups";
	static inline QString SeuilEner = "SeuilEnergie";
}
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class EA_FilterFile
{
	QString _path;
	QSettings* _settings = 0;
	/**********************************************/
	/**********************************************/
public:
	~EA_FilterFile()
	{
		if ( _settings )
			delete _settings;
	}
	/**********************************************/
	/**********************************************/
	EA_FilterFile( const QString& folderpath = QString() )
	{
		setPath( folderpath );
	}
	/**********************************************/
	/**********************************************/
	void setPath( const QString& folderpath = QString() )
	{
		if ( _settings )
		{
			delete _settings;
			_settings = 0;
		}
		QString path_EA_settings = folderpath+"/EA";
		if ( folderpath.isEmpty() || !QDir(path_EA_settings).exists() )
			return;
		_settings = new QSettings( path_EA_settings+"/filter.ini", QSettings::IniFormat );
	}
	/**********************************************/
	/**********************************************/
	QVariant value( const QString& key )
	{
		if ( _settings )
			return _settings->value( "Filter/" + key );
		return QVariant();
	}
	/**********************************************/
	/**********************************************/
	int toInt( const QString& key )
	{
		QVariant var = value( key );
		if ( var.isValid() )
			return var.toInt();
		return -1;
	}
	/**********************************************/
	/**********************************************/
	void save( const QString& key, const QVariant& value )
	{
		if ( _settings )
			_settings->setValue( "Filter/" + key, value );
	}
	/**********************************************/
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class EA_Thread
        : public QThread
{
	Q_OBJECT
	QString _path;
	EA_FilterFile _filterFile;
	/**********************************************/
	/**********************************************/
public:
	EA_Thread( const QString& path )
	{
		_path = path;
		_filterFile.setPath( _path );
	}
	/**********************************************/
	/**********************************************/
	void run() override
	{
		QFile rawDataFile( _path + "/EA/EA.dat" );
		if ( !rawDataFile.open(QFile::ReadOnly) )
			return;
		QTextStream rawData( &rawDataFile );
		rawData.setEncoding( QStringConverter::Utf8  );

		QFile treatDataFile( _path + "/EA/EA_traite.dat" );
		if ( !treatDataFile.open(QFile::WriteOnly) )
			return;
		QTextStream treatData( &treatDataFile );
		treatData.setEncoding( QStringConverter::Utf8  );
		treatData << "Time(s);" // 0
		             "Duration(microm);" // 1
		             "Amplitude(dB);" // 2
		             "AverageFrequency(Hz);" // 3
		             "ReverbationFrequency(Hz);" // 4
		             "InitiationFrequency(Hz);" // 5
		             "AverageEnergy(aJ);" // 6
		             "AbsoluteEnergy(aJ);" // 7
		             "CumulatedEnergy(aJ);" // 8
		             "counts;" // 9
		             "AccumulativeCounts;" // 10
		             "Severity(aJ)" // 11
		             "\n";
		double sumEner = 0;
		double nbvLines = 0;
		int totCount = 0;
		int id_time = -1;
		int id_rise = -1;
		int id_count = -1;
		int id_energy = -1;
		int id_durat = -1;
		int id_amplit = -1;
		int id_avFreq = -1;
		int id_revFreq = -1;
		int id_initFreq = -1;
		int id_absEner = -1;
		int max_id = 0;
		QVector<double> energyMaxes;
		while ( !rawData.atEnd() )
		{
			QString line = rawData.readLine();
			if ( line.contains("ID") && line.contains("SSSSSSSS.mmmuuun") )
			{
				QStringList headers = line.split(' ', Qt::SkipEmptyParts );
				headers.removeOne( "STRNGTH" ); // Car SIG STRNGTH n'est qu'une unique colonne !!
				for ( int it = 0; it < headers.size(); ++it )
				{
					QString header = headers.at(it);
					if ( header == "SSSSSSSS.mmmuuun" )
					{
						id_time = it;
						if ( max_id < id_time ) max_id = id_time;
					}
					else if ( header == "RISE" )
					{
						id_rise = it;
						if ( max_id < id_rise ) max_id = id_rise;
					}
					else if ( header == "COUN" )
					{
						id_count = it;
						if ( max_id < id_count ) max_id = id_count;
					}
					else if ( header == "ENER" )
					{
						id_energy = it;
						if ( max_id < id_energy ) max_id = id_energy;
					}
					else if ( header == "DURATION" )
					{
						id_durat = it;
						if ( max_id < id_durat ) max_id = id_durat;
					}
					else if ( header == "AMP" )
					{
						id_amplit = it;
						if ( max_id < id_amplit ) max_id = id_amplit;
					}
					else if ( header == "A-FRQ" )
					{
						id_avFreq = it;
						if ( max_id < id_avFreq ) max_id = id_avFreq;
					}
					else if ( header == "R-FRQ" )
					{
						id_revFreq = it;
						if ( max_id < id_revFreq ) max_id = id_revFreq;
					}
					else if ( header == "I-FRQ" )
					{
						id_initFreq = it;
						if ( max_id < id_initFreq ) max_id = id_initFreq;
					}
					else if ( header == "ABS-ENERGY" )
					{
						id_absEner = it;
						if ( max_id < id_absEner ) max_id = id_absEner;
					}
				}
				break;
			}
		}
		while ( !rawData.atEnd() )
		{
			QStringList line;
			scanLine( rawData.readLine(), line );
			if ( line.size() < max_id+1 )
				continue;
			bool saveLine = false;
			int ID = line.at(0).toInt();
			if ( ID != 1 ) saveLine = false;
			double time = -1;
			if ( id_time != -1 )
			{
				time = line.at(id_time).toDouble();
				if ( time > _filterFile.toInt(EA_KEYS::SeuilTime) )
					saveLine = true;
			}
			int rise = -1;
			if ( id_rise != -1 )
			{
				rise = line.at(id_rise).toInt();
				if ( rise <= _filterFile.toInt(EA_KEYS::SeuilRise) )
					saveLine = true;
			}
			int count = -1;
			if ( id_count != -1 )
			{
				count = line.at(id_count).toInt();
				if ( count >= _filterFile.toInt(EA_KEYS::SeuilCps) )
					saveLine = true;
			}
			int energy = -1;
			if ( id_energy != -1 )
			{
				energy = line.at(id_energy).toInt();
				if ( energy > _filterFile.toInt(EA_KEYS::SeuilEner) )
					saveLine = true;
			}
			int durat = -1;
			if ( id_durat != -1 )
			{
				durat = line.at(id_durat).toInt();
				if ( durat < _filterFile.toInt(EA_KEYS::SeuilDur) )
					saveLine = true;
			}
			int amplit = -1;
			if ( id_amplit != -1 )
			{
				amplit = line.at(id_amplit).toInt();
				if ( amplit >= _filterFile.toInt(EA_KEYS::SeuilAmpli) )
					saveLine = true;
			}
			int avFreq = -1;
			if ( id_avFreq != -1 )
				avFreq = line.at(id_avFreq).toInt();
			int revFreq = -1;
			if ( id_revFreq != -1 )
				revFreq = line.at(id_revFreq).toInt();
			int initFreq = -1;
			if ( id_initFreq != -1 )
				initFreq = line.at(id_initFreq).toInt();
			double absEner = -1;
			if ( id_absEner != -1 )
				absEner = line.at(id_absEner).toDouble();

			if ( /*ID == 1 &&
						   time > _filterFile.toInt( EA_KEYS::SeuilTime ) &&
						   rise <= _filterFile.toInt( EA_KEYS::SeuilRise ) &&
						   count >= _filterFile.toInt( EA_KEYS::SeuilCps ) &&
						   energy > _filterFile.toInt( EA_KEYS::SeuilEner ) &&
						   durat < _filterFile.toInt( EA_KEYS::SeuilDur ) &&
						   amplit >= _filterFile.toInt( EA_KEYS::SeuilAmpli )*/
			     saveLine )
			{
				++nbvLines;
				sumEner += absEner;
				double avEner = sumEner/nbvLines;
				totCount += count;
				double severity = 0;
				addEnergy( absEner, energyMaxes );
				severity = average( energyMaxes );

				treatData << time << ";"
                          << durat << ";"
                          << amplit << ";"
                          << avFreq << ";"
                          << revFreq << ";"
                          << initFreq << ";"
                          << avEner << ";"
                          << absEner << ";"
                          << sumEner << ";"
                          << count << ";"
                          << totCount << ";"
                          << severity << "\n";
			}
		}

		rawDataFile.close();
		treatDataFile.close();

		emit finished();
	}
	/**********************************************/
	/**********************************************/
	double average( const QVector<double>& values )
	{
		double total = 0;
		for ( double val : values )
		{
			total += val;
		}
		return ( total / values.size() );
	}
	/**********************************************/
	/**********************************************/
	void addEnergy( double ener, QVector<double>& vector )
	{
		int vecSize = vector.size();
		if ( vecSize == 0 )
			vector.append(ener);
		else if ( ener > vector.first() || vecSize < 10 )
		{
			if ( ener > vector.last() )
				vector.append(ener);
			else
				for ( int it = 1; it < vecSize; ++it )
				{
					if ( ener > vector.at(it-1) &&
					     ener <= vector.at(it) )
					{
						vector.insert( it, ener );
						break;
					}
				}
			if ( vector.size() == 11 )
				vector.removeFirst();
		}
	}
	/**********************************************/
	/**********************************************/
	void scanLine( const QString& line, QStringList& strs )
	{
		QString tmp;
		QString oldChr;
		for ( int it = 0; it < line.size(); ++it )
		{
			QString chr = line.at(it);
			if ( chr == " " || (chr == "-" && oldChr != "E") )
			{
				if ( !tmp.isEmpty() )
				{
					strs.append( tmp );
					tmp = "";
					if ( chr == "-" )
						tmp = chr;
				}
			}
			else
			{
				tmp += chr;
			}
			oldChr = chr;
		}
		strs.append( tmp );
	}
	/**********************************************/
	/**********************************************/
signals:
	void finished();
	/**********************************************/
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class EA : public QObject
{
	Q_OBJECT
	QString _path;
	EA_FilterFile _filterFile;
	/**********************************************/
	/**********************************************/
public:
	EA( const QString& path )
	{
		_path = path;
		_filterFile.setPath( _path );
	}
	/**********************************************/
	/**********************************************/
	void treat()
	{
		EA_Thread *thread = new EA_Thread( _path );
		connect( thread, &EA_Thread::finished,
		         this, &EA::finished );
		thread->start();
	}
	/**********************************************/
	/**********************************************/
signals:
	void finished();
	/**********************************************/
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // EA_H
