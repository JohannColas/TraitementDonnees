#ifndef RE_H
#define RE_H
/**********************************************/
#include <QTextStream>
#include <QThread>
#include "Commons/Files.h"
#include "Commons/LaTeX.h"
#include "Commons/Settings.h"
#include "Commons/TestInfo.h"
#include "Commons/Math.h"
#include "Treatments/Data.h"
#include "Treatments/Plots.h"
#include <cmath>
/**********************************************/
#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class RE_Thread
        : public QThread
{
    Q_OBJECT
    /**********************************************/
    /**********************************************/
    TestInfo* _testinfo = 0;
    int _tempFocused = 980;
    /**********************************************/
    /**********************************************/
    void run() override
    {
        if ( !_testinfo ) return;
        QString path = _testinfo->path();
        // Get Program Info
        QString firstLine;
        if ( !Files::readFirstLine( path + "/RE/RE.dat", firstLine, QStringConverter::Latin1) )
            if ( !Files::readFirstLine( path + "/RE/RE.csv", firstLine, QStringConverter::Latin1) )
                return;

        QVector<int> progfrt = DataProg::NONE;
        if ( firstLine.contains("RESCOLAS.BAS") )
            progfrt = DataProg::RESCOLAS;

        if ( progfrt == DataProg::NONE ) return;

        // Read Raw Data
        QVector<QVector<double>> raw_data;
        if ( !Files::readData( path + "/RE/RE.dat", raw_data, QStringConverter::Latin1) )
            if ( !Files::readData( path + "/RE/RE.csv", raw_data, QStringConverter::Latin1) )
                return;

        // Save RE in testinfo
        if ( _testinfo->getString( TestInfoKeys::AvailableTracking ).isEmpty() )
            _testinfo->save( "R.E.", TestInfoKeys::AvailableTracking );

        // Build header of the treated file
        QString header_data;
        header_data = DataKeys::TIME + ";" + DataKeys::DEF + ";" + DataKeys::CTR + ";";
        bool hasTemp = false;
        // Read Meca Data, if VIEIL
        QVector<QVector<double>> meca_data; double meca_time_off = 0;
        if ( _testinfo->getString(TestInfoKeys::TestName).left(8).contains("VIEIL_") )
        {
            header_data += DataKeys::T + ";";
            hasTemp = true;
            if ( _testinfo->getString(TestInfoKeys::TestName).contains( "VIEIL_1300" ) )
                _tempFocused = 1280;
			if ( !Files::readData( path + "/" + Keys::fn_MecaTreatedData + ".dat", meca_data) )
                return;
            for ( QVector<double> row : meca_data )
            {
                if ( row[2] > 10.0 )
                {
                    meca_time_off = row[0];
                    break;
                }
            }
        }
        header_data += DataKeys::RE + ";" + DataKeys::DRR0;

        // Treat data
		QVector<QVector<double>> trd_data;
        // Get data indexes
        int iTIME  = progfrt[DataProg::idTIME];
        int iDEP   = progfrt[DataProg::idDEP];
        int iFORCE = progfrt[DataProg::idFORCE];
        int iRE    = progfrt[DataProg::idRE];
        double L = _testinfo->getDouble( TestInfoKeys::Length );
        double b = _testinfo->getDouble( TestInfoKeys::Width );
        double e = _testinfo->getDouble( TestInfoKeys::Thickness );
        double tcut = _testinfo->getDouble( TestInfoKeys::TimeCut );
        double tim = -1;
        double dep = -1;
        double dep0 = -1;
        if ( iDEP != -1)
            dep0 = raw_data[0][iDEP];
        double frc = -1;
        double def = -1;
        double ctr = -1;
        double re = -1;
        double R0 = -1;
        double dRdR0 = 0;
        if ( iRE != -1)
        {
            R0 = raw_data[0][iRE];
            if ( R0 < 1 )
                R0 *= 1000;
        }
        for ( int it = 0; it < raw_data.size(); ++it )
        {
            QVector<double> line = raw_data[it];
            tim = line[iTIME];
            // Get deplacement
            if ( iDEP != -1)
                dep = line[iDEP];
            // Get Force
            if ( iFORCE != -1)
                frc = line[iFORCE];
            // Get Strain
            if ( progfrt == DataProg::RESCOLAS )
            {
                def = 100*(dep - dep0)/L;
                if ( L == -1 )
                    def = -1;
            }
            // Get Stress
            if ( progfrt == DataProg::RESCOLAS )
            {
                ctr = frc/(b*e);
                if ( e == -1 )
                    ctr *= -1;
            }
            // get RE
            if ( iRE != -1 )
            {
                re = line[iRE];
                if ( re < 1 )
                    re *= 1000; // convert in mOhm
                dRdR0 = 100*( re - R0 )/R0;
            }
            // Store data
            if ( (line[iTIME] < tcut || tcut == -1 ) )
            {
                QVector<double> dataLine = { tim, def, ctr };
                if ( hasTemp )
                    dataLine.append( 0 );
                dataLine.append( {re, dRdR0} );
                trd_data.append( dataLine );
            }
        }
        // get temp
        if ( hasTemp )
        {
            double re_time_off = 0;
            for ( QVector<double> row : trd_data )
            {
                if ( row[2] > 10 )
                {
                    re_time_off = row[0];
                    break;
                }
            }
            for ( int jt = 0; jt < trd_data.size(); ++jt )
            {
                double tim = trd_data[jt][0]-re_time_off+meca_time_off;
                for ( int it = 0; it < meca_data.size()-1; ++it )
                {
                    double tim1 = meca_data[it][0], tim2 = meca_data[it+1][0];
                    if ( tim1 <= tim && tim <= tim2 )
                    {
                        double temp1 = meca_data[it][3], temp2 = meca_data[it+1][3];
                        trd_data[jt][3] = temp1 + ( temp2 - temp1 )*( tim - tim1 )/( tim2 - tim1 );
                        break;
                    }
                    if ( tim < tim1 )
                        break;
				}
            }
        }

        // Save Data
        Math::MoyenneGlissee( trd_data );
		Files::save( path + "/RE/"+Keys::fn_RETreatedData+".dat", trd_data, header_data );
        Plots::GenerateRE( _testinfo );

        emit finished();
    }
    /**********************************************/
    /**********************************************/
public:
    RE_Thread( TestInfo* info )
    {
        _testinfo = info;
    }
    /**********************************************/
    /**********************************************/
signals:
    void finished();
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class RE
        : public QObject
{
    Q_OBJECT
    TestInfo* _testinfo = 0;
    /**********************************************/
    /**********************************************/
public:
    RE( TestInfo* info )
    {
        _testinfo = info;
    }
    /**********************************************/
    /**********************************************/
    void treat()
    {
        if ( _testinfo )
        {
            RE_Thread *thread = new RE_Thread( _testinfo );
            connect( thread, &RE_Thread::finished,
                     this, &RE::finished );
            thread->start();

        }
    }
    /**********************************************/
    /**********************************************/
signals:
    void finished();
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // RE_H
