#ifndef DATA_H
#define DATA_H
/**********************************************/
#include <QString>
#include <QVector>
/**********************************************/
/**********************************************/
namespace DataKeys {
	static inline QString TIME  = "  time (s)";
	static inline QString DEP   = "     dep (mm)";
	static inline QString FLE   = "     fle (mm)";
	static inline QString FORCE = "    force (N)";
	static inline QString DEF   = "     e (perc)";
	static inline QString DEF1  = "    e1 (perc)";
	static inline QString DEF2  = "    e2 (perc)";
	static inline QString DEF3  = "    e3 (perc)";
	static inline QString CTR   = "      s (MPa)";
	static inline QString RE    = "    RE (mOhm)";
	static inline QString DRR0  = " dR_R0 (perc)";
	static inline QString EA    = "     EA (cps)";
	static inline QString T     = "        T (C)";
    static inline QString MT    = "    Etg (GPa)";
	static inline QString MTB   = "   beta (MPa)";
	static inline QString MS    = "    Esc (GPa)";
	static inline QString Ncycl = "       nCycle";
	static inline QString Scycl = "   Cycle step";
	static inline QString DEFn  = "   e_n (perc)";
	static inline QString DEFc  = "   e_c (perc)";
	static inline QString DEFd  = "   e_d (perc)";
}
/**********************************************/
/**********************************************/
namespace DataProg { // define in which colunm are the data (-1 if don't exist)
    static inline QVector<int> NONE     = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };
    static inline QVector<int> R8562    = {  0,  0, -1,  1,  2,  3, -1, -1,  4, -1, -1 };
    static inline QVector<int> RESIST   = {  1,  5, -1, -1,  1,  2, -1,  0,  4,  3, -1 };
    static inline QVector<int> TRACMONO = {  2,  5,  3,  0,  1,  2, -1,  6, -1,  4, -1 };
    static inline QVector<int> TRAC     = {  3,  4, -1, -1,  1,  2, -1,  0, -1,  3, -1 };
    static inline QVector<int> FLEX     = {  4,  3,  1,  0,  6, -1, -1,  5, -1,  2, -1 };
    static inline QVector<int> FLEX12   = {  5,  4,  1,  0, -1, -1, -1, -1,  3,  2, -1 };
    static inline QVector<int> IOSI3J   = {  6,  3,  1,  0,  4,  5,  6, -1,  7,  2, -1 };
    static inline QVector<int> VIEIL    = {  7,  0,  8,  7, -1, -1, -1, -1, -1, -1,  9 };
    static inline QVector<int> RESIST2  = {  8,  6, -1, -1,  1,  2, -1,  0,  4,  3, -1 };
    static inline QVector<int> TRACTR   = {  9,  4, -1, -1,  1,  2, -1,  0, -1,  3, -1 };
    static inline QVector<int> ESSAIHT  = { 10,  1,  9,  8, -1, -1, -1, -1, -1, -1, 10 };
    static inline QVector<int> RESCOLAS = { 11,  0,  3,  2, -1, -1, -1, -1,  1, -1, -1 };
    static inline int idTIME  = 1;
    static inline int idDEP   = 2;
    static inline int idFORCE = 3;
    static inline int idDEF1  = 4;
    static inline int idDEF2  = 5;
    static inline int idDEF3  = 6;
    static inline int idCTR   = 7;
    static inline int idRE    = 8;
    static inline int idEA    = 9;
    static inline int idT     = 10;
}
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlotData
{
	QString _path;
	QString _dataLB;
	QString _style;
	QString _legendLB;
	QString _legend;
public:
	PlotData( const QString& path, const QString& dataLB, const QString& style, const QString& legendLB, const QString& legend )
	    : _path(path), _dataLB(dataLB), _style(style), _legendLB(legendLB), _legend(legend)
	{

	}
	PlotData( const QString& path, const QString& dataLB )
	    : _path(path), _dataLB(dataLB)
	{

	}
	QString path() const { return _path; }
	QString dataLB() const { return _dataLB; }
	QString style() const { return _style; }
	QString legendLB() const { return _legendLB; }
	QString legend() const { return _legend; }
	void setPath( const QString& path ) { _path = path; }
	void setDataLB( const QString& dataLB ) { _dataLB = dataLB; }
	void setStyle( const QString& style ) { _style = style; }
	void setLegendLB( const QString& legendLB ) { _legendLB = legendLB; }
	void setLegend( const QString& legend ) { _legend = legend; }
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // DATA_H
