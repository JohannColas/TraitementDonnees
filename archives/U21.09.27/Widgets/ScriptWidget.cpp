#include "ScriptWidget.h"
#include "ui_ScriptWidget.h"
/**********************************************/
/**********************************************/
/**********************************************/
ScriptWidget::ScriptWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ScriptWidget)
{
    ui->setupUi(this);
    ui->tb_script_help->setVisible( false );
    sc_launch = new QShortcut( this );
    sc_launch->setKey( QKeySequence( Qt::CTRL | Qt::Key_Enter ) );
    connect( sc_launch, &QShortcut::activated,
             this, &ScriptWidget::on_pb_launch_released );
    sc_launch2 = new QShortcut( this );
    sc_launch2->setKey( QKeySequence( Qt::Key_F5 ) );
    connect( sc_launch2, &QShortcut::activated,
             this, &ScriptWidget::on_pb_launch_released );
    sc_help = new QShortcut( this );
    sc_help->setKey( QKeySequence( Qt::Key_F3 ) );
    connect( sc_help, &QShortcut::activated,
             this, &ScriptWidget::on_pb_script_help_released );
    QString contents;
    Files::read( ":/resources/ScriptHelp.md", contents );

    ui->tb_script_help->setFont( QFont("monospace") );
    ui->tb_script_help->setPlainText( contents );
}
/**********************************************/
/**********************************************/
/**********************************************/
ScriptWidget::~ScriptWidget()
{
    delete ui;
}
/**********************************************/
/**********************************************/
/**********************************************/
void ScriptWidget::on_pb_launch_released()
{
    QEventLoop loop;
    ScriptThread thread( ui->cmd_Script->toPlainText() );
    connect( &thread, SIGNAL(finished()),
             &loop, SLOT(quit()) );
    thread.start();
    loop.exec();
}
/**********************************************/
/**********************************************/
/**********************************************/
void ScriptWidget::on_pb_script_help_released()
{
    ui->tb_script_help->setVisible( !ui->tb_script_help->isVisible() );
}
/**********************************************/
/**********************************************/
/**********************************************/
