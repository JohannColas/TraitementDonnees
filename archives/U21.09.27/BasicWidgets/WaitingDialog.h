#ifndef WAITINGDIALOG_H
#define WAITINGDIALOG_H
/**********************************************/
#include <QDialog>
#include <QLabel>
#include <QMovie>
#include <QVBoxLayout>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class WaitingDialog
        : public QWidget
{
    Q_OBJECT
    QString _title;
    QLabel* _text = new QLabel;
    QLabel* _icon = new QLabel;
public:
	WaitingDialog( const QString& title, const QString& text, QWidget* parent = 0 )
        : QWidget( parent )
    {
        setWindowTitle( title );
        setWindowModality( Qt::ApplicationModal );
        setWindowFlags( Qt::Dialog | Qt::FramelessWindowHint );
        _text->setText( text );
        QMovie *movie = new QMovie(":/icons/resources/waiting.gif");
        movie->setScaledSize( QSize(48,48) );
        _icon->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
        _icon->setMovie(movie);

        QVBoxLayout* layout = new QVBoxLayout;
        this->setLayout( layout );
        layout->addWidget( _text );
        layout->addWidget( _icon );
        layout->setAlignment( _text, Qt::AlignCenter );
        layout->setAlignment( _icon, Qt::AlignCenter );
        movie->start();
    }
    void close()
    {
        emit closing();
        QWidget::close();
    }
signals:
    void closing();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // WAITINGDIALOG_H
