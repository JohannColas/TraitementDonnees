#ifndef SETTINGSFILE_H
#define SETTINGSFILE_H
/**********************************************/
#include <QSettings>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SettingsFile
        : public QObject
{
    Q_OBJECT
private:
    QSettings* _settings = 0;
    /**********************************************/
    /**********************************************/
public:
    SettingsFile( const QString& path )
    {
		_settings = new QSettings( path, QSettings::IniFormat );
    }
    /**********************************************/
    /**********************************************/
    QVariant value( const QString& key,  const QString& subkey = "" )
    {
        if ( _settings )
        {
            QString _key = key;
            if ( subkey != "" )
                _key += "/" + subkey;
            QVariant var = _settings->value( _key );
            if ( var.isValid() )
                return var;
        }
        return QVariant();
    }
    /**********************************************/
    /**********************************************/
    bool getBool( const QString& key,  const QString& subkey = "" )
    {
        QVariant var = value( key, subkey );
        if ( var.isValid() )
            return var.toBool();
        return false;
    }
    /**********************************************/
    /**********************************************/
    QString getString( const QString& key, const QString& subkey = "" )
    {
        QVariant var = value( key, subkey );
        if ( var.isValid() )
            return var.toString();
        return "";
    }
    /**********************************************/
    /**********************************************/
    int getInt( const QString& key, const QString& subkey = "" )
    {
        QVariant var = value( key, subkey );
        if ( var.isValid() )
            return var.toInt();
        return -1;
    }
    /**********************************************/
    /**********************************************/
    double getDouble( const QString& key, const QString& subkey = "" )
    {
        QVariant var = value( key, subkey );
        if ( var.isValid() )
            return var.toDouble();
        return -1;
    }
    /**********************************************/
    /**********************************************/
    void save( const QVariant& value, const QString& key, const QString& subkey = ""  )
    {
        QString _key = key;
        if ( subkey != "" )
            _key += "/" + subkey;
        _settings->setValue( _key, value );
    }
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SETTINGSFILE_H
