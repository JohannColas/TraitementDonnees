#ifndef TESTINFO_H
#define TESTINFO_H
/**********************************************/
#include <QFile>
#include <QFileInfo>
#include "SettingsFile.h"
#include "Settings.h"
#include <QDebug>
/**********************************************/
/**********************************************/
namespace TestInfoKeys {
    static QString Material = "materiau";
    static QString Sample = "eprouvette";
    static QString TestName = "essai";
    static QString Length = "longueurMM";
    static QString Width = "largeurMM";
    static QString Thickness = "epaisseurMM";
    static QString TimeCut = "tcut";
    static QString TestSpeed = "vitesse";
    static QString Comments = "commentaire";
	static QString AvailableTracking = "donneesDispo";
	static QString LighterFactor = "lighterFactor";
	static QString TIME_MAX = "time_max";
	static QString DEF_MAX = "def_max";
	static QString CTR_MAX = "ctr_max";
	static QString RE_MAX = "RE_max";
	static QString EA_MAX = "EA_max";
	static QString T_MAX = "temp_max";
	static QString MT_MAX = "MT_max";
	static QString MS_MAX = "MS_max";
}
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TestInfo {
    QString _path;
    SettingsFile* _settings = 0;
    /**********************************************/
    /**********************************************/
public:
    QString path()
    {
        return _path;
    }
    /**********************************************/
    /**********************************************/
    void setPath( const QString& path )
    {
        _path = path;
        if ( _settings != 0 )
        {
            delete _settings;
            _settings = 0;
        }

        if ( !isValid() )
            return;

		_settings = new SettingsFile( _path + "/" + Keys::fn_TESTINFO );
        if ( getString( TestInfoKeys::Material ).isEmpty() &&
             getString( TestInfoKeys::Sample ).isEmpty() &&
             getString( TestInfoKeys::TestName ).isEmpty() )
        {
            QStringList paths = path.split("/");
            QStringList filename = paths.takeLast().split(" - ");
            if ( filename.size() > 1 )
            {
                save( filename[1], TestInfoKeys::Sample );
                save( filename[0], TestInfoKeys::TestName );
            }
            save( paths.last(), TestInfoKeys::Material );
        }
    }
    /**********************************************/
    /**********************************************/
    bool isValid()
    {
		QFileInfo meca( _path + "/" + Keys::fn_MecaData + ".dat" );
		QFileInfo meca2( _path + "/" + Keys::fn_MecaData + ".csv" );
        if ( meca.exists() || meca2.exists() )
            return true;
        else
        {
            return false;
        }
    }
    /**********************************************/
    /**********************************************/
    QVariant value( const QString& key,  const QString& subkey = "" )
    {
        if ( _settings )
        {
            QString _key = key;
            if ( subkey != "" )
                _key += "/" + subkey;
            QVariant var = _settings->value( _key );
            if ( var.isValid() )
                return var;
        }
        return QVariant();
    }
    /**********************************************/
    /**********************************************/
    QString getString( const QString& key, const QString& subkey = "" )
    {
        QVariant var = value( key, subkey );
        if ( var.isValid() )
            return var.toString();
        return "";
    }
    /**********************************************/
    /**********************************************/
    int getInt( const QString& key, const QString& subkey = "" )
    {
        QVariant var = value( key, subkey );
        if ( var.isValid() )
            return var.toInt();
        return -1;
    }
    /**********************************************/
    /**********************************************/
    double getDouble( const QString& key, const QString& subkey = "" )
    {
        QVariant var = value( key, subkey );
        if ( var.isValid() )
            return var.toDouble();
        return -1;
    }
    /**********************************************/
    /**********************************************/
    void save( const QVariant& value, const QString& key, const QString& subkey = ""  )
    {
        QString _key = key;
        if ( subkey != "" )
            _key += "/" + subkey;
        _settings->save( value, _key );
    }
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TESTINFO_H




