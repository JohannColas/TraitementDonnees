#ifndef MECA_H
#define MECA_H
/**********************************************/
#include <QStringList>
/**********************************************/
#include "Commons/TestInfo.h"
#include "Commons/Math.h"
#include "Data.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Meca
{
public:
	static bool Treat( TestInfo* testinfo, bool deleteOldFiles = true );
	/**********************************************/
	/**********************************************/
public:
	enum ProgName
	{
		NONE,
		R8562,
		RESIST,
		TRACMONO,
		TRAC,
		FLEX,
		FLEX12,
		IOSI3J,
		VIEIL,
		RESIST2,
		TRACTR,
		ESSAIHT,
		RESCOLAS,
		TRAC6J,
	};
	enum Index
	{
		TIME = 0,
		DEPL = 1,
		FLEC = 1,
		FORCE = 2,
		EXT_1 = 3,
		EXT_2 = 4,
		DEF_J1 = 5,
		DEF_J2 = 6,
		DEF_J3 = 7,
		DEF_J4 = 8,
		DEF_J5 = 9,
		DEF_J6 = 10,
		STRESS = 11,
		RE = 12,
		EA = 13,
		TEMP = 14,
		STRAIN = 15,
		STRAIN_n = 16,
		STRAIN_d = 17,
		STRAIN_c = 18,
		STRAIN_TV = 19,
		DRR0 = 20,
		ETG = 21,
		ETGB = 22,
		ESC = 23,
		nCycl = 24,
		Step = 25,
		NU_POIS = 26,
		COUNT = 27 // Number of Indexes
	};
protected:
	static inline TestInfo _testinfo;
	static inline double L = NAN;
	static inline double b = NAN;
	static inline double e = NAN;
	static inline double time_cut = NAN;
	static inline double dep0 = NAN;
	static inline double R0 = NAN;
	static inline QVector<QVector<double>> _raw_data;
	static inline Meca::ProgName _prog_name = Meca::NONE;
	static inline QVector<int> _program = {};
	/**********************************************/
public:
	/**********************************************/
	static void setTest( TestInfo testinfo );
	static void setTest( const QString& testpath );
	static void Treat( const QString& testpath, bool deleteOldFiles = true );
	static void TreatRE( const QString& testpath, bool deleteOldFiles = true );
	/**********************************************/
	static Meca::ProgName SelectedProgram();
	static void SelectProgram( const Meca::ProgName& prog );
	static bool AutomaticSelectProgram( const QString& path );
	/**********************************************/
	static void getConstants();
	/**********************************************/
	static void resetIndex( const Meca::Index& index );
	static int index( const Meca::Index& index );
	/**********************************************/
	static double get( const QVector<double>& line, int index );
	static double get( const QVector<double>& line, const Meca::Index& index );
	static double getStrain( const QVector<double>& line );
	static double getTransversalStrain( const QVector<double>& line );
	static double getPoisonCoef( const QVector<double>& line );
	static double getStress( const QVector<double>& line );
	static double getRE( const QVector<double>& line );
	static double getDRR0( double re );
	/**********************************************/
	static void PreTreatment();
	/**********************************************/
	static QVector<QVector<double>> raw_data();
	static QString raw_data_header();
	static QVector<QVector<double>> treated_data();
	static QString treated_data_header();
	static QVector<QVector<double>> re_treated_data();
	static QString re_treated_data_header();
	/**********************************************/
	// Tangent Modulus calculated by Linear Regression
	static void calculateTangentModulus();
	static void getCycles();
	static void getExtremes();
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // MECA_H
