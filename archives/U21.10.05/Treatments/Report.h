#ifndef REPORT_H
#define REPORT_H
/**********************************************/
#include <QProcess>
#include <QDebug>
#include "Commons/Files.h"
#include "Commons/Settings.h"
#include "Commons/TestInfo.h"
#include "Commons/LaTeX.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Report
{
    TestInfo* _testinfo = 0;
    /**********************************************/
    /**********************************************/
public:
    Report( TestInfo* info = 0)
    {
        _testinfo = info;
    }
    /**********************************************/
    /**********************************************/
    void generate()
    {
        if ( _testinfo )
        {
            QString prefix;
            QString essai = _testinfo->getString( TestInfoKeys::TestName );
            if ( essai.contains("TRAC_") )
            {
                prefix = "TRAC";
                double th = _testinfo->getDouble( TestInfoKeys::Thickness );
                if ( th == -1 )
                    prefix = "TRAC_SECTION";
            }
            else if ( essai.contains("FLEX_") )
            {
                prefix = "FLEX";
            }
            else if ( essai.contains("IOSI_") ||
                      essai.contains("CISA_") )
            {
                prefix = "CISA";
            }
            else if ( essai.left(6) == "VIEIL_" )
            {
                prefix = "VIEIL";
                double th = _testinfo->getDouble( TestInfoKeys::Thickness );
                if ( th == -1 )
                    prefix = "VIEIL_SECTION";
            }
            if ( !prefix.isEmpty() )
            {
                QString reportName = "synthese";
                // Create TeX File
                createTeX( prefix, reportName );
            }
        }
    }
    /**********************************************/
    /**********************************************/
    void createTeX( const QString& prefix, const QString& reportName )
    {
        QString tplPath = Settings::getString( Keys::Reports, Keys::TplDir);
        QString TeXpath = tplPath + "/" + prefix + ".tex";
        QString contents ;
        if ( !Files::read( TeXpath, contents ) )
            return;
        QString path = _testinfo->path();
        QString sample = _testinfo->getString( TestInfoKeys::Sample );
        QString essai = _testinfo->getString( TestInfoKeys::TestName );
        contents.replace("%%DIRESSAI%%", path);
        contents.replace("%%EPROUV%%", sample);
        QString type;
        if ( essai.contains("TRAC") )
            type = "Traction ";
        else if ( essai.contains("FLEX") )
            type = "Flexion ";
        else if ( essai.contains("IOSI") )
            type = "Iosipescu ";
        else if ( essai.contains("RAIL") )
            type = "Shear Rail ";
        else if ( essai.left(5) == "VIEIL" )
            type = "Vieillissement";
        if ( essai.contains("MONO") )
            type += "monotone";
        else if ( essai.contains("CYCL") )
        {
            type += "cyclé";
            if ( essai.contains("TRAC") ||
                 essai.contains("FLEX") )
                type += "e";
        }
        contents.replace("%%TYPE%%", type);
        QString vieil = "Non-vieilli";
        if ( essai.contains("VIEIL_") )
        {
            int ind = essai.indexOf("VIEIL_") + 6;
            QString mid = essai.mid( ind, 4);
            vieil = "Vieilli à " + mid + "\\,\\textcelsius";
        }
        contents.replace("%%VIEIL%%", vieil);
        double L = _testinfo->getDouble( TestInfoKeys::Length );
        double b = _testinfo->getDouble( TestInfoKeys::Width );
        double th = _testinfo->getDouble( TestInfoKeys::Thickness );
        contents.replace("%%LONGUEUR%%", QString::number(L));
        contents.replace("%%LARGEUR%%", QString::number(b));
        contents.replace("%%EPAISSEUR%%", QString::number(th));
        if ( !essai.left(6).contains("VIEIL_") )
        {
            QString temp = "20";
            if ( essai.contains("_1300_VIEIL") || essai.contains("_1300 ") )
            {
                temp = "1300";
            }
            else if ( essai.contains("_1000_VIEIL") || essai.contains("_1000 ") )
            {
                temp = "1000";
            }
            contents.replace("%%TEMP%%", temp);
        }
        QString speed = _testinfo->getString( TestInfoKeys::TestSpeed );
        contents.replace("%%VITES%%", speed);
        QString suivi = "Aucun";
        QString tracking = _testinfo->getString( TestInfoKeys::AvailableTracking );
        QString suiviAB = "";
        if ( tracking.contains("R.E.") )
        {
            suivi = "Résistance électrique";
            suiviAB = "-RE";
        }
        if ( tracking.contains("E.A.") )
        {
            suiviAB = "-EA" + suiviAB;
            if ( tracking.contains("R.E.") )
                suivi += " - Émission acoustique";
            else
                suivi = "Émission acoustique";
        }
        contents.replace("%%SUIVI%%", suivi);
        contents.replace("%%SUIVIAB%%", suiviAB);
        QString comments = _testinfo->getString( TestInfoKeys::Comments );
        contents.replace("%%COMMENT%%", comments.replace("\n","\\\\\n"));

        QString prop_path = _testinfo->path() + "/meca/PROP.tex";
        QFileInfo prop( prop_path );
        if ( prop.exists() )
        {
            contents += "\n\n";
            contents += QString("\t\\subsubsection*{Propriétés}\n");
            contents += "\n";
            contents += QString("\t\t\\begin{jtable}{\\textwidth}{Éprouvette \\eprouvette; Propriétés initiales}{\\eprouvette-PROP}\n");
            contents += QString("\t\t\t\\input{" + prop_path + "}\n");
            contents += "\t\t\\end{jtable}\n";
        }
        QString prop_cycl_path = _testinfo->path() + "/meca/PROP_CYCL.tex";
        QFileInfo prop_cycl( prop_cycl_path );
        if ( prop_cycl.exists() )
        {
            contents += "\n";
            contents += QString("\t\t\\begin{jtable}{\\textwidth}{Éprouvette \\eprouvette; Paramètres des boucles d'hystérésis}{\\eprouvette-PROP-CYCL}\n");
            contents += QString("\t\t\t\\input{" + prop_cycl_path + "}\n");
            contents += "\t\t\\end{jtable}\n";
        }

        QString Reportpath = _testinfo->path() + "/" + reportName + ".tex";
        Files::save( Reportpath, contents );

        // Create Synthese PDF
        TeXpath = tplPath + "/synthese.tpl.tex";
        contents.clear();
        if ( !Files::read( TeXpath, contents ) )
            return;
        QString material = _testinfo->getString( TestInfoKeys::Material );
        QString color = "black";
        if ( material.contains("safran", Qt::CaseInsensitive ) )
            color = "SAFRAN";
        if ( material.contains("sgl", Qt::CaseInsensitive ) )
            color = "SGL";
        else if ( material.contains("mta", Qt::CaseInsensitive ) )
            color = "MTA";
        contents.replace("%%CHAPTER%%", "\t\\noindent{\\color{"+color+"}\\fontsize{24pt}{0pt}\\selectfont \\bfseries "+material+"}" );
        contents.replace("%%MATERIAU%%", material );
        contents.replace("%%FILEPATH%%", _testinfo->path() + "/synthese.tex" );
        Reportpath = QDir::currentPath() + "/LaTeX/synthese.tex";
        QDir dir( QDir::currentPath() );
        if ( !dir.exists("LaTeX") )
            dir.mkdir( "LaTeX" );
        Files::save( Reportpath, contents );
        LaTeX::Compile( _testinfo->path(), "synthese", QDir::currentPath() ) ;
    }
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // REPORT_H
