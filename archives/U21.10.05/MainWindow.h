#ifndef MAINWINDOW_H
#define MAINWINDOW_H
/**********************************************/
#include <QMainWindow>
#include <QFileSystemModel>
#include "Treatments/Treatment.h"
/**********************************************/
/**********************************************/
/**********************************************/
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void clearContainer();
    /**********************************************/
    /**********************************************/
protected slots:
    void closeEvent( QCloseEvent* event ) override
    {
        QMainWindow::closeEvent( event );
    }
    void on_pb_dirEssai_released();
	void on_treeView_pressed( const QModelIndex& index );
	void on_treeView_doubleClicked( const QModelIndex& index );
	void treeView_customMenu( const QPoint& pos );
	void newFolder();
	void newGraph();
	void newMiniReport();
    void setTreeCurrent( const QString &path );
    void on_sel_essai_released();
    void on_sel_reports_released();
    void on_sel_graphs_released();
    void on_sel_script_released();
	void on_sel_parametres_released();
    void on_pb_traiterCetEssai_released();
    void on_pb_toutTraiter_released();
    void toutTraiterExt( const TreatmentInfo& trinfo );
    void treatment( const QStringList &paths, const TreatmentInfo& info = TreatmentInfo() );
	void stopTreatment();
	void resizeEvent( QResizeEvent* event ) override;
	void moveEvent( QMoveEvent* event ) override;
    /**********************************************/
	/**********************************************/
private:
    Ui::MainWindow *ui;
    QFileSystemModel *model = new QFileSystemModel;
    QString _currentPath = "";
    TestInfo* _testinfo = new TestInfo;
    bool _stopTreatement = false;
    bool _treatAll = false;
    Treatment treat;
    QString plotPath;
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // MAINWINDOW_H
