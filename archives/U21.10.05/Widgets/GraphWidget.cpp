#include "GraphWidget.h"
#include "ui_GraphWidget.h"
#include "BasicWidgets/WaitingDialog.h"
#include <QMessageBox>
/**********************************************/
/**********************************************/
/**********************************************/
GraphWidget::GraphWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GraphWidget)
{
    ui->setupUi(this);
    ui->pb_modPlot->setChecked( false );
    ui->wd_modPlot->hide();
    ui->lb_PlotToShow->hide();
    ui->plotSelector->hide();
    ui->pb_modPlot->hide();
    ui->pb_supprimer->hide();

    sc_goUp = new QShortcut( this );
    sc_goUp->setKey( QKeySequence( Qt::CTRL | Qt::Key_Up ) );
    connect( sc_goUp, &QShortcut::activated,
             ui->imageViewer, &ImageViewer::goUp );
    sc_goDown = new QShortcut( this );
    sc_goDown->setKey( QKeySequence( Qt::CTRL | Qt::Key_Down ) );
    connect( sc_goDown, &QShortcut::activated,
             ui->imageViewer, &ImageViewer::goDown );
    sc_goLeft = new QShortcut( this );
    sc_goLeft->setKey( QKeySequence( Qt::CTRL | Qt::Key_Left ) );
    connect( sc_goLeft, &QShortcut::activated,
             ui->imageViewer, &ImageViewer::goLeft );
    sc_goRight = new QShortcut( this );
    sc_goRight->setKey( QKeySequence( Qt::CTRL | Qt::Key_Right ) );
    connect( sc_goRight, &QShortcut::activated,
             ui->imageViewer, &ImageViewer::goRight );
    sc_zoomIn = new QShortcut( this );
    sc_zoomIn->setKey( QKeySequence( Qt::SHIFT | Qt::Key_Up ) );
    connect( sc_zoomIn, &QShortcut::activated,
             ui->imageViewer, &ImageViewer::zoomIn );
    sc_zoomOut = new QShortcut( this );
    sc_zoomOut->setKey( QKeySequence( Qt::SHIFT | Qt::Key_Down ) );
    connect( sc_zoomOut, &QShortcut::activated,
             ui->imageViewer, &ImageViewer::zoomOut );
    sc_normal = new QShortcut( this );
    sc_normal->setKey( QKeySequence( Qt::CTRL | Qt::Key_Space ) );
    connect( sc_normal, &QShortcut::activated,
             ui->imageViewer, &ImageViewer::normalSize );
    sc_fit = new QShortcut( this );
    sc_fit->setKey( QKeySequence( Qt::CTRL | Qt::Key_W ) );
    connect( sc_fit, &QShortcut::activated,
             ui->imageViewer, &ImageViewer::fitToWindow );
    sc_showTeX = new QShortcut( this );
    sc_showTeX->setKey( QKeySequence( Qt::Key_F2 ) );
    connect( sc_showTeX, &QShortcut::activated,
             ui->pb_modPlot, &QPushButton::click );
    sc_compile = new QShortcut( this );
    sc_compile->setKey( QKeySequence( Qt::Key_F5 ) );
    connect( sc_compile, &QShortcut::activated,
             this, &GraphWidget::on_pb_compile_released );
    sc_compile2 = new QShortcut( this );
    sc_compile2->setKey( QKeySequence( Qt::CTRL | Qt::Key_S ) );
    connect( sc_compile2, &QShortcut::activated,
	         this, &GraphWidget::on_pb_compile_released );
	sc_nextPlot = new QShortcut( this );
	sc_nextPlot->setKey( QKeySequence( Qt::Key_F4 ) );
	connect( sc_nextPlot, &QShortcut::activated,
	         this, &GraphWidget::nextPlot );
	sc_previousPlot = new QShortcut( this );
	sc_previousPlot->setKey( QKeySequence( Qt::Key_F3 ) );
	connect( sc_previousPlot, &QShortcut::activated,
	         this, &GraphWidget::previousPlot );
	sc_nextPlot2 = new QShortcut( this );
	sc_nextPlot2->setKey( QKeySequence( Qt::Key_Down ) );
	connect( sc_nextPlot2, &QShortcut::activated,
	         this, &GraphWidget::nextPlot );
	sc_previousPlot2 = new QShortcut( this );
	sc_previousPlot2->setKey( QKeySequence( Qt::Key_Up ) );
	connect( sc_previousPlot2, &QShortcut::activated,
	         this, &GraphWidget::previousPlot );
    sc_delete = new QShortcut( this );
	sc_delete->setKey( QKeySequence( Qt::CTRL | Qt::Key_Delete ) );
    connect( sc_delete, &QShortcut::activated,
             this, &GraphWidget::on_pb_supprimer_released );
}
/**********************************************/
/**********************************************/
/**********************************************/
GraphWidget::~GraphWidget()
{
    delete ui;
}
/**********************************************/
/**********************************************/
/**********************************************/
void GraphWidget::update()
{
	if ( Files::isImage( _path ) )
	{
        whenSingleFilePath();
        if ( LaTeXexists() )
            whenTeXImagePath();
        else
            whenNotTeXImagePath();
    }
    else if ( !_path.isEmpty() && QDir(_path).exists() )
    {
        whenFolderPath();
    }
    else
    {
       whenInvalidPath();
    }
}
void GraphWidget::whenInvalidPath()
{
    ui->imageViewer->loadFile( "" );
    ui->lb_plotName->hide();
    ui->lb_PlotToShow->hide();
    ui->plotSelector->hide();
    ui->pb_modPlot->hide();
    ui->wd_modPlot->hide();
    ui->pb_supprimer->hide();
    ui->te_modPlot->setPlainText( "" );
    ui->le_filename->setText( "" );
}
void GraphWidget::whenFolderPath()
{
    updatePlotSelector();
    ui->lb_plotName->setText( "" );
    ui->lb_plotName->hide();
    ui->lb_PlotToShow->show();
    ui->plotSelector->show();
    sc_nextPlot->blockSignals(false);
    sc_previousPlot->blockSignals(false);
}
void GraphWidget::whenSingleFilePath()
{
    _plotpath = _path;
	ui->imageViewer->loadFile( _plotpath );
	ui->lb_plotName->setText( QFileInfo(_path).baseName() );
    ui->lb_plotName->show();
    ui->lb_PlotToShow->hide();
    ui->plotSelector->hide();
    sc_nextPlot->blockSignals(true);
    sc_previousPlot->blockSignals(true);
}
void GraphWidget::whenTeXImagePath()
{
    ui->pb_modPlot->show();
    ui->pb_supprimer->show();
    sc_showTeX->blockSignals(false);
    sc_compile->blockSignals(false);
    sc_compile2->blockSignals(false);
    updateLaTeXWidget();
    if ( ui->pb_modPlot->isChecked() )
        showLaTeXWidget();
    else
        hideLaTeXWidget();
}
void GraphWidget::whenNotTeXImagePath()
{
    ui->pb_modPlot->hide();
    ui->wd_modPlot->hide();
    ui->pb_supprimer->hide();
    ui->te_modPlot->setPlainText( "" );
    ui->le_filename->setText( "" );
    sc_showTeX->blockSignals(true);
    sc_compile->blockSignals(true);
    sc_compile2->blockSignals(true);
}
/**********************************************/
/**********************************************/
/**********************************************/
bool GraphWidget::LaTeXexists()
{
    QFileInfo plotFile( _plotpath );
    QString plotTeXPath = plotFile.absolutePath() + "/LaTeX/" + plotFile.baseName() + ".tex";
    QFileInfo texFile( plotTeXPath );
    return texFile.exists();
}
/**********************************************/
/**********************************************/
/**********************************************/
void GraphWidget::showLaTeXWidget()
{
    ui->pb_modPlot->setText( "Cacher LaTeX" );
    ui->wd_modPlot->show();
}
/**********************************************/
/**********************************************/
/**********************************************/
void GraphWidget::hideLaTeXWidget()
{
    ui->pb_modPlot->setText( "Afficher LaTeX" );
    ui->wd_modPlot->hide();
}
/**********************************************/
/**********************************************/
/**********************************************/
void GraphWidget::updateLaTeXWidget()
{
    QFileInfo plotFile( _plotpath );
    QString plotTeXPath = plotFile.absolutePath() + "/LaTeX/" + plotFile.baseName() + ".tex";
    ui->le_filename->setText( plotFile.baseName() );
    QFileInfo texFile( plotTeXPath );
    QString content;
    Files::read( texFile.absoluteFilePath(), content );
    ui->te_modPlot->setPlainText( content );
}
/**********************************************/
/**********************************************/
/**********************************************/
void GraphWidget::on_plotSelector_currentTextChanged( const QString &arg1 )
{
    _plotpath = _path + "/" + arg1;
    ui->imageViewer->loadFile( _plotpath );
    if ( LaTeXexists() )
        whenTeXImagePath();
    else
        whenNotTeXImagePath();
}
void GraphWidget::nextPlot()
{
    int newIndex = ui->plotSelector->currentIndex() + 1;
    if ( newIndex > ui->plotSelector->count() - 1 )
        newIndex = 0;
    ui->plotSelector->setCurrentIndex( newIndex );
}
void GraphWidget::previousPlot()
{
    int newIndex = ui->plotSelector->currentIndex() - 1;
    if ( newIndex < 0 )
        newIndex = ui->plotSelector->count()-1;
    ui->plotSelector->setCurrentIndex( newIndex );
}
void GraphWidget::updatePlotSelector()
{
    QStringList images = Files::ListImages( _path, true);
    ui->plotSelector->clear();
    ui->plotSelector->addItems(images);
}
/**********************************************/
/**********************************************/
/**********************************************/
void GraphWidget::on_pb_modPlot_released()
{
    if ( ui->pb_modPlot->isChecked() )
        showLaTeXWidget();
    else
        hideLaTeXWidget();
}
/**********************************************/
/**********************************************/
/**********************************************/
void GraphWidget::on_pb_compile_released()
{
    QFileInfo plotFile( _plotpath );
    QString dirPath = plotFile.absolutePath();
    QString texPath = dirPath + "/LaTeX/" + plotFile.baseName() + ".tex";
    if ( QFileInfo::exists(texPath) )
    {
        QString plotpath2 = ui->le_filename->text();
        if ( ui->le_filename->text().isEmpty() )
            plotpath2 = QFileInfo(_plotpath).baseName();
        if (!ui->te_modPlot->toPlainText().isEmpty() )
        {
            texPath = dirPath + "/LaTeX/" + plotpath2 + ".tex";
            Files::save( texPath, ui->te_modPlot->toPlainText() );
        }

        WaitingDialog* wdiag = new WaitingDialog( "", "Compilation en cours..." );
        wdiag->show();

        QEventLoop loop;
        LaTeXThread thread = LaTeXThread( dirPath, plotpath2 );
        connect( &thread, SIGNAL(finished()),
                 &loop, SLOT(quit()) );
        thread.start();
		loop.exec();

        _plotpath = dirPath + "/" + plotpath2 + ".png";
        update();
        if ( ui->lb_plotName->isVisible() )
            emit sendPlotPath( dirPath + "/" + plotpath2 + ".png" );
        else if ( ui->plotSelector->isVisible() )
            for ( int it = 0; it < ui->plotSelector->count(); ++it )
                if ( ui->plotSelector->itemText(it).contains( plotpath2 + ".png" ) )
                {
                    ui->plotSelector->setCurrentIndex( it );
                    break;
                }
        wdiag->close();
        wdiag->deleteLater();
    }
}
/**********************************************/
/**********************************************/
/**********************************************/
void GraphWidget::on_pb_supprimer_released()
{
    QMessageBox confBox;
    QString plotname;
    if ( ui->lb_plotName->isVisible() )
        plotname = ui->lb_plotName->text();
    else if ( ui->plotSelector->isVisible() )
        plotname = ui->plotSelector->currentText();
    confBox.setText( "Le graphique " + plotname + " va être supprimer." );
    confBox.setInformativeText( "Confirmez-vous cette opération ?" );
    confBox.setIcon( QMessageBox::Warning );
    confBox.setStandardButtons( QMessageBox::Yes | QMessageBox::Cancel );
    confBox.setDefaultButton( QMessageBox::Cancel );
    int ret = confBox.exec();
    if ( ret == QMessageBox::Cancel )
        return;
    else if ( ret == QMessageBox::Yes )
    {
        QFileInfo plotFile( _plotpath );
        QFileInfoList filestodelete = QDir( plotFile.absolutePath() ).entryInfoList( {plotFile.baseName()+".*"}, QDir::Files | QDir::NoDotAndDotDot );
        filestodelete.append( QDir( plotFile.absolutePath()+ "/LaTeX" ).entryInfoList( {plotFile.baseName()+".*"}, QDir::Files | QDir::NoDotAndDotDot ) );

        for ( const QFileInfo &info : filestodelete )
            QFile::remove( info.absoluteFilePath() );

        if ( ui->lb_plotName->isVisible() )
        {
            _path = "";
            update();
        }
        else if ( ui->plotSelector->isVisible() )
        {
            int newIndex = ui->plotSelector->currentIndex() - 1;
            if ( newIndex < 0 )
                newIndex = 0;
            updatePlotSelector();
            ui->plotSelector->setCurrentIndex( newIndex );
        }
    }
}
/**********************************************/
/**********************************************/
/**********************************************/
