#ifndef TESTINFOWIDGET_H
#define TESTINFOWIDGET_H
/**********************************************/
#include <QShortcut>
#include <QWidget>
#include "Commons/TestInfo.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
namespace Ui {
    class TestInfoWidget;
}
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TestInfoWidget : public QWidget
{
    Q_OBJECT
public:
    explicit TestInfoWidget(QWidget *parent = nullptr);
    ~TestInfoWidget();
    void linkToTestInfo( TestInfo* info );
    void updateTestInfo();
    /**********************************************/
    /**********************************************/
private slots:
    void on_info_materiau_editingFinished();
    void on_info_eprouvette_editingFinished();
    void on_info_essai_editingFinished();
    void on_champ_longueur_editingFinished();
    void on_champ_largeur_editingFinished();
    void on_champ_epaisseur_editingFinished();
    void on_champ_tcut_editingFinished();
    void on_champ_suivi_editingFinished();
    void on_champ_commentaire_editingFinished();
	void on_treat();
	void on_treatAll();
	void saveEverything();
    /**********************************************/
    /**********************************************/
private:
    Ui::TestInfoWidget *ui;
    TestInfo* _testinfo = 0;
	QShortcut* sc_treat;
	QShortcut* sc_treatall;
    /**********************************************/
    /**********************************************/
signals:
	void treatAll();
	void treatThis();
	/**********************************************/
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TESTINFOWIDGET_H
