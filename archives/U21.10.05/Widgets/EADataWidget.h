#ifndef EADATAWIDGET_H
#define EADATAWIDGET_H
/**********************************************/
#include <QWidget>
#include "Commons/TestInfo.h"
#include "Treatments/EA.h"
/**********************************************/
/**********************************************/
/**********************************************/
namespace Ui {
    class EADataWidget;
}
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class EADataWidget : public QWidget
{
    Q_OBJECT
public:
    explicit EADataWidget(QWidget *parent = nullptr);
    ~EADataWidget();
    void linkToTestInfo( TestInfo* info );
    void update();
    /**********************************************/
    /**********************************************/
private slots:
    void on_traite_EA_released();
    void on_sp_seuilAmp_valueChanged( int value );
    void on_sp_seuilTime_valueChanged( int value );
    void on_sp_seuilRise_valueChanged( int value );
    void on_sp_seuilDur_valueChanged( int value );
    void on_sp_seuilCps_valueChanged( int value );
    void on_sp_seuilEner_valueChanged( int value );
    /**********************************************/
    /**********************************************/
private:
    Ui::EADataWidget *ui;
    TestInfo* _testinfo = 0;
    EA_FilterFile* EA_filterfile = new EA_FilterFile;
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // EADATAWIDGET_H
