#ifndef PROGRESSWIDGET_H
#define PROGRESSWIDGET_H

#include <QHBoxLayout>
#include <QLabel>
#include <QProgressBar>
#include <QPushButton>
#include <QTime>
#include <QWidget>
#include "Commons/Settings.h"
#include "Commons/TestInfo.h"

class ProgressWidget
        : public QWidget
{
    Q_OBJECT
    QProgressBar* progress = new QProgressBar;
    QLabel* label = new QLabel( " | " );
    QLabel* lb_task = new QLabel( "Traitement en cours" );
    QLabel* lb_dir = new QLabel( "" );
    QLabel* remainTimer = new QLabel( "" );
    QPushButton* stop = new QPushButton( "STOP" );
public:
    ProgressWidget( QWidget* parent = 0 )
        : QWidget( parent )
    {
        connect( stop, SIGNAL(released()),
                 this, SLOT(stopping()) );
		QHBoxLayout* layout = new QHBoxLayout;
        layout->setContentsMargins( 0, 0, 0, 0 );
        layout->addWidget( progress );
        layout->addWidget( stop );
        layout->addWidget( lb_dir );
        layout->addWidget( label );
        layout->addWidget( lb_task );
        layout->addWidget( remainTimer );

        setLayout( layout );
    }
    void setProgress( int prog )
    {
        progress->setValue( prog );
    }
	void setRemaintime( int time )
    {
        remainTimer->setText( "(" + System::TimeToString(time)+ ")" );
    }
    void setDir( const QString& dir )
    {
        TestInfo* testinfo = new TestInfo;
        testinfo->setPath( dir );
        QString dirT = testinfo->getString( TestInfoKeys::Material ) + " / " +
                testinfo->getString( TestInfoKeys::Sample ) + " / " +
                testinfo->getString( TestInfoKeys::TestName );
        lb_dir->setText( dirT );
    }
    void hideProgressBar()
    {
        progress->hide();
        stop->hide();
    }

public slots:
    void setTask( const QString& task )
    {
        lb_task->setText( task );
    }

protected slots:
    void stopping()
    {
        stop->setText("Stopping...");
        emit stopT();
    }

signals:
    void stopT();
};

#endif // PROGRESSWIDGET_H
