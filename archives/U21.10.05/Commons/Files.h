#ifndef FILE_H
#define FILE_H
/**********************************************/
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QVector>
#include <QDebug>
#include "Settings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Files
{
public:
	static inline QString CyclesFolder = "cycles";
	static inline QStringList ImageFilters = {"*.png", "*.tiff", "*.tif", "*.jpg"};
	static inline bool isImage( QString& path )
	{
		QString ext = path.mid( path.lastIndexOf(".") );
		if ( ext == ".pdf" )
		{
			for ( QString igext : Files::ImageFilters )
			{
				QString tpath = path;
				tpath.replace( ".pdf", igext.remove("*") );
				if ( QFileInfo(tpath).exists() )
				{
					path = tpath;
					ext = igext;
					break;
				}
			}
		}
		return Files::ImageFilters.contains( "*"+ext );
	}
	static inline QVector<QVector<double>> getData( const QString& filepath )
	{
		QVector<QVector<double>> data;
		QFile file( filepath  );
		if ( file.open(QFile::ReadOnly) )
		{
			// get the content of the file
			QTextStream in( &file );
			bool isNumber = false;
			while ( !in.atEnd() )
			{
				QString line = in.readLine();
				QStringList temp = line.split(";");
				if ( temp.size() == 1 )
					temp = temp.first().split("\t");
				temp.first().toDouble(&isNumber);
				if ( isNumber )
				{
					QVector<double> lineNumber;
					for ( QString col : temp )
					{
						lineNumber.append( col.replace(',', '.').toDouble() );
					}
					data.append( lineNumber );
				}
			}
			file.close();
		}
		return data;
	}
	/**********************************************/
	/**********************************************/
	static inline void save( const QString& path, const QString& content, const QStringConverter::Encoding& codec = QStringConverter::Utf8 )
	{
		QFile saveFile( path );
		if ( saveFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
		{
			QTextStream out( &saveFile );
			out.setEncoding( codec );
			out << content;
			saveFile.close();
		}
	}
	/**********************************************/
	/**********************************************/
	static inline void save( const QString& path, const QStringList& content, const QStringConverter::Encoding& codec = QStringConverter::Utf8 )
	{
		QFile saveFile( path );
		if ( saveFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
		{
			QTextStream out( &saveFile );
			out.setEncoding( codec );
			for ( QString line : content )
				out << line << "\n";
			saveFile.close();
		}
	}
	/**********************************************/
	/**********************************************/
	static inline void save( const QString& path, const QVector<QVector<double>>& content, const QString& header, const QStringConverter::Encoding& codec = QStringConverter::Utf8 )
	{
		QFile saveFile( path );
		if ( saveFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
		{
			QTextStream out( &saveFile );
			out.setEncoding( codec );
			if ( !header.isEmpty() )
				out << header + "\n";
			for ( QVector<double> line : content )
			{
				int size = line.size();
				for ( int it = 0; it < size; ++it )
				{
					QString number = QString::number(line.at(it));
					if ( number == "nan" ) number = "";
					int nbChar = 13;
					if (it == 0) nbChar = 10;
					while ( number.size() < nbChar )
						number.insert( 0, " " );
					out << number;//QString("%1").arg(line.at(it), 15, 'g', 8);//QString::number(line.at(it));
					if ( it+1 == size )
						out << "\n";
					else
						out << ";";
				}
			}
			saveFile.close();
		}
	}
	static inline void saveB( const QString& path, const QVector<QVector<double>>& content, const QString& header, const QStringConverter::Encoding& codec = QStringConverter::Utf8 )
	{
		QFile saveFile( path );
		if ( saveFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
		{
			QTextStream out( &saveFile );
			out.setEncoding( codec );
			if ( !header.isEmpty() )
				out << header + "\n";
			for ( QVector<double> line : content )
			{
				int size = line.size();
				for ( int it = 0; it < size; ++it )
				{
					QString number = QString::number(line.at(it));
					int nbChar = 13;
					if (it == 0) nbChar = 10;
					while ( number.size() < nbChar )
						number.insert( 0, " " );
					out << number;//QString("%1").arg(line.at(it), 15, 'g', 8);//QString::number(line.at(it));
					if ( it+1 == size )
						out << "\n";
					else
						out << "\t";
				}
			}
			saveFile.close();
		}
	}
	static inline void save( const QString& path, const QVector<QStringList>& content, const QString& header, const QStringConverter::Encoding& codec = QStringConverter::Utf8 )
	{
		QFile saveFile( path );
		if ( saveFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
		{
			QTextStream out( &saveFile );
			out.setEncoding( codec );
			if ( !header.isEmpty() )
				out << header + "\n";
			for ( QStringList line : content )
			{
				int size = line.size();
				for ( int it = 0; it < size; ++it )
				{
					QString number = line.at(it);
					int nbChar = 13;
					if (it == 0) nbChar = 10;
					while ( number.size() < nbChar )
						number.insert( 0, " " );
					out << number;
					if ( it+1 == size )
						out << "\n";
					else
						out << ";";
				}
			}
			saveFile.close();
		}
	}
	/**********************************************/
	/**********************************************/
	static inline bool readFirstLine( const QString& path, QString& content, const QStringConverter::Encoding& codec = QStringConverter::Utf8 )
	{
		QFile file( path );
		if ( !file.open(QFile::ReadOnly) )
			return false;

		QTextStream in( &file );
		in.setEncoding( codec );
		content = in.readLine().toUtf8();
		file.close();
		return true;
	}
	/**********************************************/
	/**********************************************/
	static inline bool read( const QString& path, QString& content, const QStringConverter::Encoding& codec = QStringConverter::Utf8 )
	{
		QFile file( path );
		if ( !file.open(QFile::ReadOnly) )
			return false;

		QTextStream in( &file );
		in.setEncoding( codec );
		content = in.readAll().toUtf8();
		file.close();
		return true;
	}
	/**********************************************/
	/**********************************************/
	static inline bool read( const QString& path, QStringList& content, const QStringConverter::Encoding& codec = QStringConverter::Utf8 )
	{
		QString conts;
		if ( !Files::read( path, conts, codec ) )
			return false;
		QString splitter = "\n";
		if ( !conts.contains(splitter) )
			splitter = "\r";

		content = conts.split( splitter, Qt::SkipEmptyParts );
		return true;
	}
	/**********************************************/
	/**********************************************/
	static inline bool readData( const QString& path, QVector<QVector<double>>& content, const QStringConverter::Encoding& codec = QStringConverter::Utf8 )
	{
		content.clear();
		QStringList lines;
		if ( !Files::read( path, lines, codec ) )
			return false;
		for ( QString line : lines )
		{
			bool isNumber = false;
			int size = line.size();
			if ( line.count('\t') == size ||
			     line.count(';')  == size ||
			     line.count(',')  == size ||
			     line.count(' ')  == size )
				continue;
			line.replace( ',', '.' );
			QStringList temp;
			temp = line.split('\t');
			if ( line.contains(';') && !line.contains('\t') )
				temp = line.split(';');
			temp.first().toDouble( &isNumber );
			if ( isNumber )
			{
				QVector<double> lineNumber;
				for ( QString col : temp )
				{
					lineNumber.append( col.toDouble() );
				}
				content.append( lineNumber );
			}
		}
		return true;
	}
	/**********************************************/
	/**********************************************/
	static inline bool move( const QString& oldpath, const QString& newpath, bool forced = false )
	{
		if ( forced )
			if ( QFile::exists( newpath ) )
				QFile::remove( newpath );
		if ( !QFile::copy( oldpath, newpath ) )
			return false;
		QFile::remove( oldpath );
		return true;
	}
	/**********************************************/
	/**********************************************/
	static inline QDir createDir( const QString& dirpath, const QString& dirname )
	{
		QDir dir( dirpath );
		if ( !dir.exists( dirname ) )
			dir.mkdir( dirname );
		dir.cd( dirname );
		return dir;
	}
	/**********************************************/
	/**********************************************/
	static inline bool remove( const QString& path )
	{
		if ( QFile::exists( path ) )
			return QFile::remove( path );
		return false;
	}
	static inline void removeAll( const QString& path, const QString& basename, bool recursiveSearch = false )
	{
		for ( QString filepath : Files::ListFiles( path, basename, recursiveSearch ) )
			Files::remove( path + "/" +  filepath );
	}
	/**********************************************/
	/**********************************************/
	static inline QStringList ListMecaDirectories( const QString& path, bool recursiveSearch = false )
	{
		QStringList dirList = QDir(path).entryList(QDir::NoDotAndDotDot | QDir::Dirs );
		QStringList temp;
		for ( QString dir : dirList )
		{
			QFileInfo info( path + "/" + dir + "/" + Keys::fn_TESTINFO );
			QFileInfo meca( path + "/" + dir + "/" + Keys::fn_MecaData + ".dat" );
			QFileInfo meca2( path + "/" + dir + "/" + Keys::fn_MecaData + ".csv" );
			if ( info.exists() && ( meca.exists() || meca2.exists() ) )
				temp.append( path + "/" + dir );
			// Recursive Search
			if ( recursiveSearch )
				temp.append( Files::ListMecaDirectories( path + "/" + dir, recursiveSearch ) );
		}
		return temp;
	}
	/**********************************************/
	/**********************************************/
	static inline QStringList ListAllDirectories( const QString& path, bool recursiveSearch = false )
	{
		QStringList dirList = QDir(path).entryList(QDir::NoDotAndDotDot | QDir::Dirs );
		QStringList temp;
		temp.append( path );
		for ( QString dir : dirList )
		{
			if ( !dir.contains("CNI") )
				temp.append( Files::ListAllDirectories( path + "/" + dir, recursiveSearch ) );
		}
		return temp;
	}
	/**********************************************/
	/**********************************************/
	static inline QStringList ListDataFiles( const QString& path/*, bool recursiveSearch = false*/ )
	{
		QStringList list;
		QFileInfoList tmp = QDir(path).entryInfoList( {"*.dat", "*.csv"}, QDir::NoDotAndDotDot | QDir::Files );
		tmp.append( QDir(path+"/RE").entryInfoList( {"*.dat", "*.csv"}, QDir::NoDotAndDotDot | QDir::Files ));
		for ( QFileInfo tmpfile : tmp )
			list.append( tmpfile.absoluteFilePath().remove( path+"/" ) );
		tmp = QDir(path+"/cycles").entryInfoList( {"*.dat"}, QDir::NoDotAndDotDot | QDir::Files );
		for ( QFileInfo tmpfile : tmp )
			list.append( tmpfile.absoluteFilePath().remove( path+"/" ) );
		return list;
	}
	/**********************************************/
	/**********************************************/
	static inline QStringList ListFiles( const QString& path, const QString& basename, bool recursiveSearch = false )
	{
		QStringList dirList = Files::ListAllDirectories( path, recursiveSearch );
		QStringList list;
		for ( QString dir : dirList )
		{
			QFileInfoList tmp = QDir(dir).entryInfoList( {"*"+basename+".*"}, QDir::NoDotAndDotDot | QDir::Files );
			for ( QFileInfo tmpfile : tmp )
				list.append( tmpfile.absoluteFilePath().remove( path+"/" ) );
		}
		return list;
	}
	/**********************************************/
	/**********************************************/
	static inline QStringList ListImages( const QString& path, bool recursiveSearch = false )
	{
		QStringList dirList = Files::ListAllDirectories( path, recursiveSearch );
		QStringList list;
		for ( QString dir : dirList )
		{
			QFileInfoList tmp = QDir(dir).entryInfoList( Files::ImageFilters, QDir::NoDotAndDotDot | QDir::Files );
			for ( QFileInfo tmpfile : tmp )
				list.append( tmpfile.absoluteFilePath().remove( path+"/" ) );
		}
		return list;
	}
	/**********************************************/
	/**********************************************/
	static inline QStringList ListReports( const QString& path, bool recursiveSearch = false )
	{
		QStringList dirList = Files::ListAllDirectories( path, recursiveSearch );
		QStringList list;
		for ( QString dir : dirList )
		{
			QFileInfoList tmp = QDir(dir).entryInfoList( {"*.tex"}, QDir::NoDotAndDotDot | QDir::Files );
			for ( QFileInfo tmpfile : tmp )
				list.append( tmpfile.absoluteFilePath().remove( path+"/" ) );
		}
		return list;
	}
	/**********************************************/
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // FILE_H
