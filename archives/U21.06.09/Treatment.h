#ifndef TREATMENT_H
#define TREATMENT_H
/**********************************************/
#include <QApplication>
#include <QObject>
#include <QThread>
#include "InfoFile.h"
#include "TreatMeca.h"
#include "PointsCaract.h"
#include "Plots.h"
#include "Report.h"
#include "Settings.h"
#include "EA.h"
#include "CNI.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Thread
        : public QThread
{
    Q_OBJECT
    QString _path;
    void run() override
    {
        InfoFile* infoF = new InfoFile;
        infoF->setPath( _path );

        if ( Settings::getBool( Keys::TreatMeca ) )
        {
            emit currentTask( "Treating MECA data..." );
            TreatMeca meca( infoF );
            meca.treat();
        }
        if ( Settings::getBool( Keys::TreatPtsCaract ) )
        {
            emit currentTask( "Treating Caract Points..." );
            PointsCaract ptsCaract;
            ptsCaract.run( _path );
        }
        if ( Settings::getBool( Keys::TreatEA ) )
        {
            emit currentTask( "Treating EA..." );
            EA ea( infoF->path() );
            ea.treat();
        }
        if ( Settings::getBool( Keys::TreatRE ) )
        {
            emit currentTask( "Treating RE..." );
        }
        if ( Settings::getBool( Keys::TreatCNI ) )
        {
            emit currentTask( "Treating CNI..." );
            CNI cni( infoF );
            cni.treat();
        }
        if ( Settings::getBool( Keys::TreatPlots ) )
        {
            emit currentTask( "Creating Plots..." );
            Plots::GenerateMECA( infoF );
//            Plot plot( _path, _sets );
//            plot.generate();
        }
        if ( Settings::getBool( Keys::TreatSynthese ) )
        {
            emit currentTask( "Creating Report..." );
            Report report( infoF );
            report.generate();
        }

        delete infoF;
        emit finished();
    }

public:
    Thread( const QString& path )
    {
        _path = path;
    }
signals:
    void finished();
    void currentTask( const QString& task );
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Treatment
        : public QObject
{
    Q_OBJECT

public:
    void go( const QString& path )
    {
        Thread *thread = new Thread( path );
        connect( thread, &Thread::finished,
                 this, &Treatment::finished );
        connect( thread, &Thread::currentTask,
                 this, &Treatment::currentTask );
        thread->start();
    }

public slots:

signals:
    void finished();
    void currentTask( const QString& task );
};

// obj* ob = new obj;
// od->download(...); // ou treat(...) ?
/**********************************************/
/**********************************************/
/**********************************************/

#endif // TREATMENT_H
