#ifndef SCRIPT_H
#define SCRIPT_H

#include "Files.h"
#include <QProcess>
#include <QDebug>
#include <QMessageBox>
#include "Settings.h"
#include "InfoFile.h"
#include "Plots.h"

class Script
{
    QString _path;
    static inline bool _compare = false;
    static inline QString _cmdOpt;
    static inline QString _name;
    static inline bool _add = false;
    static inline QStringList _toAddFile;
    static inline QStringList _toAddLabel;
    static inline QStringList _toAddColors;
    static inline bool _addAll = false;
    static inline QVector<QStringList> _toAddAllFilters;

public:
    Script( QString path)
    {
        _path = path;
    }
    static inline void Launch( const QString& script = "" )
    {
        QStringList scriptLines = script.split("\n", Qt::SkipEmptyParts );
        for ( QString line : scriptLines )
            Script::LaunchLine( line );
    }
    static inline void LaunchLine( const QString& script = "" )
    {
        _compare = false;
        _add = false;
        _addAll = false;
        _toAddFile.clear();
        _toAddLabel.clear();
        _toAddColors.clear();
        _toAddAllFilters.clear();

        if ( script == "" )
            return;

        QStringList scriptList = Script::SplitLine( script );

        for ( int it = 0; it < scriptList.size(); ++it )
        {
            QString arg = scriptList.at(it);
            if ( arg == "compare" )
            {
                if ( it+1 < scriptList.size() )
                {
                    _compare = true;
                    ++it;
                    _cmdOpt = scriptList.at(it);
                }
            }
            else if ( arg == "lang" )
            {
                if ( it+1 < scriptList.size() )
                {
                    ++it;
                    if ( scriptList.at(it) == "EN" )
                        LaTeX::EnglishVersion();
                }
            }
            else if ( arg == "ext" )
            {
                if ( it+1 < scriptList.size() )
                {
                    ++it;
                    LaTeX::SetImageEXT( scriptList.at(it) );
                }
            }
            else if ( arg == "add" )
            {
                if ( it+1 < scriptList.size() )
                {
                    ++it;
                    QStringList toAddOpts = scriptList.at(it).split("/", Qt::KeepEmptyParts);
                    if ( toAddOpts.size() > 1 )
                    {
                        _add = true;
                        _toAddFile.append( toAddOpts.at(0) );
                        if ( toAddOpts.size() > 1 )
                        {
                            _toAddLabel.append( toAddOpts.at(1) );
                            if ( toAddOpts.size() > 2 )
                                _toAddColors.append( toAddOpts.at(2) );
                        }
                        else
                            _toAddLabel.append( "" );
                    }
                }
            }
            else if ( arg == "addAll" )
            {
                if ( it+1 < scriptList.size() )
                {
                    ++it;
                    _addAll = true;
                    _toAddAllFilters.append( Script::Filters(scriptList.at(it)) );
                }
            }
            else if ( arg == "name" )
            {
                if ( it+1 < scriptList.size() )
                {
                    ++it;
                    _name = scriptList.at(it);
                }
            }
            else
            {
                QMessageBox msgBox;
                msgBox.setText("Commande incomprise !");
                msgBox.setIcon(QMessageBox::Warning);
                msgBox.exec();
                return;
            }

        }

        if ( _compare )
        {
            QStringList dirlist = Files::ListMecaDirectories( Settings::getString( Keys::EssaiDir ), true );
            QStringList selectedList;
            if ( _add )
            {
                int it = 0;
                for ( QString fileName : _toAddFile )
                {
                    QStringList filters = Script::Filters(fileName);
                    Script::Add( selectedList, dirlist, filters, it );
//                    for ( QString dir : dirlist )
//                    {
//                        if( Script::Compare( dir, filters ) )
//                        {
//                            selectedList.append( dir );
//                            if ( _toAddLabel.at(it).isEmpty() )
//                            {
//                                InfoFile infoF(dir);
//                                _toAddLabel.replace( it, infoF.eprouvette() );
//                            }
//                            break;
//                        }
//                    }
                    ++it;
                }
//                qDebug() << _toAddFile;
//                qDebug() << _toAddLabel;
            }
            if ( _addAll )
                for ( QStringList filters : _toAddAllFilters )
                    Script::Add( selectedList, dirlist, filters );
//                    for ( QString dir : dirlist )
//                    {
//                        if ( Script::Compare( dir, filters ) )
//                        {
//                            selectedList.append( dir );
//                            InfoFile infoF(dir);
//                            _toAddLabel.append( infoF.eprouvette() );
//                        }
//                    }
            if ( _name.isEmpty() )
            {
                _name = _cmdOpt;
                for ( QString file : _toAddFile )
                    _name += "_" + file;
            }
            Plots::GenerateComparaison( _name, _cmdOpt, selectedList, _toAddLabel, _toAddColors );
        }

    }
    static inline QStringList Filters( const QString& filters )
    {
        QStringList _filters;
        QString tmp_filter;
        for ( int it = 0; it < filters.size(); ++it )
        {
            if ( it + 1 == filters.size() )
            {
                tmp_filter += filters.at(it);
                if ( !tmp_filter.isEmpty() )
                    _filters.append( tmp_filter );
                break;
            }
            if ( filters.at(it) == "&" || filters.at(it) == "!" )
            {
                if ( !tmp_filter.isEmpty() )
                    _filters.append( tmp_filter );
                tmp_filter = filters.at(it);
            }
            else
                tmp_filter += filters.at(it);
        }
        return _filters;
    }
    static inline bool Compare( const QString& dir, const QStringList& filters )
    {
        bool toTake = true;
        for ( QString filter : filters )
        {
            QString mark = filter.at(0);
            QString tmpfilter = filter.remove(0, 1);
            if ( mark == "&" )
                toTake = toTake && dir.contains(tmpfilter);
            else if ( mark == "!" )
                toTake = toTake && !dir.contains(tmpfilter);
        }
        return toTake;
    }
    static inline QStringList SplitLine( const QString& line )
    {
        QStringList spliting;
        bool _searchSpace = true;
        bool _searchOPT = false;
        QString tmp;
        for ( int it = 0; it < line.size(); ++it )
        {
            QString tmpC = line.at(it);
            if ( it +1 == line.size() )
            {
                if ( tmpC != "\"" && tmpC != " " )
                    tmp += tmpC;
                spliting.append( tmp );
            }
            else if ( (_searchSpace && tmpC == " ") )
            {
                spliting.append( tmp );
                tmp.clear();
            }
            else if ( tmpC == "\"" )
            {
                _searchOPT = !_searchOPT;
                if ( _searchOPT )
                    _searchSpace = false;
                else
                    _searchSpace = true;
            }
            else
                tmp += tmpC;
        }
        return spliting;
    }

    static inline void Add( QStringList& selectedList, const QStringList& dirlist, const QStringList& filters, int it = -1 )
    {
        for ( QString dir : dirlist )
        {
            if ( Script::Compare( dir, filters ) )
            {
                selectedList.append( dir );
                if ( it > -1 )
                {
                    if ( _toAddLabel.at(it).isEmpty() )
                    {
                        InfoFile infoF(dir);
                        _toAddLabel.replace( it, infoF.eprouvette() );
                    }
                    break;
                }
                else
                {
                    InfoFile infoF(dir);
                    _toAddLabel.append( infoF.eprouvette() );
                }
            }
        }
    }
};

#endif // SCRIPT_H
