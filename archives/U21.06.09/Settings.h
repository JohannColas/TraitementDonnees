#ifndef SETTINGS_H
#define SETTINGS_H
/**********************************************/
#include <QApplication>
#include <QDir>
#include <QFile>
#include <QIcon>
#include <QSettings>
#include <QDebug>
#include "System.h"
#include "Files.h"
/**********************************************/
namespace Keys {
    static QString EssaiDir = "EssaiDir";
    static QString LaTeXDir = "LaTeXDir";
    static QString PythonDir = "PythonDir";
    static QString RecursiveSearch = "RecursiveSearch";
    static QString Lissage = "Lissage";
    static QString Type = "Type";
    static QString nbPoints = "nbPoints";
    static QString Plots = "Plots";
    static QString Reports = "Reports";
    static QString TplDir = "TemplateDir";
    static QString TreatMeca = "TreatMeca";
    static QString TreatPlots = "TreatPlots";
    static QString TreatPtsCaract = "TreatPtsCaract";
    static QString TreatEA = "TreatEA";
    static QString TreatRE = "TreatRE";
    static QString TreatCNI = "TreatCNI";
    static QString TreatSynthese = "TreatSynthese";
    static QString CNI = "Reports";
    static QString lighterFactor = "lighterFactor";
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SettingsFile
        : public QObject
{
    Q_OBJECT
private:
    QSettings* _settings = 0;

public:
    SettingsFile( const QString& path )
    {
        _settings = new QSettings( path, QSettings::IniFormat );
    }
    QVariant value( const QString& key,  const QString& subkey = "" )
    {
        if ( _settings )
        {
            QString _key = key;
            if ( subkey != "" )
                _key += "/" + subkey;
            QVariant var = _settings->value( _key );
            if ( var.isValid() )
                return var;
        }
        return QVariant();
    }
    bool getBool( const QString& key,  const QString& subkey = "" )
    {
        QVariant var = value( key, subkey );
        if ( var.isValid() )
            return var.toBool();
        return false;
    }
    QString getString( const QString& key, const QString& subkey = "" )
    {
        QVariant var = value( key, subkey );
        if ( var.isValid() )
            return var.toString();
        return "";
    }
    int getInt( const QString& key, const QString& subkey = "" )
    {
        QVariant var = value( key, subkey );
        if ( var.isValid() )
            return var.toInt();
        return -1;
    }
    double getDouble( const QString& key, const QString& subkey = "" )
    {
        QVariant var = value( key, subkey );
        if ( var.isValid() )
            return var.toDouble();
        return -1;
    }
    void save( const QString& key, const QVariant& value )
    {
        _settings->setValue( key, value );
    }
    void save( const QVariant& value, const QString& key, const QString& subkey = ""  )
    {
        QString _key = key;
        if ( subkey != "" )
            _key += "/" + subkey;
        _settings->setValue( _key, value );
    }
};


class Settings
{
private:
    static inline SettingsFile* _settings = new SettingsFile( QDir::currentPath() + "/settings.ini" );

public:
    static inline QVariant value( const QString& key,  const QString& subkey = "" )
    {
        if ( _settings )
        {
            QString _key = key;
            if ( subkey != "" )
                _key += "/" + subkey;
            QVariant var = _settings->value( _key );
            if ( var.isValid() )
                return var;
        }
        return QVariant();
    }
    static inline bool getBool( const QString& key,  const QString& subkey = "" )
    {
        QVariant var = _settings->value( key, subkey );
        if ( var.isValid() )
            return var.toBool();
        return false;
    }
    static inline QString getString( const QString& key, const QString& subkey = "" )
    {
        QVariant var = _settings->value( key, subkey );
        if ( var.isValid() )
            return var.toString();
        return "";
    }
    static inline int getInt( const QString& key, const QString& subkey = "" )
    {
        QVariant var = _settings->value( key, subkey );
        if ( var.isValid() )
            return var.toInt();
        return -1;
    }
    static inline double getDouble( const QString& key, const QString& subkey = "" )
    {
        QVariant var = _settings->value( key, subkey );
        if ( var.isValid() )
            return var.toDouble();
        return -1;
    }
    static inline void save( const QVariant& value, const QString& key, const QString& subkey = ""  )
    {
        QString _key = key;
        if ( subkey != "" )
            _key += "/" + subkey;
        _settings->save( _key, value );
    }
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SETTINGS_H
