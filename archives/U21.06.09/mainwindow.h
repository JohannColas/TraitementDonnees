#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QFileSystemModel>
#include <QLabel>
#include <QMainWindow>
#include "Files.h"
#include "InfoFile.h"
#include "Plots.h"
#include "Lissage.h"
#include "Settings.h"
#include "PointsCaract.h"
#include "EA.h"
#include "Script.h"
//#include "CNI.h"
#include "Treatment.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void saveInfo();
    void updateInfoView();
    void clearContainer();

protected slots:
    void closeEvent( QCloseEvent* event ) override
    {
        QMainWindow::closeEvent( event );
    }

    void on_pb_dirEssai_released();
    void on_pb_plotsTplPath_released();
    void on_pb_reportsTplPath_released();
    void on_pb_pythonPath_released();
    void on_cb_meca_toggled( bool checked );
    void on_cb_ptsCaract_toggled( bool checked );
    void on_cb_EA_toggled( bool checked );
    void on_cb_RE_toggled( bool checked );
    void on_cb_CNI_toggled( bool checked );
    void on_cb_plots_toggled( bool checked );
    void on_cb_synthese_toggled( bool checked );

    void on_treeView_clicked( const QModelIndex &index );
    void on_treeView_doubleClicked( const QModelIndex &index );

    void on_sel_essai_released();
    void on_sel_graphs_released();
    void on_sel_script_released();
    void on_sel_parametres_released();

    void on_tabWidget_currentChanged( int index );

    void on_meca_rawdata_released();
    void on_meca_traite_released();
    void on_meca_ptsCaract_released();

    void updatePlotSelector();
    void on_plotSelector_currentTextChanged( const QString &arg1 );

    void on_pb_ptsCaract_clicked();

    void on_pb_traiterCetEssai_released();
    void on_pb_toutTraiter_released();
    void treatDirectory( const QString& path );
//    QStringList listDirectories( const QString& path );
    void stopTreatment();
    void on_traite_EA_released();
    void on_traite_RE_released();
    void on_traite_CNI_released();
    void on_pb_CNIVideo_released();

private slots:
    void on_sp_seuilAmp_valueChanged( int value );
    void on_sp_seuilTime_valueChanged( int value );
    void on_sp_seuilRise_valueChanged( int value );
    void on_sp_seuilDur_valueChanged( int value );
    void on_sp_seuilCps_valueChanged( int value );
    void on_sp_seuilEner_valueChanged( int value );

    void on_cmd_Script_editingFinished();
    void on_sp_LissageMGnbPts_valueChanged(int nbPts);

    void on_pb_script_help_released();

    void on_pb_modPlot_released();

    void on_pb_compile_released();

private:
    Ui::MainWindow *ui;
    QFileSystemModel *model = new QFileSystemModel;
    InfoFile* info_file = new InfoFile;
    bool _stopTreatement = false;
    bool _treatAll = false;
    EA_FilterFile* EA_filterfile = new EA_FilterFile;
    Treatment treat;
    QString plotPath;
};
#endif // MAINWINDOW_H
