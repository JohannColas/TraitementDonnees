#ifndef LISSAGE_H
#define LISSAGE_H

#include <QVector>
#include "Settings.h"


class Lissage
{

public:
    static inline void MoyenneGlissee( QVector<QVector<double>>& _data )
    {
        if ( _data.size() == 0 )
            return;

        int nbPts = Settings::getInt( Keys::Lissage, Keys::nbPoints );
        int hNbPts = 0.5*nbPts;
        int size = _data.size();
        QVector<QVector<double>> temp = _data;
        for ( int it = 0; it < size; ++it )
        {
            if ( it < hNbPts || it > size - hNbPts - 1 )
                for ( int jt = 0; jt < _data[it].size(); ++jt )
                    temp[it][jt] = _data[it][jt];
            else
            {
                temp[it][0] = _data[it][0];
                for ( int jt = 1; jt < _data[it].size(); ++jt )
                {
                    double somme = 0;
                    int count = 0;
                    for ( int lt = it - hNbPts; lt <= it + hNbPts; ++lt )
                    {
                        somme += _data[lt][jt];
                        ++count;
                    }
                    temp[it][jt] = somme/count;
                }
                _data = temp;
            }
        }
    }
};

#endif // LISSAGE_H
