#ifndef FILE_H
#define FILE_H

#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QVector>
#include <QDebug>

class Files
{

public:
    static inline QVector<QVector<double>> getData( const QString& filepath )
    {
        QVector<QVector<double>> data;
        QFile file( filepath  );
        if ( file.open(QFile::ReadOnly) )
        {
            // get the content of the file
            QTextStream in( &file );
            bool isNumber = false;
            while ( !in.atEnd() )
            {
                QString line = in.readLine();
                QStringList temp = line.split(";");
                if ( temp.size() == 1 )
                    temp = temp.first().split("\t");
                temp.first().toDouble(&isNumber);
                if ( isNumber )
                {
                    QVector<double> lineNumber;
                    for ( QString col : temp )
                    {
                        lineNumber.append( col.replace(',', '.').toDouble() );
                    }
                    data.append( lineNumber );
                }
            }
            file.close();
        }
        return data;
    }
    static inline void save( const QString& path, const QString& content, const char* codec = "UTF-8" )
    {
        QFile saveFile( path );
        saveFile.open( QIODevice::WriteOnly | QIODevice::Text );
        QTextStream out( &saveFile );
        out.setCodec( codec );
        out << content;
        saveFile.close();
    }
    static inline void save( const QString& path, const QStringList& content, const char* codec = "UTF-8" )
    {
        QFile saveFile( path );
        saveFile.open( QIODevice::WriteOnly | QIODevice::Text );
        QTextStream out( &saveFile );
        out.setCodec( codec );
        for ( QString line : content )
            out << line << "\n";
        saveFile.close();
    }
    static inline void save( const QString& path, const QVector<QVector<double>>& content, const QString& header, const char* codec = "UTF-8" )
    {
        QFile saveFile( path );
        saveFile.open( QIODevice::WriteOnly | QIODevice::Text );
        QTextStream out( &saveFile );
        out.setCodec( codec );
        out << header + "\n";
        for ( QVector<double> line : content )
        {
            int size = line.size();
            for ( int it = 0; it < size; ++it )
            {
                out << QString::number(line.at(it));
                if ( it == size-1 )
                    out << "\n";
                else
                    out << ";";
            }
        }
        saveFile.close();
    }
    static inline bool read( const QString& path, QString& content, const char* codec = "UTF-8" )
    {
        QFile file( path );
        if ( !file.open(QFile::ReadOnly) )
            return false;

        QTextStream in( &file );
        in.setCodec( codec );
        content = in.readAll().toUtf8();
        file.close();
        return true;
    }
    static inline bool read( const QString& path, QStringList& content, const char* codec = "UTF-8" )
    {
        QString conts;
        if ( !Files::read( path, conts, codec ) )
            return false;
        QString splitter = "\n";
        if ( !conts.contains(splitter) )
            splitter = "\r";

        content = conts.split( splitter, Qt::SkipEmptyParts );
//        if ( conts.contains())
//        QFile file( path );
//        if ( !file.open(QFile::ReadOnly) )
//            return false;
//        QTextStream in( &file );
//        in.setCodec( codec );
//        while ( !in.atEnd() )
//        {

//            content.append( in.readLine().toUtf8() );
//        }
//        file.close();
        return true;
    }
    static inline bool move( const QString& oldpath, const QString& newpath, bool forced = false )
    {
        if ( forced )
            if ( QFile::exists( newpath ) )
                QFile::remove( newpath );
        if ( !QFile::copy( oldpath, newpath ) )
            return false;
       QFile::remove( oldpath );
       return true;
    }
    static inline QDir createDir( const QString& dirpath, const QString& dirname )
    {
        QDir dir( dirpath );
        if ( !dir.exists( dirname ) )
            dir.mkdir( dirname );
        dir.cd( dirname );
        return dir;
    }
    static inline bool remove( const QString& path)
    {
        if ( QFile::exists( path ) )
            return QFile::remove( path );
       return false;
    }
    static inline QStringList ListMecaDirectories( const QString& path, bool recursiveSearch = false )
    {
        QStringList dirList = QDir(path).entryList(QDir::NoDotAndDotDot | QDir::Dirs );
        QStringList temp;
        for ( QString dir : dirList )
        {
            QFileInfo info( path + "/" + dir + "/ESSAI.info" );
            QFileInfo meca( path + "/" + dir + "/MECA.dat" );
            QFileInfo meca2( path + "/" + dir + "/MECA.csv" );
            if ( info.exists() && ( meca.exists() || meca2.exists() ) )
                temp.append( path + "/" + dir );
            // Recursive Search
            if ( recursiveSearch )
                temp.append( Files::ListMecaDirectories( path + "/" + dir, recursiveSearch ) );
        }
        return temp;
    }
    static inline QStringList ListAllDirectories( const QString& path, bool recursiveSearch = false )
    {
        QStringList dirList = QDir(path).entryList(QDir::NoDotAndDotDot | QDir::Dirs );
//        qDebug() << dirList;
        QStringList temp;
        temp.append( path );
        for ( QString dir : dirList )
        {
            if ( !dir.contains("CNI") )
                temp.append( Files::ListAllDirectories( path + "/" + dir, recursiveSearch ) );
        }
        return temp;
    }
    static inline QStringList ListImages( const QString& path, bool recursiveSearch = false )
    {
        QStringList dirList = Files::ListAllDirectories( path, recursiveSearch );
        QStringList list;
        for ( QString dir : dirList )
        {
            QFileInfoList tmp = QDir(dir).entryInfoList( {"*.png", "*.tiff", "*.jpg"}, QDir::NoDotAndDotDot | QDir::Files );
            for ( QFileInfo tmpfile : tmp )
                list.append( tmpfile.absoluteFilePath().remove( path+"/" ) );
        }
        return list;
    }
};

#endif // FILE_H
