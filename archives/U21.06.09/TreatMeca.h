#ifndef TREATMECA_H
#define TREATMECA_H

#include <QFile>
#include "Files.h"
#include "LaTeX.h"
#include <QTextStream>
#include <QDebug>

#include "Settings.h"
#include "InfoFile.h"
#include "Lissage.h"
#include <cmath>

#include <iostream>
using namespace std;

class TreatMeca
{
    enum Program
    {
        NONE,
        R8562,
        RESIST,
        TRACMONO,
        TRACTION,
        FLEXION,
        FLEXION1_2,
        IOSI3J,
        VIEIL
    };

    InfoFile* _info = 0;
    QString _path = "";
    TreatMeca::Program _prog = TreatMeca::NONE;
    int index_TIME = -1;
    int index_DEP = -1;
    int index_FORCE = -1;
    int index_DEF1 = -1;
    int index_DEF2 = -1;
    int index_DEF3 = -1;
    int index_CTR = -1;
    int index_RE = -1;
    int index_EA = -1;
    int index_TEMP = -1;
    bool _tempFour = false;
    int _tempFocused = 980;
    QStringList header;
    QVector<QVector<double>> data;
    QVector<QVector<double>> treated_data;
    QVector<QVector<double>> treated_data_focused;
    QVector<QVector<double>> treated_rawdata;
    QString header_data;
    QString header_rawdata;

public:
    TreatMeca( InfoFile* info_file = 0 )
    {
        _info = info_file;
        if ( _info != 0)
            _path = _info->path();
    }
    void treat()
    {
        if ( readMecaFile() )
        {
            treatData();
            _info->save();

            Lissage::MoyenneGlissee( treated_data );

            Files::save( _path + "/MECA_DEF_CONT.dat", treated_data, header_data );
            if ( treated_rawdata.size() > 0 )
            {
                Lissage::MoyenneGlissee( treated_rawdata );
                Files::save( _path + "/MECA_DEP_FORCE.dat", treated_rawdata, header_rawdata );
            }
            if ( treated_data_focused.size() > 0 )
            {
                Lissage::MoyenneGlissee( treated_data_focused );
                Files::save( _path + "/MECA_DEF_CONT_focused.dat", treated_data_focused, header_data );
            }
        }
    }
    bool readMecaFile()
    {
        header.clear();
        data.clear();
        QStringList contents;
        if ( !Files::read( _path + "/MECA.dat", contents, "ISO 8859-1") )
            if ( !Files::read( _path + "/MECA.csv", contents, "ISO 8859-1") )
                return false;

        bool isNumber = false;
        for ( QString line : contents )
        {
            if ( line.count('\t') == line.size() || line.count(';') == line.size() || line.count(',') == line.size() || line.count(' ') == line.size() )
                continue;
            line.replace( ',', '.' ); // Replace for number
            QStringList temp;
            temp = line.split('\t');
            if ( line.contains(';') && !line.contains('\t') )
                temp = line.split(';');
            // Checking if is Number
            temp.first().toDouble( &isNumber );
            if ( !isNumber )
            {
                header.append( temp.first() );
                if ( temp.first().contains(">>") )
                {
                     if ( temp.first().contains("R8562.BAS") )
                         _prog = Program::R8562;
                     else if ( temp.first().contains("RESIST.BAS") )
                         _prog = Program::RESIST;
                     else if ( temp.first().contains("TRACMONO.BAS") )
                         _prog = Program::TRACMONO;
                     else if ( temp.first().contains("TRACTION.BAS") )
                         _prog = Program::TRACTION;
                     else if ( temp.first().contains("FLEXION.BAS") )
                         _prog = Program::FLEXION;
                     else if ( temp.first().contains("FLEXION1.BAS") || temp.first().contains("FLEXION2.BAS") )
                         _prog = Program::FLEXION1_2;
                     else if ( temp.first().contains("IOSI3J.BAS") )
                         _prog = Program::IOSI3J;
                }
                else if ( temp.first().contains("Temps total (s)") )
                    _prog = Program::VIEIL;
            }
            else
            {
                QVector<double> lineNumber;
                for ( QString col : temp )
                {
                    lineNumber.append( col.toDouble() );
                }
                data.append( lineNumber );
            }
        }
        return true;
    }
    void treatData()
    {
        QString suiviDispo;
        if ( _prog == Program::R8562 )
        {
            suiviDispo = "R.E.";
            index_TIME = 0;
            index_FORCE = 1;
            index_DEF1 = 2;
            index_DEF2 = 3;
            index_RE = 4;
        }
        else if ( _prog == Program::RESIST )
        {
            suiviDispo = "R.E. - E.A.";
            index_TIME = 5;
            index_CTR = 0;
            index_DEF1 = 1;
            index_DEF2 = 2;
            index_RE = 4;
            index_EA = 3;
        }
        else if ( _prog == Program::TRACTION )
        {
            suiviDispo = "E.A.";
            index_TIME = 4;
            index_CTR = 0;
            index_DEF1 = 1;
            index_DEF2 = 2;
            index_EA = 3;
        }
        else if ( _prog == Program::TRACMONO )
        {
            suiviDispo = "E.A.";
            index_TIME = 5;
            index_DEP = 3;
            index_FORCE = 0;
            index_DEF1 = 1;
            index_DEF2 = 2;
            index_CTR = 6;
            index_EA = 4;
        }
        else if ( _prog == Program::FLEXION )
        {
            suiviDispo = "E.A.";
            index_TIME = 3;
            index_DEP = 1;
            index_FORCE = 0;
            index_DEF1 = 6;
            index_CTR = 5;
            index_EA = 2;
        }
        else if ( _prog == Program::FLEXION1_2 )
        {
            suiviDispo = "R.E. - E.A.";
            index_TIME = 4;
            index_DEP = 1;
            index_FORCE = 0;
            index_RE = 3;
            index_EA = 2;
        }
        else if ( _prog == Program::IOSI3J )
        {
            suiviDispo = "R.E. - E.A.";
            index_TIME = 3;
            index_DEP = 1;
            index_FORCE = 0;
            index_DEF1 = 4;
            index_DEF2 = 5;
            index_DEF3 = 6;
            index_RE = 7;
            index_EA = 2;
        }
        else if ( _prog == Program::VIEIL )
        {
            index_TIME = 0;
            index_DEP = 8;
            index_FORCE = 7;
            index_TEMP = 9;
            if ( _path.contains("VIEIL_1300") )
                _tempFocused = 1280;
            QString rmqs;
            if ( Files::read( _path+"/Remarques.txt", rmqs, "ISO 8859-1" ) )
            {
                rmqs.remove("\r");
                QStringList coms = rmqs.split("\n");
                for ( QString com : coms )
                {
                    if ( (com.contains("sservissement") || com.contains("égulation")) &&
                         com.contains("thermocouple") && com.contains("four") && !com.contains("erreur") )
                        _tempFour = true;
                    if ( !_info->commentaire().contains(com) )
                        _info->setCommentaire( _info->commentaire() + com + "\n" );
                }
//                if ( (rmqs.contains("sservissement") || rmqs.contains("égulation")) &&
//                     rmqs.contains("thermocouple") && rmqs.contains("four") && !rmqs.contains("erreur") )
//                    _tempFour = true;
//                if ( !_info->commentaire().contains(rmqs) )
//                    _info->setCommentaire( _info->commentaire() + "\n" + rmqs + "\n" );
            }
        }

        if ( _info->donneesDispo().isEmpty() )
            _info->setDonneesDispo(suiviDispo);
        if ( !_info->donneesDispo().contains( "R.E." ) )
            index_RE = -1;
        if ( !_info->donneesDispo().contains( "E.A." ) )
            index_EA = -1;

        header_data = "t(s);e(perc);s(MPa)";
        if ( _prog == Program::VIEIL )
            header_data += ";T(C)";
        if ( index_RE != -1 )
            header_data += ";RE(mOhm);dR_R0(perc)";
        if ( index_EA != -1 )
            header_data += ";EA(cps)";
        if ( index_DEP != -1 && index_FORCE != -1 )
        {
            header_rawdata = "t(s);fle(mm);forc(N)";
            if ( index_RE != -1 )
                header_rawdata += ";RE(mOhm);dR_R0(perc)";
            if ( index_EA != -1 )
                header_rawdata += ";EA(coups)";
        }
        // Get RawData
        double L = _info->longueur();
        double b = _info->largeur();
        double e = _info->epaisseur();
        double tim = -1;
        double dep = -1;
        double dep0 = -1;
        if ( index_DEP != -1)
            dep0 = data[0][index_DEP];
        double frc = -1;
        double def = -1;
        double ctr = -1;
        double re = -1;
        double R0 = -1;
        double dRdR0 = 0;
        if ( index_RE != -1)
        {
            R0 = data[0][index_RE];
            if ( R0 < 1 )
                R0 *= 1000;
        }
        double ea = -1;
        double temp = -1;
        for ( int it = 0; it < data.size(); ++it )
        {
            QVector<double> line = data[it];
            tim = line[index_TIME];
            // Get deplacement
            if ( index_DEP != -1)
                dep = line[index_DEP];
            // Get Force
            if ( index_FORCE != -1)
                frc = line[index_FORCE];
            // Get Strain
            if ( _prog == Program::FLEXION1_2 )
                def = 100*(6*e/(L*L))*line[index_DEP];
            else if ( _prog == Program::IOSI3J )
                def = line[index_DEF1] - line[index_DEF2];
            else if ( _prog == Program::VIEIL )
            {
                def = 100*(dep - dep0)/L;
                if ( L == -1 )
                    def = -1;
            }
            else if ( index_DEF1 != -1 )
            {
                if ( index_DEF2 == -1 )
                    def = line[index_DEF1];
                else
                    def = 0.5*( line[index_DEF1] + line[index_DEF2] );
            }
            // Get Stress
            if ( _prog == Program::FLEXION1_2 )
                ctr = 1.5*(L/(b*e*e))*line[index_FORCE];
            else if ( _prog == Program::IOSI3J || _prog == Program::R8562 || _prog == Program::VIEIL )
            {
                ctr = frc/(b*e);
                if ( e == -1 )
                    ctr *= -1;
            }
            else if ( index_CTR != -1)
                ctr = line[index_CTR];
            // get RE
            if ( index_RE != -1 )
            {
                re = line[index_RE];
                if ( re < 1 )
                    re *= 1000; // convert in mOhm
                dRdR0 = 100*( re - R0 )/R0;
            }
            // get EA
            if ( index_EA != -1 )
                ea = line[index_EA];
            // get Temperature
            if ( index_TEMP != -1 )
            {
                temp = line[index_TEMP];
                if ( _tempFour )
                    temp = - 0.0000005127092*pow(temp, 3)
                            + 0.0007545365*pow(temp, 2)
                            + 0.9624921*temp
                            + 5.306989;
            }
            // Save data
            if ( /*def >= -0.001 &&*/
                 (line[index_TIME] < _info->tcut() || _info->tcut() == -1 ) )
            {
                QVector<double> dataLine = { tim, def, ctr };
                if ( index_TEMP != -1 )
                    dataLine.append( temp );
                if ( index_RE != -1 )
                    dataLine.append( {re, dRdR0} );
                if ( index_EA != -1 )
                    dataLine.append( ea );
                treated_data.append( dataLine );
                if ( index_TEMP != -1 && temp > _tempFocused )
                    treated_data_focused.append( dataLine );

                if ( index_DEP != 1 && index_FORCE != -1 )
                {
                    QVector<double> rawdataLine = { tim, dep, frc };
                    if ( index_TEMP != -1 )
                        rawdataLine.append( temp );
                    if ( index_RE != -1 )
                        rawdataLine.append( {re, dRdR0} );
                    if ( index_EA != -1 )
                        rawdataLine.append( ea );
                    treated_rawdata.append( rawdataLine );
                }
            }
        }
    }
};

#endif // TREATMECA_H
