#ifndef REPORT_H
#define REPORT_H

#include "Files.h"
#include <QProcess>
#include <QDebug>
#include "Settings.h"
#include "InfoFile.h"
#include "LaTeX.h"

class Report
{
    InfoFile* _info = 0;

public:
    Report( InfoFile* info = 0)
    {
        _info = info;
    }
    void generate()
    {
        if ( _info != 0 )
        {
            QString prefix;
            if ( _info->essai().contains("TRAC_") )
            {
                prefix = "TRAC";
                if ( _info->epaisseur() == -1 )
                    prefix = "TRAC_SECTION";
            }
            else if ( _info->essai().contains("FLEX_") )
            {
                prefix = "FLEX";
            }
            else if ( _info->essai().contains("IOSI_") ||
                      _info->essai().contains("CISA_") )
            {
                prefix = "CISA";
            }
            else if ( _info->essai().left(6) == "VIEIL_" )
            {
                prefix = "VIEIL";
                if ( _info->epaisseur() == -1 )
                    prefix = "VIEIL_SECTION";
            }
            if ( !prefix.isEmpty() )
            {
                QString reportName = "synthese";
                // Create TeX File
                createTeX( prefix, reportName );
            }
        }
    }
    void createTeX( const QString& prefix, const QString& reportName )
    {
        QString tplPath = Settings::getString( Keys::Reports, Keys::TplDir);
        QString TeXpath = tplPath + "/" + prefix + ".tex";
        QString contents ;
        if ( !Files::read( TeXpath, contents ) )
            return;
        contents.replace("%%DIRESSAI%%", _info->path());
        contents.replace("%%EPROUV%%", _info->eprouvette());
        QString essai = _info->essai();
        QString type;
        if ( essai.contains("TRAC") )
            type = "Traction ";
        else if ( essai.contains("FLEX") )
            type = "Flexion ";
        else if ( essai.contains("IOSI") )
            type = "Iosipescu ";
        else if ( essai.contains("RAIL") )
            type = "Shear Rail ";
        else if ( essai.left(5) == "VIEIL" )
            type = "Vieillissement";
        if ( essai.contains("MONO") )
            type += "monotone";
        else if ( essai.contains("CYCL") )
        {
            type += "cyclé";
            if ( essai.contains("TRAC") ||
                 essai.contains("FLEX") )
                type += "e";
        }
        contents.replace("%%TYPE%%", type);
        QString vieil = "Non-vieilli";
        if ( essai.contains("VIEIL_") )
        {
            int ind = essai.indexOf("VIEIL_") + 6;
            QString mid = essai.mid( ind, 4);
            vieil = "Vieilli à " + mid + "\\,\\textcelsius";
        }
        contents.replace("%%VIEIL%%", vieil);
        contents.replace("%%LONGUEUR%%", QString::number(_info->longueur()));
        contents.replace("%%LARGEUR%%", QString::number(_info->largeur()));
        contents.replace("%%EPAISSEUR%%", QString::number(_info->epaisseur()));
        if ( !essai.left(6).contains("VIEIL_") )
        {
            QString temp = "20";
            if ( essai.contains("_1300_VIEIL") || essai.contains("_1300 ") )
            {
                temp = "1300";
            }
            else if ( essai.contains("_1000_VIEIL") || essai.contains("_1000 ") )
            {
                temp = "1000";
            }
            contents.replace("%%TEMP%%", temp);
        }
        contents.replace("%%VITES%%", _info->vitesseEssai());
        QString suivi = "Aucun";
        QString suiviAB = "";
        if ( _info->donneesDispo().contains("R.E.") )
        {
            suivi = "Résistance électrique";
            suiviAB = "-RE";
        }
        if ( _info->donneesDispo().contains("E.A.") )
        {
            suiviAB = "-EA" + suiviAB;
            if ( _info->donneesDispo().contains("R.E.") )
                suivi += " - Émission acoustique";
            else
                suivi = "Émission acoustique";
        }
        contents.replace("%%SUIVI%%", suivi);
        contents.replace("%%SUIVIAB%%", suiviAB);
        contents.replace("%%COMMENT%%", _info->commentaire().replace("\n","\\\\\n"));

        QString prop_path = _info->path() + "/meca/PROP.tex";
        QFileInfo prop( prop_path );
        if ( prop.exists() )
        {
            contents += "\n\n";
            contents += QString("\t\\subsubsection{Propriétés}\n");
            contents += "\n";
            contents += QString("\t\t\\begin{jtable}{\\textwidth}{Éprouvette \\eprouvette; Propriétés initiales}{\\eprouvette-PROP}\n");
            contents += QString("\t\t\t\\input{" + prop_path + "}\n");
            contents += "\t\t\\end{jtable}\n";
        }
        QString prop_cycl_path = _info->path() + "/meca/PROP_CYCL.tex";
        QFileInfo prop_cycl( prop_cycl_path );
        if ( prop_cycl.exists() )
        {
            contents += "\n";
            contents += QString("\t\t\\begin{jtable}{\\textwidth}{Éprouvette \\eprouvette; Paramètres des boucles d'hystérésis}{\\eprouvette-PROP-CYCL}\n");
            contents += QString("\t\t\t\\input{" + prop_cycl_path + "}\n");
            contents += "\t\t\\end{jtable}\n";
        }

        QString Reportpath = _info->path() + "/" + reportName + ".tex";
        Files::save( Reportpath, contents );

        // Create Synthese PDF
        TeXpath = tplPath + "/synthese.tpl.tex";
        contents.clear();
        if ( !Files::read( TeXpath, contents ) )
            return;
        contents.replace("%%MATERIAU%%", _info->materiau() );
        contents.replace("%%FILEPATH%%", _info->path() + "/synthese.tex" );
        Reportpath = QDir::currentPath() + "/LaTeX/synthese.tex";
        QDir dir( QDir::currentPath() );
        if ( !dir.exists("LaTeX") )
            dir.mkdir( "LaTeX" );
        Files::save( Reportpath, contents );
        LaTeX::Compile( _info->path(), "synthese", QDir::currentPath() ) ;
//        compile( Reportpath );
    }
//    void compile( const QString& path )
//    {
//        QString cmd = "pdflatex";
//        QStringList args;
//        args << "-synctex=1"
//             << "-interaction=nonstopmode"
//             << "\"" + path + "\"";
//        // Execute compile TeX File
//        System::exec( cmd, args, QDir::currentPath() + "/compile" );
//        Files::move( QDir::currentPath() + "/compile/synthese.pdf",
//                     _info->path() + "/synthese.pdf", true); // true is to force move
//    }
};

#endif // REPORT_H
