#ifndef POINTSCARACT_H
#define POINTSCARACT_H

#include <QObject>
#include <QProcess>
#include <QThread>
#include <QDebug>
#include <QDir>
#include "Settings.h"
#include "Files.h"

class Point
{
    QString _name;
    double _def;
    double _cont;

public:
    Point( const QString& name, double def = -1, double cont = -1 )
    {
        _name = name;
        if ( def != -1 )
            _def = def;
        if ( cont != -1 )
            _cont = cont;
    }
    Point( const QStringList& line )
    {
        if ( line.size() > 2 )
        {
            _name = line[0];
            _def = line[1].toDouble();
            _cont = line[2].toDouble();
        }
    }
    QString name() { return _name; }
    double def() { return _def; }
    double cont() { return _cont; }
};

struct PC_Treatement
{
    static inline void addTo( QStringList& strL, int ind, QString str )
    {
        strL.replace( ind, strL.at(ind) + str );
    }
    static inline void run( const QString& path )
    {
        QFile file( path + "/meca/points_caracteristiques.dat" );
        if ( file.open( QIODevice::ReadOnly ) )
        {
            QStringList prop_strs = {"\\begin{tabular}{*{5}{c}}",
                                     "\\Sub{E}{0}\\ (\\gpa) & "
                                     "\\Sub{\\upsigma}{0,005}\\ (\\mpa) & "
                                     "\\Sub{\\upvarepsilon}{0,005}\\ (\\perc) & "
                                     "\\Sub{\\upsigma}{rup}\\ (\\mpa) & "
                                     "\\Sub{\\upvarepsilon}{rup}\\ (\\perc) \\\\\\hline",
                                     "",
                                     "\\end{tabular}"};
            QStringList cycles_strs = {"\\begin{tabular}{c*{!nbC!}{c}}",
                                       "\t& ",
                                       "\t\\Sub{E}{ap}\\ (\\gpa) & ",
                                       "\t\\Subsup{E}{d}{\\ast}\\ (\\gpa) & ",
                                       "\t\\Subsup{E}{d}{p}\\ (\\gpa) & ",
                                       "\t\\Subsup{E}{c}{\\ast}\\ (\\gpa) & ",
                                       "\t\\Subsup{E}{c}{p}\\ (\\gpa) & ",
                                       "\t\\Sub{\\upsigma}{u}\\ (\\mpa) & ",
                                       "\t\\Sub{\\upvarepsilon}{u}\\ (\\perc) & ",
                                       "\t\\Sub{\\upvarepsilon}{r}\\ (\\perc) & ",
                                       "\t\\Sub{\\upvarepsilon}{p}\\ (\\perc) & ",
                                       "\t\\Sub{\\upvarepsilon}{e}\\ (\\perc) & ",
                                       "\t\\Sub{\\upvarepsilon}{a}\\ (\\perc) & ",
                                       "\t\\Sub{\\upvarepsilon}{ar}\\ (\\perc) & ",
                                       "\tD (\\perc) & ",
                                       "\\end{tabular}"};
            QVector<Point> points;
            file.readLine();
            while ( !file.atEnd() )
            {
                QStringList line = QString(file.readLine()).split(";");
                if ( line.size() > 1)
                    points.append( Point( line ) );
            }
            int nbC = 0;
            if ( points.size() > 2 )
            {
                QVector<QVector<double>> data = Files::getData( path + "/MECA_DEF_CONT.dat" );
//                QStringList results;
                nbC = points.size()/10;
                cycles_strs.replace( 0, cycles_strs[0].replace("!nbC!", QString::number(nbC)) );
                int itC = 0; // incrément de cycle
                double E0 = -1 ; // Module initial

                double e_of = points[0].def(); // Déformation offset

                for ( int it = 1; it < points.size(); ++it )
                {
                    if ( it == 1 )
                    {
                        Point p0 = points[it-1];
                        Point p1 = points[it];
                        E0 = 0.1*(p1.cont()-p0.cont())/(p1.def()-p0.def()) ;
                        double e_linear = -1;
                        double s_linear = -1;
                        double b = -10*E0*(0.005 + e_of);
                        for ( int jt = 1; jt < data.size(); ++jt )
                        {
                            QVector<double> row1 = data.at(jt-1);
                            QVector<double> row2 = data.at(jt);
                            QRectF dataRect;
                            dataRect.setBottomLeft( {row1.at(1), row1.at(2)} );
                            dataRect.setTopRight( {row2.at(1), row2.at(2)} );
                            double defmoy = 0.5*(row1.at(1) + row2.at(1));
                            double cont = 10*E0*defmoy + b;
                            if ( dataRect.contains( defmoy, cont ) )
                            {
                                e_linear = defmoy;
                                s_linear = cont;
                                break;
                            }
                        }
                        // Adding Properties to the Stringlist
                        addTo( prop_strs, 2, QString::number(E0, 'f', 1).replace(".", ",") + " & " );
                        addTo( prop_strs, 2, QString::number(s_linear, 'f', 1).replace(".", ",") + " & " );
                        addTo( prop_strs, 2, QString::number(e_linear, 'f', 2).replace(".", ",") + " & " );
                    }
                    else if ( (it-2)%10 == 0 && it != 2 )
                    {
                        ++itC;
                        Point pu = points[it-10];
                        double Es_Decharge = 0.1 * ( points[it-9].cont() - points[it-8].cont() ) /
                                ( points[it-9].def() - points[it-8].def() );
                        double Ep_Decharge = 0.1 * ( points[it-7].cont() - points[it-6].cont() ) /
                                ( points[it-7].def() - points[it-6].def() );
                        double Es_Charge = 0.1 * ( points[it-3].cont() - points[it-4].cont() ) /
                                ( points[it-3].def() - points[it-4].def() );
                        double Ep_Charge = 0.1 * ( points[it-1].cont() - points[it-2].cont() ) /
                                ( points[it-1].def() - points[it-2].def() );
                        double Eap = 0.1 * ( points[it-5].cont() - pu.cont() ) /
                                ( points[it-5].def() - pu.def() );
                        double ee = pu.cont()/(10*Es_Decharge);
                        double ea = pu.def() - ee;
                        double ear = ea - points[it-5].def();
                        double D = 100*( 1 - Es_Decharge / E0 );
                        double er = 0;
                        for ( int jt = 1; jt < data.size(); ++jt )
                        {
                            if ( data[jt][1] < pu.def() )
                                continue;
                            else
                                if ( data[jt-1][2] <= pu.cont() && pu.cont() <= data[jt][2] )
                                {
                                    er = data[jt][1];
                                    break;
                                }
                        }
                        // Cycles
                        QString str = QString("Cycle ") + QString::number(itC);
                        if ( itC != nbC ) str += " & "; else str += " \\\\\\hline" ;
                        addTo( cycles_strs, 1, str );
                        // Eap
                        str = QString::number( Eap, 'g', 1 ).replace(".", ",");
                        if ( itC != nbC ) str += " & "; else str += " \\\\" ;
                        addTo( cycles_strs, 2, str );
                        // Esd
                        str = QString::number( Es_Decharge, 'f', 1 ).replace(".", ",");
                        if ( itC != nbC ) str += " & "; else str += " \\\\" ;
                        addTo( cycles_strs, 3, str );
                        // Epd
                        str = QString::number( Ep_Decharge, 'f', 1 ).replace(".", ",");
                        if ( itC != nbC ) str += " & "; else str += " \\\\" ;
                        addTo( cycles_strs, 4, str );
                        // Esc
                        str = QString::number( Es_Charge, 'f', 1 ).replace(".", ",");
                        if ( itC != nbC ) str += " & "; else str += " \\\\" ;
                        addTo( cycles_strs, 5, str );
                        // Epc
                        str = QString::number( Ep_Charge, 'f', 1 ).replace(".", ",");
                        if ( itC != nbC ) str += " & "; else str += " \\\\" ;
                        addTo( cycles_strs, 6, str );
                        // su
                        str = QString::number( pu.cont(), 'f', 1 ).replace(".", ",");
                        if ( itC != nbC ) str += " & "; else str += " \\\\" ;
                        addTo( cycles_strs, 7, str );
                        // eu
                        str = QString::number( pu.def() - e_of, 'g', 2 ).replace(".", ",");
                        if ( itC != nbC ) str += " & "; else str += " \\\\" ;
                        addTo( cycles_strs, 8, str);
                        // er
                        str = QString::number( er - e_of, 'g', 2 ).replace(".", ",");
                        if ( itC != nbC ) str += " & "; else str += " \\\\" ;
                        addTo( cycles_strs, 9, str );
                        // ep
                        str = QString::number( points[it-5].def() - e_of, 'g', 2 ).replace(".", ",");
                        if ( itC != nbC ) str += " & "; else str += " \\\\" ;
                        addTo( cycles_strs, 10, str );
                        // ee
                        str = QString::number( ee, 'g', 2 ).replace(".", ",");
                        if ( itC != nbC )  str += " & "; else str += " \\\\" ;
                        addTo( cycles_strs, 11, str );
                        // ea
                        str = QString::number( ea - e_of, 'g', 2 ).replace(".", ",");
                        if ( itC != nbC ) str += " & "; else str += " \\\\" ;
                        addTo( cycles_strs, 12, str );
                        // ear
                        str = QString::number( ear, 'g', 2 ).replace(".", ",");
                        if ( itC != nbC ) str += " & "; else str += " \\\\" ;
                        addTo( cycles_strs, 13, str );
                        // D
                        str = QString::number( D, 'f', 1 ).replace(".", ",");
                        if ( itC != nbC ) str += " & "; else str += " \\\\\\hline" ;
                        addTo( cycles_strs, 14, str );
                    }
                    if ( it+1 == points.size())
                    {
                        addTo( prop_strs, 2, QString::number(points[it].cont(), 'f', 1).replace(".", ",") + " & " );
                        addTo( prop_strs, 2, QString::number(points[it].def() - e_of, 'f', 2).replace(".", ",") + " \\\\\\hline" );
                    }
                }
            }

            file.close();
            Files::save( path + "/meca/PROP.tex",
                        prop_strs );
            if ( nbC )
                Files::save( path + "/meca/PROP_CYCL.tex",
                            cycles_strs );
        }
    }
};

class PC_Thread
        : public QThread
{
    Q_OBJECT
    QString _path;
    void run() override
    {
        QString pathToPyScript = Settings::getString( Keys::PythonDir ) + "/getPointsCaract.py";
        QString cmd = "/usr/bin/python3";
        QStringList args;
        args << pathToPyScript
             << _path;

        System::exec( cmd, args );

        PC_Treatement::run( _path );

        emit finished();
    }


public:
    PC_Thread( const QString& path )
    {
        _path = path;
        connect( this, &PC_Thread::finished,
                 this, &PC_Thread::deleteLater );
    }
signals:
    void finished();
};

class PointsCaract
        : public QObject
{
    Q_OBJECT
    QString _path;
public:
    void run( const QString& path )
    {
        _path = path;
        PC_Thread *thread = new PC_Thread( path );
        connect( thread, &PC_Thread::finished,
                 this, &PointsCaract::finished );
        thread->start();

    }
signals:
    void finished();
};

#endif // POINTSCARACT_H
