#ifndef SCRIPTEDIT_H
#define SCRIPTEDIT_H

#include <QPlainTextEdit>

class ScriptEdit
        : public QPlainTextEdit
{
    Q_OBJECT

public:
    ScriptEdit( QWidget* parent = 0 )
        : QPlainTextEdit( parent )
    {
        this->installEventFilter(this);
        setPlainText( "compare e-s name SGL-FLEX addAll &SGL&FLEX!VIEIL" );
    }

protected:
    bool eventFilter( QObject *object, QEvent *ev ) override
    {
        Q_UNUSED( object )
        if ( ev->type() == QEvent::KeyPress )
        {
             QKeyEvent* keyEvent = (QKeyEvent*)ev;

             if ( ( (keyEvent->key() == Qt::Key_Enter ||
                     keyEvent->key() == Qt::Key_Return) &&
                    keyEvent->modifiers() == Qt::ShiftModifier) || keyEvent->key() == Qt::Key_F5 )
             {
                 emit editingFinished();
                 return true;
             }
//             else if ( keyEvent->key() == Qt::Key_A )
//             {
//                this->setPlainText("Key pressed : A");
//             }
      }
      return false;
    }

signals:
    void editingFinished();
};

#endif // SCRIPTEDIT_H
