#ifndef LATEX_H
#define LATEX_H

#include "Settings.h"

namespace PlotKeys {
    static QString xlabel = "%%%XLABEL%%%";
    static QString ylabel = "%%%YLABEL%%%";
    static QString yblabel = "%%%YBLABEL%%%";
    static QString ytlabel = "%%%YTLABEL%%%";
    static QString xindex = "%%%XIND%%%";
    static QString yindex = "%%%YIND%%%";
    static QString ybindex = "%%%YBIND%%%";
    static QString ytindex = "%%%YTIND%%%";
    static QString ycolor = "%%%YCLR%%%";
    static QString ybcolor = "%%%YBCLR%%%";
    static QString ytcolor = "%%%YTCLR%%%";
    static QString ylegend = "%%%YLEGEND%%%";
    static QString yblegend = "%%%YBLEGEND%%%";
    static QString ytlegend = "%%%YTLEGEND%%%";
    static QString fLimits = "%%%FLIMITS%%%";

    static QString TIME = "Temps (s)";
    static QString DEF_LABEL = "Déformation (\\%)";
    static QString CTR_LABEL = "Contrainte (MPa)";
    static QString EA_LABEL = "E.A. (coups)";
    static QString RE_LABEL = "$\\mathbfsf{\\frac{\\Updelta R}{R_0}}$ (\\%)";
    static QString T_LABEL = "Température (°C)";
    static QString TIME_LEGEND = "0";
    static QString DEF_LEGEND = "Déformation";
    static QString CTR_LEGEND = "Contrainte";
    static QString EA_LEGEND = "Émission acoustique";
    static QString RE_LEGEND = "Résistance électrique";
    static QString T_LEGEND = "Température";
    static QString TIME_EN = "Time (s)";
    static QString DEF_LABEL_EN = "Stain (\\%)";
    static QString CTR_LABEL_EN = "Stress (MPa)";
    static QString EA_LABEL_EN = "E.A. (counts)";
    static QString T_LABEL_EN = "Temperature (°C)";
    static QString TIME_LEGEND_EN = "0";
    static QString DEF_LEGEND_EN = "Stain";
    static QString CTR_LEGEND_EN = "Stress";
    static QString EA_LEGEND_EN = "Acoustic emission";
    static QString RE_LEGEND_EN = "Electrical resistance";
    static QString T_LEGEND_EN = "Temperature";
    static QString DEF_COLOR = "DEF";
    static QString CTR_COLOR = "CTR";
    static QString EA_COLOR = "EA";
    static QString RE_COLOR = "RE";
    static QString T_COLOR = "TEMP";

    static QString PlotLine = "\t\t\t\\addplot+[%%%COLOR%%%] table[x index=\\xdataInd,y index=\\ydataInd,col sep=semicolon] {%%%FILENAME%%%};\\addlegendentry{%%%LABEL%%%}";
}

class LaTeX_INFO
{
    QString _type;
    QString _filename;
    QString _path;
    QStringList _dataDirs;
    QStringList _dataLabels;
    QStringList _dataColors;
    QString _lang = "FR";
    QString _ext = "png";
    QStringList _axeslimits;
public:
    LaTeX_INFO()
    {

    }
    void clear()
    {
        _type.clear();
        _filename.clear();
        _path.clear();
        _dataDirs.clear();
        _dataLabels.clear();
        _dataColors.clear();
        _lang = "FR";
        _ext = "png";
        _axeslimits.clear();
    }
    void setType( const QString& arg ) { _type = arg; }
    QString type() { return _type; }
    void setFileName( const QString& arg ) { _filename = arg; }
    QString filename() { return _filename; }
    void setPath( const QString& arg ) { _path = arg; }
    QString path() { return _path; }
    void setDataDirs( const QStringList& arg ) { _dataDirs = arg; }
    QStringList dataDirs() { return _dataDirs; }
    void setDataLabels( const QStringList& arg ) { _dataLabels = arg; }
    QStringList dataLabels() { return _dataLabels; }
    void setDataColors( const QStringList& arg ) { _dataColors = arg; }
    QStringList dataColors() { return _dataColors; }
    void setLang( const QString& arg ) { _lang = arg; }
    QString lang() { return _lang; }
    void setExt( const QString& arg ) { _ext = arg; }
    QString ext() { return _ext; }
    void setAxesLimits( const QStringList& arg ) { _axeslimits = arg; }
    QStringList axesLimits() { return _axeslimits; }
};

class LaTeX
{
private:
    static inline bool _isEnglish = false;
    static inline QString _imageEXT = "png";
public:
    static inline void EnglishVersion()
    {
        _isEnglish = true;
    }
    static inline void SetImageEXT( const QString& ext )
    {
        _imageEXT = ext;
    }
    static inline void Compile( const QString& path, const QString& fileName, const QString& tmpPath = "" )
    {
        QString cmd = "pdflatex";
        QStringList args;
        args << "-synctex=1"
             << "-interaction=nonstopmode"
             << "\"" + fileName + ".tex\"";
        // Execute compile TeX File
        // Need two compile twice to correct legend labeling !!
        QString compilePath = path;
        if ( tmpPath != "" )
            compilePath = tmpPath;
        System::exec( cmd, args, compilePath + "/LaTeX" );
        System::exec( cmd, args, compilePath + "/LaTeX" );
        //
        Files::move( compilePath + "/LaTeX/" + fileName + ".pdf",
                     path + "/" + fileName + ".pdf", true); // true is to force move
    }

    static inline void convertPDFtoImage( const QString& path, const QString& fileName )
    {
        QString cmd = "convert";
        QStringList args;
        args << "-density"
             << "600"
             << fileName + ".pdf"
             << fileName + "." + _imageEXT;
        // Execute conversion PDF to PNG
        System::exec( cmd, args, path );
    }
    static inline void CompileAndConvertToPNG( const QString& path, const QString& fileName )
    {
        Compile( path, fileName );
        convertPDFtoImage( path, fileName );
    }
    static inline void buildPlotTeX( const QString& path,
                                     const QString& dataFilePath,
                                     const QString& plotName,
                                     const QString& suffix = "" )
    {
        if (  !QDir(path).exists() )
            return;
//        Files::createDir( path, "plots" );
//        Files::createDir( path + "/plots", "LaTeX" );
        QString plotTplDir = Settings::getString( Keys::Plots, Keys::TplDir);
        QString plotTpl = "x-y";
        if ( plotName.count('-') == 2 )
            plotTpl = "x-y-y";
        else if ( plotName.count('-') == 3 )
            plotTpl = "x-y-y-y";

        // Get contents in the TeX Template File
        QString origPath = plotTplDir + "/" + plotTpl + ".tex";
        QString contents;
        if ( !Files::read( origPath, contents ) )
            return;

        contents.replace("%%%FILENAME%%%", path + "/" + dataFilePath);
        LaTeX::ReplaceMarkers( plotName,  contents );

        // Save contents in the TeX File
        QString newname = plotName;
        if ( _isEnglish )
            newname = "EN_" + newname;

        QString TeX_Path = path + "/plots/LaTeX/" + newname + suffix + ".tex";
        Files::save( TeX_Path, contents );

        // Compilation et Conversion en PNG
        LaTeX::CompileAndConvertToPNG( path + "/plots", newname + suffix );
        _isEnglish = false;
    }
    static inline void buildPlotTeX( const QString& path,
                                     const QString& name,
                                     const QString& plotName,
                                     const QStringList& dataDirs,
                                     const QStringList& labels,
                                     const QStringList& dataColors = {} )
    {
        if (  !QDir(path).exists() )
            return;
//        Files::createDir( path, "plots" );
//        Files::createDir( path + "/plots", "LaTeX" );
        QString plotTplDir = Settings::getString( Keys::Plots, Keys::TplDir);
        QString plotTpl = "x-y_m";
        if ( plotName.count('-') == 2 )
            plotTpl = "x-y-y_m";
        else if ( plotName.count('-') == 3 )
            plotTpl = "x-y-y-y_m";

        // Get contents in the TeX Template File
        QString origPath = plotTplDir + "/" + plotTpl + ".tex";
        QString contents;
        if ( !Files::read( origPath, contents ) )
            return;

//        int it_col = 1;
        QString plotlines;
        LaTeX::BuildPlotLines( plotlines, dataDirs, labels, dataColors );
//        for ( QString dataDir : dataDirs )
//        {
//            plotlines += PlotKeys::PlotLine + "\n";
//            plotlines.replace("%%%FILENAME%%%", dataDir + "/MECA_DEF_CONT.dat");
//            if ( it_col-1 < dataColors.size() && !dataColors.isEmpty() )
//                plotlines.replace("%%%COLOR%%%", dataColors.at(it_col-1) );
//            else
//                plotlines.replace("%%%COLOR%%%", "CLR" + QString::number(it_col) );
//            plotlines.replace("%%%LABEL%%%", labels.at(it_col-1) );
//            ++it_col;
//        }
        contents.replace("%%%PLOTS%%%", plotlines);
        contents.replace("%%%FLIMITS%%%", "");
        LaTeX::ReplaceMarkers( plotName,  contents );

        // Save contents in the TeX File
        QString newname = name;
        if ( _isEnglish )
            newname = "EN_" + newname;
        QString TeX_Path = path + "/LaTeX/" + newname + ".tex";
        Files::save( TeX_Path, contents );

        // Compilation et Conversion en PNG
        LaTeX::CompileAndConvertToPNG( path, newname );
        _isEnglish = false;
    }
    static inline void ReplaceMarkers( const QString& plotName, QString& contents )
    {
        QStringList strl = plotName.split("-");
        if ( strl.at(0) == "t" )
        {
            QString label = PlotKeys::TIME;
            if ( _isEnglish )
                label = PlotKeys::TIME_EN;
            contents.replace( PlotKeys::xlabel, label );
            contents.replace( PlotKeys::xindex, "0" );
        }
        else if ( strl.at(0) == "e" )
        {
            QString label = PlotKeys::DEF_LABEL;
            if ( _isEnglish )
                label = PlotKeys::DEF_LABEL_EN;
            contents.replace( PlotKeys::xlabel, label );
            contents.replace( PlotKeys::xindex, "1" );
        }
        else if ( strl.at(0) == "s" )
        {
            QString label = PlotKeys::CTR_LABEL;
            if ( _isEnglish )
                label = PlotKeys::CTR_LABEL_EN;
            contents.replace( PlotKeys::xlabel, label );
            contents.replace( PlotKeys::xindex, "2" );
        }
        if ( strl.at(1) == "s" )
        {
            QString label = PlotKeys::CTR_LABEL;
            QString legend = PlotKeys::CTR_LEGEND;
            if ( _isEnglish )
            {
                label = PlotKeys::CTR_LABEL_EN;
                legend = PlotKeys::CTR_LEGEND_EN;
            }
            contents.replace( PlotKeys::ylabel, label );
            contents.replace( PlotKeys::yindex, "2" );
            contents.replace( PlotKeys::ylegend, legend );
            contents.replace( PlotKeys::ycolor, PlotKeys::CTR_COLOR );
        }
        else if ( strl.at(1) == "RE" )
        {
            QString label = PlotKeys::RE_LABEL;
            QString legend = PlotKeys::RE_LEGEND;
            if ( _isEnglish )
            {
                label = PlotKeys::RE_LABEL;
                legend = PlotKeys::RE_LEGEND_EN;
            }
            contents.replace( PlotKeys::ylabel, label );
            contents.replace( PlotKeys::yindex, "5" );
            contents.replace( PlotKeys::ylegend, legend);
            contents.replace( PlotKeys::ycolor, PlotKeys::RE_COLOR );
        }
        if ( strl.size() == 3 )
        {
            if ( strl.at(2) == "EA" )
            {
                QString label = PlotKeys::EA_LABEL;
                QString legend = PlotKeys::EA_LEGEND;
                if ( _isEnglish )
                {
                    label = PlotKeys::EA_LABEL_EN;
                    legend = PlotKeys::EA_LEGEND_EN;
                }
                contents.replace( PlotKeys::yblabel, label);
                contents.replace( PlotKeys::ybindex, "3" );
                contents.replace( PlotKeys::yblegend, legend );
                contents.replace( PlotKeys::ybcolor, PlotKeys::EA_COLOR );
            }
            else if ( strl.at(2) == "RE" )
            {
                QString label = PlotKeys::RE_LABEL;
                QString legend = PlotKeys::RE_LEGEND;
                if ( _isEnglish )
                {
                    label = PlotKeys::RE_LABEL;
                    legend = PlotKeys::RE_LEGEND_EN;
                }
                contents.replace( PlotKeys::yblabel, label );
                contents.replace( PlotKeys::ybindex, "4" );
                contents.replace( PlotKeys::yblegend, legend);
                contents.replace( PlotKeys::ybcolor, PlotKeys::RE_COLOR );
            }
            else if ( strl.at(2) == "T" )
            {
                QString label = PlotKeys::T_LABEL;
                QString legend = PlotKeys::T_LEGEND;
                if ( _isEnglish )
                {
                    label = PlotKeys::T_LABEL_EN;
                    legend = PlotKeys::T_LEGEND_EN;
                }
                contents.replace( PlotKeys::yblabel, label );
                contents.replace( PlotKeys::ybindex, "3" );
                contents.replace( PlotKeys::yblegend, legend );
                contents.replace( PlotKeys::ybcolor, PlotKeys::T_COLOR );
            }
            else if ( strl.at(2) == "e" )
            {
                QString label = PlotKeys::DEF_LABEL;
                QString legend = PlotKeys::DEF_LEGEND;
                if ( _isEnglish )
                {
                    label = PlotKeys::DEF_LABEL_EN;
                    legend = PlotKeys::DEF_LEGEND_EN;
                }
                contents.replace( PlotKeys::yblabel, label);
                contents.replace( PlotKeys::ybindex, "1" );
                contents.replace( PlotKeys::yblegend, legend );
                contents.replace( PlotKeys::ybcolor, PlotKeys::DEF_COLOR );
            }
        }
        else if ( strl.size() == 4 )
        {
            if ( strl.at(2) == "EA" && strl.at(3) == "RE" )
            {
                QString label = PlotKeys::EA_LABEL;
                QString legend = PlotKeys::EA_LEGEND;
                if ( _isEnglish )
                {
                    label = PlotKeys::EA_LABEL_EN;
                    legend = PlotKeys::EA_LEGEND_EN;
                }
                contents.replace( PlotKeys::yblabel, label);
                contents.replace( PlotKeys::ybindex, "5" );
                contents.replace( PlotKeys::yblegend, legend );
                contents.replace( PlotKeys::ybcolor, PlotKeys::EA_COLOR );
                label = PlotKeys::RE_LABEL;
                legend = PlotKeys::RE_LEGEND;
                if ( _isEnglish )
                {
                    label = PlotKeys::RE_LABEL;
                    legend = PlotKeys::RE_LEGEND_EN;
                }
                contents.replace( PlotKeys::ytlabel, label );
                contents.replace( PlotKeys::ytindex, "4" );
                contents.replace( PlotKeys::ytlegend, legend);
                contents.replace( PlotKeys::ytcolor, PlotKeys::RE_COLOR );
            }
            else if ( strl.at(2) == "e" && strl.at(3) == "T" )
            {
                QString label = PlotKeys::DEF_LABEL;
                QString legend = PlotKeys::DEF_LEGEND;
                if ( _isEnglish )
                {
                    label = PlotKeys::DEF_LABEL_EN;
                    legend = PlotKeys::DEF_LEGEND_EN;
                }
                contents.replace( PlotKeys::yblabel, label);
                contents.replace( PlotKeys::ybindex, "1" );
                contents.replace( PlotKeys::yblegend, legend );
                contents.replace( PlotKeys::ybcolor, PlotKeys::DEF_COLOR );
                label = PlotKeys::T_LABEL;
                legend = PlotKeys::T_LEGEND;
                if ( _isEnglish )
                {
                    label = PlotKeys::T_LABEL_EN;
                    legend = PlotKeys::T_LEGEND_EN;
                }
                contents.replace( PlotKeys::ytlabel, label );
                contents.replace( PlotKeys::ytindex, "3" );
                contents.replace( PlotKeys::ytlegend, legend );
                contents.replace( PlotKeys::ytcolor, PlotKeys::T_COLOR );
            }
            else if ( strl.at(2) == "e" && strl.at(3) == "RE" )
            {
                QString label = PlotKeys::DEF_LABEL;
                QString legend = PlotKeys::DEF_LEGEND;
                if ( _isEnglish )
                {
                    label = PlotKeys::DEF_LABEL_EN;
                    legend = PlotKeys::DEF_LEGEND_EN;
                }
                contents.replace( PlotKeys::yblabel, label);
                contents.replace( PlotKeys::ybindex, "1" );
                contents.replace( PlotKeys::yblegend, legend );
                contents.replace( PlotKeys::ybcolor, PlotKeys::DEF_COLOR );
                label = PlotKeys::RE_LABEL;
                legend = PlotKeys::RE_LEGEND;
                if ( _isEnglish )
                {
                    label = PlotKeys::RE_LABEL;
                    legend = PlotKeys::RE_LEGEND_EN;
                }
                contents.replace( PlotKeys::ytlabel, label );
                contents.replace( PlotKeys::ytindex, "5" );
                contents.replace( PlotKeys::ytlegend, legend);
                contents.replace( PlotKeys::ytcolor, PlotKeys::RE_COLOR );
            }
        }
    }

private:
    static inline void BuildPlotLines( QString& plotLines,
                                          const QStringList& dataDirs,
                                          const QStringList& labels,
                                          const QStringList& dataColors = {} )
    {
        int it_col = 1;
        plotLines.clear();
        for ( QString dataDir : dataDirs )
        {
            plotLines += PlotKeys::PlotLine + "\n";
            plotLines.replace("%%%FILENAME%%%", dataDir + "/MECA_DEF_CONT.dat");
            if ( it_col-1 < dataColors.size() && !dataColors.isEmpty() )
                plotLines.replace("%%%COLOR%%%", dataColors.at(it_col-1) );
            else
                plotLines.replace("%%%COLOR%%%", "CLR" + QString::number(it_col) );
            plotLines.replace("%%%LABEL%%%", labels.at(it_col-1) );
            ++it_col;
        }
    }
};


#endif // LATEX_H
