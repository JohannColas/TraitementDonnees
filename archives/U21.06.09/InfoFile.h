#ifndef INFOFILE_H
#define INFOFILE_H

#include <QFile>
#include <QFileInfo>
#include <QSettings>
#include <QDebug>

class InfoFile
{
    QString _path;
    QString _materiau;
    QString _eprouvette;
    QString _essai;
    // Dimensions éprouvette
    double _longueur = -1;
    double _largeur = -1;
    double _epaisseur = -1;
    double _tcut = -1;
    QString _vitesseEssai = "";
    // Commentaire
    QString _commentaire;
    // Commentaire
    QString _donneesDispo;
    // CNI
    int _lighterFactor = 1500;


public:
    InfoFile()
    {
    }
    InfoFile( const QString& path )
    {
        if ( !path.isEmpty())
            setPath( path );
    }
    QString path()
    {
        return _path;
    }
    void setPath( const QString& path )
    {
        _path = path;
        QFileInfo infoF( _path + "/ESSAI.info" );
        if ( !infoF.exists() )
        {
            infoF.setFile( _path );
            QStringList paths = path.split("/");
            QStringList filename = paths.takeLast().split(" - ");
            if ( filename.size() > 1 )
            {
                setEprouvette( filename[1] );
                setEssai( filename[0] );
            }
            QString materiau = paths.last();
            setMateriau( materiau );
        }
        else
            read();
    }
    void clear()
    {
        _longueur = -1;
        _largeur = -1;
        _epaisseur = -1;
        _tcut = -1;
        _commentaire = "";
        _donneesDispo = "";
        _lighterFactor = 1500;
    }
    void fullClear()
    {
        _path = "";
        _materiau = "";
        _eprouvette = "";
        _essai = "";
        clear();
    }
    QString materiau()
    {
        return _materiau;
    }
    void setMateriau( const QString& materiau )
    {
        _materiau = materiau;
    }
    QString eprouvette()
    {
        return _eprouvette;
    }
    void setEprouvette( const QString& eprouvette )
    {
        _eprouvette = eprouvette;
    }
    QString essai()
    {
        return _essai;
    }
    void setEssai( const QString& essai )
    {
        _essai = essai;
    }
    double longueur()
    {
        return _longueur;
    }
    void setLongueur( double longueur )
    {
        _longueur = longueur;
    }
    double largeur()
    {
        return _largeur;
    }
    void setLargeur( double largeur )
    {
        _largeur = largeur;
    }
    double epaisseur()
    {
        return _epaisseur;
    }
    void setEpaisseur( double epaisseur )
    {
        _epaisseur = epaisseur;
    }
    double tcut()
    {
        return _tcut;
    }
    void setTcut( double tcut )
    {
        _tcut = tcut;
    }
    QString vitesseEssai()
    {
        return _vitesseEssai;
    }
    void setVitesseEssai( QString vitesse )
    {
        _vitesseEssai = vitesse;
    }
    QString commentaire()
    {
        return _commentaire;
    }
    void setCommentaire( const QString& comment )
    {
        _commentaire = comment;
    }
    QString donneesDispo()
    {
        return _donneesDispo;
    }
    void setDonneesDispo( const QString& donneesDispo )
    {
        _donneesDispo = donneesDispo;
    }
    int lighterFactor() const
    {
        return _lighterFactor;
    }
    void setLighterFactor( int factor )
    {
        _lighterFactor = factor;
    }
    void read()
    {
        QFileInfo infoF( _path );
        if ( infoF.exists() )
        {
            QSettings setsFile( _path + "/ESSAI.info", QSettings::IniFormat );
            setsFile.setIniCodec("UTF-8");
            _materiau = setsFile.value("materiau").toString();
            _eprouvette = setsFile.value("eprouvette").toString();
            _essai = setsFile.value("essai").toString();
            if ( !setsFile.value("longueurMM").isNull() )
                _longueur = setsFile.value("longueurMM").toDouble();
            if ( !setsFile.value("largeurMM").isNull() )
                _largeur = setsFile.value("largeurMM").toDouble();
            if ( !setsFile.value("epaisseurMM").isNull() )
                _epaisseur = setsFile.value("epaisseurMM").toDouble();
            _commentaire = setsFile.value("commentaire").toString();
            _donneesDispo = setsFile.value("donneesDispo").toString();
            if ( !setsFile.value("tcut").isNull() )
                _tcut = setsFile.value("tcut").toDouble();
            if ( setsFile.value("lighterFactor").isValid() )
                _lighterFactor = setsFile.value("lighterFactor").toInt();
        }
    }
    void save()
    {
        QFile file( _path + "/ESSAI.info" );
        file.remove();
        QSettings setsFile( _path + "/ESSAI.info", QSettings::IniFormat );
        setsFile.setIniCodec("UTF-8");
        setsFile.setValue("materiau", _materiau );
        setsFile.setValue("eprouvette", _eprouvette );
        setsFile.setValue("essai", _essai );
        setsFile.setValue("longueurMM", _longueur );
        setsFile.setValue("largeurMM", _largeur );
        setsFile.setValue("epaisseurMM", _epaisseur );
        setsFile.setValue("commentaire", _commentaire );
        setsFile.setValue("donneesDispo", _donneesDispo );
        setsFile.setValue("tcut", _tcut );
        setsFile.setValue("lighterFactor", _lighterFactor );
    }
};

#endif // INFOFILE_H




