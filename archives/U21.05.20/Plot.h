#ifndef PLOT_H
#define PLOT_H

#include "Files.h"
#include "LaTeX.h"
#include <QProcess>
#include <QDebug>
#include "Settings.h"
#include "InfoFile.h"

//namespace PlotKeys {
//    static QString xlabel = "%%%XLABEL%%%";
//    static QString ylabel = "%%%YLABEL%%%";
//    static QString yblabel = "%%%YBLABEL%%%";
//    static QString ytlabel = "%%%YTLABEL%%%";
//    static QString xindex = "%%%XIND%%%";
//    static QString yindex = "%%%YIND%%%";
//    static QString ybindex = "%%%YBIND%%%";
//    static QString ytindex = "%%%YTIND%%%";
//    static QString ycolor = "%%%YCLR%%%";
//    static QString ybcolor = "%%%YBCLR%%%";
//    static QString ytcolor = "%%%YTCLR%%%";
//    static QString ylegend = "%%%YLEGEND%%%";
//    static QString yblegend = "%%%YBLEGEND%%%";
//    static QString ytlegend = "%%%YTLEGEND%%%";

//    static QString TIME = "Temps (s)";
//    static QString DEF_LABEL = "Déformation (\\%)";
//    static QString CTR_LABEL = "Contrainte (MPa)";
//    static QString EA_LABEL = "E.A. (coups)";
//    static QString RE_LABEL = "$\\mathbfsf{\\frac{\\Updelta R}{R_0}}$ (\\%)";
//    static QString T_LABEL = "Température (°C)";
//    static QString TIME_LEGEND = "0";
//    static QString DEF_LEGEND = "Déformation";
//    static QString CTR_LEGEND = "Contrainte";
//    static QString EA_LEGEND = "Émission acoustique";
//    static QString RE_LEGEND = "Résistance électrique";
//    static QString T_LEGEND = "Température";
//    static QString DEF_COLOR = "DEF";
//    static QString CTR_COLOR = "CTR";
//    static QString EA_COLOR = "EA";
//    static QString RE_COLOR = "RE";
//    static QString T_COLOR = "TEMP";
//}

class Plot
{
    QString _path;
    Settings* _sets = 0;

public:
    Plot( QString path, Settings* sets = 0 )
    {
        _path = path;
        _sets = sets;
    }
    void generate( const QString& path = "" )
    {
        if ( path != "" )
            _path = path;

        Files::createDir( _path, "plots" );
        Files::createDir( _path + "/plots", "LaTeX" );

        InfoFile infoF( _path );
        QStringList plots;
        //            if ( prefix != "VIEIL-" )
        if ( infoF.essai().left(6) != "VIEIL_" )
        {
            plots.append( { "t-s-e", "e-s" } );
            if ( infoF.donneesDispo().contains("R.E.") &&
                 infoF.donneesDispo().contains("E.A.") )
                plots.append( {"t-s-EA-RE","e-s-EA-RE"} );
            else
                if ( infoF.donneesDispo().contains("R.E.") )
                    plots.append( {"t-s-RE", "e-s-RE"} );
                else if ( infoF.donneesDispo().contains("E.A.") )
                    plots.append( {"t-s-EA", "e-s-EA"} );
        }
        else
        {
            plots.append( {"t-s-T", "t-s-e-T", "e-s"} );
        }

        // Pour chaque graphe à construire
        for ( QString plotName : plots )
        {
            // Create TeX File
//            createTeX( plotName );
            LaTeX::buildPlotTeX( _path, "MECA_DEF_CONT.dat", plotName );
            // Compilation et Conversion en PNG
            LaTeX::CompileAndConvertToPNG( _path + "/plots", plotName );
            if ( QFileInfo(_path+"/MECA_DEF_CONT_focused.dat").exists() )
            {
                // Create TeX File
                LaTeX::buildPlotTeX( _path, "MECA_DEF_CONT_focused.dat", plotName, "_focused" );
                // Compilation et Conversion en PNG
                LaTeX::CompileAndConvertToPNG( _path + "/plots", plotName + "_focused" );
            }
        }

//        if ( QFileInfo(_path+"/MECA_DEF_CONT_focused.dat").exists() )
//            for ( QString plotName : plots )
//            {
//                // Create TeX File
//                createTeX( plotName, "_focused" );
//                LaTeX::buildPlotTeX( _path, "MECA_DEF_CONT_focused.dat", plotName, "_focused" );
//                // Compilation et Conversion en PNG
//                LaTeX::CompileAndConvertToPNG( _path + "/plots", plotName + "_focused" );
//            }
    }
    void createTeX( const QString& plotName, const QString& focused = "" )
    {
        QString plotTplDir = _sets->getString( Keys::Plots + "/" + Keys::TplDir);
        QString plotTpl = "x-y";
        if ( plotName.count('-') == 2 )
            plotTpl = "x-y-y";
        else if ( plotName.count('-') == 3 )
            plotTpl = "x-y-y-y";

        // Get contents in the TeX Template File
        QString origPath = plotTplDir + "/" + plotTpl + ".tex";
        QString contents;
        if ( !Files::read( origPath, contents ) )
            return;

        contents.replace("%%%FILENAME%%%", _path+"/MECA_DEF_CONT"+focused+".dat");
        QStringList strl = plotName.split("-");
        if ( strl.at(0) == "t" )
        {
            contents.replace( PlotKeys::xlabel, PlotKeys::TIME );
            contents.replace( PlotKeys::xindex, "0" );
        }
        else if ( strl.at(0) == "e" )
        {
            contents.replace( PlotKeys::xlabel, PlotKeys::DEF_LABEL );
            contents.replace( PlotKeys::xindex, "1" );
        }
        if ( strl.at(1) == "s" )
        {
            contents.replace( PlotKeys::ylabel, PlotKeys::CTR_LABEL );
            contents.replace( PlotKeys::yindex, "2" );
            contents.replace( PlotKeys::ylegend, PlotKeys::CTR_LEGEND );
            contents.replace( PlotKeys::ycolor, PlotKeys::CTR_COLOR );
        }
        if ( strl.size() == 3 )
        {
            if ( strl.at(2) == "EA" )
            {
                contents.replace( PlotKeys::yblabel, PlotKeys::EA_LABEL );
                contents.replace( PlotKeys::ybindex, "3" );
                contents.replace( PlotKeys::yblegend, PlotKeys::EA_LEGEND );
                contents.replace( PlotKeys::ybcolor, PlotKeys::EA_COLOR );
            }
            else if ( strl.at(2) == "RE" )
            {
                contents.replace( PlotKeys::yblabel, PlotKeys::RE_LABEL );
                contents.replace( PlotKeys::ybindex, "4" );
                contents.replace( PlotKeys::yblegend, PlotKeys::RE_LEGEND );
                contents.replace( PlotKeys::ybcolor, PlotKeys::RE_COLOR );
            }
            else if ( strl.at(2) == "T" )
            {
                contents.replace( PlotKeys::yblabel, PlotKeys::T_LABEL );
                contents.replace( PlotKeys::ybindex, "3" );
                contents.replace( PlotKeys::yblegend, PlotKeys::T_LEGEND );
                contents.replace( PlotKeys::ybcolor, PlotKeys::T_COLOR );
            }
            else if ( strl.at(2) == "e" )
            {
                contents.replace( PlotKeys::yblabel, PlotKeys::DEF_LABEL );
                contents.replace( PlotKeys::ybindex, "1" );
                contents.replace( PlotKeys::yblegend, PlotKeys::DEF_LEGEND );
                contents.replace( PlotKeys::ybcolor, PlotKeys::DEF_COLOR );
            }
        }
        else if ( strl.size() == 4 )
        {
            if ( strl.at(2) == "EA" && strl.at(3) == "RE" )
            {
                contents.replace( PlotKeys::yblabel, PlotKeys::EA_LABEL );
                contents.replace( PlotKeys::ybindex, "5" );
                contents.replace( PlotKeys::yblegend, PlotKeys::EA_LEGEND );
                contents.replace( PlotKeys::ybcolor, PlotKeys::EA_COLOR );
                contents.replace( PlotKeys::ytlabel, PlotKeys::RE_LABEL );
                contents.replace( PlotKeys::ytindex, "4" );
                contents.replace( PlotKeys::ytlegend, PlotKeys::RE_LEGEND );
                contents.replace( PlotKeys::ytcolor, PlotKeys::RE_COLOR );
            }
            else if ( strl.at(2) == "e" && strl.at(3) == "T" )
            {
                contents.replace( PlotKeys::yblabel, PlotKeys::DEF_LABEL );
                contents.replace( PlotKeys::ybindex, "1" );
                contents.replace( PlotKeys::yblegend, PlotKeys::DEF_LEGEND );
                contents.replace( PlotKeys::ybcolor, PlotKeys::DEF_COLOR );
                contents.replace( PlotKeys::ytlabel, PlotKeys::T_LABEL );
                contents.replace( PlotKeys::ytindex, "3" );
                contents.replace( PlotKeys::ytlegend, PlotKeys::T_LEGEND );
                contents.replace( PlotKeys::ytcolor, PlotKeys::T_COLOR );
            }
        }

        // Save contents in the TeX File
        QString TeX_Path = _path + "/plots/LaTeX/" + plotName + focused + ".tex";
        Files::save( TeX_Path, contents );
    }
};

#endif // PLOT_H
