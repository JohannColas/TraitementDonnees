#ifndef EA_H
#define EA_H

#include <QFile>
#include <QFileInfo>
#include <QSettings>
#include <QDebug>
#include <QDir>
#include <QThread>

namespace EA_KEYS {
    static inline QString SeuilAmpli = "SeuilAmpli";
    static inline QString SeuilTime = "SeuilTemps";
    static inline QString SeuilRise = "SeuilRise";
    static inline QString SeuilDur = "SeuilDuree";
    static inline QString SeuilCps = "SeuilCoups";
    static inline QString SeuilEner = "SeuilEnergie";
}

class EA_FilterFile
{
    QString _path;
    QSettings* _settings = 0;

public:
    ~EA_FilterFile()
    {
        if ( _settings )
            delete _settings;
    }
    EA_FilterFile( const QString& folderpath = QString() )
    {
        setPath( folderpath );
    }
    void setPath( const QString& folderpath = QString() )
    {
        if ( _settings )
        {
            delete _settings;
            _settings = 0;
        }
        QString path_EA_settings = folderpath+"/EA";
        if ( folderpath.isEmpty() || !QDir(path_EA_settings).exists() )
            return;
        _settings = new QSettings( path_EA_settings+"/filter.ini", QSettings::IniFormat );
        _settings->setIniCodec("UTF-8");
//        if ( !QFileInfo(path_EA_settings+"/filter.ini").exists() )
//        {
//            save( EA_KEYS::SeuilAmpli, 0 );
//            save( EA_KEYS::SeuilTime, 0 );
//            save( EA_KEYS::SeuilRise, 0 );
//            save( EA_KEYS::SeuilDur, 0 );
//            save( EA_KEYS::SeuilCps, 0 );
//            save( EA_KEYS::SeuilEner, 0 );
//        }
    }
    QVariant value( const QString& key )
    {
        if ( _settings )
            return _settings->value( "Filter/" + key );
        return QVariant();
    }
    int toInt( const QString& key )
    {
        QVariant var = value( key );
        if ( var.isValid() )
            return var.toInt();
        return -1;
    }
    void save( const QString& key, const QVariant& value )
    {
        if ( _settings )
            _settings->setValue( "Filter/" + key, value );
    }
};

class EA_Thread
        : public QThread
{
    Q_OBJECT

    QString _path;
    EA_FilterFile _filterFile;

public:
    EA_Thread( const QString& path )
    {
        _path = path;
        _filterFile.setPath( _path );
    }
    void run() override
    {
        QFile rawDataFile( _path + "/EA/EA.dat" );
        if ( !rawDataFile.open(QFile::ReadOnly) )
            return;
        QTextStream rawData( &rawDataFile );
        rawData.setCodec( "UTF-8" );

        QFile treatDataFile( _path + "/EA/EA_traite.dat" );
        if ( !treatDataFile.open(QFile::WriteOnly) )
            return;
        QTextStream treatData( &treatDataFile );
        treatData.setCodec( "UTF-8" );
        treatData << "Time(s);" // 0
                     "Duration(microm);" // 1
                     "Amplitude(dB);" // 2
                     "AverageFrequency(Hz);" // 3
                     "ReverbationFrequency(Hz);" // 4
                     "InitiationFrequency(Hz);" // 5
                     "AverageEnergy(aJ);" // 6
                     "AbsoluteEnergy(aJ);" // 7
                     "CumulatedEnergy(aJ);" // 8
                     "counts;" // 9
                     "AccumulativeCounts;" // 10
                     "Severity(aJ)" // 11
                     "\n";
        double sumEner = 0;
        double nbvLines = 0;
        int totCount = 0;
        QVector<double> energyMaxes;
        while ( !rawData.atEnd() )
        {
            QString line = rawData.readLine();
            if ( line.contains("ID") && line.contains("SSSSSSSS.mmmuuun") )
                break;
        }
        while ( !rawData.atEnd() )
        {
            QStringList line;
            scanLine( rawData.readLine(), line );
            if ( line.size() < 15 )
                continue;
            int ID = line.at(0).toInt();
            double time = line.at(1).toDouble();
            int rise = line.at(4).toInt();
            int count = line.at(5).toInt();
            int energy = line.at(6).toInt();
            int durat = line.at(7).toInt();
            int amplit = line.at(8).toInt();
            if ( ID == 1 &&
                 time > _filterFile.toInt( EA_KEYS::SeuilTime ) &&
                 rise <= _filterFile.toInt( EA_KEYS::SeuilRise ) &&
                 count >= _filterFile.toInt( EA_KEYS::SeuilCps ) &&
                 energy > _filterFile.toInt( EA_KEYS::SeuilEner ) &&
                 durat < _filterFile.toInt( EA_KEYS::SeuilDur ) &&
                 amplit >= _filterFile.toInt( EA_KEYS::SeuilAmpli ) )
            {
                ++nbvLines;
                int avFreq = line.at(9).toInt();
                int revFreq = line.at(11).toInt();
                int initFreq = line.at(12).toInt();
                double absEner = line.at(14).toDouble();
                sumEner += absEner;
                double avEner = sumEner/nbvLines;
                totCount += count;
                double severity = 0;
                addEnergy( absEner, energyMaxes );
                severity = average( energyMaxes );

                treatData << time << ";"
                          << durat << ";"
                          << amplit << ";"
                          << avFreq << ";"
                          << revFreq << ";"
                          << initFreq << ";"
                          << avEner << ";"
                          << absEner << ";"
                          << sumEner << ";"
                          << count << ";"
                          << totCount << ";"
                          << severity << "\n";
            }
        }

        rawDataFile.close();
        treatDataFile.close();

        emit finished();
    }
    double average( const QVector<double>& values )
    {
        double total = 0;
        for ( double val : values )
        {
            total += val;
        }
        return ( total / values.size() );
    }
    void addEnergy( double ener, QVector<double>& vector )
    {
        int vecSize = vector.size();
        if ( vecSize == 0 )
            vector.append(ener);
        else if ( ener > vector.first() || vecSize < 10 )
        {
            if ( ener > vector.last() )
                vector.append(ener);
            else
                for ( int it = 1; it < vecSize; ++it )
                {
                    if ( ener > vector.at(it-1) &&
                         ener <= vector.at(it) )
                    {
                        vector.insert( it, ener );
                        break;
                    }
                }
            if ( vector.size() == 11 )
                vector.removeFirst();
        }
    }
    void scanLine( const QString& line, QStringList& strs )
    {
        QString tmp;
        QString oldChr;
        for ( int it = 0; it < line.size(); ++it )
        {
            QString chr = line.at(it);
            if ( chr == " " || (chr == "-" && oldChr != "E") )
            {
                if ( !tmp.isEmpty() )
                {
                    strs.append( tmp );
                    tmp = "";
                    if ( chr == "-" )
                        tmp = chr;
                }
            }
            else
            {
                tmp += chr;
            }
            oldChr = chr;
        }
        strs.append( tmp );
    }

signals:
    void finished();
};

class EA
        : public QObject
{
    Q_OBJECT

    QString _path;
    EA_FilterFile _filterFile;

public:
    EA( const QString& path )
    {
        _path = path;
        _filterFile.setPath( _path );
    }
    void treat()
    {
            EA_Thread *thread = new EA_Thread( _path );
            connect( thread, &EA_Thread::finished,
                     this, &EA::finished );
            thread->start();
    }

signals:
    void finished();
};
#endif // EA_H
