#ifndef LATEX_H
#define LATEX_H

#include "Settings.h"

namespace PlotKeys {
    static QString xlabel = "%%%XLABEL%%%";
    static QString ylabel = "%%%YLABEL%%%";
    static QString yblabel = "%%%YBLABEL%%%";
    static QString ytlabel = "%%%YTLABEL%%%";
    static QString xindex = "%%%XIND%%%";
    static QString yindex = "%%%YIND%%%";
    static QString ybindex = "%%%YBIND%%%";
    static QString ytindex = "%%%YTIND%%%";
    static QString ycolor = "%%%YCLR%%%";
    static QString ybcolor = "%%%YBCLR%%%";
    static QString ytcolor = "%%%YTCLR%%%";
    static QString ylegend = "%%%YLEGEND%%%";
    static QString yblegend = "%%%YBLEGEND%%%";
    static QString ytlegend = "%%%YTLEGEND%%%";

    static QString TIME = "Temps (s)";
    static QString DEF_LABEL = "Déformation (\\%)";
    static QString CTR_LABEL = "Contrainte (MPa)";
    static QString EA_LABEL = "E.A. (coups)";
    static QString RE_LABEL = "$\\mathbfsf{\\frac{\\Updelta R}{R_0}}$ (\\%)";
    static QString T_LABEL = "Température (°C)";
    static QString TIME_LEGEND = "0";
    static QString DEF_LEGEND = "Déformation";
    static QString CTR_LEGEND = "Contrainte";
    static QString EA_LEGEND = "Émission acoustique";
    static QString RE_LEGEND = "Résistance électrique";
    static QString T_LEGEND = "Température";
    static QString DEF_COLOR = "DEF";
    static QString CTR_COLOR = "CTR";
    static QString EA_COLOR = "EA";
    static QString RE_COLOR = "RE";
    static QString T_COLOR = "TEMP";
}

class LaTeX
{
    static inline Settings* _settings = 0;

public:
    static inline void setSettings( Settings* settings = 0 )
    {
        _settings = settings;
    }
    static inline void compile( const QString& path, const QString& fileName )
    {

        QString cmd = "pdflatex";
        QStringList args;
        args << "-synctex=1"
             << "-interaction=nonstopmode"
             << "\"" + fileName + ".tex\"";
        // Execute compile TeX File
        // Need two compile twice to correct legend labeling !!
        System::exec( cmd, args, path + "/LaTeX" );
        System::exec( cmd, args, path + "/LaTeX" );
        //
        Files::move( path + "/LaTeX/" + fileName + ".pdf",
                     path + "/" + fileName + ".pdf", true); // true is to force move
    }

    static inline void convertPDFtoPNG( const QString& path, const QString& fileName )
    {
        QString cmd = "convert";
        QStringList args;
        args << "-density"
             << "600"
             << fileName + ".pdf"
             << fileName + ".png";
        // Execute conversion PDF to PNG
        System::exec( cmd, args, path );
    }
    static inline void CompileAndConvertToPNG( const QString& path, const QString& fileName )
    {
        compile( path, fileName );
        convertPDFtoPNG( path, fileName );
    }
    static inline void buildPlotTeX( const QString& path, const QString& dataFilePath, const QString& plotName, const QString& suffix = "" )
    {
        if ( !_settings || !QDir(path).exists() )
            return;
        Files::createDir( path, "plots" );
        Files::createDir( path + "/plots", "LaTeX" );
        QString plotTplDir = _settings->getString( Keys::Plots + "/" + Keys::TplDir);
        QString plotTpl = "x-y";
        if ( plotName.count('-') == 2 )
            plotTpl = "x-y-y";
        else if ( plotName.count('-') == 3 )
            plotTpl = "x-y-y-y";

        // Get contents in the TeX Template File
        QString origPath = plotTplDir + "/" + plotTpl + ".tex";
        QString contents;
        if ( !Files::read( origPath, contents ) )
            return;

        contents.replace("%%%FILENAME%%%", path + "/" + dataFilePath);
        QStringList strl = plotName.split("-");
        if ( strl.at(0) == "t" )
        {
            contents.replace( PlotKeys::xlabel, PlotKeys::TIME );
            contents.replace( PlotKeys::xindex, "0" );
        }
        else if ( strl.at(0) == "e" )
        {
            contents.replace( PlotKeys::xlabel, PlotKeys::DEF_LABEL );
            contents.replace( PlotKeys::xindex, "1" );
        }
        else if ( strl.at(0) == "s" )
        {
            contents.replace( PlotKeys::xlabel, PlotKeys::CTR_LABEL );
            contents.replace( PlotKeys::xindex, "2" );
        }
        if ( strl.at(1) == "s" )
        {
            contents.replace( PlotKeys::ylabel, PlotKeys::CTR_LABEL );
            contents.replace( PlotKeys::yindex, "2" );
            contents.replace( PlotKeys::ylegend, PlotKeys::CTR_LEGEND );
            contents.replace( PlotKeys::ycolor, PlotKeys::CTR_COLOR );
        }
        else if ( strl.at(1) == "RE" )
        {
            contents.replace( PlotKeys::ylabel, PlotKeys::RE_LABEL );
            contents.replace( PlotKeys::yindex, "5" );
            contents.replace( PlotKeys::ylegend, PlotKeys::RE_LEGEND );
            contents.replace( PlotKeys::ycolor, PlotKeys::RE_COLOR );
        }
        if ( strl.size() == 3 )
        {
            if ( strl.at(2) == "EA" )
            {
                contents.replace( PlotKeys::yblabel, PlotKeys::EA_LABEL );
                contents.replace( PlotKeys::ybindex, "3" );
                contents.replace( PlotKeys::yblegend, PlotKeys::EA_LEGEND );
                contents.replace( PlotKeys::ybcolor, PlotKeys::EA_COLOR );
            }
            else if ( strl.at(2) == "RE" )
            {
                contents.replace( PlotKeys::yblabel, PlotKeys::RE_LABEL );
                contents.replace( PlotKeys::ybindex, "4" );
                contents.replace( PlotKeys::yblegend, PlotKeys::RE_LEGEND );
                contents.replace( PlotKeys::ybcolor, PlotKeys::RE_COLOR );
            }
            else if ( strl.at(2) == "T" )
            {
                contents.replace( PlotKeys::yblabel, PlotKeys::T_LABEL );
                contents.replace( PlotKeys::ybindex, "3" );
                contents.replace( PlotKeys::yblegend, PlotKeys::T_LEGEND );
                contents.replace( PlotKeys::ybcolor, PlotKeys::T_COLOR );
            }
            else if ( strl.at(2) == "e" )
            {
                contents.replace( PlotKeys::yblabel, PlotKeys::DEF_LABEL );
                contents.replace( PlotKeys::ybindex, "1" );
                contents.replace( PlotKeys::yblegend, PlotKeys::DEF_LEGEND );
                contents.replace( PlotKeys::ybcolor, PlotKeys::DEF_COLOR );
            }
        }
        else if ( strl.size() == 4 )
        {
            if ( strl.at(2) == "EA" && strl.at(3) == "RE" )
            {
                contents.replace( PlotKeys::yblabel, PlotKeys::EA_LABEL );
                contents.replace( PlotKeys::ybindex, "5" );
                contents.replace( PlotKeys::yblegend, PlotKeys::EA_LEGEND );
                contents.replace( PlotKeys::ybcolor, PlotKeys::EA_COLOR );
                contents.replace( PlotKeys::ytlabel, PlotKeys::RE_LABEL );
                contents.replace( PlotKeys::ytindex, "4" );
                contents.replace( PlotKeys::ytlegend, PlotKeys::RE_LEGEND );
                contents.replace( PlotKeys::ytcolor, PlotKeys::RE_COLOR );
            }
            else if ( strl.at(2) == "e" && strl.at(3) == "T" )
            {
                contents.replace( PlotKeys::yblabel, PlotKeys::DEF_LABEL );
                contents.replace( PlotKeys::ybindex, "1" );
                contents.replace( PlotKeys::yblegend, PlotKeys::DEF_LEGEND );
                contents.replace( PlotKeys::ybcolor, PlotKeys::DEF_COLOR );
                contents.replace( PlotKeys::ytlabel, PlotKeys::T_LABEL );
                contents.replace( PlotKeys::ytindex, "3" );
                contents.replace( PlotKeys::ytlegend, PlotKeys::T_LEGEND );
                contents.replace( PlotKeys::ytcolor, PlotKeys::T_COLOR );
            }
            else if ( strl.at(2) == "e" && strl.at(3) == "RE" )
            {
                contents.replace( PlotKeys::yblabel, PlotKeys::DEF_LABEL );
                contents.replace( PlotKeys::ybindex, "1" );
                contents.replace( PlotKeys::yblegend, PlotKeys::DEF_LEGEND );
                contents.replace( PlotKeys::ybcolor, PlotKeys::DEF_COLOR );
                contents.replace( PlotKeys::ytlabel, PlotKeys::RE_LABEL );
                contents.replace( PlotKeys::ytindex, "5" );
                contents.replace( PlotKeys::ytlegend, PlotKeys::RE_LEGEND );
                contents.replace( PlotKeys::ytcolor, PlotKeys::RE_COLOR );
            }
        }

        // Save contents in the TeX File
        QString TeX_Path = path + "/plots/LaTeX/" + plotName + suffix + ".tex";
        Files::save( TeX_Path, contents );

        // Compilation et Conversion en PNG
        LaTeX::CompileAndConvertToPNG( path + "/plots", plotName + suffix );
    }


};


#endif // LATEX_H
