#ifndef FILE_H
#define FILE_H

#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QVector>

class Files
{

public:
    static inline QVector<QVector<double>> getData( const QString& filepath )
    {
        QVector<QVector<double>> data;
        QFile file( filepath  );
        if ( file.open(QFile::ReadOnly) )
        {
            // get the content of the file
            QTextStream in( &file );
            bool isNumber = false;
            while ( !in.atEnd() )
            {
                QString line = in.readLine();
                QStringList temp = line.split(";");
                if ( temp.size() == 1 )
                    temp = temp.first().split("\t");
                temp.first().toDouble(&isNumber);
                if ( isNumber )
                {
                    QVector<double> lineNumber;
                    for ( QString col : temp )
                    {
                        lineNumber.append( col.replace(',', '.').toDouble() );
                    }
                    data.append( lineNumber );
                }
            }
            file.close();
        }
        return data;
    }
    static inline void save( const QString& path, const QString& content, const char* codec = "UTF-8" )
    {
        QFile saveFile( path );
        saveFile.open( QIODevice::WriteOnly | QIODevice::Text );
        QTextStream out( &saveFile );
        out.setCodec( codec );
        out << content;
        saveFile.close();
    }
    static inline void save( const QString& path, const QStringList& content, const char* codec = "UTF-8" )
    {
        QFile saveFile( path );
        saveFile.open( QIODevice::WriteOnly | QIODevice::Text );
        QTextStream out( &saveFile );
        out.setCodec( codec );
        for ( QString line : content )
            out << line << "\n";
        saveFile.close();
    }
    static inline void save( const QString& path, const QVector<QVector<double>>& content, const QString& header, const char* codec = "UTF-8" )
    {
        QFile saveFile( path );
        saveFile.open( QIODevice::WriteOnly | QIODevice::Text );
        QTextStream out( &saveFile );
        out.setCodec( codec );
        out << header + "\n";
        for ( QVector<double> line : content )
        {
            int size = line.size();
            for ( int it = 0; it < size; ++it )
            {
                out << QString::number(line.at(it));
                if ( it == size-1 )
                    out << "\n";
                else
                    out << ";";
            }
        }
        saveFile.close();
    }
    static inline bool read( const QString& path, QString& content, const char* codec = "UTF-8" )
    {
        QFile file( path );
        if ( !file.open(QFile::ReadOnly) )
            return false;

        QTextStream in( &file );
        in.setCodec( codec );
        content = in.readAll().toUtf8();
        file.close();
        return true;
    }
    static inline bool read( const QString& path, QStringList& content, const char* codec = "UTF-8" )
    {
        QFile file( path );
        if ( !file.open(QFile::ReadOnly) )
            return false;
        QTextStream in( &file );
        in.setCodec( codec );
        while ( !in.atEnd() )
        {
            content.append( in.readLine().toUtf8() );
        }
        file.close();
        return true;
    }
    static inline bool move( const QString& oldpath, const QString& newpath, bool forced = false )
    {
        if ( forced )
            if ( QFile::exists( newpath ) )
                QFile::remove( newpath );
        if ( !QFile::copy( oldpath, newpath ) )
            return false;
       QFile::remove( oldpath );
       return true;
    }
    static inline QDir createDir( const QString& dirpath, const QString& dirname )
    {
        QDir dir( dirpath );
        if ( !dir.exists( dirname ) )
            dir.mkdir( dirname );
        dir.cd( dirname );
        return dir;
    }
    static inline bool remove( const QString& path)
    {
        if ( QFile::exists( path ) )
            return QFile::remove( path );
       return false;
    }
};

#endif // FILE_H
