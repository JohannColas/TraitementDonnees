#ifndef SETTINGS_H
#define SETTINGS_H
/**********************************************/
#include <QApplication>
#include <QDir>
#include <QFile>
#include <QIcon>
#include <QSettings>
#include <QDebug>
#include "System.h"
#include "Files.h"
/**********************************************/
namespace Keys {
    static QString EssaiDir = "EssaiDir";
    static QString LaTeXDir = "LaTeXDir";
    static QString PythonDir = "PythonDir";
    static QString RecursiveSearch = "RecursiveSearch";
    static QString Lissage = "Lissage";
    static QString Type = "Type";
    static QString nbPoints = "nbPoints";
    static QString Plots = "Plots";
    static QString Reports = "Reports";
    static QString TplDir = "TemplateDir";
    static QString TreatMeca = "TreatMeca";
    static QString TreatPlots = "TreatPlots";
    static QString TreatPtsCaract = "TreatPtsCaract";
    static QString TreatEA = "TreatEA";
    static QString TreatRE = "TreatRE";
    static QString TreatCNI = "TreatCNI";
    static QString TreatSynthese = "TreatSynthese";
    static QString CNI = "Reports";
    static QString lighterFactor = "lighterFactor";
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Settings
        : public QObject
{
    Q_OBJECT
private:
    QSettings* _settings = 0;
    QString _essaisDir = QDir::homePath() + "/Documents/Essais";
    // Lissage par Moyenne Glissée
    int _nbPoints = 9;
    // Plots
    QString _templatesPlotDir = QDir::homePath() + "/Documents/LaTeX/Plots_Templates";
    QString _templatesReportDir = QDir::homePath() + "/Documents/LaTeX/Rapport_Essai_Templates";
    QString _pythonScriptDir = QDir::homePath() + "/Documents/Programmes/#Python";

public:
    Settings()
    {
        _settings = new QSettings( QDir::currentPath() + "/settings.ini", QSettings::IniFormat );
    }
    Settings( const QString& path )
    {
        _settings = new QSettings( path, QSettings::IniFormat );
    }
    QVariant value( const QString& key,  const QString& subkey = "" )
    {
        if ( _settings )
        {
            QString _key = key;
            if ( subkey != "" )
                _key += "/" + subkey;
            QVariant var = _settings->value( _key );
            if ( var.isValid() )
                return var;
        }
        return QVariant();
    }
    bool getBool( const QString& key,  const QString& subkey = "" )
    {
        QVariant var = value( key, subkey );
        if ( var.isValid() )
            return var.toBool();
        return false;
    }
    QString getString( const QString& key, const QString& subkey = "" )
    {
        QVariant var = value( key, subkey );
        if ( var.isValid() )
            return var.toString();
        return "";
    }
    int getInt( const QString& key, const QString& subkey = "" )
    {
        QVariant var = value( key, subkey );
        if ( var.isValid() )
            return var.toInt();
        return -1;
    }
    double getDouble( const QString& key, const QString& subkey = "" )
    {
        QVariant var = value( key, subkey );
        if ( var.isValid() )
            return var.toDouble();
        return -1;
    }
    void save( const QString& key, const QVariant& value )
    {
        _settings->setValue( key, value );
    }
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SETTINGS_H
