#ifndef SYSTEM_H
#define SYSTEM_H

#include <QDir>
#include <QProcess>
#include <QDebug>



class System
{
public:
    static void exec( const QString& cmd, const QStringList& args = {}, const QString& workingPath = "", int killAt = -1 )
    {
            QProcess process;
            if ( workingPath != "" )
                process.setWorkingDirectory( workingPath );
            else
            {
                QDir( QDir::currentPath() ).mkdir( "PDFLaTeX_Compilation" );
                process.setWorkingDirectory( QDir::currentPath() + "/PDFLaTeX_Compilation" );
            }

            process.start( cmd, args, QIODevice::ReadOnly ); //Starts execution of command
            process.waitForFinished( killAt );

//            for ( QString str : cmd )
//                qDebug() << str;
//            qDebug() << "Failed:" << process.readAllStandardError();
//            qDebug() << "Output:" << process.readAll();
//            qDebug() << "";
//            qDebug() << "";
    }
};

#endif // SYSTEM_H
