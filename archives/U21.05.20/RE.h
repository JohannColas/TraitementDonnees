#ifndef RE_H
#define RE_H

#include <QFile>
#include "Files.h"
#include "LaTeX.h"
#include <QTextStream>
#include <QDebug>
#include <QThread>

#include "Settings.h"
#include "InfoFile.h"
#include "Plot.h"
#include <cmath>

#include <iostream>
using namespace std;

enum RE_Program
{
    NONE,
    RESCOLAS,
};

class RE_Thread
        : public QThread
{
    Q_OBJECT

    Settings* _sets = 0;
    InfoFile* _info = 0;
    QString _path = "";
    RE_Program _prog = RE_Program::NONE;
    int index_TIME = -1;
    int index_FORCE = -1;
    int index_DEF1 = -1;
    int index_DEP = -1;
    int index_RE = -1;
    int index_TEMP = -1;
    bool _tempFour = false;
    int _tempFocused = 980;
    QStringList header;
    QVector<QVector<double>> data;
    QVector<QVector<double>> treated_data;
    QVector<QVector<double>> treated_data_focused;
    QVector<QVector<double>> treated_rawdata;
    QString header_data;
    QString header_rawdata;

    void run() override
    {
        if ( readREFile() )
        {
            treatData();
            _info->save();

            lissageParMoyenneGlissee( treated_data );

            Files::save( _path + "/RE/RE_DEF_CONT.dat", treated_data, header_data );
            if ( treated_rawdata.size() > 0 )
            {
                lissageParMoyenneGlissee( treated_rawdata );
                Files::save( _path + "/RE/RE_DEP_FORCE.dat", treated_rawdata, header_rawdata );
            }
            if ( treated_data_focused.size() > 0 )
            {
                lissageParMoyenneGlissee( treated_data_focused );
                Files::save( _path + "/RE/RE_DEF_CONT_focused.dat", treated_data_focused, header_rawdata );
            }

            makePlots();
        }
        emit finished();
    }
    bool readREFile()
    {
        header.clear();
        data.clear();
        QStringList contents;
        if ( !Files::read( _path + "/RE/RE.dat", contents, "UTF-8") )
                return false;
        if ( contents.first().contains('\r') )
            contents = contents.first().split('\r');

        if ( _path.contains("VIEIL_1300") )
            _tempFocused = 1280;

        bool isNumber = false;
        for ( QString line : contents )
        {
            if ( line.count('\t') == line.size() || line.count(';') == line.size() || line.count(',') == line.size() || line.count(' ') == line.size() )
                continue;
            line.replace( ',', '.' ); // Replace for number
            QStringList temp;
            temp = line.split('\t');
            if ( line.contains(';') && !line.contains('\t') )
                temp = line.split(';');
            // Checking if is Number
            temp.first().toDouble( &isNumber );
            if ( !isNumber )
            {
                header.append( temp.first() );
                if ( temp.first().contains(">>") )
                {
                     if ( temp.first().contains("RESCOLAS.BAS") )
                         _prog = RE_Program::RESCOLAS;
                }
            }
            else
            {
                QVector<double> lineNumber;
                for ( QString col : temp )
                {
                    lineNumber.append( col.toDouble() );
                }
                data.append( lineNumber );
            }
        }
        return true;
    }
    void treatData()
    {
        QString suiviDispo;
        if ( _prog == RE_Program::RESCOLAS )
        {
            suiviDispo = "R.E.";
            index_TIME = 0;
            index_FORCE = 2;
            index_DEP = 3;
            index_DEF1 = -1;
            index_RE = 1;
        }

        if ( _info->donneesDispo().isEmpty() )
            _info->setDonneesDispo(suiviDispo);

        header_data = "t(s);e(perc);s(MPa);T(C);RE(mOhm);dR_R0(perc)";
        header_rawdata = "t(s);def(mm);forc(N);T(C);RE(mOhm);dR_R0(perc)";

        // Get RawData
        double L = _info->longueur();
        double b = _info->largeur();
        double e = _info->epaisseur();
        double tim = -1;
        double dep = -1;
        double dep0 = -1;
        if ( index_DEP != -1)
            dep0 = data[0][index_DEP];
        double frc = -1;
        double def = -1;
        double ctr = -1;
        double re = -1;
        double R0 = -1;
        double dRdR0 = 0;
        if ( index_RE != -1)
        {
            R0 = data[0][index_RE];
            if ( R0 < 1 )
                R0 *= 1000;
        }
        double temp = -1;
        for ( int it = 0; it < data.size(); ++it )
        {
            QVector<double> line = data[it];
            tim = line[index_TIME];
            // Get deplacement
            if ( index_DEP != -1)
                dep = line[index_DEP];
            // Get Force
            if ( index_FORCE != -1)
                frc = line[index_FORCE];
            // Get Strain
            if ( _prog == RE_Program::RESCOLAS )
            {
                def = 100*(dep - dep0)/L;
                if ( L == -1 )
                    def = -1;
            }
            else if ( index_DEF1 != -1 )
            {
                def = line[index_DEF1];
            }
            // Get Stress
            if ( _prog == RE_Program::RESCOLAS )
            {
                ctr = frc/(b*e);
                if ( e == -1 )
                    ctr *= -1;
            }
            // get RE
            if ( index_RE != -1 )
            {
                re = line[index_RE];
                if ( re < 1 )
                    re *= 1000; // convert in mOhm
                dRdR0 = 100*( re - R0 )/R0;
            }
            // get Temperature
            if ( index_TEMP != -1 )
            {
                temp = line[index_TEMP];
                if ( _tempFour )
                    temp = - 0.0000005127092*pow(temp, 3)
                            + 0.0007545365*pow(temp, 2)
                            + 0.9624921*temp
                            + 5.306989;
            }
            // Save data
            if ( /*def >= -0.001 &&*/
                 (line[index_TIME] < _info->tcut() || _info->tcut() == -1 ) )
            {
                QVector<double> dataLine = { tim, def, ctr, temp, re, dRdR0};
                treated_data.append( dataLine );
                if ( ctr >= _tempFocused )
                    treated_data_focused.append( dataLine );

                if ( index_DEP != 1 && index_FORCE != -1 )
                {
                    QVector<double> rawdataLine = { tim, dep, frc, temp , re, dRdR0};
                    treated_rawdata.append( rawdataLine );
                }
            }
        }
    }
    void lissageParMoyenneGlissee( QVector<QVector<double>>& _data )
    {
        int nbPts = _sets->getInt( Keys::Lissage, Keys::nbPoints );
        int hNbPts = 0.5*nbPts;
        int size = _data.size();
        QVector<QVector<double>> temp = _data;
        for ( int it = 0; it < size; ++it )
        {
            if ( it < hNbPts || it > size - hNbPts - 1 )
                for ( int jt = 0; jt < _data[it].size(); ++jt )
                    temp[it][jt] = _data[it][jt];
            else
            {
                temp[it][0] = _data[it][0];
                for ( int jt = 1; jt < _data[it].size(); ++jt )
                {
                    double somme = 0;
                    int count = 0;
                    for ( int lt = it - hNbPts; lt <= it + hNbPts; ++lt )
                    {
                        somme += _data[lt][jt];
                        ++count;
                    }
                    temp[it][jt] = somme/count;
                }
                _data = temp;
            }
        }
    }
    void makePlots()
    {
//        Files::createDir( _path + "/RE", "plots" );
//        Files::createDir( _path + "/RE/plots", "LaTeX" );

        QStringList plots;
        plots.append( { "t-s-e-RE", "e-RE", "e-s-RE", "s-RE" } );

        for ( QString plotName : plots )
        {
//            QString plotTplDir = _sets->getString( Keys::Plots + "/" + Keys::TplDir);
//            QString plotTpl = "x-y";
//            if ( plotName.count('-') == 2 )
//                plotTpl = "x-y-y";
//            else if ( plotName.count('-') == 3 )
//                plotTpl = "x-y-y-y";

//            // Get contents in the TeX Template File
//            QString origPath = plotTplDir + "/" + plotTpl + ".tex";
//            QString contents;
//            if ( !Files::read( origPath, contents ) )
//                return;
//            QString contents_focused;
//            if ( !Files::read( origPath, contents_focused ) )
//                return;

//            contents.replace("%%%FILENAME%%%", _path+"/RE/RE_DEF_CONT.dat");
//            contents_focused.replace("%%%FILENAME%%%", _path+"/RE/RE_DEF_CONT_focused.dat");
//            QStringList strl = plotName.split("-");
//            if ( strl.at(0) == "t" )
//            {
//                contents.replace( PlotKeys::xlabel, PlotKeys::TIME );
//                contents.replace( PlotKeys::xindex, "0" );
//                contents_focused.replace( PlotKeys::xlabel, PlotKeys::TIME );
//                contents_focused.replace( PlotKeys::xindex, "0" );
//            }
//            else if ( strl.at(0) == "e" )
//            {
//                contents.replace( PlotKeys::xlabel, PlotKeys::DEF_LABEL );
//                contents.replace( PlotKeys::xindex, "1" );
//                contents_focused.replace( PlotKeys::xlabel, PlotKeys::DEF_LABEL );
//                contents_focused.replace( PlotKeys::xindex, "1" );
//            }
//            else if ( strl.at(0) == "s" )
//            {
//                contents.replace( PlotKeys::xlabel, PlotKeys::CTR_LABEL );
//                contents.replace( PlotKeys::xindex, "2" );
//                contents_focused.replace( PlotKeys::xlabel, PlotKeys::CTR_LABEL );
//                contents_focused.replace( PlotKeys::xindex, "2" );
//            }
//            if ( strl.at(1) == "s" )
//            {
//                contents.replace( PlotKeys::ylabel, PlotKeys::CTR_LABEL );
//                contents.replace( PlotKeys::yindex, "2" );
//                contents.replace( PlotKeys::ylegend, PlotKeys::CTR_LEGEND );
//                contents.replace( PlotKeys::ycolor, PlotKeys::CTR_COLOR );
//                contents_focused.replace( PlotKeys::ylabel, PlotKeys::CTR_LABEL );
//                contents_focused.replace( PlotKeys::yindex, "2" );
//                contents_focused.replace( PlotKeys::ylegend, PlotKeys::CTR_LEGEND );
//                contents_focused.replace( PlotKeys::ycolor, PlotKeys::CTR_COLOR );
//            }
//            else if ( strl.at(1) == "RE" )
//            {
//                contents.replace( PlotKeys::ylabel, PlotKeys::RE_LABEL );
//                contents.replace( PlotKeys::yindex, "5" );
//                contents.replace( PlotKeys::ylegend, PlotKeys::RE_LEGEND );
//                contents.replace( PlotKeys::ycolor, PlotKeys::RE_COLOR );
//                contents_focused.replace( PlotKeys::ylabel, PlotKeys::RE_LABEL );
//                contents_focused.replace( PlotKeys::yindex, "5" );
//                contents_focused.replace( PlotKeys::ylegend, PlotKeys::RE_LEGEND );
//                contents_focused.replace( PlotKeys::ycolor, PlotKeys::RE_COLOR );
//            }
//            if ( strl.size() >= 3 )
//            {
//                if ( strl.at(2) == "e" )
//                {
//                    contents.replace( PlotKeys::yblabel, PlotKeys::DEF_LABEL );
//                    contents.replace( PlotKeys::ybindex, "1" );
//                    contents.replace( PlotKeys::yblegend, PlotKeys::DEF_LEGEND );
//                    contents.replace( PlotKeys::ybcolor, PlotKeys::DEF_COLOR );
//                    contents_focused.replace( PlotKeys::yblabel, PlotKeys::DEF_LABEL );
//                    contents_focused.replace( PlotKeys::ybindex, "1" );
//                    contents_focused.replace( PlotKeys::yblegend, PlotKeys::DEF_LEGEND );
//                    contents_focused.replace( PlotKeys::ybcolor, PlotKeys::DEF_COLOR );
//                }
//                else if ( strl.at(2) == "RE" )
//                {
//                    contents.replace( PlotKeys::yblabel, PlotKeys::RE_LABEL );
//                    contents.replace( PlotKeys::ybindex, "5" );
//                    contents.replace( PlotKeys::yblegend, PlotKeys::RE_LEGEND );
//                    contents.replace( PlotKeys::ybcolor, PlotKeys::RE_COLOR );
//                    contents_focused.replace( PlotKeys::yblabel, PlotKeys::RE_LABEL );
//                    contents_focused.replace( PlotKeys::ybindex, "5" );
//                    contents_focused.replace( PlotKeys::yblegend, PlotKeys::RE_LEGEND );
//                    contents_focused.replace( PlotKeys::ybcolor, PlotKeys::RE_COLOR );
//                }
//            }
//            if ( strl.size() == 4 )
//            {
//                if ( strl.at(3) == "RE" )
//                {
//                    contents.replace( PlotKeys::ytlabel, PlotKeys::RE_LABEL );
//                    contents.replace( PlotKeys::ytindex, "5" );
//                    contents.replace( PlotKeys::ytlegend, PlotKeys::RE_LEGEND );
//                    contents.replace( PlotKeys::ytcolor, PlotKeys::RE_COLOR );
//                    contents_focused.replace( PlotKeys::ytlabel, PlotKeys::RE_LABEL );
//                    contents_focused.replace( PlotKeys::ytindex, "5" );
//                    contents_focused.replace( PlotKeys::ytlegend, PlotKeys::RE_LEGEND );
//                    contents_focused.replace( PlotKeys::ytcolor, PlotKeys::RE_COLOR );
//                }
//            }

//            // Save contents in the TeX File
//            QString TeX_Path = _path + "/RE/plots/LaTeX/" + plotName + ".tex";
//            Files::save( TeX_Path, contents );
//            QString TeX_Path_focused = _path + "/RE/plots/LaTeX/" + plotName + "_focused.tex";
//            Files::save( TeX_Path_focused, contents_focused );

            LaTeX::buildPlotTeX( _path + "/RE", "RE_DEF_CONT.dat", plotName );
            LaTeX::buildPlotTeX( _path + "/RE", "RE_DEF_CONT_focused.dat", plotName, "_focused" );

//            QString cmd = "pdflatex";
//            QStringList args;
//            args << "-synctex=1"
//                 << "-interaction=nonstopmode"
//                 << "\"" + plotName + ".tex\"";
//            QStringList args_focused;
//            args_focused << "-synctex=1"
//                 << "-interaction=nonstopmode"
//                 << "\"" + plotName + "_focused.tex\"";
//            // Execute compile TeX File
//            System::exec( cmd, args, _path + "/RE/plots/LaTeX" );
//            System::exec( cmd, args, _path + "/RE/plots/LaTeX" );
//            Files::move( _path + "/RE/plots/LaTeX/" + plotName + ".pdf",
//                         _path + "/RE/plots/" + plotName + ".pdf", true); // true is to force move
//            System::exec( cmd, args_focused, _path + "/RE/plots/LaTeX" );
//            System::exec( cmd, args_focused, _path + "/RE/plots/LaTeX" );
//            Files::move( _path + "/RE/plots/LaTeX/" + plotName + "_focused.pdf",
//                         _path + "/RE/plots/" + plotName + "_focused.pdf", true); // true is to force move

//            // Convert to png
//            cmd = "convert";
//            args.clear();
//            args << "-density"
//                 << "600"
//                 << plotName + ".pdf"
//                 << plotName + ".png";
//            args_focused.clear();
//            args_focused << "-density"
//                 << "600"
//                 << plotName + "_focused.pdf"
//                 << plotName + "_focused.png";
//            // Execute conversion PDF to PNG
//            System::exec( cmd, args, _path + "/RE/plots" );
//            System::exec( cmd, args_focused, _path + "/RE/plots" );
        }
    }

public:
    RE_Thread( InfoFile* info = 0, Settings* sets = 0 )
    {
        _info = info;
        _sets = sets;
        if ( _info != 0)
            _path = _info->path();
    }

signals:
    void finished();
};


class RE
        : public QObject
{
    Q_OBJECT

    InfoFile* _info = 0;
    Settings* _sets = 0;

public:
    RE( InfoFile* info_file = 0, Settings* sets = 0 )
    {
        _sets = sets;
        _info = info_file;
    }
    void treat()
    {
        if ( _info )
        {
            RE_Thread *thread = new RE_Thread( _info, _sets );
            connect( thread, &RE_Thread::finished,
                     this, &RE::finished );
            thread->start();

        }
    }

signals:
    void finished();
};

#endif // RE_H
