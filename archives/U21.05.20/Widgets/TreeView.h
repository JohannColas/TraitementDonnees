#ifndef TREEVIEW_H
#define TREEVIEW_H

#include <QAbstractItemModel>
#include <QKeyEvent>
#include <QTreeView>

class TreeView
    : public QTreeView
{
    Q_OBJECT

public:
    TreeView( QWidget* parent = 0 )
        : QTreeView( parent )
    {

    }


protected slots:
    void keyPressEvent( QKeyEvent* event ){

        QTreeView::keyPressEvent(event);

        if ( event->key() == Qt::Key_Down )
        {
            QModelIndex qmi = this->currentIndex();
            emit clicked( qmi );
        }
        else if ( event->key() == Qt::Key_Up )
        {
            QModelIndex qmi = this->currentIndex();
            emit clicked( qmi );
        }
        else
        {
        }
    }
};

#endif // TREEVIEW_H
