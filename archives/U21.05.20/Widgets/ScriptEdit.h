#ifndef SCRIPTEDIT_H
#define SCRIPTEDIT_H

#include <QPlainTextEdit>

class ScriptEdit
        : public QPlainTextEdit
{
    Q_OBJECT

public:
    ScriptEdit( QWidget* parent = 0 )
        : QPlainTextEdit( parent )
    {
        this->installEventFilter(this);
    }

protected:
    bool eventFilter( QObject *object, QEvent *ev ) override
    {
        if ( ev->type() == QEvent::KeyPress )
        {
             QKeyEvent* keyEvent = (QKeyEvent*)ev;

             if ( keyEvent->key() == Qt::Key_Enter ||
                  keyEvent->key() == Qt::Key_Return )
             {
                 emit editingFinished();
             }
//             else if ( keyEvent->key() == Qt::Key_A )
//             {
//                this->setPlainText("Key pressed : A");
//             }
      }
      return false;
    }

signals:
    void editingFinished();
};

#endif // SCRIPTEDIT_H
