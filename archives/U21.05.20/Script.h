#ifndef SCRIPT_H
#define SCRIPT_H

#include "Files.h"
#include <QProcess>
#include <QDebug>
#include <QMessageBox>
#include "Settings.h"
#include "InfoFile.h"

namespace ScriptKeys {
    static QString xlabel = "%%%XLABEL%%%";
    static QString ylabel = "%%%YLABEL%%%";
    static QString yblabel = "%%%YBLABEL%%%";
    static QString ytlabel = "%%%YTLABEL%%%";
    static QString xindex = "%%%XIND%%%";
    static QString yindex = "%%%YIND%%%";
    static QString ybindex = "%%%YBIND%%%";
    static QString ytindex = "%%%YTIND%%%";
    static QString ycolor = "%%%YCLR%%%";
    static QString ybcolor = "%%%YBCLR%%%";
    static QString ytcolor = "%%%YTCLR%%%";
    static QString ylegend = "%%%YLEGEND%%%";
    static QString yblegend = "%%%YBLEGEND%%%";
    static QString ytlegend = "%%%YTLEGEND%%%";

    static QString TIME = "Temps (s)";
    static QString DEF_LABEL = "Déformation (\\%)";
    static QString CTR_LABEL = "Contrainte (MPa)";
    static QString EA_LABEL = "E.A. (coups)";
    static QString RE_LABEL = "$\\mathbfsf{\\frac{\\Updelta R}{R_0}}$ (\\%)";
    static QString T_LABEL = "Température (°C)";
    static QString TIME_LEGEND = "0";
    static QString DEF_LEGEND = "Déformation";
    static QString CTR_LEGEND = "Contrainte";
    static QString EA_LEGEND = "Émission acoustique";
    static QString RE_LEGEND = "Résistance électrique";
    static QString T_LEGEND = "Température";
    static QString DEF_COLOR = "DEF";
    static QString CTR_COLOR = "CTR";
    static QString EA_COLOR = "EA";
    static QString RE_COLOR = "RE";
    static QString T_COLOR = "TEMP";
}

class Script
{
    QString _path;
    Settings* _sets = 0;
    static inline QStringList _toAdd;
    static inline QString _cmdOpt;

public:
    Script( QString path, Settings* sets = 0 )
    {
        _path = path;
        _sets = sets;
    }
    static inline void launch( const QString& script = "" )
    {
        _toAdd.clear();

        if ( script == "" )
            return;

        int ind_firstSpace = script.indexOf(" ");
        QString command = script.left( ind_firstSpace );

        QStringList scriptList = script.split(QRegExp("( |&-|-&|\n)"), Qt::SkipEmptyParts);

        for ( int it = 0; it < scriptList.size(); ++it )
        {
            QString arg = scriptList.at(it);
            if ( arg == "compare" )
            {
                if ( it+1 < scriptList.size() )
                {
                    ++it;
                    _cmdOpt = scriptList.at(it);
                }
            }
            else if ( arg == "add" )
            {
                if ( it+1 < scriptList.size() )
                {
                    ++it;
                    _toAdd.append( scriptList.at(it) );
                }
            }
            else
            {
                QMessageBox msgBox;
                msgBox.setText("Commande incomprise !");
                msgBox.setIcon(QMessageBox::Warning);
                msgBox.exec();
                return;
            }

        }

        qDebug() << scriptList;
        qDebug() << _toAdd;
    }
    void generate( const QString& script = "" )
    {
        if ( script != "" )
            _path = script;
        else
            return;

        int ind_firstSpace = script.indexOf(" ");
        QString command = script.left( ind_firstSpace );


        Files::createDir( _path, "plots" );
        Files::createDir( _path + "/plots", "LaTeX" );

        InfoFile infoF( _path );
        QStringList plots;
        //            if ( prefix != "VIEIL-" )
        if ( infoF.essai().left(6) != "VIEIL_" )
        {
            plots.append( { "t-s-e", "e-s" } );
            if ( infoF.donneesDispo().contains("R.E.") &&
                 infoF.donneesDispo().contains("E.A.") )
                plots.append( {"t-s-EA-RE","e-s-EA-RE"} );
            else
                if ( infoF.donneesDispo().contains("R.E.") )
                    plots.append( {"t-s-RE", "e-s-RE"} );
                else if ( infoF.donneesDispo().contains("E.A.") )
                    plots.append( {"t-s-EA", "e-s-EA"} );
        }
        else
        {
            plots.append( {"t-s-T", "t-s-e-T", "e-s"} );
        }

        // Pour chaque graphe à construire
        for ( QString plotName : plots )
        {
            // Create TeX File
            createTeX( plotName );
            // Compilation le fichier TEX
            // Compile twice to avoid some problems !!!
            compile( plotName );
            compile( plotName );
            // Convertion du PDF vers PNG
            convertPDFtoPNG( plotName );
        }
    }
    void createTeX( const QString& plotName )
    {
        QString plotTplDir = _sets->getString( Keys::Plots + "/" + Keys::TplDir);
        QString plotTpl = "x-y";
        if ( plotName.count('-') == 2 )
            plotTpl = "x-y-y";
        else if ( plotName.count('-') == 3 )
            plotTpl = "x-y-y-y";

        // Get contents in the TeX Template File
        QString origPath = plotTplDir + "/" + plotTpl + ".tex";
        QString contents;
        if ( !Files::read( origPath, contents ) )
            return;

        contents.replace("%%%FILENAME%%%", _path+"/MECA_DEF_CONT.dat");
        QStringList strl = plotName.split("-");
        if ( strl.at(0) == "t" )
        {
            contents.replace( ScriptKeys::xlabel, ScriptKeys::TIME );
            contents.replace( ScriptKeys::xindex, "0" );
        }
        else if ( strl.at(0) == "e" )
        {
            contents.replace( ScriptKeys::xlabel, ScriptKeys::DEF_LABEL );
            contents.replace( ScriptKeys::xindex, "1" );
        }
        if ( strl.at(1) == "s" )
        {
            contents.replace( ScriptKeys::ylabel, ScriptKeys::CTR_LABEL );
            contents.replace( ScriptKeys::yindex, "2" );
            contents.replace( ScriptKeys::ylegend, ScriptKeys::CTR_LEGEND );
            contents.replace( ScriptKeys::ycolor, ScriptKeys::CTR_COLOR );
        }
        if ( strl.size() == 3 )
        {
            if ( strl.at(2) == "EA" )
            {
                contents.replace( ScriptKeys::yblabel, ScriptKeys::EA_LABEL );
                contents.replace( ScriptKeys::ybindex, "3" );
                contents.replace( ScriptKeys::yblegend, ScriptKeys::EA_LEGEND );
                contents.replace( ScriptKeys::ybcolor, ScriptKeys::EA_COLOR );
            }
            else if ( strl.at(2) == "RE" )
            {
                contents.replace( ScriptKeys::yblabel, ScriptKeys::RE_LABEL );
                contents.replace( ScriptKeys::ybindex, "4" );
                contents.replace( ScriptKeys::yblegend, ScriptKeys::RE_LEGEND );
                contents.replace( ScriptKeys::ybcolor, ScriptKeys::RE_COLOR );
            }
            else if ( strl.at(2) == "T" )
            {
                contents.replace( ScriptKeys::yblabel, ScriptKeys::T_LABEL );
                contents.replace( ScriptKeys::ybindex, "3" );
                contents.replace( ScriptKeys::yblegend, ScriptKeys::T_LEGEND );
                contents.replace( ScriptKeys::ybcolor, ScriptKeys::T_COLOR );
            }
            else if ( strl.at(2) == "e" )
            {
                contents.replace( ScriptKeys::yblabel, ScriptKeys::DEF_LABEL );
                contents.replace( ScriptKeys::ybindex, "1" );
                contents.replace( ScriptKeys::yblegend, ScriptKeys::DEF_LEGEND );
                contents.replace( ScriptKeys::ybcolor, ScriptKeys::DEF_COLOR );
            }
        }
        else if ( strl.size() == 4 )
        {
            if ( strl.at(2) == "EA" && strl.at(3) == "RE" )
            {
                contents.replace( ScriptKeys::yblabel, ScriptKeys::EA_LABEL );
                contents.replace( ScriptKeys::ybindex, "5" );
                contents.replace( ScriptKeys::yblegend, ScriptKeys::EA_LEGEND );
                contents.replace( ScriptKeys::ybcolor, ScriptKeys::EA_COLOR );
                contents.replace( ScriptKeys::ytlabel, ScriptKeys::RE_LABEL );
                contents.replace( ScriptKeys::ytindex, "4" );
                contents.replace( ScriptKeys::ytlegend, ScriptKeys::RE_LEGEND );
                contents.replace( ScriptKeys::ytcolor, ScriptKeys::RE_COLOR );
            }
            else if ( strl.at(2) == "e" && strl.at(3) == "T" )
            {
                contents.replace( ScriptKeys::yblabel, ScriptKeys::DEF_LABEL );
                contents.replace( ScriptKeys::ybindex, "1" );
                contents.replace( ScriptKeys::yblegend, ScriptKeys::DEF_LEGEND );
                contents.replace( ScriptKeys::ybcolor, ScriptKeys::DEF_COLOR );
                contents.replace( ScriptKeys::ytlabel, ScriptKeys::T_LABEL );
                contents.replace( ScriptKeys::ytindex, "3" );
                contents.replace( ScriptKeys::ytlegend, ScriptKeys::T_LEGEND );
                contents.replace( ScriptKeys::ytcolor, ScriptKeys::T_COLOR );
            }
        }

        // Save contents in the TeX File
        QString TeX_Path = _path + "/plots/LaTeX/" + plotName + ".tex";
        Files::save( TeX_Path, contents );
    }
    void compile( const QString& plotName )
    {
        QString cmd = "pdflatex";
        QStringList args;
        args << "-synctex=1"
             << "-interaction=nonstopmode"
             << "\"" + plotName + ".tex\"";
        // Execute compile TeX File
        System::exec( cmd, args, _path + "/plots/LaTeX" );
        Files::move( _path + "/plots/LaTeX/" + plotName + ".pdf",
                     _path + "/plots/" + plotName + ".pdf", true); // true is to force move
    }
    void convertPDFtoPNG( const QString& plotName )
    {
        QString cmd = "convert";
        QStringList args;
        args << "-density"
             << "600"
             << plotName + ".pdf"
             << plotName + ".png";
        // Execute conversion PDF to PNG
        System::exec( cmd, args, _path + "/plots" );
    }
};

#endif // SCRIPT_H
