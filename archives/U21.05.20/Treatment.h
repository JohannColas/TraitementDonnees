#ifndef TREATMENT_H
#define TREATMENT_H
/**********************************************/
#include <QApplication>
#include <QObject>
#include <QThread>
#include "InfoFile.h"
#include "TreatMeca.h"
#include "PointsCaract.h"
#include "Plot.h"
#include "Report.h"
#include "Settings.h"
#include "EA.h"
#include "CNI.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Thread
        : public QThread
{
    Q_OBJECT
    QString _path;
    Settings* _sets = 0;
    void run() override
    {
        InfoFile* infoF = new InfoFile;
        infoF->setPath( _path );

        if ( _sets->getBool( Keys::TreatMeca ) )
        {
            emit currentTask( "Treating MECA data..." );
            TreatMeca meca( infoF, _sets );
            meca.treat();
        }
        if ( _sets->getBool( Keys::TreatPtsCaract ) )
        {
            emit currentTask( "Treating Caract Points..." );
            PointsCaract ptsCaract;
            ptsCaract.run( _path, _sets );
        }
        if ( _sets->getBool( Keys::TreatEA ) )
        {
            emit currentTask( "Treating EA..." );
            EA ea( infoF->path() );
            ea.treat();
        }
        if ( _sets->getBool( Keys::TreatRE ) )
        {
            emit currentTask( "Treating RE..." );
        }
        if ( _sets->getBool( Keys::TreatCNI ) )
        {
            emit currentTask( "Treating CNI..." );
            CNI cni( infoF, _sets );
            cni.treat();
        }
        if ( _sets->getBool( Keys::TreatPlots ) )
        {
            emit currentTask( "Creating Plots..." );
            Plot plot( _path, _sets );
            plot.generate();
        }
        if ( _sets->getBool( Keys::TreatSynthese ) )
        {
            emit currentTask( "Creating Report..." );
            Report report( infoF, _sets );
            report.generate();
        }

        delete infoF;
        emit finished();
    }

public:
    Thread( const QString& path, Settings* sets )
    {
        _path = path;
        _sets = sets;
    }
signals:
    void finished();
    void currentTask( const QString& task );
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Treatment
        : public QObject
{
    Q_OBJECT

public:
    void go( const QString& path, Settings* sets )
    {
        Thread *thread = new Thread( path, sets );
        connect( thread, &Thread::finished,
                 this, &Treatment::finished );
        connect( thread, &Thread::currentTask,
                 this, &Treatment::currentTask );
        thread->start();
    }

public slots:

signals:
    void finished();
    void currentTask( const QString& task );
};

// obj* ob = new obj;
// od->download(...); // ou treat(...) ?
/**********************************************/
/**********************************************/
/**********************************************/

#endif // TREATMENT_H
