#ifndef CNI_H
#define CNI_H

#include <QObject>
#include <QFile>
#include <QProcess>
#include <QDebug>
#include <QElapsedTimer>
#include <QThread>
#include "Settings.h"
#include "InfoFile.h"

class CNIThread
        : public QThread
{
    Q_OBJECT
    InfoFile* _info = 0;
    Settings* _sets = 0;
    bool _treatImages = true;
    bool _makeVideo = true;
    void run() override
    {
        QDir dir( _info->path() + "/CNI/originales" );
        if ( dir.exists() )
        {
            QDir( _info->path() + "/CNI" ).mkpath("traitees");
            QStringList imagePaths = dir.entryList( {"*.tif"} );
            QString argFormat = "%03d";
            if ( imagePaths.size() > 1000 )
                argFormat = "%04d";
            if ( _treatImages )
            {
                emit sendTotImage( imagePaths.size() );
                double iter = 0;
                int scale = _info->lighterFactor();
                for ( QString path : imagePaths )
                {
                    QImage im( _info->path() + "/CNI/originales/" + path );
                    int width = im.width();
                    int height = im.height();
                    for ( int it = 0; it < width; ++it )
                        for ( int jt = 0; jt < height; ++jt )
                        {
                            QColor col = im.pixelColor( it, jt );
                            im.setPixelColor( it, jt, col.lighter(scale));
                        }
                    QString newpath = path.remove(".tif").remove("image");
                    int nbImage = newpath.toInt();
                    im.save( _info->path() + "/CNI/traitees/" + QString().asprintf( argFormat.toStdString().c_str(),  nbImage ) + ".png" );
                    ++iter;
                    emit sendCurrentImage( iter );
                }
            }
            if ( _makeVideo )
            {
                emit makingVideo();
                QString cmd = "ffmpeg";
                QStringList args;
                args << "-y" // -y overwrite output file
                     << "-framerate" << "5";
                //        args << "-f image2 ";
                //        args << "-s " + QString::number(width)+"x"+QString::number(height) + " ";
                args << "-i" << "traitees/" + argFormat + ".png";
                //        args << "-vcodec libx264";
                args << "-crf" << "20";
                args << "-pix_fmt" << "yuv420p";
                args << "CNI.mp4";

                System::exec( cmd, args, _info->path() + "/CNI" );
            }

        }

        emit finished();
    }

public:
    CNIThread( InfoFile* info = 0, Settings* sets = 0 )
    {
        _info = info;
        _sets = sets;
    }
    void avoidTreatImages()
    {
        _treatImages = false;
    }
    void avoidMakeVideo()
    {
        _makeVideo = false;
    }
signals:
    void finished();
    void sendCurrentImage( int currentImage );
    void sendTotImage( int totImages );
    void makingVideo();
};

class CNI
        : public QObject
{
    Q_OBJECT
    InfoFile* _info = 0;
    Settings* _sets = 0;

public:
    virtual ~CNI()
    {

    }
    CNI( InfoFile* info = 0, Settings* sets = 0 )
        : QObject()
    {
        _info = info;
        _sets = sets;
    }
    void treat()
    {
        if ( _info )
        {
            CNIThread *thread = new CNIThread( _info, _sets );
            connect( thread, &CNIThread::sendCurrentImage,
                     this, &CNI::sendCurrentImage );
            connect( thread, &CNIThread::sendTotImage,
                     this, &CNI::sendTotImage );
            connect( thread, &CNIThread::makingVideo,
                     this, &CNI::makingVideo );
            connect( thread, &CNIThread::finished,
                     this, &CNI::finished );
            thread->start();

        }
    }
    void treatTIF()
    {
        CNIThread *thread = new CNIThread( _info, _sets );
        connect( thread, &CNIThread::sendCurrentImage,
                 this, &CNI::sendCurrentImage );
        connect( thread, &CNIThread::sendTotImage,
                 this, &CNI::sendTotImage );
        connect( thread, &CNIThread::finished,
                 this, &CNI::finished );
        thread->avoidMakeVideo();
        thread->start();
    }
    void makeVideo()
    {
        CNIThread *thread = new CNIThread( _info, _sets );
        connect( thread, &CNIThread::makingVideo,
                 this, &CNI::makingVideo );
        connect( thread, &CNIThread::finished,
                 this, &CNI::finished );
        thread->avoidTreatImages();
        thread->start();
    }

signals:
    void sendCurrentImage( int currentImage );
    void sendTotImage( int totImages );
    void finished();
    void makingVideo();
};

#endif // CNI_H
