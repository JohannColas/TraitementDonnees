#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QUrl>
#include <QDesktopServices>
#include <QFileDialog>
#include <QDebug>
#include <QProgressBar>
#include <QElapsedTimer>
#include <QTime>
#include <QGraphicsScene>

#include "LaTeX.h"
#include "TreatMeca.h"
#include "RE.h"
#include "Treatment.h"
#include "Widgets/ProgressWidget.h"


MainWindow::~MainWindow()
{
    delete ui;
}
MainWindow::MainWindow( QWidget *parent )
    : QMainWindow( parent )
    , ui( new Ui::MainWindow )
{
    ui->setupUi( this );
    //
    LaTeX::setSettings( _settings );
    //
    QString essairDir = _settings->getString( Keys::EssaiDir );
    ui->pb_dirEssai->setText( "Chemin : " + essairDir );
    ui->pb_plotsTplPath->setText( _settings->getString( Keys::Plots, Keys::TplDir ) );
    ui->pb_reportsTplPath->setText( _settings->getString( Keys::Reports, Keys::TplDir ) );
    ui->pb_pythonPath->setText( _settings->getString( Keys::PythonDir ) );
    ui->cb_meca->setChecked( _settings->getBool( Keys::TreatMeca ) );
    ui->cb_plots->setChecked( _settings->getBool( Keys::TreatPlots ) );
    ui->cb_ptsCaract->setChecked( _settings->getBool( Keys::TreatPtsCaract ) );
    ui->cb_CNI->setChecked( _settings->getBool( Keys::TreatCNI ) );
    ui->cb_EA->setChecked( _settings->getBool( Keys::TreatEA ) );
    ui->cb_RE->setChecked( _settings->getBool( Keys::TreatRE ) );
    ui->cb_synthese->setChecked( _settings->getBool( Keys::TreatSynthese ) );
    // TreeWidget
    model->setRootPath( essairDir );
    ui->treeView->setModel( model );
    ui->treeView->setRootIndex( model->index(essairDir) );
    //
    ui->treeView->setColumnHidden( 1, true );
    ui->treeView->setColumnHidden( 2, true );
    ui->treeView->setColumnHidden( 3, true );
    ui->treeView->header()->hide();

    ui->tabWidget->setTabVisible( 2, false );
    ui->tabWidget->setTabVisible( 3, false );
    ui->tabWidget->setTabVisible( 4, false );
//    ui->tabWidget->setTabVisible( 8, false );
    //
    QRegExp doubleRE = QRegExp("^(-?)(0|([1-9][0-9]*))(\\.[0-9]+)?$");
    ui->champ_longueur->setValidator( new QRegExpValidator( doubleRE, ui->champ_longueur ));
    ui->champ_largeur->setValidator( new QRegExpValidator( doubleRE, ui->champ_largeur ));
    ui->champ_epaisseur->setValidator( new QRegExpValidator( doubleRE, ui->champ_epaisseur ));
    ui->champ_tcut->setValidator( new QRegExpValidator( doubleRE, ui->champ_tcut ));

    connect( ui->info_materiau, &QLineEdit::editingFinished,
             this, &MainWindow::saveInfo );
    connect( ui->info_eprouvette, &QLineEdit::editingFinished,
             this, &MainWindow::saveInfo );
    connect( ui->info_essai, &QLineEdit::editingFinished,
             this, &MainWindow::saveInfo );
    connect( ui->champ_longueur, &QLineEdit::editingFinished,
             this, &MainWindow::saveInfo );
    connect( ui->champ_largeur, &QLineEdit::editingFinished,
             this, &MainWindow::saveInfo );
    connect( ui->champ_epaisseur, &QLineEdit::editingFinished,
             this, &MainWindow::saveInfo );
    connect( ui->champ_tcut, &QLineEdit::editingFinished,
             this, &MainWindow::saveInfo );
    connect( ui->champ_suivi, &QLineEdit::editingFinished,
             this, &MainWindow::saveInfo );
    connect( ui->champ_commentaire, &PlainTextEdit::editingFinished,
             this, &MainWindow::saveInfo );
    connect( ui->sp_lighterFactor, &QSpinBox::editingFinished,
             this, &MainWindow::saveInfo );

    //
    ui->splitter->widget(0)->setMaximumWidth(350);

    ui->wid_CNIProcessing->hide();
}


// ---------------------------------------- //
// ---------------------------------------- //
// ------------- PARAMÈTRAGE -------------- //
// ---------------------------------------- //
// ---------------------------------------- //
void MainWindow::on_pb_dirEssai_released()
{
    QString path = _settings->getString( Keys::EssaiDir );
    if ( path.isEmpty() )
        path = QDir::homePath();
    QString newpath = QFileDialog::getExistingDirectory( this,
                                                         "Veuillez choisir le dossier contenant les résusltats de vos essais !",
                                                         path,
                                                         QFileDialog::ShowDirsOnly );
    if ( !newpath.isEmpty() )
    {
        ui->pb_dirEssai->setText( "Chemin : " + newpath );
        _settings->save( Keys::EssaiDir, newpath );
        model->setRootPath( newpath );
        ui->treeView->setRootIndex( model->index(newpath) );
    }
}
void MainWindow::on_pb_plotsTplPath_released()
{
    QString path = _settings->getString( Keys::Plots + "/" + Keys::TplDir );
    if ( path.isEmpty() )
        path = QDir::homePath();
    QString newpath = QFileDialog::getExistingDirectory( this,
                                                         "Veuillez choisir le dossier contenant les templates LaTeX des graphes !",
                                                         path,
                                                         QFileDialog::ShowDirsOnly );
    if ( !newpath.isEmpty() ) {
        ui->pb_plotsTplPath->setText( newpath );
        _settings->save( Keys::Plots + "/" + Keys::TplDir, newpath );
    }
}
void MainWindow::on_pb_reportsTplPath_released()
{
    QString path = _settings->getString( Keys::Reports, Keys::TplDir );
    if ( path.isEmpty() )
        path = QDir::homePath();
    QString newpath = QFileDialog::getExistingDirectory( this,
                                                         "Veuillez choisir le dossier contenant les templates LaTeX des rapports !",
                                                         path,
                                                         QFileDialog::ShowDirsOnly );
    if ( !newpath.isEmpty() ) {
        ui->pb_reportsTplPath->setText( newpath );
        _settings->save( Keys::Reports + "/" + Keys::TplDir, newpath );
    }
}
void MainWindow::on_pb_pythonPath_released()
{
    QString path = _settings->getString( Keys::PythonDir);
    if ( path.isEmpty() )
        path = QDir::homePath();
    QString newpath = QFileDialog::getExistingDirectory( this,
                                                         "Veuillez choisir le dossier contenant les templates LaTeX des rapports !",
                                                         path,
                                                         QFileDialog::ShowDirsOnly );
    if ( !newpath.isEmpty() ) {
        ui->pb_pythonPath->setText( newpath );
        _settings->save( Keys::PythonDir, newpath );
    }
}
void MainWindow::on_cb_meca_toggled( bool checked )
{
    _settings->save( Keys::TreatMeca, checked );
}
void MainWindow::on_cb_ptsCaract_toggled( bool checked )
{
    _settings->save( Keys::TreatPtsCaract, checked );
}
void MainWindow::on_cb_EA_toggled( bool checked )
{
    _settings->save( Keys::TreatEA, checked );
}
void MainWindow::on_cb_RE_toggled( bool checked )
{
    _settings->save( Keys::TreatRE, checked );
}
void MainWindow::on_cb_CNI_toggled( bool checked )
{
    _settings->save( Keys::TreatCNI, checked );
}
void MainWindow::on_cb_plots_toggled( bool checked )
{
    _settings->save( Keys::TreatPlots, checked );
}
void MainWindow::on_cb_synthese_toggled( bool checked )
{
    _settings->save( Keys::TreatSynthese, checked );
}


void MainWindow::on_treeView_clicked( const QModelIndex &index )
{
    QString path = model->filePath( index );
    info_file->clear();
    QFileInfo meca( path + "/MECA.dat" );
    QFileInfo meca2( path + "/MECA.csv" );
    if ( meca.exists() || meca2.exists() )
    {
        info_file->setPath( path );
        EA_filterfile->setPath( path );
        updateInfoView();
        updatePlotSelector();

        if ( QFileInfo::exists(path+"/EA/EA.dat") )
        {
            ui->tabWidget->setTabVisible( 2, true );
            int ampli = EA_filterfile->toInt(EA_KEYS::SeuilAmpli);
            if ( ampli == -1 )
            {
                ampli = 40;
                EA_filterfile->save(EA_KEYS::SeuilAmpli, ampli);
            }
            ui->sp_seuilAmp->setValue( ampli );
            int cps = EA_filterfile->toInt(EA_KEYS::SeuilCps);
            if ( cps == -1 )
            {
                cps = 2;
                EA_filterfile->save(EA_KEYS::SeuilCps, cps);
            }
            ui->sp_seuilCps->setValue( cps );
            int durat = EA_filterfile->toInt(EA_KEYS::SeuilDur);
            if ( durat == -1 )
            {
                durat = 2000;
                EA_filterfile->save(EA_KEYS::SeuilDur, durat);
            }
            ui->sp_seuilDur->setValue( durat );
            int ener = EA_filterfile->toInt(EA_KEYS::SeuilEner);
            if ( ener == -1 )
            {
                ener = 20;
                EA_filterfile->save(EA_KEYS::SeuilEner, ener);
            }
            ui->sp_seuilEner->setValue( ener);
            int rise = EA_filterfile->toInt(EA_KEYS::SeuilRise);
            if ( rise == -1 )
            {
                rise = 100;
                EA_filterfile->save(EA_KEYS::SeuilRise, rise);
            }
            ui->sp_seuilRise->setValue( rise );
            int time = EA_filterfile->toInt(EA_KEYS::SeuilTime);
            if ( time == -1 )
            {
                time = 0;
                EA_filterfile->save(EA_KEYS::SeuilTime, time);
            }
            ui->sp_seuilTime->setValue( time );
        }
        else
            ui->tabWidget->setTabVisible( 2, false );
        if ( QFileInfo::exists(path+"/RE/RE.dat")  )
            ui->tabWidget->setTabVisible( 3, true );
        else
            ui->tabWidget->setTabVisible( 3, false );
        if ( QDir(path+"/CNI").exists() )
            ui->tabWidget->setTabVisible( 4, true );
        else
            ui->tabWidget->setTabVisible( 4, false );

        if ( ui->tabWidget->currentIndex() == 0 )
        {
        }
        else if ( ui->tabWidget->currentIndex() == 1 )
            on_meca_rawdata_released();
        else if ( ui->tabWidget->currentIndex() == 2 )
        {

        }
        else if ( ui->tabWidget->currentIndex() == 3 )
        {

        }
        else if ( ui->tabWidget->currentIndex() == 4 )
        {

        }
    }
    else
    {
        clearContainer();
        EA_filterfile->setPath();
    }
}

void MainWindow::on_treeView_doubleClicked( const QModelIndex &index )
{
    QString path = model->filePath( index );
    QDesktopServices::openUrl( QUrl( "file:"+path ) );
}

void MainWindow::on_meca_rawdata_released()
{
    ui->meca_rawdata->setChecked( true );
    ui->meca_traite->setChecked( false );
    ui->meca_ptsCaract->setChecked( false );
    QString path = model->filePath( ui->treeView->currentIndex() );
    QString contents;
    if ( !Files::read( path + "/MECA.dat", contents, "ISO 8859-1" ) )
        if ( !Files::read( path + "/MECA.csv", contents ) )
            contents =  "Les données de l'essai sont indisponible !!\n\n"
                        "Veuillez vérifier que le fichier MECA.dat ou MECA.csv existe !!";
    ui->meca_text_file->setPlainText( contents );
}
void MainWindow::on_meca_traite_released()
{
    ui->meca_rawdata->setChecked( false );
    ui->meca_traite->setChecked( true );
    ui->meca_ptsCaract->setChecked( false );
    QString path = model->filePath( ui->treeView->currentIndex() );
    QString contents;
    if ( !Files::read( path + "/MECA_DEF_CONT.dat", contents ) )
        contents =  "Le traitement n'a pas été réalisé !!";
    ui->meca_text_file->setPlainText( contents );
}
void MainWindow::on_meca_ptsCaract_released()
{
    ui->meca_rawdata->setChecked( false );
    ui->meca_traite->setChecked( false );
    ui->meca_ptsCaract->setChecked( true );
    QString path = model->filePath( ui->treeView->currentIndex() );
    QString contents;
    if ( !Files::read( path + "/meca/points_caracteristiques.dat", contents ) )
        contents =  "Les points caractéristiques n'ont pas été récupérés !!\n\n"
                    "Veuillez cliquez sur le bouton en bas pour réaliser cette tâche !";
    ui->meca_text_file->setPlainText( contents );
}
void MainWindow::on_pb_ptsCaract_clicked()
{
    PointsCaract ptsCaract;
    ptsCaract.run( info_file->path(), _settings );
}


void MainWindow::updateInfoView()
{
    ui->info_materiau->setText( info_file->materiau() );
    ui->info_eprouvette->setText( info_file->eprouvette() );
    ui->info_essai->setText( info_file->essai() );
    // Dimensions éprouvettes
    ui->champ_longueur->setText( QString::number( info_file->longueur() ) );
    ui->champ_largeur->setText( QString::number( info_file->largeur() ) );
    ui->champ_epaisseur->setText( QString::number( info_file->epaisseur() ) );
    ui->champ_tcut->setText( QString::number( info_file->tcut() ) );
    ui->champ_suivi->setText( info_file->donneesDispo() );
    // Commentaires
    ui->champ_commentaire->setPlainText( info_file->commentaire() );
    //
    ui->sp_lighterFactor->setValue( info_file->lighterFactor() );
}
void MainWindow::saveInfo()
{
    QString path = model->filePath( ui->treeView->currentIndex() );
    if ( path.contains(" - ") )
    {
        //
        info_file->setMateriau( ui->info_materiau->text() );
        info_file->setEprouvette( ui->info_eprouvette->text());
        info_file->setEssai( ui->info_essai->text() );
        // Dimensions éprouvettes
        info_file->setLongueur( ui->champ_longueur->text().toDouble() );
        info_file->setLargeur( ui->champ_largeur->text().toDouble() );
        info_file->setEpaisseur( ui->champ_epaisseur->text().toDouble() );
        info_file->setTcut( ui->champ_tcut->text().toDouble() );
        info_file->setDonneesDispo( ui->champ_suivi->text() );
        // Commentaires
        info_file->setCommentaire( ui->champ_commentaire->toPlainText() );
        //
        info_file->setLighterFactor( ui->sp_lighterFactor->value() );
        info_file->save();
    }
}


void MainWindow::clearContainer()
{
    ui->tabWidget->setTabVisible( 2, false );
    ui->tabWidget->setTabVisible( 3, false );
    ui->tabWidget->setTabVisible( 4, false );
    //
    info_file->fullClear();
    ui->info_materiau->setText( "" );
    ui->info_eprouvette->setText( "" );
    ui->info_essai->setText( "" );
    // Dimensions éprouvettes
    ui->champ_longueur->setText( "" );
    ui->champ_largeur->setText( "" );
    ui->champ_epaisseur->setText( "" );
    ui->champ_tcut->setText( "" );
    ui->champ_suivi->setText( "" );
    // Commentaires
    ui->champ_commentaire->setPlainText( "" );
    //
    ui->meca_text_file->setPlainText( "" );
    //
    updatePlotSelector();
}

void MainWindow::on_tabWidget_currentChanged( int index )
{
    if ( index == 0 )
    {
//        QString path = model->filePath( ui->treeView->currentIndex() );
//        QFile file( path+"/INFO" );
    }
    else if ( index == 1 )
    {
        on_meca_rawdata_released();
    }
}

void MainWindow::updatePlotSelector()
{
    QDir dir( info_file->path() + "/plots" );
    QStringList pngs = dir.entryList( {"*.png", "*.tiff", "*.jpg"}, QDir::NoDotAndDotDot | QDir::Files );
    ui->plotSelector->clear();
    ui->plotSelector->addItems(pngs);
}
void MainWindow::on_plotSelector_currentTextChanged( const QString &arg1 )
{
    ui->imageViewer->loadFile( info_file->path() + "/plots/" + arg1 );
}


void MainWindow::on_pb_traiterCetEssai_released()
{
    _treatAll = false;
    QString path = model->filePath( ui->treeView->currentIndex() );
    QFileInfo info( path + "/ESSAI.info" );
    QFileInfo meca( path + "/MECA.dat" );
    QFileInfo meca2( path + "/MECA.csv" );
    if ( info.exists() && ( meca.exists() || meca2.exists() ) )
    {
        //
        saveInfo();
        //
        treatDirectory( path );
        //
        updateInfoView();
        updatePlotSelector();
        on_meca_traite_released();
    }
}
void MainWindow::on_pb_toutTraiter_released()
{
    _treatAll = true;
    ProgressWidget* progress = new ProgressWidget;
    connect( progress, SIGNAL(stopT()),
             this, SLOT(stopTreatment()) );
    statusBar()->addWidget( progress );


    QStringList list = listDirectories( _settings->getString( Keys::EssaiDir ) );
    int size = list.size(), it = 0;
    QElapsedTimer timer;
    timer.start();
    for ( const QString &dir : list )
    {
        if ( _stopTreatement == true )
            break;
        progress->setProgress( (100*it)/size );

        if ( it != 0 )
        {
            QTime time = QTime(0,0,0).addSecs( (size-it)*timer.elapsed()/(1000*it) );
            progress->setRemaintime( time );
        }
        treatDirectory( dir );

        ++it;
    }

    statusBar()->removeWidget( progress );
    delete progress;
    _stopTreatement = false;
    _treatAll = false;
    updatePlotSelector();
}
void MainWindow::treatDirectory( const QString& path )
{
    QLabel* statusLabel = 0;
    if ( !_treatAll )
    {
        statusLabel = new QLabel("Processing...");
        statusBar()->addWidget( statusLabel );
    }
    QEventLoop loop;
    Treatment treat;
    connect( &treat, SIGNAL(finished()),
             &loop, SLOT(quit()) );
    if ( !_treatAll )
        connect( &treat, &Treatment::currentTask,
                 statusLabel, &QLabel::setText );
    treat.go( path, _settings );
    loop.exec();
    if ( !_treatAll )
        statusBar()->removeWidget( statusLabel );
    if ( statusLabel )
        delete statusLabel;
}
QStringList MainWindow::listDirectories( const QString& path )
{
    QStringList dirList = QDir(path).entryList(QDir::NoDotAndDotDot | QDir::Dirs );
    QStringList temp;
    bool recursiveSearch = _settings->getBool( Keys::RecursiveSearch );
    for ( QString dir : dirList )
    {
        QFileInfo info( path + "/" + dir + "/ESSAI.info" );
        QFileInfo meca( path + "/" + dir + "/MECA.dat" );
        QFileInfo meca2( path + "/" + dir + "/MECA.csv" );
        if ( info.exists() && ( meca.exists() || meca2.exists() ) )
            temp.append( path + "/" + dir );
        // Recursive Search
        if ( recursiveSearch )
            temp.append( listDirectories( path + "/" + dir ) );
    }
    return temp;
}
void MainWindow::stopTreatment()
{
    _stopTreatement = true;
}
void MainWindow::on_traite_EA_released()
{
    EA ea( info_file->path() );
    ea.treat();
}
void MainWindow::on_traite_RE_released()
{
    RE treat( info_file, _settings );
    treat.treat();
}
void MainWindow::on_traite_CNI_released()
{
    ui->wid_CNIProcessing->show();
    ui->wid_CNItreatImgs->show();
    ui->lb_CNImakingVideo->hide();
    CNI* cni = new CNI( info_file, _settings );
    connect( cni, &CNI::sendCurrentImage,
             ui->lb_curImg, &NumberLabel::setInt );
    connect( cni, &CNI::sendTotImage,
             ui->lb_totImg, &NumberLabel::setInt );
    connect( cni, &CNI::makingVideo,
             ui->lb_CNImakingVideo, &QWidget::show );
    connect( cni, &CNI::makingVideo,
             ui->wid_CNItreatImgs, &QWidget::hide );
    connect( cni, &CNI::finished,
             ui->wid_CNIProcessing, &QWidget::hide );
    cni->treat();
}
void MainWindow::on_pb_CNIVideo_released()
{
    ui->wid_CNIProcessing->show();
    ui->wid_CNItreatImgs->hide();
    ui->lb_CNImakingVideo->show();
    CNI* cni = new CNI( info_file, _settings );
    connect( cni, &CNI::finished,
             ui->wid_CNIProcessing, &QWidget::hide );
    cni->makeVideo();
}




void MainWindow::on_sp_seuilAmp_valueChanged( int value )
{
    EA_filterfile->save( EA_KEYS::SeuilAmpli, value );
}
void MainWindow::on_sp_seuilTime_valueChanged( int value )
{
    EA_filterfile->save( EA_KEYS::SeuilTime, value );
}
void MainWindow::on_sp_seuilRise_valueChanged( int value )
{
    EA_filterfile->save( EA_KEYS::SeuilRise, value );
}
void MainWindow::on_sp_seuilDur_valueChanged( int value )
{
    EA_filterfile->save( EA_KEYS::SeuilDur, value );
}
void MainWindow::on_sp_seuilCps_valueChanged( int value )
{
    EA_filterfile->save( EA_KEYS::SeuilCps, value );
}
void MainWindow::on_sp_seuilEner_valueChanged( int value )
{
    EA_filterfile->save( EA_KEYS::SeuilEner, value );
}

void MainWindow::on_cmd_Script_editingFinished()
{
    ui->cmd_Script->setPlainText( ui->cmd_Script->toPlainText() + "\n" );
    Script::launch( ui->cmd_Script->toPlainText() );
}
