QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    #Widgets/TreeView.h \
    #Widgets/TreeView.h \
    CNI.h \
    EA.h \
    Files.h \
    InfoFile.h \
    LaTeX.h \
    Plot.h \
    PointsCaract.h \
    RE.h \
    Report.h \
    Script.h \
    Settings.h \
    System.h \
    TreatMeca.h \
    Treatment.h \
    Widgets/Graph.h \
    Widgets/ImageViewer.h \
    Widgets/NumberLabel.h \
    Widgets/PlainTextEdit.h \
    Widgets/ProgressWidget.h \
    Widgets/ScriptEdit.h \
    Widgets/TreeView.h \
    mainwindow.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
	resources.qrc
