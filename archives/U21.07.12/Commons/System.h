#ifndef SYSTEM_H
#define SYSTEM_H
/**********************************************/
#include <QDir>
#include <QProcess>
#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class System
{
public:
    static inline bool move( const QString& oldpath, const QString& newpath, bool forced = false )
    {
        if ( forced )
            if ( QFile::exists( newpath ) )
                QFile::remove( newpath );
        if ( !QFile::copy( oldpath, newpath ) )
            return false;
       QFile::remove( oldpath );
       return true;
    }
    /**********************************************/
    /**********************************************/
    static inline QDir createDir( const QString& dirpath, const QString& dirname )
    {
        QDir dir( dirpath );
        if ( !dir.exists( dirname ) )
            dir.mkdir( dirname );
        dir.cd( dirname );
        return dir;
    }
    /**********************************************/
    /**********************************************/
    static inline bool remove( const QString& path)
    {
        if ( QFile::exists( path ) )
            return QFile::remove( path );
       return false;
    }
    /**********************************************/
    /**********************************************/
    static void exec( const QString& cmd, const QStringList& args = {}, const QString& workingPath = "", int killAt = -1 )
    {
            QProcess process;
            if ( workingPath != "" )
                process.setWorkingDirectory( workingPath );
            else
            {
                QDir( QDir::currentPath() ).mkdir( "PDFLaTeX_Compilation" );
                process.setWorkingDirectory( QDir::currentPath() + "/PDFLaTeX_Compilation" );
            }

            process.start( cmd, args, QIODevice::ReadOnly ); //Starts execution of command
            process.waitForFinished( killAt );

//            for ( QString str : cmd )
//                qDebug() << str;
//            qDebug() << "Failed:" << process.readAllStandardError();
//            qDebug() << "Output:" << process.readAll();
//            qDebug() << "";
//            qDebug() << "";
    }
    /**********************************************/
    /**********************************************/
    static QString TimeToString( int timems )
    {
        QString str;
        int nbs = timems/1000;
        int hh = nbs/3600;
        int mm = (nbs - 3600*hh)/60;
        int ss = nbs - 3600*hh - 60*mm;
        if ( hh != 0 )
            str += QString("%1h").arg(hh);
        if ( !str.isEmpty() )
            str += " ";
        if ( mm != 0 )
            str += QString("%1m").arg(mm);
        if ( !str.isEmpty() )
            str += " ";
        str += QString("%1s").arg(ss);
        return str;
    }
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SYSTEM_H
