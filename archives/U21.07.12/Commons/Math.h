#ifndef MATH_H
#define MATH_H
/**********************************************/
#include <QVector>
#include <cmath>
#include "Settings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Math
{
public:
    static inline void MoyenneGlissee( QVector<QVector<double>>& _data )
    {
        int size = _data.size();
        if ( size == 0 )
            return;

        int nbPts = Settings::getInt( Keys::Lissage, Keys::nbPoints );
        int hNbPts = 0.5*nbPts;
        QVector<QVector<double>> temp = _data;
        for ( int it = 0; it < size; ++it )
        {
            int lmin = 0;
            int lmax = 0;
            if ( it < hNbPts )
            {
                lmin = 0;
                lmax = it + hNbPts;
            }
            else if ( it > size - 1 - hNbPts )
            {
                lmin = it - hNbPts;
                lmax = size-1;
            }
            else
            {
                lmin = it - hNbPts;
                lmax = it + hNbPts;
            }
            temp[it][0] = _data[it][0];
            for ( int jt = 1; jt < _data[it].size(); ++jt )
            {
                double somme = 0;
                int count = 0;
                for ( int lt = lmin; lt <= lmax; ++lt )
                {
                    somme += _data[lt][jt];
                    ++count;
                }
                temp[it][jt] = somme/count;
            }
        }
        _data = temp;
    }
    /**********************************************/
    /**********************************************/
    static inline QVector<QVector<double>> RegressionLineaire( const QVector<QVector<double>>& _data, int xcol, int ycol )
    {
        if ( _data.size() == 0 )
            return {};

        int nbPts = Settings::getInt( Keys::Regression, Keys::sampling );
        int hNbPts = 0.5*nbPts;
        int size = _data.size();
        QVector<QVector<double>> temp;
        for ( int it = 0; it < size; ++it )
        {
            double alpha = 0;
            double beta = 0;
            double xi = 0;
            double yi = 0;
            double wi = 1;
            double A = 0;
            double B = 0;
            double C = 0;
            double D = 0;
            double E = 0;
            int jmin = 0;
            int jmax = 0;
            if ( it < hNbPts )
            {
                jmin = 0;
                jmax = it + hNbPts;
            }
            else if ( it > size - hNbPts - 1 )
            {
                jmin = it - hNbPts;
                jmax = size;
            }
            else
            {
                jmin = it - hNbPts;
                jmax = it + hNbPts;
            }
            for ( int jt = jmin; jt < jmax ; ++jt )
            {
                xi = _data[jt][xcol]/100;
                yi = _data[jt][ycol]/1000;
                A += wi*xi*xi;
                B += wi;
                C += wi*xi;
                D += wi*xi*yi;
                E += wi*yi;
            }
            alpha = (B*D - C*E)/(A*B-C*C);
            beta = (D - A*alpha)/C;
            temp.append( {_data[it][0], _data[it][1], _data[it][2], alpha,beta} );
        }
        return temp;
    }
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // MATH_H
