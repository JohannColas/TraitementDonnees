#ifndef PLAINTEXTEDIT_H
#define PLAINTEXTEDIT_H

#include <QTextEdit>

class PlainTextEdit
        : public QTextEdit
{
    Q_OBJECT
public:
    PlainTextEdit( QWidget* parent = 0 )
        : QTextEdit( parent )
    {

    }


protected slots:
    void focusOutEvent( QFocusEvent* event )
    {
        emit editingFinished();
        QTextEdit::focusOutEvent( event );
    }

signals:
    void editingFinished();
};

#endif // PLAINTEXTEDIT_H
