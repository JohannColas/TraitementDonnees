#ifndef SCRIPTEDIT_H
#define SCRIPTEDIT_H

#include <QPlainTextEdit>

class ScriptEdit
        : public QPlainTextEdit
{
    Q_OBJECT

public:
    ScriptEdit( QWidget* parent = 0 )
        : QPlainTextEdit( parent )
    {
        setPlainText( "compare e-s name SGL-FLEX addAll &SGL&FLEX!VIEIL" );
    }
};

#endif // SCRIPTEDIT_H
