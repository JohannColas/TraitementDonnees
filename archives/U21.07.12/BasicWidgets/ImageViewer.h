#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H
/**********************************************/
#include <QMenu>
#include <QAction>
#include <QImageReader>
#include <QImage>
#include <QPixmap>
#include <QColorSpace>
#include <QHBoxLayout>
#include <QImageReader>
#include <QImageWriter>
#include <QKeyEvent>
#include <QLabel>
#include <QWheelEvent>
#include <QShortcut>
/**********************************************/
/**********************************************/
/**********************************************/
class ImageViewer
        : public QWidget
{
    Q_OBJECT
private:
    QImage image;
    QLabel *imageLabel;
    double scaleFactor = 1;
    QAction *zoomInAct;
    QAction *zoomOutAct;
    QAction *normalSizeAct;
    QAction *fitToWindowAct;
    QMenu *viewMenu;
    int _x_inc = 20;
    int _y_inc = 20;

public:
    ImageViewer( QWidget* parent = 0 )
        : QWidget(parent), imageLabel(new QLabel(this))
     {
         imageLabel->setBackgroundRole(QPalette::Base);
         imageLabel->setStyleSheet( "background:white;" );
         imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
         imageLabel->setScaledContents(true);
         imageLabel->hide();

         setContextMenuPolicy( Qt::CustomContextMenu );
         connect( this, &ImageViewer::customContextMenuRequested,
                  this, &ImageViewer::showContextMenu );
         createActions();

         installEventFilter(this);
     }

     bool loadFile(const QString &fileName)
     {
         QImageReader reader(fileName);
         reader.setAutoTransform(true);
         const QImage newImage = reader.read();
         if (newImage.isNull()) {
             imageLabel->hide();
             return false;
         }
         setImage(newImage);

         setWindowFilePath(fileName);
         normalSize();
         imageLabel->show();
         return true;
     }
private:
     void setImage(const QImage &newImage)
     {
         image = newImage;
         if (image.colorSpace().isValid())
             image.convertToColorSpace(QColorSpace::SRgb);
         imageLabel->setPixmap(QPixmap::fromImage(image));
         scaleFactor = 1.0;

         fitToWindowAct->setEnabled(true);
         updateActions();

         imageLabel->move( 0, 0 );
         normalSize();
     }

public slots:
     void showContextMenu( const QPoint& pos )
     {
         viewMenu->exec( this->mapToGlobal(pos) );
     }
     void zoomIn()
     {
         scaleImage(1.25);
     }
     void zoomOut()
     {
         scaleImage(0.8);
     }
     void goLeft()
     {
         int pos = imageLabel->pos().x() + _x_inc;
         bool imageIsLarger = ( imageLabel->width() > this->width() );

         if ( imageIsLarger && pos > 0 )
             pos = 0;
         else if ( !imageIsLarger && pos > this->width() - imageLabel->width() )
             pos = this->width() - imageLabel->width();

         imageLabel->move( pos, imageLabel->pos().y() );
     }
     void goRight()
     {
         int pos = imageLabel->pos().x() - _x_inc;
         bool imageIsLarger = ( imageLabel->width() > this->width() );

         if ( imageIsLarger && pos < this->width() - imageLabel->width() )
             pos = this->width() - imageLabel->width();
         else if ( !imageIsLarger && pos < 0 )
             pos = 0;

         imageLabel->move( pos, imageLabel->pos().y() );
     }
     void goDown()
     {
         int pos = imageLabel->pos().y() - _y_inc;
         bool imageIsHigher = ( imageLabel->height() > this->height() );

         if ( imageIsHigher && pos < this->height() - imageLabel->height() )
             pos = this->height() - imageLabel->height();
         else if ( !imageIsHigher && pos < 0 )
             pos = 0;

         imageLabel->move( imageLabel->pos().x(), pos );
     }
     void goUp()
     {
         int pos = imageLabel->pos().y() + _y_inc;
         bool imageIsHigher = ( imageLabel->height() > this->height() );

         if ( imageIsHigher && pos > 0 )
             pos = 0;
         else if ( !imageIsHigher && pos > this->height() - imageLabel->height() )
             pos = this->height() - imageLabel->height();

         imageLabel->move( imageLabel->pos().x(), pos );
     }
     void normalSize()
     {
         int width = imageLabel->pixmap(Qt::ReturnByValue).size().width();
         int height = imageLabel->pixmap(Qt::ReturnByValue).size().height();
         double scaleFx = ((double)this->width()-20)/width;
         double scaleFy = ((double)this->height()-20)/height;
         double scaleF = scaleFy;
         if ( scaleFy > scaleFx )
             scaleF = scaleFx;

         scaleImage( scaleF/scaleFactor );
         imageLabel->move( 0, 0 );
     }
     void fitToWindow()
     {
         bool fitToWindow = fitToWindowAct->isChecked();
         if (!fitToWindow)
             normalSize();
         updateActions();
         imageLabel->move( 0, 0 );
     }
     void createActions()
     {

         viewMenu = new QMenu(tr("&View"));

         zoomInAct = viewMenu->addAction(tr("Zoom &In (25%)"), this, &ImageViewer::zoomIn);
//         zoomInAct->setShortcut(QKeySequence( Qt::CTRL + Qt::Key_Z ));
         zoomInAct->setEnabled(false);

         zoomOutAct = viewMenu->addAction(tr("Zoom &Out (25%)"), this, &ImageViewer::zoomOut);
//         zoomOutAct->setShortcut(QKeySequence( Qt::CTRL + Qt::Key_S ));
         zoomOutAct->setEnabled(false);

         normalSizeAct = viewMenu->addAction(tr("&Normal Size"), this, &ImageViewer::normalSize);
//         normalSizeAct->setShortcut(QKeySequence( Qt::CTRL + Qt::Key_A ));
         normalSizeAct->setEnabled(false);

         viewMenu->addSeparator();

         fitToWindowAct = viewMenu->addAction(tr("&Fit to Window"), this, &ImageViewer::fitToWindow);
         fitToWindowAct->setEnabled(false);
         fitToWindowAct->setCheckable(true);
//         fitToWindowAct->setShortcut(QKeySequence( Qt::CTRL + Qt::Key_Q ));
     }
     void updateActions()
     {
         zoomInAct->setEnabled(!fitToWindowAct->isChecked());
         zoomOutAct->setEnabled(!fitToWindowAct->isChecked());
         normalSizeAct->setEnabled(!fitToWindowAct->isChecked());
     }
     void scaleImage( double factor )
     {
         scaleFactor *= factor;
         if ( 0.05 < scaleFactor && scaleFactor < 20.0)
         {
             imageLabel->resize(scaleFactor * imageLabel->pixmap(Qt::ReturnByValue).size());
             imageLabel->move( scaleFactor * imageLabel->pos() );
         }
         zoomInAct->setEnabled(scaleFactor < 20.0);
         zoomOutAct->setEnabled(scaleFactor > 0.05);
     }
     bool eventFilter(QObject* /*obj*/, QEvent* evt)
     {
         if ( evt->type() == QEvent::Wheel )
         {
             QWheelEvent* event = static_cast<QWheelEvent*>(evt);
             if ( event->modifiers() == Qt::ControlModifier )
             {
                 if ( event->angleDelta().y() > 0 )
                     goUp();
                 else
                     goDown();
             }
             else if ( event->modifiers() == Qt::ShiftModifier )
             {
                 if ( event->angleDelta().y() > 0 )
                     goRight();
                 else
                     goLeft();
             }
             else
             {
                 if ( event->angleDelta().y() > 0 )
                     zoomIn();
                 else
                     zoomOut();
             }
             evt->ignore();
             return true;
         }
         else if ( evt->type() == QEvent::Resize )
         {
             normalSize();
         }
         return false;
     }
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // IMAGEVIEWER_H
