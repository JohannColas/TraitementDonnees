#ifndef NUMBERLABEL_H
#define NUMBERLABEL_H
#include <QLabel>

class NumberLabel
        : public QLabel
{
    Q_OBJECT
public:
    NumberLabel( QWidget* parent = 0 )
        : QLabel( parent )
    {

    }

public slots:
    void setInt( int number )
    {
        setText( QString::number( number ) );
    }
};

#endif // NUMBERLABEL_H
