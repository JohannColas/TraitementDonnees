QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Widgets/CNIDataWidget.cpp \
    Widgets/EADataWidget.cpp \
    Widgets/GraphWidget.cpp \
    Widgets/MecaDataWidget.cpp \
    Widgets/REDataWidget.cpp \
    Widgets/ScriptWidget.cpp \
    Widgets/SettingsWidget.cpp \
    Widgets/TestInfoWidget.cpp \
    main.cpp \
    MainWindow.cpp

HEADERS += \
    BasicWidgets/Graph.h \
    BasicWidgets/ImageViewer.h \
    BasicWidgets/NumberLabel.h \
    BasicWidgets/PlainTextEdit.h \
    BasicWidgets/ProgressWidget.h \
    BasicWidgets/ScriptEdit.h \
    BasicWidgets/TreeView.h \
    Commons/Files.h \
    Commons/LaTeX.h \
    Commons/Math.h \
    Commons/Settings.h \
    Commons/SettingsFile.h \
    Commons/System.h \
    Commons/TestInfo.h \
    MainWindow.h \
    Treatments/CNI.h \
    Treatments/EA.h \
    Treatments/Meca.h \
    Treatments/MecaPlots.h \
    Treatments/Plots.h \
    Treatments/PointsCaract.h \
    Treatments/RE.h \
    Treatments/Report.h \
    Treatments/Script.h \
    Treatments/Treatment.h \
    Widgets/CNIDataWidget.h \
    Widgets/EADataWidget.h \
    Widgets/GraphWidget.h \
    Widgets/MecaDataWidget.h \
    Widgets/REDataWidget.h \
    Widgets/ScriptWidget.h \
    Widgets/SettingsWidget.h \
    Widgets/TestInfoWidget.h

FORMS += \
    MainWindow.ui \
    Widgets/CNIDataWidget.ui \
    Widgets/EADataWidget.ui \
    Widgets/GraphWidget.ui \
    Widgets/MecaDataWidget.ui \
    Widgets/REDataWidget.ui \
    Widgets/ScriptWidget.ui \
    Widgets/SettingsWidget.ui \
    Widgets/TestInfoWidget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
	resources.qrc
