#ifndef TREATMENT_H
#define TREATMENT_H
/**********************************************/
#include <QApplication>
#include <QObject>
#include <QThread>
#include "Commons/TestInfo.h"
#include "Commons/Settings.h"
#include "Meca.h"
#include "PointsCaract.h"
#include "Plots.h"
#include "MecaPlots.h"
#include "Report.h"
#include "EA.h"
#include "CNI.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
struct TreatmentInfo
{
    bool TreatMeca = false;
    bool TreatPtsCaract = false;
    bool TreatEA = false;
    bool TreatRE = false;
    bool TreatCNI = false;
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TreatmentThread : public QThread
{
    Q_OBJECT
    QString _path;
    TreatmentInfo _treatInfo;
    /**********************************************/
    /**********************************************/
    void run() override
    {
        TestInfo* testinfo = new TestInfo;
        testinfo->setPath( _path );

        if ( Settings::getBool( Keys::TreatMeca ) )
        {
            emit currentTask( "Treating MECA data..." );
            Meca::Treat( testinfo );
        }
        if ( Settings::getBool( Keys::TreatPtsCaract ) )
        {
            emit currentTask( "Treating Caract Points..." );
            PointsCaract ptsCaract;
            ptsCaract.run( _path );
        }
        if ( Settings::getBool( Keys::TreatEA ) )
        {
            emit currentTask( "Treating EA..." );
            EA ea( testinfo->path() );
            ea.treat();
        }
        if ( Settings::getBool( Keys::TreatRE ) )
        {
            emit currentTask( "Treating RE..." );
        }
        if ( Settings::getBool( Keys::TreatCNI ) )
        {
            emit currentTask( "Treating CNI..." );
        }
        if ( Settings::getBool( Keys::TreatPlots ) )
            //if ( _treatInfo.TreatPlots )
        {
            emit currentTask( "Creating Plots..." );
            MecaPlots::GenerateMECA( testinfo );
        }
        if ( Settings::getBool( Keys::TreatSynthese ) )
        {
            emit currentTask( "Creating Report..." );
            Report report( testinfo );
            report.generate();
        }

        delete testinfo;
        emit finished();
    }
    /**********************************************/
    /**********************************************/
public:
    TreatmentThread( const QString& path, const TreatmentInfo& info = TreatmentInfo()  )
    {
        _path = path;
        _treatInfo = info;
    }
    /**********************************************/
    /**********************************************/
signals:
    void finished();
    void currentTask( const QString& task );
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Treatment
        : public QObject
{
    Q_OBJECT
public:
    void run( const QString& path, const TreatmentInfo& info = TreatmentInfo() )
    {
        TreatmentThread *thread = new TreatmentThread( path, info );
        connect( thread, &TreatmentThread::finished,
                 this, &Treatment::finished );
        connect( thread, &TreatmentThread::currentTask,
                 this, &Treatment::currentTask );
        thread->start();
    }
    /**********************************************/
    /**********************************************/
signals:
    void finished();
    void currentTask( const QString& task );
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/

#endif // TREATMENT_H
