#ifndef PLOTS_H
#define PLOTS_H
/**********************************************/
#include "Commons/Files.h"
#include "Commons/LaTeX.h"
#include <QProcess>
#include <QDebug>
#include "Commons/Settings.h"
#include "Commons/TestInfo.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Plots
{
public:
    static inline void Generate( const QString& path, const QStringList& plots, const QString& dataFileName )
    {
        // Pour chaque graphe à construire
        for ( QString plotName : plots )
        {
            // Create TeX File
//            LaTeX::buildPlotTeX( path, dataFileName+".dat", plotName );
            if ( QFileInfo(path+"/"+dataFileName+"_focused.dat").exists() )
            {
                // Create TeX File
//                LaTeX::buildPlotTeX( path, dataFileName+"_focused.dat", plotName, "_focused" );
            }
        }
    }
    /**********************************************/
    /**********************************************/
    static inline void GenerateMultiData( const QString& path,
                                          const QString& name,
                                          const QStringList& plots,
                                          const QStringList& dataDirs,
                                          const QStringList& dataLabels,
                                          const QStringList& dataColors = {} )
    {
        // Pour chaque graphe à construire
        for ( QString plotName : plots )
        {
            // Create TeX File
//            LaTeX::buildPlotTeX( path, name, plotName, dataDirs, dataLabels, dataColors );
        }
    }
    /**********************************************/
    /**********************************************/
    static inline void GenerateMECA( TestInfo* info )
    {
        QString path = info->path();
        Files::createDir( path, "plots" );
        Files::createDir( path + "/plots", "LaTeX" );

        QStringList plots;
        QString essai = info->getString( TestInfoKeys::TestName );
        QString suivi = info->getString( TestInfoKeys::AvailableTracking );
        if ( essai.left(6) != "VIEIL_" )
        {
            plots.append( { "t-s-e", "e-s" } );
            if ( suivi.contains("R.E.") &&
                 suivi.contains("E.A.") )
                plots.append( {"t-s-EA-RE","e-s-EA-RE"} );
            else
                if ( suivi.contains("R.E.") )
                    plots.append( {"t-s-RE", "e-s-RE"} );
                else if ( suivi.contains("E.A.") )
                    plots.append( {"t-s-EA", "e-s-EA"} );
        }
        else
        {
            plots.append( {"t-s-T", "t-s-e-T", "e-s"} );
        }

        Plots::Generate( path, plots, "MECA_DEF_CONT" );
        Plots::Generate( path, {"e-mt"}, "MECA_MOD_TANG" );
    }
    /**********************************************/
    /**********************************************/
    static inline void GenerateRE( TestInfo* info = 0 )
    {
        QString path = info->path()+"/RE";
        Files::createDir( path, "plots" );
        Files::createDir( path + "/plots", "LaTeX" );

        QStringList plots;
        plots.append( { "t-s-e-RE", "e-RE", "e-s-RE", "s-RE" } );

        Plots::Generate( path, plots, "RE_DEF_CONT" );
    }
    /**********************************************/
    /**********************************************/
    static inline void GenerateComparaison( const QString& name,
                                            const QString& dataToShow,
                                            const QStringList& dataToAdd,
                                            const QStringList& dataLabels,
                                            const QStringList& dataColors = {})
    {
        QString path = Settings::getString( Keys::EssaiDir );
        Files::createDir( path, "Comparaisons" );
        Files::createDir( path + "/Comparaisons", "LaTeX" );
        path += "/Comparaisons";

        Plots::GenerateMultiData( path, name, {dataToShow}, dataToAdd, dataLabels, dataColors );
//        LaTeX::buildPlotTeX( path, dataFileName+".dat", plotName );
    }
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLOTS_H
