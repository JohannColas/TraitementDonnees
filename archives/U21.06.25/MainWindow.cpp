#include "MainWindow.h"
#include "ui_MainWindow.h"
/**********************************************/
/**********************************************/
/**********************************************/
#include <QUrl>
#include <QDesktopServices>
#include <QFileDialog>
#include <QDebug>
#include <QProgressBar>
#include <QElapsedTimer>
#include <QTime>
#include <QGraphicsScene>
/**********************************************/
#include "Commons/LaTeX.h"
#include "Treatments/Meca.h"
#include "Treatments/RE.h"
#include "Treatments/Treatment.h"
#include "BasicWidgets/ProgressWidget.h"
/**********************************************/
/**********************************************/
/**********************************************/
MainWindow::~MainWindow()
{
    delete ui;
}
/**********************************************/
/**********************************************/
/**********************************************/
MainWindow::MainWindow( QWidget *parent )
    : QMainWindow( parent )
    , ui( new Ui::MainWindow )
{
    ui->setupUi( this );
    ui->wid_testinfo->linkToTestInfo( _testinfo );
    ui->wid_mecadata->linkToTestInfo( _testinfo );
    ui->wid_REdata->linkToTestInfo( _testinfo );
    ui->wid_EAdata->linkToTestInfo( _testinfo );
    ui->wid_CNIdata->linkToTestInfo( _testinfo );
    //
    //
    QString essairDir = Settings::getString( Keys::EssaiDir );
    ui->pb_dirEssai->setText( "Chemin : " + essairDir );
    // TreeWidget
    model->setRootPath( essairDir );
    ui->treeView->setModel( model );
    ui->treeView->setRootIndex( model->index(essairDir) );
    //
    ui->treeView->setColumnHidden( 1, true );
    ui->treeView->setColumnHidden( 2, true );
    ui->treeView->setColumnHidden( 3, true );
    ui->treeView->header()->hide();

    QPalette pal = palette();
    pal.setColor( QPalette::Window, pal.alternateBase().color() );
    ui->sel_menu->setAutoFillBackground(true);
    ui->sel_menu->setPalette(pal);
    ui->tabWidget->setTabVisible( 2, false );
    ui->tabWidget->setTabVisible( 3, false );
    ui->tabWidget->setTabVisible( 4, false );
    //
    ui->splitter->widget(0)->setMaximumWidth(350);
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::on_pb_dirEssai_released()
{
    QString path = Settings::getString( Keys::EssaiDir );
    if ( path.isEmpty() )
        path = QDir::homePath();
    QString newpath = QFileDialog::getExistingDirectory( this,
                                                         "Veuillez choisir le dossier contenant les résusltats de vos essais !",
                                                         path,
                                                         QFileDialog::ShowDirsOnly );
    if ( !newpath.isEmpty() )
    {
        ui->pb_dirEssai->setText( "Chemin : " + newpath );
        Settings::save( Keys::EssaiDir, newpath );
        model->setRootPath( newpath );
        ui->treeView->setRootIndex( model->index(newpath) );
    }
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::on_treeView_clicked( const QModelIndex &index )
{
    _currentPath = model->filePath( index );
    _testinfo->setPath( _currentPath );
    ui->wid_testinfo->updateTestInfo();
    ui->wid_graphs->setPath( _currentPath );
    if ( QFileInfo::exists(_currentPath+"/EA/EA.dat") )
        ui->tabWidget->setTabVisible( 2, true );
    else
        ui->tabWidget->setTabVisible( 2, false );
    if ( QFileInfo::exists(_currentPath+"/RE/RE.dat")  )
        ui->tabWidget->setTabVisible( 3, true );
    else
        ui->tabWidget->setTabVisible( 3, false );
    if ( QDir(_currentPath+"/CNI").exists() )
        ui->tabWidget->setTabVisible( 4, true );
    else
        ui->tabWidget->setTabVisible( 4, false );
    if ( _testinfo->isValid() )
    {

        if ( ui->tabWidget->currentIndex() == 0 )
        {
        }
        else if ( ui->tabWidget->currentIndex() == 1 )
        {
            ui->wid_mecadata->update();
        }
    }
    else
    {
        clearContainer();
    }
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::on_treeView_doubleClicked( const QModelIndex &index )
{
    QString path = model->filePath( index );
    QDesktopServices::openUrl( QUrl( "file:"+path ) );
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::on_sel_essai_released()
{
    ui->stackedWidget->setCurrentIndex(0);
    ui->sel_essai->setChecked( true );
    ui->sel_graphs->setChecked( false );
    ui->sel_script->setChecked( false );
    ui->sel_parametres->setChecked( false );
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::on_sel_graphs_released()
{
    ui->stackedWidget->setCurrentIndex(1);
    ui->sel_essai->setChecked( false );
    ui->sel_graphs->setChecked( true );
    ui->sel_script->setChecked( false );
    ui->sel_parametres->setChecked( false );
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::on_sel_script_released()
{
    ui->stackedWidget->setCurrentIndex(2);
    ui->sel_essai->setChecked( false );
    ui->sel_graphs->setChecked( false );
    ui->sel_script->setChecked( true );
    ui->sel_parametres->setChecked( false );
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::on_sel_parametres_released()
{
    ui->stackedWidget->setCurrentIndex(3);
    ui->sel_essai->setChecked( false );
    ui->sel_graphs->setChecked( false );
    ui->sel_script->setChecked( false );
    ui->sel_parametres->setChecked( true );
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::on_tabWidget_currentChanged( int index )
{
    if ( index == 0 )
    {
//        QString path = model->filePath( ui->treeView->currentIndex() );
//        QFile file( path+"/INFO" );
    }
    else if ( index == 1 )
    {
//        on_meca_rawdata_released();
    }
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::clearContainer()
{
    ui->tabWidget->setTabVisible( 2, false );
    ui->tabWidget->setTabVisible( 3, false );
    ui->tabWidget->setTabVisible( 4, false );
    //
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::on_pb_traiterCetEssai_released()
{
    _treatAll = false;
    QString path = model->filePath( ui->treeView->currentIndex() );
    QFileInfo info( path + "/ESSAI.info" );
    QFileInfo meca( path + "/MECA.dat" );
    QFileInfo meca2( path + "/MECA.csv" );
    if ( info.exists() && ( meca.exists() || meca2.exists() ) )
    {
        //
        treatDirectory( path );
        //
        ui->wid_mecadata->update();
        ui->wid_graphs->update();
    }
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::on_pb_toutTraiter_released()
{
    _treatAll = true;
    ProgressWidget* progress = new ProgressWidget;
    connect( progress, SIGNAL(stopT()),
             this, SLOT(stopTreatment()) );
    statusBar()->addWidget( progress );

    QStringList list = Files::ListMecaDirectories( Settings::getString( Keys::EssaiDir ),
                                                   Settings::getBool( Keys::RecursiveSearch ) );
    int size = list.size(), it = 0;
    QElapsedTimer timer;
    timer.start();
    for ( const QString &dir : list )
    {
        if ( _stopTreatement == true )
            break;
        progress->setProgress( (100*it)/size );

        if ( it != 0 )
        {
            QTime time = QTime(0,0,0).addSecs( (size-it)*timer.elapsed()/(1000*it) );
            progress->setRemaintime( time );
        }
        treatDirectory( dir );

        ++it;
    }

    statusBar()->removeWidget( progress );
    delete progress;
    _stopTreatement = false;
    _treatAll = false;
    ui->wid_graphs->update();
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::treatDirectory( const QString& path )
{
    QLabel* statusLabel = 0;
    if ( !_treatAll )
    {
        statusLabel = new QLabel("Processing...");
        statusBar()->addWidget( statusLabel );
    }
    QEventLoop loop;
    Treatment treat;
    connect( &treat, SIGNAL(finished()),
             &loop, SLOT(quit()) );
    if ( !_treatAll )
        connect( &treat, &Treatment::currentTask,
                 statusLabel, &QLabel::setText );
    treat.run( path );
    loop.exec();
    if ( !_treatAll )
        statusBar()->removeWidget( statusLabel );
    if ( statusLabel )
        delete statusLabel;
}
/**********************************************/
/**********************************************/
/**********************************************/
void MainWindow::stopTreatment()
{
    _stopTreatement = true;
}
/**********************************************/
/**********************************************/
/**********************************************/
