#include "TestInfoWidget.h"
#include "ui_TestInfoWidget.h"
/**********************************************/
#include <QRegExpValidator>
/**********************************************/
/**********************************************/
TestInfoWidget::TestInfoWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TestInfoWidget)
{
    ui->setupUi(this);

    QRegExp doubleRE = QRegExp("^(-?)(0|([1-9][0-9]*))(\\.[0-9]+)?$");
    ui->champ_longueur->setValidator( new QRegExpValidator( doubleRE, ui->champ_longueur ));
    ui->champ_largeur->setValidator( new QRegExpValidator( doubleRE, ui->champ_largeur ));
    ui->champ_epaisseur->setValidator( new QRegExpValidator( doubleRE, ui->champ_epaisseur ));
    ui->champ_tcut->setValidator( new QRegExpValidator( doubleRE, ui->champ_tcut ));
}
/**********************************************/
/**********************************************/
/**********************************************/
TestInfoWidget::~TestInfoWidget()
{
    delete ui;
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::linkToTestInfo( TestInfo* info )
{
    _testinfo = info;
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::updateTestInfo()
{
    bool testinfoisvalid = _testinfo->isValid();
    QString val = "";
    if ( testinfoisvalid )
        val = _testinfo->getString(TestInfoKeys::Material);
    ui->info_materiau->setText( val );

    if ( testinfoisvalid )
        val = _testinfo->getString(TestInfoKeys::Sample);
    ui->info_eprouvette->setText( val );

    if ( testinfoisvalid )
        val = _testinfo->getString(TestInfoKeys::TestName);
    ui->info_essai->setText( val );

    if ( testinfoisvalid )
        val = QString::number( _testinfo->getDouble(TestInfoKeys::Length) );
    ui->champ_longueur->setText( val );

    if ( testinfoisvalid )
        val = QString::number( _testinfo->getDouble(TestInfoKeys::Width) );
    ui->champ_largeur->setText( val);

    if ( testinfoisvalid )
        val = QString::number( _testinfo->getDouble(TestInfoKeys::Thickness) );
    ui->champ_epaisseur->setText( val );

    if ( testinfoisvalid )
        val = QString::number( _testinfo->getDouble(TestInfoKeys::TimeCut) );
    ui->champ_tcut->setText( val );

    if ( testinfoisvalid )
        val = _testinfo->getString(TestInfoKeys::AvailableTracking);
    ui->champ_suivi->setText( val );

    if ( testinfoisvalid )
        val = _testinfo->getString(TestInfoKeys::Comments);
    ui->champ_commentaire->setPlainText( val );
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::on_info_materiau_editingFinished()
{
    if ( _testinfo->isValid() )
        _testinfo->save( ui->info_materiau->text(), TestInfoKeys::Material );
    else
        ui->info_materiau->setText("");
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::on_info_eprouvette_editingFinished()
{
    if ( _testinfo->isValid() )
        _testinfo->save( ui->info_eprouvette->text(), TestInfoKeys::Sample );
    else
        ui->info_eprouvette->setText("");
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::on_info_essai_editingFinished()
{
    if ( _testinfo->isValid() )
        _testinfo->save( ui->info_essai->text(), TestInfoKeys::TestName );
    else
        ui->info_essai->setText("");
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::on_champ_longueur_editingFinished()
{
    if ( _testinfo->isValid() )
        _testinfo->save( ui->champ_longueur->text().toDouble(), TestInfoKeys::Length );
    else
        ui->champ_longueur->setText("");
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::on_champ_largeur_editingFinished()
{
    if ( _testinfo->isValid() )
        _testinfo->save( ui->champ_largeur->text().toDouble(), TestInfoKeys::Width );
    else
        ui->champ_largeur->setText("");
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::on_champ_epaisseur_editingFinished()
{
    if ( _testinfo->isValid() )
        _testinfo->save( ui->champ_epaisseur->text().toDouble(), TestInfoKeys::Thickness );
    else
        ui->champ_epaisseur->setText("");
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::on_champ_tcut_editingFinished()
{
    if ( _testinfo->isValid() )
        _testinfo->save( ui->champ_tcut->text().toDouble(), TestInfoKeys::TimeCut );
    else
        ui->champ_tcut->setText("");
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::on_champ_suivi_editingFinished()
{
    if ( _testinfo->isValid() )
        _testinfo->save( ui->champ_suivi->text(), TestInfoKeys::AvailableTracking );
    else
        ui->champ_suivi->setText("");
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::on_champ_commentaire_editingFinished()
{
    if ( _testinfo->isValid() )
        _testinfo->save( ui->champ_commentaire->toPlainText(), TestInfoKeys::Comments );
    else
        ui->champ_commentaire->setText("");
}
/**********************************************/
/**********************************************/
/**********************************************/
