#ifndef GRAPHWIDGET_H
#define GRAPHWIDGET_H
/**********************************************/
#include <QWidget>
#include "Commons/Files.h"
#include "Commons/LaTeX.h"
#include "Commons/TestInfo.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
namespace Ui {
    class GraphWidget;
}
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class GraphWidget : public QWidget
{
    Q_OBJECT
public:
    explicit GraphWidget(QWidget *parent = nullptr);
    ~GraphWidget();
    void setPath( const QString& path )
    {
        _path = path;
        update();
    }
    void update();
    void checkLaTeX();
    /**********************************************/
    /**********************************************/
private slots:
    void on_plotSelector_currentTextChanged(const QString &arg1);
    void on_pb_modPlot_released();
    void on_pb_compile_released();
    /**********************************************/
    /**********************************************/
private:
    Ui::GraphWidget *ui;
    QString _path = QString();
    QString _plotpath = QString();
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPHWIDGET_H
