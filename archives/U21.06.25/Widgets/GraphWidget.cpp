#include "GraphWidget.h"
#include "ui_GraphWidget.h"
/**********************************************/
/**********************************************/
/**********************************************/
GraphWidget::GraphWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GraphWidget)
{
    ui->setupUi(this);
    ui->pb_modPlot->setChecked( false );
    ui->wd_modPlot->hide();
    ui->lb_PlotToShow->hide();
    ui->plotSelector->hide();
    ui->pb_modPlot->hide();
}
/**********************************************/
/**********************************************/
/**********************************************/
GraphWidget::~GraphWidget()
{
    delete ui;
}
/**********************************************/
/**********************************************/
/**********************************************/
void GraphWidget::update()
{
    QStringList extLs = {".png", ".tiff", ".jpg"};
    QString ext = _path.mid( _path.lastIndexOf(".") );
    if ( extLs.contains( ext, Qt::CaseInsensitive ) )
    {
        _plotpath = _path;
        ui->imageViewer->loadFile( _plotpath );
        ui->lb_PlotToShow->hide();
        ui->plotSelector->hide();
        checkLaTeX();
    }
    else
    {
        if ( QDir(_path).exists() )
        {
            QStringList images = Files::ListImages( _path, true);
            ui->lb_PlotToShow->show();
            ui->plotSelector->show();
            ui->plotSelector->clear();
            ui->plotSelector->addItems(images);
        }
    }
}
/**********************************************/
/**********************************************/
/**********************************************/
void GraphWidget::checkLaTeX()
{
    QFileInfo plotFile( _plotpath );
    QString plotTeXPath = plotFile.absolutePath() + "/LaTeX/" + plotFile.baseName() + ".tex";
    QFileInfo texFile( plotTeXPath );
    if ( texFile.exists() )
    {
        ui->pb_modPlot->show();
        on_pb_modPlot_released();
    }
    else
    {
        ui->pb_modPlot->setChecked( false );
        ui->pb_modPlot->hide();
        ui->wd_modPlot->hide();
    }
}
/**********************************************/
/**********************************************/
/**********************************************/
void GraphWidget::on_plotSelector_currentTextChanged( const QString &arg1 )
{
    _plotpath = _path + "/" + arg1;
    ui->imageViewer->loadFile( _plotpath );
    checkLaTeX();
    on_pb_modPlot_released();
}
/**********************************************/
/**********************************************/
/**********************************************/
void GraphWidget::on_pb_modPlot_released()
{
    if ( ui->pb_modPlot->isChecked() )
    {
        ui->pb_modPlot->setText( "Cacher LaTeX" );
        ui->le_plotSuffix->setText("");
        QFileInfo plotFile( _plotpath );
        QString plotTeXPath = plotFile.absolutePath() + "/LaTeX/" + plotFile.baseName() + ".tex";
        QFileInfo texFile( plotTeXPath );
        if ( texFile.exists() )
        {
            QString content;
            Files::read( texFile.absoluteFilePath(), content );
            ui->te_modPlot->setPlainText( content );
            ui->wd_modPlot->show();
        }
        else
            ui->te_modPlot->setPlainText( "" );
    }
    else
    {
        ui->pb_modPlot->setText( "Afficher LaTeX" );
        ui->te_modPlot->setPlainText( "" );
        ui->wd_modPlot->hide();
    }
}
/**********************************************/
/**********************************************/
/**********************************************/
void GraphWidget::on_pb_compile_released()
{
    QFileInfo plotFile( _plotpath );
    QString texPath = plotFile.absolutePath() + "/LaTeX/" + plotFile.baseName() + ".tex";
    if ( QFileInfo::exists(texPath) )
    {
        texPath = plotFile.absolutePath() + "/LaTeX/" + plotFile.baseName() + ui->le_plotSuffix->text() + ".tex";
        Files::save( texPath, ui->te_modPlot->toPlainText() );
        LaTeX::CompileAndConvertToPNG( plotFile.absolutePath(), plotFile.baseName() + ui->le_plotSuffix->text() );
        _plotpath = plotFile.absolutePath() + "/" + plotFile.baseName() + ui->le_plotSuffix->text() + ".png";
        QString plotpath2 = _plotpath.remove( _path + "/" );
        ui->le_plotSuffix->setText("");
        update();
        ui->plotSelector->setCurrentText( plotpath2 );
    }
}
/**********************************************/
/**********************************************/
/**********************************************/
