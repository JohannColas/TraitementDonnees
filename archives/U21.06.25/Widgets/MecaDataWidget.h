#ifndef MECADATAWIDGET_H
#define MECADATAWIDGET_H
/**********************************************/
#include <QWidget>
#include "Commons/Files.h"
#include "Commons/TestInfo.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
namespace Ui {
    class MecaDataWidget;
}
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class MecaDataWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MecaDataWidget( QWidget *parent = nullptr );
    ~MecaDataWidget();
    void linkToTestInfo( TestInfo *info );
    void update();
    void setMecaTab( const QStringList& lst );
    /**********************************************/
    /**********************************************/
private slots:
    void on_pb_ptsCaract_released();
    void on_cb_availabledata_currentTextChanged( const QString &file );
    /**********************************************/
    /**********************************************/
private:
    Ui::MecaDataWidget *ui;
    TestInfo* _testinfo = 0;
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // MECADATAWIDGET_H
