#ifndef TESTINFOWIDGET_H
#define TESTINFOWIDGET_H
/**********************************************/
#include <QWidget>
#include "Commons/TestInfo.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
namespace Ui {
    class TestInfoWidget;
}
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TestInfoWidget : public QWidget
{
    Q_OBJECT
public:
    explicit TestInfoWidget(QWidget *parent = nullptr);
    ~TestInfoWidget();
    void linkToTestInfo( TestInfo* info );
    void updateTestInfo();
    /**********************************************/
    /**********************************************/
private slots:
    void on_info_materiau_editingFinished();
    void on_info_eprouvette_editingFinished();
    void on_info_essai_editingFinished();
    void on_champ_longueur_editingFinished();
    void on_champ_largeur_editingFinished();
    void on_champ_epaisseur_editingFinished();
    void on_champ_tcut_editingFinished();
    void on_champ_suivi_editingFinished();
    void on_champ_commentaire_editingFinished();
    /**********************************************/
    /**********************************************/
private:
    Ui::TestInfoWidget *ui;
    TestInfo* _testinfo = 0;
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TESTINFOWIDGET_H
