#include "GraphWidget.h"
#include "ui_GraphWidget.h"

#include <QMessageBox>
/**********************************************/
/**********************************************/
/**********************************************/
GraphWidget::GraphWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GraphWidget)
{
    ui->setupUi(this);
    ui->pb_modPlot->setChecked( false );
    ui->wd_modPlot->hide();
    ui->lb_PlotToShow->hide();
    ui->plotSelector->hide();
    ui->pb_modPlot->hide();
    ui->pb_supprimer->hide();

    sc_showTeX = new QShortcut( this );
    sc_showTeX->setKey( QKeySequence( Qt::Key_F2 ) );
    connect( sc_showTeX, &QShortcut::activated,
             this, &GraphWidget::on_pb_modPlot_released );
    sc_compile = new QShortcut( this );
    sc_compile->setKey( QKeySequence( Qt::Key_F5 ) );
    connect( sc_compile, &QShortcut::activated,
             this, &GraphWidget::on_pb_compile_released );
    sc_nextPlot = new QShortcut( this );
    sc_nextPlot->setKey( QKeySequence( Qt::Key_F4 ) );
    connect( sc_nextPlot, &QShortcut::activated,
             this, &GraphWidget::nextPlot );
    sc_previousPlot = new QShortcut( this );
    sc_previousPlot->setKey( QKeySequence( Qt::Key_F3 ) );
    connect( sc_previousPlot, &QShortcut::activated,
             this, &GraphWidget::previousPlot );
}
/**********************************************/
/**********************************************/
/**********************************************/
GraphWidget::~GraphWidget()
{
    delete ui;
}
/**********************************************/
/**********************************************/
/**********************************************/
void GraphWidget::update()
{
    QStringList extLs = {".png", ".tiff", ".jpg"};
    QString ext = _path.mid( _path.lastIndexOf(".") );
    if ( !ext.isEmpty() && extLs.contains( ext, Qt::CaseInsensitive ) )
    {
        _plotpath = _path;
        ui->imageViewer->loadFile( _plotpath );
        ui->lb_plotName->setText( _path.mid( _path.lastIndexOf("/")+1 ) );
        ui->lb_plotName->show();
        ui->lb_PlotToShow->hide();
        ui->plotSelector->hide();
        ui->pb_supprimer->show();
        checkLaTeX();
    }
    else if ( !_path.isEmpty() && QDir(_path).exists() )
    {
        QStringList images = Files::ListImages( _path, true);
        ui->lb_plotName->setText( "" );
        ui->lb_plotName->hide();
        ui->lb_PlotToShow->show();
        ui->plotSelector->show();
        ui->plotSelector->clear();
        ui->plotSelector->addItems(images);
        ui->pb_supprimer->show();
    }
    else
    {
        ui->imageViewer->loadFile( "" );
        ui->lb_plotName->hide();
        ui->lb_PlotToShow->hide();
        ui->plotSelector->hide();
        ui->pb_modPlot->hide();
        ui->wd_modPlot->hide();
        ui->te_modPlot->setPlainText( "" );
        ui->le_filename->setText( "" );
        ui->pb_supprimer->hide();
    }
}
/**********************************************/
/**********************************************/
/**********************************************/
void GraphWidget::checkLaTeX()
{
    QFileInfo plotFile( _plotpath );
    QString plotTeXPath = plotFile.absolutePath() + "/LaTeX/" + plotFile.baseName() + ".tex";
    QFileInfo texFile( plotTeXPath );
    if ( texFile.exists() )
    {
        ui->pb_modPlot->show();
        showLaTeX();
    }
    else
    {
        ui->pb_modPlot->hide();
        ui->wd_modPlot->hide();
        ui->te_modPlot->setPlainText( "" );
    }
}
void GraphWidget::showLaTeX()
{
    if ( ui->pb_modPlot->isChecked() )
    {
        ui->pb_modPlot->setText( "Cacher LaTeX" );
        ui->le_filename->setText("");
        QFileInfo plotFile( _plotpath );
        QString plotTeXPath = plotFile.absolutePath() + "/LaTeX/" + plotFile.baseName() + ".tex";
        ui->le_filename->setText( plotFile.baseName() );
        QFileInfo texFile( plotTeXPath );
        QString content;
        Files::read( texFile.absoluteFilePath(), content );
        ui->te_modPlot->setPlainText( content );
        ui->wd_modPlot->show();
    }
    else
    {
        ui->pb_modPlot->setText( "Afficher LaTeX" );
        ui->te_modPlot->setPlainText( "" );
        ui->le_filename->setText( "" );
        ui->wd_modPlot->hide();
    }
}
/**********************************************/
/**********************************************/
/**********************************************/
void GraphWidget::on_plotSelector_currentTextChanged( const QString &arg1 )
{
    _plotpath = _path + "/" + arg1;
    ui->imageViewer->loadFile( _plotpath );
    checkLaTeX();
    showLaTeX();
}
void GraphWidget::nextPlot()
{
    int newIndex = ui->plotSelector->currentIndex() + 1;
    if ( newIndex > ui->plotSelector->count() - 1 )
        newIndex = 0;
    ui->plotSelector->setCurrentIndex( newIndex );
}
void GraphWidget::previousPlot()
{
    int newIndex = ui->plotSelector->currentIndex() - 1;
    if ( newIndex < 0 )
        newIndex = ui->plotSelector->count()-1;
    ui->plotSelector->setCurrentIndex( newIndex );
}
/**********************************************/
/**********************************************/
/**********************************************/
void GraphWidget::on_pb_modPlot_released()
{
    showLaTeX();
}
/**********************************************/
/**********************************************/
/**********************************************/
void GraphWidget::on_pb_compile_released()
{
    if ( ui->te_modPlot->isHidden() || ui->te_modPlot->toPlainText().isEmpty() )
        return;
    QFileInfo plotFile( _plotpath );
    QString texPath = plotFile.absolutePath() + "/LaTeX/" + plotFile.baseName() + ".tex";
    if ( QFileInfo::exists(texPath) )
    {
        QString plotpath2 = ui->le_filename->text();
        texPath = plotFile.absolutePath() + "/LaTeX/" + plotpath2 + ".tex";
        Files::save( texPath, ui->te_modPlot->toPlainText() );
        LaTeX::CompileAndConvertToPNG( plotFile.absolutePath(), plotpath2 );
        _plotpath = plotFile.absolutePath() + "/" + plotpath2 + ".png";
        //        ui->le_plotSuffix->setText("");
        update();
        if ( ui->lb_plotName->isVisible() )
            emit sendPlotPath( plotFile.absolutePath() + "/" + plotpath2 + ".png" );
        else if ( ui->plotSelector->isVisible() )
            ui->plotSelector->setCurrentText( plotpath2 + ".png" );
    }
}
/**********************************************/
/**********************************************/
/**********************************************/
void GraphWidget::on_pb_supprimer_released()
{
    QMessageBox confBox;
    QString plotname;
    if ( ui->lb_plotName->isVisible() )
        plotname = ui->lb_plotName->text();
    else if ( ui->plotSelector->isVisible() )
        plotname = ui->plotSelector->currentText();
    confBox.setText( "Le graphique " + plotname + " va être supprimer." );
    confBox.setInformativeText( "Confirmez-vous cette opération ?" );
    confBox.setIcon( QMessageBox::Warning );
    confBox.setStandardButtons( QMessageBox::Yes | QMessageBox::Cancel );
    confBox.setDefaultButton( QMessageBox::Cancel );
    int ret = confBox.exec();
    if ( ret == QMessageBox::Cancel )
        return;
    if ( ret == QMessageBox::Yes )
    {
        QFileInfo plotFile( _plotpath );
        QFileInfoList filestodelete = QDir( plotFile.absolutePath() ).entryInfoList( {plotFile.baseName()+".*"}, QDir::Files | QDir::NoDotAndDotDot );
        filestodelete.append( QDir( plotFile.absolutePath()+ "/LaTeX" ).entryInfoList( {plotFile.baseName()+".*"}, QDir::Files | QDir::NoDotAndDotDot ) );

        for ( QFileInfo info : filestodelete )
            QFile::remove( info.absoluteFilePath() );

        if ( ui->lb_plotName->isVisible() )
        {
            _path = "";
            update();
        }
        else if ( ui->plotSelector->isVisible() )
            previousPlot();
    }
}
/**********************************************/
/**********************************************/
/**********************************************/
