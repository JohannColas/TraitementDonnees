Usage: <COMMAND> <OPTIONS>
   <COMMAND> list :
      compare    Permet de comparer les données en créant un graph
                 Attention ! Il faut indiquer les données en les 
                 ajoutant après "compare"
                    Ex : "compare e-s", compare "t-s-e"
         <OPTIONS> list :
            name     FACULTATIF : indique le nom du graph.
                     À ajouter après l'option
            -----------------------------------------------------------
            add      add ajoute la première occurance correspondant
            addAll   aux filtre, et addAll ajoute toutes les
                     occurances.
                     Les filtres doivant être auau format 
                     &<filtre1>!<filtre1>..., où & est inclusif et
                     ! est exclusif.
                     Pour "add", il est possible d'indiquer la 
                     legende de la donnée et le style appliqué au
                     graph en ajoutant au filtre suivant le format
                     suivant : <filtres>/<legende>/<style>
                        Ex : "add &VIEIL&TRAC!P3B4/Donnee 1/CLR1"
                            "addAll &SGL&FLEX&MONO!VIEIL"
            -----------------------------------------------------------
      -----------------------------------------------------------------
      treat         
         <OPTIONS> list :
            all        
            -----------------------------------------------------------
            this       
            -----------------------------------------------------------
            add        
            -----------------------------------------------------------
      -----------------------------------------------------------------
