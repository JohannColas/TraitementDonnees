#ifndef RE_H
#define RE_H
/**********************************************/
#include <QTextStream>
#include <QThread>
#include "Commons/Files.h"
#include "Commons/LaTeX.h"
#include "Commons/Settings.h"
#include "Commons/TestInfo.h"
#include "Commons/Math.h"
#include <cmath>
/**********************************************/
#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class RE_Thread
        : public QThread
{
    Q_OBJECT
    enum RE_Program
    {
        NONE,
        RESCOLAS,
    };
    /**********************************************/
    /**********************************************/
    TestInfo* _testinfo = 0;
    QString _path = "";
    RE_Program _prog = RE_Program::NONE;
    int index_TIME = -1;
    int index_FORCE = -1;
    int index_DEF1 = -1;
    int index_DEP = -1;
    int index_RE = -1;
    int index_TEMP = -1;
    bool _tempFour = false;
    int _tempFocused = 980;
    QStringList header;
    QVector<QVector<double>> data;
    QVector<QVector<double>> treated_data;
    QVector<QVector<double>> treated_data_focused;
    QVector<QVector<double>> treated_rawdata;
    QString header_data;
    QString header_rawdata;
    /**********************************************/
    /**********************************************/
    void run() override
    {
        if ( readREFile() )
        {
            treatData();

            Math::MoyenneGlissee( treated_data );

            Files::save( _path + "/RE/RE_DEF_CONT.dat", treated_data, header_data );
            if ( treated_rawdata.size() > 0 )
            {
                Math::MoyenneGlissee( treated_rawdata );
                Files::save( _path + "/RE/RE_DEP_FORCE.dat", treated_rawdata, header_rawdata );
            }
            if ( treated_data_focused.size() > 0 )
            {
                Math::MoyenneGlissee( treated_data_focused );
                Files::save( _path + "/RE/RE_DEF_CONT_focused.dat", treated_data_focused, header_rawdata );
            }

//            Plots::GenerateRE( _info );
        }
        emit finished();
    }
    /**********************************************/
    /**********************************************/
    bool readREFile()
    {
        header.clear();
        data.clear();
        QStringList contents;
        if ( !Files::read( _path + "/RE/RE.dat", contents, "UTF-8") )
                return false;
        if ( contents.first().contains('\r') )
            contents = contents.first().split('\r');

        if ( _path.contains("VIEIL_1300") )
            _tempFocused = 1280;

        bool isNumber = false;
        for ( QString line : contents )
        {
            if ( line.count('\t') == line.size() || line.count(';') == line.size() || line.count(',') == line.size() || line.count(' ') == line.size() )
                continue;
            line.replace( ',', '.' ); // Replace for number
            QStringList temp;
            temp = line.split('\t');
            if ( line.contains(';') && !line.contains('\t') )
                temp = line.split(';');
            // Checking if is Number
            temp.first().toDouble( &isNumber );
            if ( !isNumber )
            {
                header.append( temp.first() );
                if ( temp.first().contains(">>") )
                {
                     if ( temp.first().contains("RESCOLAS.BAS") )
                         _prog = RE_Program::RESCOLAS;
                }
            }
            else
            {
                QVector<double> lineNumber;
                for ( QString col : temp )
                {
                    lineNumber.append( col.toDouble() );
                }
                data.append( lineNumber );
            }
        }
        return true;
    }
    /**********************************************/
    /**********************************************/
    void treatData()
    {
        QString suiviDispo;
        if ( _prog == RE_Program::RESCOLAS )
        {
            suiviDispo = "R.E.";
            index_TIME = 0;
            index_FORCE = 2;
            index_DEP = 3;
            index_DEF1 = -1;
            index_RE = 1;
        }

        if ( _testinfo->getString( TestInfoKeys::AvailableTracking ).isEmpty() )
            _testinfo->save( suiviDispo, TestInfoKeys::AvailableTracking );

        header_data = "t(s);e(perc);s(MPa);T(C);RE(mOhm);dR_R0(perc)";
        header_rawdata = "t(s);def(mm);forc(N);T(C);RE(mOhm);dR_R0(perc)";

        // Get RawData
        double L = _testinfo->getDouble( TestInfoKeys::Length );
        double b = _testinfo->getDouble( TestInfoKeys::Width );
        double e = _testinfo->getDouble( TestInfoKeys::Thickness );
        double tcut = _testinfo->getDouble( TestInfoKeys::TimeCut );
        double tim = -1;
        double dep = -1;
        double dep0 = -1;
        if ( index_DEP != -1)
            dep0 = data[0][index_DEP];
        double frc = -1;
        double def = -1;
        double ctr = -1;
        double re = -1;
        double R0 = -1;
        double dRdR0 = 0;
        if ( index_RE != -1)
        {
            R0 = data[0][index_RE];
            if ( R0 < 1 )
                R0 *= 1000;
        }
        double temp = -1;
        for ( int it = 0; it < data.size(); ++it )
        {
            QVector<double> line = data[it];
            tim = line[index_TIME];
            // Get deplacement
            if ( index_DEP != -1)
                dep = line[index_DEP];
            // Get Force
            if ( index_FORCE != -1)
                frc = line[index_FORCE];
            // Get Strain
            if ( _prog == RE_Program::RESCOLAS )
            {
                def = 100*(dep - dep0)/L;
                if ( L == -1 )
                    def = -1;
            }
            else if ( index_DEF1 != -1 )
            {
                def = line[index_DEF1];
            }
            // Get Stress
            if ( _prog == RE_Program::RESCOLAS )
            {
                ctr = frc/(b*e);
                if ( e == -1 )
                    ctr *= -1;
            }
            // get RE
            if ( index_RE != -1 )
            {
                re = line[index_RE];
                if ( re < 1 )
                    re *= 1000; // convert in mOhm
                dRdR0 = 100*( re - R0 )/R0;
            }
            // get Temperature
            if ( index_TEMP != -1 )
            {
                temp = line[index_TEMP];
                if ( _tempFour )
                    temp = - 0.0000005127092*pow(temp, 3)
                            + 0.0007545365*pow(temp, 2)
                            + 0.9624921*temp
                            + 5.306989;
            }
            // Save data
            if ( /*def >= -0.001 &&*/
                 (line[index_TIME] < tcut || tcut == -1 ) )
            {
                QVector<double> dataLine = { tim, def, ctr, temp, re, dRdR0};
                treated_data.append( dataLine );
                if ( ctr >= _tempFocused )
                    treated_data_focused.append( dataLine );

                if ( index_DEP != 1 && index_FORCE != -1 )
                {
                    QVector<double> rawdataLine = { tim, dep, frc, temp , re, dRdR0};
                    treated_rawdata.append( rawdataLine );
                }
            }
        }
    }
    /**********************************************/
    /**********************************************/
public:
    RE_Thread( TestInfo* info )//InfoFile* info = 0 )
    {
        _testinfo = info;
        if ( _testinfo->isValid() )
            _path = _testinfo->path();
    }
    /**********************************************/
    /**********************************************/
signals:
    void finished();
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class RE
        : public QObject
{
    Q_OBJECT
    TestInfo* _testinfo = 0;
    /**********************************************/
    /**********************************************/
public:
    RE( TestInfo* info )
    {
        _testinfo = info;
    }
    /**********************************************/
    /**********************************************/
    void treat()
    {
        if ( _testinfo )
        {
            RE_Thread *thread = new RE_Thread( _testinfo );
            connect( thread, &RE_Thread::finished,
                     this, &RE::finished );
            thread->start();

        }
    }
    /**********************************************/
    /**********************************************/
signals:
    void finished();
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // RE_H
