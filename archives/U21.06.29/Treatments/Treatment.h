#ifndef TREATMENT_H
#define TREATMENT_H
/**********************************************/
#include <QApplication>
#include <QObject>
#include <QThread>
#include "Commons/TestInfo.h"
#include "Commons/Settings.h"
#include "Meca.h"
#include "PointsCaract.h"
#include "MecaPlots.h"
#include "Report.h"
#include "EA.h"
#include "CNI.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TreatmentInfo
{
public:
    bool isActive = false;
    bool doMeca = false;
    bool doPtsCaract = false;
    bool doEA = false;
    bool doRE = false;
    bool doCNI = false;
    bool doPlots = false;
    bool doReport = false;
    void onlyMeca()
    {
        isActive = true;
        doMeca = true;
    }
    void onlyPtsCaract()
    {
        isActive = true;
        doPtsCaract = true;
    }
    void onlyEA()
    {
        isActive = true;
        doEA = true;
    }
    void onlyRE()
    {
        isActive = true;
        doRE = true;
    }
    void onlyCNI()
    {
        isActive = true;
        doCNI = true;
    }
    void onlyPlots()
    {
        isActive = true;
        doPlots = true;
    }
    void onlyReport()
    {
        isActive = true;
        doReport = true;
    }
    void clear()
    {
        isActive = false;
        doMeca = false;
        doPtsCaract = false;
        doEA = false;
        doRE = false;
        doCNI = false;
        doPlots = false;
        doReport = false;
    }
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TreatmentThread : public QThread
{
    Q_OBJECT
    QString _path;
    TreatmentInfo _treatInfo;
    /**********************************************/
    /**********************************************/
    void run() override
    {
        TestInfo* testinfo = new TestInfo;
        testinfo->setPath( _path );

        if ( (Settings::getBool( Keys::TreatMeca ) && !_treatInfo.isActive) || _treatInfo.doMeca  )
        {
            emit currentTask( "Treating MECA data..." );
            Meca::Treat( testinfo );
        }
        if ( (Settings::getBool( Keys::TreatPtsCaract ) && !_treatInfo.isActive) || _treatInfo.doPtsCaract )
        {
            emit currentTask( "Treating Caract Points..." );
            PointsCaract ptsCaract;
            ptsCaract.run( _path );
        }
        if ( (Settings::getBool( Keys::TreatEA ) && !_treatInfo.isActive) || _treatInfo.doEA )
        {
            emit currentTask( "Treating EA..." );
            EA ea( testinfo->path() );
            ea.treat();
        }
        if ( (Settings::getBool( Keys::TreatRE ) && !_treatInfo.isActive) || _treatInfo.doRE )
        {
            emit currentTask( "Treating RE..." );
        }
        if ( (Settings::getBool( Keys::TreatCNI ) && !_treatInfo.isActive) || _treatInfo.doCNI )
        {
            emit currentTask( "Treating CNI..." );
        }
        if ( (Settings::getBool( Keys::TreatPlots ) && !_treatInfo.isActive) || _treatInfo.doPlots )
            //if ( _treatInfo.TreatPlots )
        {
            emit currentTask( "Creating Plots..." );
            MecaPlots::GenerateMECA( testinfo );
        }
        if ( (Settings::getBool( Keys::TreatSynthese ) && !_treatInfo.isActive) || _treatInfo.doReport )
        {
            emit currentTask( "Creating Report..." );
            Report report( testinfo );
            report.generate();
        }

        delete testinfo;
        emit finished();
    }
    /**********************************************/
    /**********************************************/
public:
    TreatmentThread( const QString& path, const TreatmentInfo& info = TreatmentInfo()  )
    {
        _path = path;
        _treatInfo = info;
    }
    /**********************************************/
    /**********************************************/
signals:
    void finished();
    void currentTask( const QString& task );
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Treatment
        : public QObject
{
    Q_OBJECT
public:
    void run( const QString& path, const TreatmentInfo& info = TreatmentInfo() )
    {
        TreatmentThread *thread = new TreatmentThread( path, info );
        connect( thread, &TreatmentThread::finished,
                 this, &Treatment::finished );
        connect( thread, &TreatmentThread::currentTask,
                 this, &Treatment::currentTask );
        thread->start();
    }
    /**********************************************/
    /**********************************************/
signals:
    void finished();
    void currentTask( const QString& task );
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/

#endif // TREATMENT_H
