#ifndef ELIDEDBUTTON_H
#define ELIDEDBUTTON_H
/**********************************************/
#include <QPushButton>
#include <QPainter>
#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ElidedButton
        : public QPushButton
{
    Q_OBJECT
    Q_PROPERTY( QString text READ text WRITE setText )
    Q_PROPERTY( bool isElided READ isElided )

public:
    explicit ElidedButton( QWidget *parent = 0 )
        : QPushButton( parent )
    {
        setStyleSheet("text-align: left;padding:4;padding-left:20;padding-right:20;");
    }
    /**********************************************/
    explicit ElidedButton( const QString &text, QWidget *parent = 0 )
        : QPushButton( parent ), _elided( false ), _content( text )
    {
        this->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Preferred );
        setStyleSheet("text-align: left;padding:4;padding-left:20;padding-right:20;");
    }
    /**********************************************/
    void setText( const QString &text )
    {
        _content = text;
        this->update();
    }
    /**********************************************/
    const QString &text() const
    {
        return _content;
    }
    /**********************************************/
    bool isElided() const
    {
        return _elided;
    }
    /**********************************************/
    /**********************************************/
protected:
    void paintEvent( QPaintEvent *event ) override
    {
        QPainter painter(this);
        const QFontMetrics fontMetrics = painter.fontMetrics();
        const int usableWidth = qRound( 0.9 * this->width() );

        const QString elidedText = fontMetrics.elidedText( _content, Qt::ElideLeft, usableWidth );
        _elided = ( elidedText != _content );
        QPushButton::setText( elidedText );
        QPushButton::paintEvent( event );
    }
    /**********************************************/
    /**********************************************/
private:
    bool _elided = false;
    QString _content;
    /**********************************************/
    /**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ELIDEDBUTTON_H
