#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H
/**********************************************/
#include <QMenu>
#include <QAction>
#include <QImageReader>
#include <QLabel>
#include <QWheelEvent>
#include "Commons/Settings.h"
/**********************************************/
/**********************************************/
/**********************************************/
class ImageViewer
        : public QWidget
{
    Q_OBJECT
private:
    QImage image;
    QLabel *imageLabel;
    double scaleFactor = 1;
    QAction *zoomInAct;
    QAction *zoomOutAct;
    QAction *normalSizeAct;
    QAction *fitToWindowAct;
    QMenu *viewMenu;
    int _x_inc = 20;
    int _y_inc = 20;
	bool _allow_move = false;
	QPoint _old_pos;

public:
    ImageViewer( QWidget* parent = 0 )
        : QWidget(parent), imageLabel(new QLabel(this))
     {
         imageLabel->setBackgroundRole(QPalette::Base);
         imageLabel->setStyleSheet( "background:white;" );
         imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
         imageLabel->setScaledContents(true);
         imageLabel->hide();

         setContextMenuPolicy( Qt::CustomContextMenu );
         connect( this, &ImageViewer::customContextMenuRequested,
                  this, &ImageViewer::showContextMenu );
         createActions();
		 scaleFactor = Settings::getDouble( Keys::ImageViewer, Keys::Scale );

         installEventFilter(this);
     }

     bool loadFile(const QString &fileName)
     {
         QImageReader reader(fileName);
         reader.setAutoTransform(true);
         const QImage newImage = reader.read();
         if (newImage.isNull()) {
             imageLabel->hide();
             return false;
         }
         setImage(newImage);

		 setWindowFilePath(fileName);
         imageLabel->show();
         return true;
     }
private:
     void setImage(const QImage &newImage)
     {
		 image = newImage;
         imageLabel->setPixmap(QPixmap::fromImage(image));
		 scaleFactor = Settings::getDouble( Keys::ImageViewer, Keys::Scale );

         fitToWindowAct->setEnabled(true);
         updateActions();

         imageLabel->move( 0, 0 );
		 scaleImage(scaleFactor);
     }

public slots:
     void showContextMenu( const QPoint& pos )
     {
         viewMenu->exec( this->mapToGlobal(pos) );
     }
     void zoomIn()
     {
		 scaleImage(1.25*scaleFactor);
     }
     void zoomOut()
     {
		 scaleImage(0.8*scaleFactor);
     }
	 void move( int dx, int dy )
	 {
		 int x = imageLabel->pos().x() + dx;
		 bool imageIsLarger = ( imageLabel->width() > this->width() );
		 if ( dx >= 0 )
		 {
			 if ( imageIsLarger && x > 0 )
				 x = 0;
			 else if ( !imageIsLarger && x > this->width() - imageLabel->width() )
				 x = this->width() - imageLabel->width();
		 }
		 else
		 {
			 if ( imageIsLarger && x < this->width() - imageLabel->width() )
				 x = this->width() - imageLabel->width();
			 else if ( !imageIsLarger && x < 0 )
				 x = 0;
		 }

		 int y = imageLabel->pos().y() + dy;
		 bool imageIsHigher = ( imageLabel->height() > this->height() );
		 if ( dy >= 0 )
		 {
			 if ( imageIsHigher && y > 0 )
				 y = 0;
			 else if ( !imageIsHigher && y > this->height() - imageLabel->height() )
				 y = this->height() - imageLabel->height();
		 }
		 else
		 {
			 if ( imageIsHigher && y < this->height() - imageLabel->height() )
				 y = this->height() - imageLabel->height();
			 else if ( !imageIsHigher && y < 0 )
				 y = 0;
		 }
		 imageLabel->move( x, y );
	 }
     void goLeft()
     {
         int pos = imageLabel->pos().x() + _x_inc;
         bool imageIsLarger = ( imageLabel->width() > this->width() );

         if ( imageIsLarger && pos > 0 )
             pos = 0;
         else if ( !imageIsLarger && pos > this->width() - imageLabel->width() )
             pos = this->width() - imageLabel->width();

         imageLabel->move( pos, imageLabel->pos().y() );
     }
     void goRight()
     {
         int pos = imageLabel->pos().x() - _x_inc;
         bool imageIsLarger = ( imageLabel->width() > this->width() );

         if ( imageIsLarger && pos < this->width() - imageLabel->width() )
             pos = this->width() - imageLabel->width();
         else if ( !imageIsLarger && pos < 0 )
             pos = 0;

         imageLabel->move( pos, imageLabel->pos().y() );
     }
     void goDown()
     {
         int pos = imageLabel->pos().y() - _y_inc;
         bool imageIsHigher = ( imageLabel->height() > this->height() );

         if ( imageIsHigher && pos < this->height() - imageLabel->height() )
             pos = this->height() - imageLabel->height();
         else if ( !imageIsHigher && pos < 0 )
             pos = 0;

         imageLabel->move( imageLabel->pos().x(), pos );
     }
     void goUp()
     {
         int pos = imageLabel->pos().y() + _y_inc;
         bool imageIsHigher = ( imageLabel->height() > this->height() );

         if ( imageIsHigher && pos > 0 )
             pos = 0;
         else if ( !imageIsHigher && pos > this->height() - imageLabel->height() )
             pos = this->height() - imageLabel->height();

         imageLabel->move( imageLabel->pos().x(), pos );
     }
     void normalSize()
     {
         int width = imageLabel->pixmap(Qt::ReturnByValue).size().width();
         int height = imageLabel->pixmap(Qt::ReturnByValue).size().height();
         double scaleFx = ((double)this->width()-20)/width;
         double scaleFy = ((double)this->height()-20)/height;
         double scaleF = scaleFy;
         if ( scaleFy > scaleFx )
             scaleF = scaleFx;

		 scaleImage( scaleF );
         imageLabel->move( 0, 0 );
     }
     void fitToWindow()
     {
         bool fitToWindow = fitToWindowAct->isChecked();
         if (!fitToWindow)
             normalSize();
         updateActions();
         imageLabel->move( 0, 0 );
     }
     void createActions()
     {

         viewMenu = new QMenu(tr("&View"));

		 zoomInAct = viewMenu->addAction(tr("Zoom &In (25%)"), this, &ImageViewer::zoomIn);
         zoomInAct->setEnabled(false);

		 zoomOutAct = viewMenu->addAction(tr("Zoom &Out (25%)"), this, &ImageViewer::zoomOut);
         zoomOutAct->setEnabled(false);

		 normalSizeAct = viewMenu->addAction(tr("&Normal Size"), this, &ImageViewer::normalSize);
         normalSizeAct->setEnabled(false);

         viewMenu->addSeparator();

         fitToWindowAct = viewMenu->addAction(tr("&Fit to Window"), this, &ImageViewer::fitToWindow);
         fitToWindowAct->setEnabled(false);
		 fitToWindowAct->setCheckable(true);
     }
     void updateActions()
     {
         zoomInAct->setEnabled(!fitToWindowAct->isChecked());
         zoomOutAct->setEnabled(!fitToWindowAct->isChecked());
         normalSizeAct->setEnabled(!fitToWindowAct->isChecked());
     }
     void scaleImage( double factor )
     {
		 scaleFactor = factor;
		 if ( scaleFactor < 0.05 )
			 scaleFactor = 0.05;
		 if ( scaleFactor > 20 )
			 scaleFactor = 20;
		 imageLabel->resize(scaleFactor * imageLabel->pixmap(Qt::ReturnByValue).size());
		 imageLabel->move( scaleFactor * imageLabel->pos() );
		 Settings::save( (round(10000*scaleFactor)/10000.0), Keys::ImageViewer, Keys::Scale );
     }
     bool eventFilter(QObject* /*obj*/, QEvent* evt)
     {
         if ( evt->type() == QEvent::Wheel )
         {
             QWheelEvent* event = static_cast<QWheelEvent*>(evt);
             if ( event->modifiers() == Qt::ControlModifier )
             {
				 if ( event->angleDelta().y() > 0 )
					 zoomIn();
				 else
					 zoomOut();
             }
             else if ( event->modifiers() == Qt::ShiftModifier )
             {
                 if ( event->angleDelta().y() > 0 )
                     goRight();
                 else
                     goLeft();
             }
             else
			 {
				 if ( event->angleDelta().y() > 0 )
					 goUp();
				 else
					 goDown();
             }
             evt->ignore();
             return true;
		 }
		 else if ( evt->type() == QEvent::MouseButtonPress )
		 {
			 QMouseEvent* event = static_cast<QMouseEvent*>(evt);
			 if ( event->button() == Qt::LeftButton )
			 {
				 _allow_move = true;
				 _old_pos = event->pos();
				 this->setCursor( QCursor(Qt::ClosedHandCursor) );
			 }
		 }
		 else if ( evt->type() == QEvent::MouseButtonRelease )
		 {
			 _allow_move = false;
			 this->setCursor( QCursor(Qt::ArrowCursor) );
		 }
		 else if ( evt->type() == QEvent::MouseMove )
		 {
			 if ( _allow_move )
			 {
				 QMouseEvent* event = static_cast<QMouseEvent*>(evt);
				 QPoint new_pos = event->pos();
				 this->move( new_pos.x()-_old_pos.x(), new_pos.y()-_old_pos.y() );
				 _old_pos = new_pos;
			 }
		 }
         return false;
     }
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // IMAGEVIEWER_H
