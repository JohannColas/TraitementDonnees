#ifndef EA_H
#define EA_H
/**********************************************/
#include <QFile>
#include <QFileInfo>
#include <QSettings>
#include <QDebug>
#include <QDir>
#include <QThread>

#include "Commons/TestInfo.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
namespace EA_KEYS {
	static inline QString SeuilAmpli = "SeuilAmpli";
	static inline QString SeuilTime = "SeuilTemps";
	static inline QString SeuilRise = "SeuilRise";
	static inline QString SeuilDur = "SeuilDuree";
	static inline QString SeuilCps = "SeuilCoups";
	static inline QString SeuilEner = "SeuilEnergie";
}
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class EA_FilterFile
{
	QString _path;
	QSettings* _settings = 0;
	/**********************************************/
	/**********************************************/
public:
	~EA_FilterFile()
	{
		if ( _settings )
			delete _settings;
	}
	/**********************************************/
	/**********************************************/
	EA_FilterFile( const QString& folderpath = QString() )
	{
		setPath( folderpath );
	}
	/**********************************************/
	/**********************************************/
	void setPath( const QString& folderpath = QString() )
	{
		if ( _settings )
		{
			delete _settings;
			_settings = 0;
		}
		QString path_EA_settings = folderpath+"/EA";
		if ( folderpath.isEmpty() || !QDir(path_EA_settings).exists() )
			return;
		_settings = new QSettings( path_EA_settings+"/filter.ini", QSettings::IniFormat );
	}
	/**********************************************/
	/**********************************************/
	QVariant value( const QString& key )
	{
		if ( _settings )
			return _settings->value( "Filter/" + key );
		return QVariant();
	}
	/**********************************************/
	/**********************************************/
	int toInt( const QString& key )
	{
		QVariant var = value( key );
		if ( var.isValid() )
			return var.toInt();
		return -1;
	}
	/**********************************************/
	/**********************************************/
	void save( const QString& key, const QVariant& value )
	{
		if ( _settings )
			_settings->setValue( "Filter/" + key, value );
	}
	/**********************************************/
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class EA_Thread
        : public QThread
{
	Q_OBJECT
	QString _path;
	EA_FilterFile _filterFile;
	/**********************************************/
	/**********************************************/
public:
	EA_Thread( const QString& path )
	{
		_path = path;
		_filterFile.setPath( _path );
	}
	/**********************************************/
	/**********************************************/
	void run() override
	{
		QFile treatDataFile( _path + "/EA/EA_traite.dat" );
		if ( !treatDataFile.open(QFile::WriteOnly) )
			return;
		QTextStream treatData( &treatDataFile );
		treatData.setEncoding( QStringConverter::Utf8  );
		treatData << "Time(s);" // 0
		             "Stress(MPa);" // 1
		             "Duration(microm);" // 2
		             "Amplitude(dB);" // 3
		             "AverageFrequency(Hz);" // 4
		             "ReverbationFrequency(Hz);" // 5
		             "InitiationFrequency(Hz);" // 6
		             "AverageEnergy(aJ);" // 7
		             "AbsoluteEnergy(aJ);" // 8
		             "CumulatedEnergy(aJ);" // 9
		             "counts;" // 10
		             "AccumulativeCounts;" // 11
		             "Severity(aJ)" // 12
		             "\n";
		double sumEner = 0;
		double nbvLines = 0;
		int totCount = 0;
		int id_time = -1;
		int id_force = -1;
		int id_rise = -1;
		int id_count = -1;
		int id_energy = -1;
		int id_durat = -1;
		int id_amplit = -1;
		int id_avFreq = -1;
		int id_revFreq = -1;
		int id_initFreq = -1;
		int id_absEner = -1;
		TestInfo info;
		info.setPath(_path);
		double L = info.getDouble( TestInfoKeys::Length );
		double b = info.getDouble( TestInfoKeys::Width );
		double e = info.getDouble( TestInfoKeys::Thickness );
		QVector<double> energyMaxes;

		QStringList _paths;
		if ( QFile::exists( _path + "/EA/EA.dat" ) )
			_paths.append( "EA.dat" );
		else
			_paths.append( QDir(_path+"/EA").entryList({"EA_*.dat"}, QDir::Files) );
		_paths.removeAll("EA_traite.dat");
		for (const QString &path : _paths)
		{
			QString fullpath = _path+"/EA/" + path;

			QFile rawDataFile( fullpath );
			if ( !rawDataFile.open(QFile::ReadOnly) )
				    continue;

			QTextStream rawData( &rawDataFile );
			rawData.setEncoding( QStringConverter::Utf8  );
			bool readdata = false;
			double old_time = -1;
			QVector<int> collength;// = {3,17,9,4,6,6,6,10,4,6,6,6,6,12,12};
			while ( !rawData.atEnd() )
			{
				QString line = rawData.readLine();
				if ( !line.contains("SSSSSSSS.mmmuuun") && !readdata )
					continue;
				if ( line.contains("ID") && line.contains("SSSSSSSS.mmmuuun") )
				{
					line = line.remove("SIG ");
					QStringList strlst = line.split(" ", Qt::SkipEmptyParts);
					for ( int it = 0; it < strlst.size(); ++it )
					{
						QString str = strlst.at(it);
						if ( str == "SSSSSSSS.mmmuuun" )
							id_time = it;
						else if ( str == "PARA1" )
							id_force = it;
						else if ( str == "RISE" )
							id_rise = it;
						else if ( str == "COUN" )
							id_count = it;
						else if ( str == "ENER" )
							id_energy = it;
						else if ( str == "DURATION" )
							id_durat = it;
						else if ( str == "AMP" )
							id_amplit = it;
						else if ( str == "A-FRQ" )
							id_avFreq = it;
						else if ( str == "I-FRQ" )
							id_initFreq = it;
						else if ( str == "R-FRQ" )
							id_revFreq = it;
						else if ( str == "ABS-ENERGY" )
							id_absEner = it;
					}

					line = rawData.readLine();
					QChar old_c = ' ';
					int last_ind = 0;
					for ( int it = 0; it < line.length(); ++it )
					{
						QChar c = line.at(it);
						if ( old_c != ' ' && c == ' ' )
						{
							collength.append( it-last_ind );
							last_ind = it;
						}
						old_c = c;
					}
					readdata = true;
				}
				if ( readdata )
				{
					QStringList dataline;
					for ( int len : collength )
					{
						dataline.append( line.left(len).remove(' ') );
						line.remove(0,len);
					}
					dataline.append( line.remove(' ') );
					if ( dataline.size() < id_absEner+1 )
						continue;
					bool saveLine = false;
					int ID = dataline.at(0).toInt();
					if ( ID != 1 ) saveLine = false;
					double time = -1;
					if ( id_time != -1 )
					{
						time = dataline.at(id_time).toDouble();
						if ( time > _filterFile.toInt(EA_KEYS::SeuilTime) )
							saveLine = true;
					}
					int rise = -1;
					if ( id_rise != -1 )
					{
						rise = dataline.at(id_rise).toInt();
						if ( rise <= _filterFile.toInt(EA_KEYS::SeuilRise) )
							saveLine = true;
					}
					int count = -1;
					if ( id_count != -1 )
					{
						count = dataline.at(id_count).toInt();
						if ( count >= _filterFile.toInt(EA_KEYS::SeuilCps) )
							saveLine = true;
					}
					int energy = -1;
					if ( id_energy != -1 )
					{
						energy = dataline.at(id_energy).toInt();
						if ( energy > _filterFile.toInt(EA_KEYS::SeuilEner) )
							saveLine = true;
					}
					int durat = -1;
					if ( id_durat != -1 )
					{
						durat = dataline.at(id_durat).toInt();
						if ( durat < _filterFile.toInt(EA_KEYS::SeuilDur) )
							saveLine = true;
					}
					int amplit = -1;
					if ( id_amplit != -1 )
					{
						amplit = dataline.at(id_amplit).toInt();
						if ( amplit >= _filterFile.toInt(EA_KEYS::SeuilAmpli) )
							saveLine = true;
					}
					int avFreq = -1;
					if ( id_avFreq != -1 )
						avFreq = dataline.at(id_avFreq).toInt();
					int revFreq = -1;
					if ( id_revFreq != -1 )
						revFreq = dataline.at(id_revFreq).toInt();
					int initFreq = -1;
					if ( id_initFreq != -1 )
						initFreq = dataline.at(id_initFreq).toInt();
					double absEner = -1;
					if ( id_absEner != -1 )
						absEner = dataline.at(id_absEner).toDouble();

					if ( saveLine )
					{
						++nbvLines;
						sumEner += absEner;
						double avEner = sumEner/nbvLines;
						totCount += count;
						double severity = 0;
						addEnergy( absEner, energyMaxes );
						severity = average( energyMaxes );
						if ( time - old_time > 0.5 )
						{
							old_time = time;
						}
						else
						{
							continue;
						}
						double force = dataline.at(id_force).toDouble();
						double stress;
						if ( info.getString(TestInfoKeys::TestName).contains("FLEX_") )
							stress = 1.5*(L/(b*e*e))*force;
						else
						{
							stress = force/(b*e);
							if ( e == -1 )
								stress *= -1;
						}

						treatData << time << ";"
						          << stress << ";"
						          << durat << ";"
						          << amplit << ";"
						          << avFreq << ";"
						          << revFreq << ";"
						          << initFreq << ";"
						          << avEner << ";"
						          << absEner << ";"
						          << sumEner << ";"
						          << count << ";"
						          << totCount << ";"
						          << severity << "\n";
					}
				}
			}
			rawDataFile.close();
		}
		treatDataFile.close();

		emit finished();
	}
	/**********************************************/
	/**********************************************/
	double average( const QVector<double>& values )
	{
		double total = 0;
		for ( double val : values )
		{
			total += val;
		}
		return ( total / values.size() );
	}
	/**********************************************/
	/**********************************************/
	void addEnergy( double ener, QVector<double>& vector )
	{
		int vecSize = vector.size();
		if ( vecSize == 0 )
			vector.append(ener);
		else if ( ener > vector.first() || vecSize < 10 )
		{
			if ( ener > vector.last() )
				vector.append(ener);
			else
				for ( int it = 1; it < vecSize; ++it )
				{
					if ( ener > vector.at(it-1) &&
					     ener <= vector.at(it) )
					{
						vector.insert( it, ener );
						break;
					}
				}
			if ( vector.size() == 11 )
				vector.removeFirst();
		}
	}
	/**********************************************/
	/**********************************************/
signals:
	void finished();
	/**********************************************/
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class EA //: public QObject
{
//	Q_OBJECT
	static inline QString _path;
//	static inline EA_FilterFile _filterFile;
	/**********************************************/
	/**********************************************/
public:
//	EA( const QString& path )
//	{
//		_path = path;
//		_filterFile.setPath( _path );
//	}
	/**********************************************/
	/**********************************************/
	static inline void Treat( const QString& path )
	{
//		EA_Thread *thread = new EA_Thread( _path );
//		connect( thread, &EA_Thread::finished,
//		         this, &EA::finished );
//		thread->start();		_path = path;
		_path = path;
		EA_FilterFile filterFile;
		filterFile.setPath( _path );
		QFile treatDataFile( _path + "/EA/EA_traite.dat" );
		if ( !treatDataFile.open(QFile::WriteOnly) )
			return;
		QTextStream treatData( &treatDataFile );
		treatData.setEncoding( QStringConverter::Utf8  );
		treatData << "Time(s);" // 0
		             "Stress(MPa);" // 1
		             "Duration(microm);" // 2
		             "Amplitude(dB);" // 3
		             "AverageFrequency(Hz);" // 4
		             "ReverbationFrequency(Hz);" // 5
		             "InitiationFrequency(Hz);" // 6
		             "AverageEnergy(aJ);" // 7
		             "AbsoluteEnergy(aJ);" // 8
		             "CumulatedEnergy(aJ);" // 9
		             "counts;" // 10
		             "AccumulativeCounts;" // 11
		             "Severity(aJ)" // 12
		             "\n";
		treatData << 0 << ";"
		          << 0 << ";"
		          << 0 << ";"
		          << 0 << ";"
		          << 0 << ";"
		          << 0 << ";"
		          << 0 << ";"
		          << 0 << ";"
		          << 0 << ";"
		          << 0 << ";"
		          << 0 << ";"
		          << 0 << ";"
		          << 0 << "\n";
		double sumEner = 0;
		double nbvLines = 0;
		int totCount = 0;
		int id_time = -1;
		int id_force = -1;
		int id_rise = -1;
		int id_count = -1;
		int id_energy = -1;
		int id_durat = -1;
		int id_amplit = -1;
		int id_avFreq = -1;
		int id_revFreq = -1;
		int id_initFreq = -1;
		int id_absEner = -1;
		double time_filter = filterFile.toInt(EA_KEYS::SeuilTime);
		double rise_filter = filterFile.toInt(EA_KEYS::SeuilRise);
		double count_filter = filterFile.toInt(EA_KEYS::SeuilCps);
		double energy_filter = filterFile.toInt(EA_KEYS::SeuilEner);
		double duration_filter = filterFile.toInt(EA_KEYS::SeuilDur);
		double amplitude_filter = filterFile.toInt(EA_KEYS::SeuilAmpli);
		TestInfo info;
		info.setPath(_path);
		double L = info.getDouble( TestInfoKeys::Length );
		double b = info.getDouble( TestInfoKeys::Width );
		double e = info.getDouble( TestInfoKeys::Thickness );
		QVector<double> energyMaxes;

		QStringList _paths;
		if ( QFile::exists( _path + "/EA/EA.dat" ) )
			_paths.append( "EA.dat" );
		else
			_paths.append( QDir(_path+"/EA").entryList({"EA_*.dat"}, QDir::Files) );
		_paths.removeAll("EA_traite.dat");
		for (const QString &path : _paths)
		{
			QString fullpath = _path+"/EA/" + path;

			QFile rawDataFile( fullpath );
			if ( !rawDataFile.open(QFile::ReadOnly) )
				    continue;

			QTextStream rawData( &rawDataFile );
			rawData.setEncoding( QStringConverter::Utf8  );
			bool readdata = false;
			double old_time = -1;
			QVector<int> collength;// = {3,17,9,4,6,6,6,10,4,6,6,6,6,12,12};
			while ( !rawData.atEnd() )
			{
				QString line = rawData.readLine();
				if ( !line.contains("SSSSSSSS.mmmuuun") && !readdata )
					continue;
				if ( line.contains("ID") && line.contains("SSSSSSSS.mmmuuun") )
				{
					line = line.remove("SIG ");
					QStringList strlst = line.split(" ", Qt::SkipEmptyParts);
					for ( int it = 0; it < strlst.size(); ++it )
					{
						QString str = strlst.at(it);
						if ( str == "SSSSSSSS.mmmuuun" )
							id_time = it;
						else if ( str == "PARA1" )
							id_force = it;
						else if ( str == "RISE" )
							id_rise = it;
						else if ( str == "COUN" )
							id_count = it;
						else if ( str == "ENER" )
							id_energy = it;
						else if ( str == "DURATION" )
							id_durat = it;
						else if ( str == "AMP" )
							id_amplit = it;
						else if ( str == "A-FRQ" )
							id_avFreq = it;
						else if ( str == "I-FRQ" )
							id_initFreq = it;
						else if ( str == "R-FRQ" )
							id_revFreq = it;
						else if ( str == "ABS-ENERGY" )
							id_absEner = it;
					}

					line = rawData.readLine();
					QChar old_c = ' ';
					int last_ind = 0;
					for ( int it = 0; it < line.length(); ++it )
					{
						QChar c = line.at(it);
						if ( old_c != ' ' && c == ' ' )
						{
							collength.append( it-last_ind );
							last_ind = it;
						}
						old_c = c;
					}
					readdata = true;
				}
				if ( readdata )
				{
					QStringList dataline;
					for ( int len : collength )
					{
						dataline.append( line.left(len).remove(' ') );
						line.remove(0,len);
					}
					dataline.append( line.remove(' ') );
					if ( dataline.size() < id_absEner+1 )
						continue;
					bool saveLine = false;
					int ID = dataline.at(0).toInt();
					if ( ID != 1 ) saveLine = false;
					double time = -1;
					if ( id_time != -1 )
					{
						time = dataline.at(id_time).toDouble();
						if ( time > time_filter)
							saveLine = true;
					}
					int rise = -1;
					if ( id_rise != -1 )
					{
						rise = dataline.at(id_rise).toInt();
						if ( rise <= rise_filter )
							saveLine = true;
					}
					int count = -1;
					if ( id_count != -1 )
					{
						count = dataline.at(id_count).toInt();
						if ( count >= count_filter )
							saveLine = true;
					}
					int energy = -1;
					if ( id_energy != -1 )
					{
						energy = dataline.at(id_energy).toInt();
						if ( energy > energy_filter )
							saveLine = true;
					}
					int durat = -1;
					if ( id_durat != -1 )
					{
						durat = dataline.at(id_durat).toInt();
						if ( durat < duration_filter )
							saveLine = true;
					}
					int amplit = -1;
					if ( id_amplit != -1 )
					{
						amplit = dataline.at(id_amplit).toInt();
						if ( amplit >= amplitude_filter )
							saveLine = true;
					}
					int avFreq = -1;
					if ( id_avFreq != -1 )
						avFreq = dataline.at(id_avFreq).toInt();
					int revFreq = -1;
					if ( id_revFreq != -1 )
						revFreq = dataline.at(id_revFreq).toInt();
					int initFreq = -1;
					if ( id_initFreq != -1 )
						initFreq = dataline.at(id_initFreq).toInt();
					double absEner = -1;
					if ( id_absEner != -1 )
						absEner = dataline.at(id_absEner).toDouble();

					if ( saveLine )
					{
						++nbvLines;
						sumEner += absEner;
						double avEner = sumEner/nbvLines;
						totCount += count;
						double severity = 0;
						EA::addEnergy( absEner, energyMaxes );
						severity = EA::average( energyMaxes );
						if ( time - old_time > 0.5 )
						{
							old_time = time;
						}
						else
						{
							continue;
						}
						double force = dataline.at(id_force).toDouble();
						double stress;
						if ( info.getString(TestInfoKeys::TestName).contains("FLEX_") )
							stress = 1.5*(L/(b*e*e))*force;
						else
						{
							stress = force/(b*e);
							if ( e == -1 )
								stress *= -1;
						}

						treatData << time << ";"
						          << stress << ";"
						          << durat << ";"
						          << amplit << ";"
						          << avFreq << ";"
						          << revFreq << ";"
						          << initFreq << ";"
						          << avEner << ";"
						          << absEner << ";"
						          << sumEner << ";"
						          << count << ";"
						          << totCount << ";"
						          << severity << "\n";
					}
				}
			}
			rawDataFile.close();
		}
		treatDataFile.close();

//		emit finished();
	}
	/**********************************************/
	/**********************************************/
	static inline double average( const QVector<double>& values )
	{
		double total = 0;
		for ( double val : values )
		{
			total += val;
		}
		return ( total / values.size() );
	}
	/**********************************************/
	/**********************************************/
	static inline void addEnergy( double ener, QVector<double>& vector )
	{
		int vecSize = vector.size();
		if ( vecSize == 0 )
			vector.append(ener);
		else if ( ener > vector.first() || vecSize < 10 )
		{
			if ( ener > vector.last() )
				vector.append(ener);
			else
				for ( int it = 1; it < vecSize; ++it )
				{
					if ( ener > vector.at(it-1) &&
					     ener <= vector.at(it) )
					{
						vector.insert( it, ener );
						break;
					}
				}
			if ( vector.size() == 11 )
				vector.removeFirst();
		}
	}
	/**********************************************/
	/**********************************************/
	/**********************************************/
//	/**********************************************/
//signals:
//	void finished();
	/**********************************************/
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // EA_H
