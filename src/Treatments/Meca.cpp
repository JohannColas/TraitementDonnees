#include "Meca.h"
/**********************************************/
#include <QTextStream>
/**********************************************/
/**********************************************/
/**********************************************/
bool Meca::Treat( TestInfo* testinfo )
{
	if ( !testinfo )
		return false;

	Meca::Treat( testinfo->path() );

	return true;
}
/**********************************************/
/**********************************************/
/**********************************************/
void Meca::setTest( TestInfo testinfo )
{
	_testinfo = testinfo;
}
void Meca::setTest( const QString& testpath )
{
	_testinfo = TestInfo();
	_testinfo.setPath(testpath);
}
void Meca::Treat( const QString& testpath )
{
	//
	_testinfo = TestInfo();
	_testinfo.setPath(testpath);

	QString EAdatapath = testpath+"/EA/EA_traite.dat";
	QFileInfo info( EAdatapath );
	if ( info.size() > 0 && info.size() <= 10000000 )
		Files::readData(EAdatapath, _EAdata);
	_EAdatalastindex = 1;

	// Select Program (Automatically)
	if ( !Meca::AutomaticSelectProgram(_testinfo.MecaFilePath()) )
		return;

	// Read Raw Data
	_raw_data.clear();
	if ( !Files::readData( _testinfo.MecaFilePath(), _raw_data, QStringConverter::Latin1)  )
		return;

	Meca::getConstants();

	// Raw Data Pre-treatment
	Meca::PreTreatment();

	if ( index(Meca::DEPL) != -1 && index(Meca::FORCE) != -1 )
	{
		QVector<QVector<double>> trd_raw_data = Meca::raw_data();
		Files::save( _testinfo.path() + "/" + Keys::fn_MecaOriginalData + ".dat",
		             trd_raw_data,
		             Meca::raw_data_header() );
	}

	QVector<QVector<double>> trd_data = Meca::treated_data();
	Files::save( _testinfo.path() + "/" + Keys::fn_MecaTreatedData + ".dat",
	             trd_data,
	             Meca::treated_data_header() );

	// Cycles Treatment
	Meca::getCycles();

	_EAdata.clear();
}
/**********************************************/
/**********************************************/
/**********************************************/
void Meca::TreatRE( const QString& testpath )
{
	//
	_testinfo = TestInfo();
	_testinfo.setPath(testpath);

	// Select Program (Automatically)
	if ( !Meca::AutomaticSelectProgram(_testinfo.REFilePath()) )
		return;

	// Read Raw Data
	_raw_data.clear();
	if ( !Files::readData( _testinfo.REFilePath(), _raw_data, QStringConverter::Latin1)  )
		return;

	Meca::getConstants();

	// Raw Data Pre-treatment
	Meca::PreTreatment();

	QVector<QVector<double>> trd_data = Meca::re_treated_data();
	Files::save( _testinfo.path() + "/RE/" + Keys::fn_RETreatedData + ".dat",
	             trd_data,
	             Meca::re_treated_data_header() );
}
/**********************************************/
/**********************************************/
/**********************************************/
Meca::ProgName Meca::SelectedProgram()
{
	return _prog_name;
}
/**********************************************/
/**********************************************/
/**********************************************/
void Meca::SelectProgram( const ProgName& prog )
{
	_prog_name = prog;
	_program.clear();
	for ( int it = 0; it < (int)(Meca::COUNT); ++it )
		_program.append( -1 );
	if ( _prog_name == Meca::R8562 )
	{
		_program[Meca::TIME]  = 0;
		_program[Meca::FORCE] = 1;
		_program[Meca::EXT_1] = 2;
		_program[Meca::EXT_2] = 3;
		_program[Meca::RE]    = 4;
	}
	else if ( _prog_name == Meca::R8562DEC21 )
	{
		_program[Meca::TIME]  = 0;
		_program[Meca::STRESS] = 1;
		_program[Meca::EXT_1] = 2;
		_program[Meca::EXT_2] = 3;
		_program[Meca::RE]    = 4;
	}
	else if ( _prog_name == Meca::RESIST )
	{
		_program[Meca::STRESS] = 0;
		_program[Meca::EXT_1]  = 1;
		_program[Meca::EXT_2]  = 2;
		_program[Meca::EA]     = 3;
		_program[Meca::RE]     = 4;
		_program[Meca::TIME]   = 5;
	}
	else if ( _prog_name == Meca::TRACMONO )
	{
		_program[Meca::FORCE] = 0;
		_program[Meca::EXT_1] = 1;
		_program[Meca::EXT_2] = 2;
		_program[Meca::EA]    = 4;
		_program[Meca::TIME]  = 5;
	}
	else if ( _prog_name == Meca::TRAC )
	{
		_program[Meca::STRESS] =  0;
		_program[Meca::EXT_1]  =  1;
		_program[Meca::EXT_2]  =  2;
		_program[Meca::EA]     =  3;
		_program[Meca::TIME]   =  4;
		_program[Meca::DEF_J1] =  5;
		_program[Meca::DEF_J2] =  6;
		_program[Meca::DEF_J3] =  7;
		_program[Meca::DEF_J4] =  8;
		_program[Meca::DEF_J5] =  9;
		_program[Meca::DEF_J6] = 10;
	}
	else if ( _prog_name == Meca::FLEX )
	{
		_program[Meca::FORCE]  = 0;
		_program[Meca::DEPL]   = 1;
		_program[Meca::EA]     = 2;
		_program[Meca::TIME]   = 3;
		_program[Meca::STRESS] = 5;
		_program[Meca::STRAIN] = 6;
	}
	else if ( _prog_name == Meca::FLEX12 )
	{
		_program[Meca::FORCE] = 0;
		_program[Meca::DEPL]  = 1;
		_program[Meca::EA]    = 2;
		_program[Meca::RE]    = 3;
		_program[Meca::TIME]  = 4;
	}
	else if ( _prog_name == Meca::IOSI3J )
	{
		_program[Meca::FORCE]  = 0;
		_program[Meca::DEPL]   = 1;
		_program[Meca::EA]     = 2;
		_program[Meca::TIME]   = 3;
		_program[Meca::DEF_J1] = 4;
		_program[Meca::DEF_J2] = 5;
		_program[Meca::DEF_J3] = 6;
		_program[Meca::RE]     = 7;
	}
	else if ( _prog_name == Meca::VIEIL )
	{
		_program[Meca::TIME]  = 0;
		_program[Meca::FORCE] = 7;
		_program[Meca::DEPL]  = 8;
		_program[Meca::TEMP]  = 9;
	}
	else if ( _prog_name == Meca::RESIST2 )
	{
		_program[Meca::STRESS] = 0;
		_program[Meca::EXT_1]  = 1;
		_program[Meca::EXT_2]  = 2;
		_program[Meca::EA]     = 3;
		_program[Meca::RE]     = 4;
		_program[Meca::TIME]   = 6;
	}
	else if ( _prog_name == Meca::TRACTR )
	{
		_program[Meca::STRESS] = 0;
		_program[Meca::EXT_1]  = 1;
		_program[Meca::EXT_2]  = 2;
		_program[Meca::EA]     = 3;
		_program[Meca::TIME]   = 4;
	}
	else if ( _prog_name == Meca::ESSAIHT )
	{
		_program[Meca::TIME]  =  1;
		_program[Meca::FORCE] =  8;
		_program[Meca::DEPL]  =  9;
		_program[Meca::TEMP]  = 10;
	}
	else if ( _prog_name == Meca::TRAC6J )
	{
		_program[Meca::STRESS]   =  0;
		_program[Meca::EXT_1]  =  1;
		_program[Meca::EXT_2]  =  2;
		_program[Meca::EA]     =  3;
		_program[Meca::TIME]   =  4;
		_program[Meca::DEF_J1] =  5;
		_program[Meca::DEF_J2] =  6;
		_program[Meca::DEF_J3] =  7;
		_program[Meca::DEF_J4] =  8;
		_program[Meca::DEF_J5] =  9;
		_program[Meca::DEF_J6] = 10;
	}
	else if ( _prog_name == Meca::TRAC3J )
	{
		_program[Meca::FORCE]  =  0;
		_program[Meca::EXT_1]  =  1;
		_program[Meca::EXT_2]  =  2;
		_program[Meca::EA]     =  3;
		_program[Meca::TIME]   =  4;
		_program[Meca::DEF_J1] =  5;
		_program[Meca::DEF_J2] =  6;
		_program[Meca::DEF_J3] =  7;
	}
	else if ( _prog_name == Meca::RESCOLAS )
	{
		_program[Meca::TIME]  = 0;
		_program[Meca::RE]    = 1;
		_program[Meca::FORCE] = 2;
		_program[Meca::DEPL]  = 3;
	}
}
/**********************************************/
/**********************************************/
/**********************************************/
bool Meca::AutomaticSelectProgram( const QString& path )
{
	QString firstLine;
	if ( !Files::readFirstLine( path, firstLine, QStringConverter::Latin1) )
		return false;

	Meca::SelectProgram( Meca::NONE );
	if ( firstLine.contains("R8562.BAS") && !firstLine.contains("(DECEMBRE 2021)") )
		Meca::SelectProgram( Meca::R8562 );
	else if ( firstLine.contains("R8562.BAS") && firstLine.contains("(DECEMBRE 2021)") )
		Meca::SelectProgram( Meca::R8562DEC21 );
	else if ( firstLine.contains("RESIST.BAS") )
		Meca::SelectProgram( Meca::RESIST );
	else if ( firstLine.contains("RESIST2.BAS") )
		Meca::SelectProgram( Meca::RESIST2 );
	else if ( firstLine.contains("TRACTR.BAS") )
		Meca::SelectProgram( Meca::TRACTR );
	else if ( firstLine.contains("TRACMONO.BAS") )
		Meca::SelectProgram( Meca::TRACMONO );
	else if ( firstLine.contains("TRACTION.BAS") )
		Meca::SelectProgram( Meca::TRAC );
	else if ( firstLine.contains("FLEXION.BAS") )
		Meca::SelectProgram( Meca::FLEX );
	else if ( firstLine.contains("FLEXION1.BAS") || firstLine.contains("FLEXION2.BAS") )
		Meca::SelectProgram( Meca::FLEX12 );
	else if ( firstLine.contains("IOSI3J.BAS") )
		Meca::SelectProgram( Meca::IOSI3J );
	else if ( firstLine.contains("TRAC6J.BAS") )
		Meca::SelectProgram( Meca::TRAC6J );
	else if ( firstLine.contains("TRAC3J.BAS") )
		Meca::SelectProgram( Meca::TRAC3J );
	else if ( firstLine.contains("RESCOLAS.BAS") )
		Meca::SelectProgram( Meca::RESCOLAS );
	else if ( firstLine.contains("Temps total (s)") && _testinfo.isValid() )
	{
		QString testname = _testinfo.getString( TestInfoKeys::TestName );
		if ( testname.startsWith("VIEIL_") )
			Meca::SelectProgram( Meca::VIEIL );
		else
			Meca::SelectProgram( Meca::ESSAIHT );
	}

	if ( Meca::SelectedProgram() == Meca::NONE ) return false;

	// Check damage monitoring
	if ( _testinfo.isValid() )
	{
		QString suiviDispo;
		if ( Meca::index(Meca::RE) != -1 )
			suiviDispo = "RE";
		if ( Meca::index(Meca::EA) != -1 )
			suiviDispo = (suiviDispo.isEmpty() ? "" : suiviDispo + " - ") + "EA";
		QString suivi = _testinfo.getString( TestInfoKeys::AvailableTracking );
		if ( suivi.isEmpty() )
			_testinfo.save( suiviDispo, TestInfoKeys::AvailableTracking );
		if ( !suivi.contains("RE") && !suivi.contains("R.E.") )
			Meca::resetIndex(Meca::RE);
		if ( !suivi.contains("EA") && !suivi.contains("E.A.") )
			Meca::resetIndex(Meca::EA);
	}
	return true;
}
/**********************************************/
/**********************************************/
/**********************************************/
void Meca::getConstants()
{
	if ( _testinfo.isValid() )
	{
		L = _testinfo.getDouble( TestInfoKeys::Length );
		b = _testinfo.getDouble( TestInfoKeys::Width );
		e = _testinfo.getDouble( TestInfoKeys::Thickness );
		time_cut = _testinfo.getDouble( TestInfoKeys::TimeCut );
	}
	dep0 = get( _raw_data.first(), Meca::DEPL );
	R0 = get( _raw_data.first(), Meca::RE );
	if ( R0 < 1 )
		R0 *= 1000;
}
/**********************************************/
/**********************************************/
/**********************************************/
void Meca::resetIndex( const Index& index )
{
	if ( index > 0 && index < _program.size() )
		_program.replace( index, -1 );
}
/**********************************************/
/**********************************************/
/**********************************************/
int Meca::index( const Index& index )
{
	if ( index < 0 || index >= _program.size() )
		return -1;
	return _program.at( index );
}
/**********************************************/
/**********************************************/
/**********************************************/
double Meca::get( const QVector<double>& line, int index )
{
	if ( index < 0 || index > line.size() )
		return NAN;
	return line.at( index );
}
/**********************************************/
/**********************************************/
/**********************************************/
double Meca::get( const QVector<double>& line, const Meca::Index& index )
{
	int lineIndex = Meca::index( index );
	if ( lineIndex < 0 || lineIndex > line.size() )
		return NAN;
	return line.at( lineIndex );
}
/**********************************************/
/**********************************************/
/**********************************************/
double Meca::getStrain( const QVector<double>& line )
{
	double strain = NAN;
	if ( _prog_name == Meca::FLEX12 || _prog_name == Meca::FLEX )
		strain = 100*(6*e/(L*L))*get(line, Meca::DEPL);
	else if ( _prog_name == Meca::IOSI3J ||
	          _prog_name == Meca::TRAC3J )
		strain = get(line, Meca::DEF_J1) - get(line, Meca::DEF_J2);
	else if ( _prog_name == Meca::VIEIL ||
	          _prog_name == Meca::ESSAIHT ||
	          _prog_name == Meca::RESCOLAS )
	{
		strain = 100*(get(line, Meca::DEPL) - dep0)/L;
		if ( L == -1 )
			strain = -1;
	}
	else if ( _prog_name == Meca::TRAC6J )
	{
		strain = 0.5*( get(line, Meca::DEF_J1) + get(line, Meca::DEF_J2) );
	}
	else if ( Meca::index(Meca::EXT_1) != -1 )
	{
		if ( Meca::index(Meca::EXT_2)  == -1 )
			strain = get(line, Meca::EXT_1);
		else
			strain = 0.5*( get(line, Meca::EXT_1) + get(line, Meca::EXT_2) );
	}
	return strain;
}
/**********************************************/
/**********************************************/
/**********************************************/
double Meca::getTransversalStrain( const QVector<double>& line )
{
	double strain = NAN;
	if ( _prog_name == Meca::TRAC6J )
	{
		strain = 0.5*( get(line, Meca::DEF_J3) + get(line, Meca::DEF_J4) );
	}
	return strain;
}
/**********************************************/
/**********************************************/
/**********************************************/
double Meca::getPoisonCoef( const QVector<double>& line )
{
	double strain = NAN;
	if ( _prog_name == Meca::TRAC6J )
	{
		strain = abs(get(line, Meca::STRAIN_TV) / get(line, Meca::STRAIN));
	}
	return strain;
}
/**********************************************/
/**********************************************/
/**********************************************/
double Meca::getStress( const QVector<double>& line )
{
	double stress = NAN;
	if ( index(Meca::STRESS) != -1)
		stress = get(line, Meca::STRESS);
	else if ( _prog_name == Meca::FLEX12 )
		stress = 1.5*(L/(b*e*e))*get(line, Meca::FORCE);
	else if ( _prog_name == Meca::IOSI3J ||
	          _prog_name == Meca::R8562 ||
	          _prog_name == Meca::VIEIL ||
	          _prog_name == Meca::ESSAIHT ||
	          _prog_name == Meca::TRAC3J ||
	          _prog_name == Meca::RESCOLAS ||
	          _prog_name == Meca::TRACMONO )
	{
		stress = get(line, Meca::FORCE)/(b*e);
		if ( e == -1 )
			stress *= -1;
	}
	return stress;
}
/**********************************************/
/**********************************************/
/**********************************************/
double Meca::getRE( const QVector<double>& line )
{
	double re = NAN;
	re = get(line, Meca::RE);
	if ( !isnan(re) )
		if ( re < 1 )
			re *= 1000;
	return re;
}
/**********************************************/
/**********************************************/
/**********************************************/
double Meca::getDRR0( double re )
{
	return 100*( re - R0 )/R0;
}
/**********************************************/
/**********************************************/
/**********************************************/
double Meca::getEA( const QVector<double>& line, double stress )
{
	double ea = NAN;
	if ( Meca::index(Meca::EA) != -1 )
	{
		ea = get(line, Meca::EA);
	}
	else if ( !_EAdata.isEmpty() )
	{
		for ( int it = _EAdatalastindex; it < _EAdata.size(); ++it )
		{
			if ( _EAdata.at(it-1).size() < 13 || _EAdata.at(it).size() < 13)
				break;

			if ( _ea_delta_t == -100000 && stress > 10 )
			{
				double s1 = _EAdata.at(it-1).at(1);
				double s2 = _EAdata.at(it).at(1);
				if ( (s1 <= stress && stress < s2) || (s2 <= stress && stress < s1) )
				{
					double t1 = _EAdata.at(it-1).at(0);
					double t2 = _EAdata.at(it).at(0);
					double t_ea = (t2-t1)*(stress-s1)/(s2-s1)+t1;
					double t = get(line, Meca::TIME);
					_ea_delta_t = t - t_ea;
				}
				else
					continue;
			}

			if ( _ea_delta_t != -100000 )
			{
				double t1 = _EAdata.at(it-1).at(0);
				double t2 = _EAdata.at(it).at(0);
				double t = get(line, Meca::TIME) - _ea_delta_t;

				if ( t1 <= t && t < t2 )
				{
					double ea1 = _EAdata.at(it-1).at(11);
					double ea2 = _EAdata.at(it).at(11);
					ea = ea2+(ea1-ea2)*(t-t2)/(t1-t2);
					_EAdatalastindex = it - 1;
					if ( _EAdatalastindex < 1 )
						_EAdatalastindex = 1;
					break;
				}
				else
					continue;
			}
			else
			{
				double s1 = _EAdata.at(it-1).at(1);
				double s2 = _EAdata.at(it).at(1);
				if ( (s1 <= stress && stress < s2) || (s2 <= stress && stress < s1) )
				{
					double ea1 = _EAdata.at(it-1).at(11);
					double ea2 = _EAdata.at(it).at(11);
					ea = ea2+(ea1-ea2)*(stress-s2)/(s1-s2);
					_EAdatalastindex = it - 10;
					if ( _EAdatalastindex < 1 )
						_EAdatalastindex = 1;
					break;
				}
				else
					continue;
			}

//			if ( (s1 <= stress && stress < s2) ||
//			     (s2 <= stress && stress < s1) ||
//			     (stress < 10 && abs(s2-stress) > min_diff) )
//			{
//				ea = ( _EAdata.at(it-1).at(11) - _EAdata.at(it).at(11) )*(stress-s2)/(s1-s2) + _EAdata.at(it).at(11);
//				_EAdatalastindex = it - 20;
//				if ( stress < 10 )
//					_EAdatalastindex = it;
//				if ( _EAdatalastindex < 1 )
//					_EAdatalastindex = 1;
//				break;
//			}
//			if ( s2 > _ea_max_stress )
//				_ea_max_stress = s2;
		}
	}
	return ea;
}
/**********************************************/
/**********************************************/
/**********************************************/
void Meca::PreTreatment()
{
	QVector<QVector<double>> data_tmp;
	for ( const QVector<double>& row : _raw_data )
	{
		int time = Meca::get(row, Meca::TIME);
		if ( time > time_cut && time_cut > 0 ) continue;
		QVector<double> row_tmp;
		for ( int index : _program )
		{
			row_tmp.append( Meca::get(row, index) );
		}
		// Save calculated Strain
		double strain_tmp = Meca::getStrain(row);
		if ( strain_tmp < 0 ) continue;
		row_tmp[Meca::STRAIN] = strain_tmp;
		// Save calculated Stress
		double stress_tmp = Meca::getStress(row);
		if ( stress_tmp < 0 ) continue;
		row_tmp[Meca::STRESS] = stress_tmp;
		// Save corrected Electrical Resistance
		double re_tmp = Meca::getRE(row);
		row_tmp[Meca::RE] = re_tmp;
		// Save RE Variation
		row_tmp[Meca::DRR0] = Meca::getDRR0(re_tmp);
		// Save EA
		row_tmp[Meca::EA] = Meca::getEA(row, stress_tmp);
		// Save Secant Modulus
		row_tmp[Meca::ESC] = 0.1*stress_tmp/strain_tmp;
		if ( _prog_name == Meca::TRAC6J )
		{
			double strain_tv_tmp = Meca::getTransversalStrain(row);
			row_tmp[Meca::STRAIN_TV] = strain_tv_tmp;
			row_tmp[Meca::NU_POIS] = -strain_tv_tmp/strain_tmp;
		}
		// Save treated row
		data_tmp.append( row_tmp );
	}
	_raw_data = data_tmp;
	Meca::calculateTangentModulus();
	Math::MoyenneGlissee( _raw_data );
	//
	Meca::getExtremes();
	_EAdatalastindex = 1;
	_ea_delta_t = -100000;
}
/**********************************************/
/**********************************************/
/**********************************************/
QVector<QVector<double>> Meca::raw_data()
{
	QVector<int> trd_data_frmt = { Meca::TIME, Meca::DEPL, Meca::FORCE };
	if ( Meca::index(Meca::TEMP) != -1 )
	{
		trd_data_frmt.append( Meca::TEMP );
	}
	if ( Meca::index(Meca::RE) != -1 )
	{
		trd_data_frmt.append( Meca::RE );
		trd_data_frmt.append( Meca::DRR0 );
	}
	if ( Meca::index(Meca::EA) != -1 || !_EAdata.isEmpty() )
	{
		trd_data_frmt.append( Meca::EA );
	}
	QVector<QVector<double>> data;
	for ( QVector<double> row : _raw_data )
	{
		QVector<double> row_tmp;
		for ( int index : trd_data_frmt )
		{
			row_tmp.append( row[index] );
		}
		data.append( row_tmp );
	}
	return data;
}
/**********************************************/
/**********************************************/
/**********************************************/
QString Meca::raw_data_header()
{
	QString header =  DataHeader::TIME + ";" + DataHeader::DEPL + ";" + DataHeader::FORCE;
	if ( Meca::index(Meca::TEMP) != -1 )
	{
		header.append( ";" + DataHeader::TEMP );
	}
	if ( Meca::index(Meca::RE) != -1 )
	{
		header.append( ";" + DataHeader::RE );
		header.append( ";" + DataHeader::DRR0 );
	}
	if ( Meca::index(Meca::EA) != -1 || !_EAdata.isEmpty() )
	{
		header.append( ";" + DataHeader::EA );
	}
	return header;
}
/**********************************************/
/**********************************************/
/**********************************************/
QVector<QVector<double>> Meca::treated_data()
{
	QVector<int> trd_data = { Meca::TIME, Meca::STRAIN, Meca::STRESS };
	if ( Meca::index(Meca::TEMP) != -1 )
	{
		trd_data.append( Meca::TEMP );
	}
	if ( Meca::index(Meca::RE) != -1 )
	{
		trd_data.append( Meca::RE );
		trd_data.append( Meca::DRR0 );
	}
	if ( Meca::index(Meca::EA) != -1 || !_EAdata.isEmpty() )
	{
		trd_data.append( Meca::EA );
	}
	if ( _prog_name != Meca::VIEIL )
	{
		trd_data.append( Meca::ETG );
		trd_data.append( Meca::ETGB );
		trd_data.append( Meca::ESC );
		if ( _prog_name == Meca::TRAC6J )
		{
			trd_data.append( Meca::STRAIN_TV );
			trd_data.append( Meca::NU_POIS );
		}
	}
	QVector<QVector<double>> data;
	for ( QVector<double> row : _raw_data )
	{
		QVector<double> row_tmp;
		for ( int index : trd_data )
		{
			row_tmp.append( row[index] );
		}
		data.append( row_tmp );
	}
	return data;
}
/**********************************************/
/**********************************************/
/**********************************************/
QString Meca::treated_data_header()
{
	QString header = DataHeader::TIME + ";" +  DataHeader::STRAIN + ";" + DataHeader::STRESS;
	if ( Meca::index(Meca::TEMP) != -1 )
	{
		header.append( ";" + DataHeader::TEMP );
	}
	if ( Meca::index(Meca::RE) != -1 )
	{
		header.append( ";" + DataHeader::RE );
		header.append( ";" + DataHeader::DRR0 );
	}
	if ( Meca::index(Meca::EA) != -1 || !_EAdata.isEmpty() )
	{
		header.append( ";" + DataHeader::EA );
	}
	if ( _prog_name != Meca::VIEIL )
	{
		header.append( ";" + DataHeader::ETG );
		header.append( ";" + DataHeader::ETGB );
		header.append( ";" + DataHeader::ESC );
		if ( _prog_name == Meca::TRAC6J )
		{
			header.append( ";" + DataHeader::DEF_TV );
			header.append( ";" + DataHeader::NU_POIS );
		}
	}
	return header;
}
/**********************************************/
/**********************************************/
/**********************************************/
QVector<QVector<double> > Meca::re_treated_data()
{
	QVector<QVector<double>> data;
	QVector<int> trd_data = { Meca::TIME, Meca::STRAIN, Meca::STRESS };
	if (  _testinfo.getString(TestInfoKeys::TestName).startsWith( "VIEIL_1300" ) )
	{
		trd_data.append( Meca::TEMP );
		QVector<QVector<double>> meca_data;
		if ( Files::readData( _testinfo.path() + "/" + Keys::fn_MecaTreatedData + ".dat", meca_data) )
		{
			double meca_time_off = 0;
			for ( QVector<double> row : meca_data )
			{
				if ( row[0] > 10.0 )
				{
					meca_time_off = row[2];
					break;
				}
			}
			double re_time_off = 0;
			for ( QVector<double> row : _raw_data )
			{
				if ( row[Meca::STRESS] > 10 )
				{
					re_time_off = row[Meca::TIME];
					break;
				}
			}
			for ( int jt = 0; jt < _raw_data.size(); ++jt )
			{
				double tim = _raw_data[jt][Meca::TIME]-re_time_off+meca_time_off;
				for ( int it = 0; it < meca_data.size()-1; ++it )
				{
					double tim1 = meca_data[it][0], tim2 = meca_data[it+1][0];
					if ( tim1 <= tim && tim <= tim2 )
					{
						double temp1 = meca_data[it][3], temp2 = meca_data[it+1][3];
						_raw_data[jt][Meca::TEMP] = temp1 + ( temp2 - temp1 )*( tim - tim1 )/( tim2 - tim1 );
						break;
					}
					if ( tim < tim1 )
						break;
				}
			}
		}
	}
	if ( Meca::index(Meca::RE) != -1 )
	{
		trd_data.append( Meca::RE );
		trd_data.append( Meca::DRR0 );
	}
	for ( QVector<double> row : _raw_data )
	{
		QVector<double> row_tmp;
		for ( int index : trd_data )
		{
			row_tmp.append( row[index] );
		}
		data.append( row_tmp );
	}
	return data;
}
/**********************************************/
/**********************************************/
/**********************************************/
QString Meca::re_treated_data_header()
{
	QString header = DataHeader::TIME + ";" +  DataHeader::STRAIN + ";" + DataHeader::STRESS;
	if (  _testinfo.getString(TestInfoKeys::TestName).startsWith( "VIEIL_1300" ) )
	{
		header.append( ";" + DataHeader::TEMP );
	}
	if ( Meca::index(Meca::RE) != -1 )
	{
		header.append( ";" + DataHeader::RE );
		header.append( ";" + DataHeader::DRR0 );
	}
	return header;
}
/**********************************************/
/**********************************************/
/**********************************************/
void Meca::calculateTangentModulus()
{
	if ( _raw_data.size() == 0 ||
	     _prog_name == Meca::VIEIL ) return;

	int nbPts = Settings::getInt( Keys::Regression, Keys::sampling );
	int hNbPts = 0.5*nbPts;
	int size = _raw_data.size();
	for ( int it = 0; it < size; ++it )
	{
		double alpha = NAN, beta = NAN;
		double xi = 0, yi = 0, wi = 1;
		double A = 0, B = 0, C = 0, D = 0, E = 0;
		if ( it >= hNbPts && it <= size - 1 - hNbPts )
		{
			int jmin = it - hNbPts;
			int jmax = it + hNbPts;
			for ( int jt = jmin; jt <= jmax ; ++jt )
			{
				xi = _raw_data[jt][Meca::STRAIN]/100;
				yi = _raw_data[jt][Meca::STRESS]/1000;
				A += wi*xi*xi;
				B += wi;
				C += wi*xi;
				D += wi*xi*yi;
				E += wi*yi;
			}
			alpha = (B*D - C*E)/(A*B-C*C);
			beta = (D - A*alpha)/C;
		}
		_raw_data[it][Meca::ETG] = alpha;
		_raw_data[it][Meca::ETGB] = beta;
		if ( _raw_data[it][Meca::STRAIN] != 0 )
			_raw_data[it][Meca::ESC] = 0.1*_raw_data[it][Meca::STRESS]/_raw_data[it][Meca::STRAIN];
		else
			_raw_data[it][Meca::ESC] = NAN;

	}
}
/**********************************************/
/**********************************************/
/**********************************************/
void Meca::getCycles()
{
	if ( !_testinfo.isValid() || _prog_name == Meca::VIEIL ) return;
	if ( !_testinfo.getString(TestInfoKeys::TestName).contains("_CYCL") )
		return;
	System::createDir(_testinfo.path(), Files::CyclesFolder);
	QString cpath = _testinfo.path() + "/" + Files::CyclesFolder;
	// Headers
	QString header = DataHeader::TIME + ";" + DataHeader::NCYCL + ";" + DataHeader::STCYCL + ";" + DataHeader::STRAIN + ";" + DataHeader::DEF_N + ";" + DataHeader::DEF_D + ";" + DataHeader::DEF_C + ";" + DataHeader::STRESS + ";";
	if ( Meca::index(Meca::TEMP) != -1 )
		header += DataHeader::TEMP + ";";
	if ( Meca::index(Meca::RE) != -1 )
		header += DataHeader::RE + ";" + DataHeader::DRR0 + ";";
	if ( Meca::index(Meca::EA) != -1 || !_EAdata.isEmpty() )
		header += DataHeader::EA + ";";
	header += DataHeader::ETG + ";" + DataHeader::ESC;
	QString header2 = DataHeader::STRAIN + ";" + DataHeader::STRESS + ";" + DataHeader::ETG + ";" + DataHeader::ESC;
	// Treated data
	QVector<QStringList> trd_data;
	QString t = QString::number(_raw_data.at(0).at(Meca::TIME)), ncycl = "0", step = "n";
	QString e = QString::number(_raw_data.at(0).at(Meca::STRAIN)), e_n = e, e_d = "", e_c = "";
	QString s = QString::number(_raw_data.at(0).at(Meca::STRESS));
	QString Etg = QString::number(_raw_data.at(0).at(Meca::ETG));
	QString Esc = QString::number(_raw_data.at(0).at(Meca::ESC));
	if ( Etg == "nan" ) Etg = "";
	if ( Esc == "nan" ) Esc = "";
	trd_data.append( {t, ncycl, step, e, e_n, e_d, e_c, s, Etg, Esc} );
	// Local variables
	int n_cycle = 0;
	bool charge = true;
	double lim_def = -1;
	QVector<QStringList> norm_data;
	QVector<QStringList> dech_data;
	QVector<QStringList> char_data;
	for ( int it = 1; it < _raw_data.size(); ++it )
	{
		QVector<double> row = _raw_data.at(it);
		double def = row[Meca::STRAIN];
		t = QString::number(row[Meca::TIME]), ncycl = QString::number(n_cycle);
		e = QString::number(def), e_n = "", e_d = "", e_c = "";
		s   = QString::number(row[Meca::STRESS]);
		Etg = QString::number(row[Meca::ETG]);
		if ( Etg == "nan" ) Etg = "";
		Esc = QString::number(row[Meca::ESC]);
		if ( Esc == "nan" ) Esc = "";
		step = "";

		int sensi = 20;
		if ( it >= sensi && it <= _raw_data.size() - 1 - sensi )
		{
			double alpha1 = 0, alpha2 = 0;
			int jmin = it-sensi, jmax = it+sensi;
			double xi = 0, yi = 0, wi = 1;
			double A = 0, B = 0, C = 0, D = 0, E = 0;
			for ( int jt = jmin; jt < it ; ++jt )
			{
				xi = _raw_data[jt][Meca::TIME];
				yi = _raw_data[jt][Meca::STRAIN];
				A += wi*xi*xi;
				B += wi;
				C += wi*xi;
				D += wi*xi*yi;
				E += wi*yi;
			}
			alpha1 = (B*D - C*E)/(A*B-C*C);
			A = 0, B = 0, C = 0, D = 0, E = 0;
			for ( int jt = it+1; jt <= jmax ; ++jt )
			{
				xi = _raw_data[jt][Meca::TIME];
				yi = _raw_data[jt][Meca::STRAIN];
				A += wi*xi*xi;
				B += wi;
				C += wi*xi;
				D += wi*xi*yi;
				E += wi*yi;
			}
			alpha2 = (B*D - C*E)/(A*B-C*C);
			if ( alpha1 > 0 && alpha2 < 0  && charge)
			{
				lim_def = _raw_data[it][Meca::STRAIN];
				charge = false;
				if ( char_data.size() > 0 )
					Files::save( cpath + "/c"+ncycl+".dat", char_data, header2 );
				char_data.clear();
				++n_cycle;
			}
			else if ( alpha1 < 0 && alpha2 > 0 && !charge )
			{
				charge = true;
				if ( dech_data.size() > 0 )
					Files::save( cpath + "/d"+ncycl+".dat", dech_data, header2 );
				dech_data.clear();
			}

		}


		if ( charge )
		{
			if ( def > lim_def )
			{
				e_n = e;
				step = "n";
				norm_data.append( {e, s, Etg, Esc} );
			}
			else
			{
				e_c = e;
				step = "c";
				char_data.append( {e, s, Etg, Esc} );
			}
		}
		else
		{
			e_d = e;
			step = "d";
			dech_data.append( {e, s, Etg, Esc} );
		}
		QStringList trd_data_line = {t, ncycl, step, e, e_n, e_d, e_c, s};
		if ( Meca::index(Meca::TEMP) != -1 )
		{
			trd_data_line.append( QString::number(_raw_data.at(it).at(Meca::TEMP)) );
		}
		if ( Meca::index(Meca::RE) != -1 )
		{
			trd_data_line.append( QString::number(_raw_data.at(it).at(Meca::RE)) );
			trd_data_line.append( QString::number(_raw_data.at(it).at(Meca::DRR0)) );
		}
		if ( Meca::index(Meca::EA) != -1 || !_EAdata.isEmpty())
		{
			trd_data_line.append( QString::number(_raw_data.at(it).at(Meca::EA)) );
		}
		trd_data_line.append( {Etg, Esc} );
		trd_data.append( trd_data_line );

		//		++lim_it;
		// Save Last Charge
		if ( it+1 == _raw_data.size() )
			Files::save( cpath + "/c"+ncycl+".dat", char_data, header2 );
	}
	Files::save( cpath + "/n.dat", norm_data, header2 );
	Files::save( cpath + "/cycles.dat", trd_data, header );
}
/**********************************************/
/**********************************************/
/**********************************************/
void Meca::getExtremes()
{
	if ( !_testinfo.isValid() ) return;
	QVector<double> maxes = Math::max( _raw_data );
	_testinfo.save( maxes[Meca::TIME], TestInfoKeys::TIME_MAX );
	_testinfo.save( maxes[Meca::STRAIN], TestInfoKeys::DEF_MAX );
	_testinfo.save( maxes[Meca::STRESS], TestInfoKeys::CTR_MAX );

	double extrem = maxes[Meca::DRR0];
	if ( !isnan(extrem) && !isinf(extrem) )
		_testinfo.save( extrem, TestInfoKeys::RE_MAX );
	extrem = Math::min( _raw_data, Meca::DRR0 );
	if ( !isnan(extrem) && !isinf(extrem) )
		_testinfo.save( extrem, TestInfoKeys::RE_MIN );

	extrem = maxes[Meca::TEMP];
	if ( !isnan(extrem) && !isinf(extrem) )
		_testinfo.save( extrem, TestInfoKeys::T_MAX );

	extrem = maxes[Meca::EA];
	if ( !isnan(extrem) && !isinf(extrem) )
		_testinfo.save( extrem, TestInfoKeys::EA_MAX );

	if ( _prog_name != Meca::VIEIL )
	{
		extrem = maxes[Meca::ETG];
		if ( !isnan(extrem) && !isinf(extrem) )
			_testinfo.save( extrem, TestInfoKeys::MT_MAX );

		extrem = maxes[Meca::ESC];
		if ( !isnan(extrem) && !isinf(extrem) )
			_testinfo.save( extrem, TestInfoKeys::MS_MAX );
	}
}
/**********************************************/
/**********************************************/
/**********************************************/
