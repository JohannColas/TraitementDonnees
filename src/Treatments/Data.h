#ifndef DATA_H
#define DATA_H
/**********************************************/
#include <QString>
#include <QVector>
#include <math.h>
#include "Commons/TestInfo.h"
/**********************************************/
/**********************************************/
struct DataHeader {
	static inline QString TIME    = "  time (s)";
	static inline QString DEPL    = "     dep (mm)";
	static inline QString FLE     = "     fle (mm)";
	static inline QString FORCE   = "    force (N)";
	static inline QString STRAIN  = "     e (perc)";
	static inline QString DEF_J1  = "  e_j1 (perc)";
	static inline QString DEF_J2  = "  e_j2 (perc)";
	static inline QString DEF_J3  = "  e_j3 (perc)";
	static inline QString STRESS  = "      s (MPa)";
	static inline QString RE      = "    RE (mOhm)";
	static inline QString DRR0    = " dR_R0 (perc)";
	static inline QString EA      = "     EA (cps)";
	static inline QString TEMP    = "        T (C)";
	static inline QString ETG     = "    Etg (GPa)";
	static inline QString ETGB    = "   beta (MPa)";
	static inline QString ESC     = "    Esc (GPa)";
	static inline QString NCYCL   = "       nCycle";
	static inline QString STCYCL  = "   Cycle step";
	static inline QString DEF_N   = "   e_n (perc)";
	static inline QString DEF_D   = "   e_d (perc)";
	static inline QString DEF_C   = "   e_c (perc)";
	static inline QString DEF_TV  = "  e_tv (perc)";
	static inline QString NU_POIS = "   nu_poisson";
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PlotData
{
	QString _path;
	QString _dataLB;
	QString _style;
	QString _legendLB;
	QString _legend;
public:
	PlotData( const QString& path, const QString& dataLB, const QString& style, const QString& legendLB, const QString& legend )
	    : _path(path), _dataLB(dataLB), _style(style), _legendLB(legendLB), _legend(legend)
	{

	}
	PlotData( const QString& path, const QString& dataLB )
	    : _path(path), _dataLB(dataLB)
	{

	}
	QString path() const { return _path; }
	QString dataLB() const { return _dataLB; }
	QString style() const { return _style; }
	QString legendLB() const { return _legendLB; }
	QString legend() const { return _legend; }
	void setPath( const QString& path ) { _path = path; }
	void setDataLB( const QString& dataLB ) { _dataLB = dataLB; }
	void setStyle( const QString& style ) { _style = style; }
	void setLegendLB( const QString& legendLB ) { _legendLB = legendLB; }
	void setLegend( const QString& legend ) { _legend = legend; }
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // DATA_H
