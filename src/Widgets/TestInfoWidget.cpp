#include "TestInfoWidget.h"
#include "ui_TestInfoWidget.h"
/**********************************************/
#include <QRegularExpressionValidator>
/**********************************************/
/**********************************************/
TestInfoWidget::TestInfoWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TestInfoWidget)
{
    ui->setupUi(this);

    QRegularExpression doubleRE = QRegularExpression("^(-?)(0|([1-9][0-9]*))(\\.[0-9]+)?$");
    ui->champ_longueur->setValidator( new QRegularExpressionValidator( doubleRE, ui->champ_longueur ));
    ui->champ_largeur->setValidator( new QRegularExpressionValidator( doubleRE, ui->champ_largeur ));
    ui->champ_epaisseur->setValidator( new QRegularExpressionValidator( doubleRE, ui->champ_epaisseur ));
    ui->champ_tcut->setValidator( new QRegularExpressionValidator( doubleRE, ui->champ_tcut ));

	sc_treat = new QShortcut( this );
	sc_treat->setKey( QKeySequence( Qt::Key_F5 ) );
	connect( sc_treat, &QShortcut::activated,
	         this, &TestInfoWidget::treatThis );
	sc_treatall = new QShortcut( this );
	sc_treatall->setKey( QKeySequence( Qt::CTRL | Qt::Key_F5 ) );
	connect( sc_treatall, &QShortcut::activated,
	         this, &TestInfoWidget::treatAll );
}
/**********************************************/
/**********************************************/
/**********************************************/
TestInfoWidget::~TestInfoWidget()
{
    delete ui;
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::linkToTestInfo( TestInfo* info )
{
    _testinfo = info;
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::updateTestInfo()
{
    bool testinfoisvalid = _testinfo->isValid();
    QString val = "";
	if ( !testinfoisvalid ) return;
	val = _testinfo->getString(TestInfoKeys::Material);
	ui->info_materiau->setText( val );
	val = _testinfo->getString(TestInfoKeys::Sample);
	ui->info_eprouvette->setText( val );
	val = _testinfo->getString(TestInfoKeys::TestName);
	ui->info_essai->setText( val );
	val = _testinfo->getString(TestInfoKeys::TestDate);
	QDate date = QDate::fromString(val,"dd/MM/yyyy");
	if ( !date.isValid() )
		date = QDate::fromString("01/01/2000","dd/MM/yyyy");
	ui->de_dateEssai->setDate( date );
	val = QString::number( _testinfo->getDouble(TestInfoKeys::Length) );
	ui->champ_longueur->setText( val );
	val = QString::number( _testinfo->getDouble(TestInfoKeys::Width) );
	ui->champ_largeur->setText( val);
	val = QString::number( _testinfo->getDouble(TestInfoKeys::Thickness) );
	ui->champ_epaisseur->setText( val );
	val = QString::number( _testinfo->getDouble(TestInfoKeys::TimeCut) );
	ui->champ_tcut->setText( val );
	val = _testinfo->getString(TestInfoKeys::AvailableTracking);
	ui->champ_suivi->setText( val );
	val = _testinfo->getString(TestInfoKeys::Comments);
	ui->champ_commentaire->setPlainText( val );
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::on_info_materiau_editingFinished()
{
    if ( _testinfo->isValid() )
        _testinfo->save( ui->info_materiau->text(), TestInfoKeys::Material );
    else
        ui->info_materiau->setText("");
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::on_info_eprouvette_editingFinished()
{
    if ( _testinfo->isValid() )
        _testinfo->save( ui->info_eprouvette->text(), TestInfoKeys::Sample );
    else
        ui->info_eprouvette->setText("");
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::on_info_essai_editingFinished()
{
    if ( _testinfo->isValid() )
        _testinfo->save( ui->info_essai->text(), TestInfoKeys::TestName );
    else
        ui->info_essai->setText("");
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::on_de_dateEssai_userDateChanged(const QDate &date)
{
	if ( _testinfo->isValid() )
		_testinfo->save( ui->de_dateEssai->text(), TestInfoKeys::TestDate );
	else
		ui->de_dateEssai->setDate(QDate());
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::on_champ_longueur_editingFinished()
{
    if ( _testinfo->isValid() )
        _testinfo->save( ui->champ_longueur->text().toDouble(), TestInfoKeys::Length );
    else
        ui->champ_longueur->setText("");
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::on_champ_largeur_editingFinished()
{
    if ( _testinfo->isValid() )
        _testinfo->save( ui->champ_largeur->text().toDouble(), TestInfoKeys::Width );
    else
        ui->champ_largeur->setText("");
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::on_champ_epaisseur_editingFinished()
{
    if ( _testinfo->isValid() )
        _testinfo->save( ui->champ_epaisseur->text().toDouble(), TestInfoKeys::Thickness );
    else
        ui->champ_epaisseur->setText("");
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::on_champ_tcut_editingFinished()
{
    if ( _testinfo->isValid() )
        _testinfo->save( ui->champ_tcut->text().toDouble(), TestInfoKeys::TimeCut );
    else
        ui->champ_tcut->setText("");
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::on_champ_suivi_editingFinished()
{
    if ( _testinfo->isValid() )
        _testinfo->save( ui->champ_suivi->text(), TestInfoKeys::AvailableTracking );
    else
        ui->champ_suivi->setText("");
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::on_champ_commentaire_editingFinished()
{
    if ( _testinfo->isValid() )
        _testinfo->save( ui->champ_commentaire->toPlainText(), TestInfoKeys::Comments );
    else
		ui->champ_commentaire->setPlainText("");
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::on_treat()
{
	saveEverything();
	emit treatThis();
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::on_treatAll()
{
	saveEverything();
	emit treatAll();
}
/**********************************************/
/**********************************************/
/**********************************************/
void TestInfoWidget::saveEverything()
{
	if ( !_testinfo->isValid() ) return;
	_testinfo->save( ui->info_materiau->text(), TestInfoKeys::Material );
	_testinfo->save( ui->info_eprouvette->text(), TestInfoKeys::Sample );
	_testinfo->save( ui->info_essai->text(), TestInfoKeys::TestName );
	_testinfo->save( ui->de_dateEssai->text(), TestInfoKeys::TestDate );
	_testinfo->save( ui->champ_longueur->text().toDouble(), TestInfoKeys::Length );
	_testinfo->save( ui->champ_largeur->text().toDouble(), TestInfoKeys::Width );
	_testinfo->save( ui->champ_epaisseur->text().toDouble(), TestInfoKeys::Thickness );
	_testinfo->save( ui->champ_tcut->text().toDouble(), TestInfoKeys::TimeCut );
	_testinfo->save( ui->champ_suivi->text(), TestInfoKeys::AvailableTracking );
	_testinfo->save( ui->champ_commentaire->toPlainText(), TestInfoKeys::Comments );
}
/**********************************************/
/**********************************************/
/**********************************************/


