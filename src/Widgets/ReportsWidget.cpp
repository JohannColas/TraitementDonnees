#include "ReportsWidget.h"
#include "ui_ReportsWidget.h"
/**********************************************/
#include <QDesktopServices>
#include <QMessageBox>
#include <QInputDialog>
/**********************************************/
#include "Commons/Files.h"
#include <BasicWidgets/WaitingDialog.h>
#include <Commons/LaTeX.h>
/**********************************************/
/**********************************************/
/**********************************************/
ReportsWidget::ReportsWidget( QWidget *parent ) :
    QWidget( parent ),
    ui( new Ui::ReportsWidget )
{
	ui->setupUi(this);
	ui->le_name->show();
	ui->wid_cb->hide();

	sc_compile = new QShortcut( this );
	sc_compile->setKey( QKeySequence( Qt::Key_F5 ) );
	connect( sc_compile, &QShortcut::activated,
	         this, &ReportsWidget::on_pb_savecompile_released );
	sc_compile2 = new QShortcut( this );
	sc_compile2->setKey( QKeySequence( Qt::CTRL | Qt::Key_S ) );
	connect( sc_compile2, &QShortcut::activated,
	         this, &ReportsWidget::on_pb_savecompile_released );
	sc_nextReport = new QShortcut( this );
	sc_nextReport->setKey( QKeySequence( Qt::Key_F4 ) );
	connect( sc_nextReport, &QShortcut::activated,
	         this, &ReportsWidget::nextReport );
	sc_previousReport = new QShortcut( this );
	sc_previousReport->setKey( QKeySequence( Qt::Key_F3 ) );
	connect( sc_previousReport, &QShortcut::activated,
	         this, &ReportsWidget::previousReport );
	sc_delete = new QShortcut( this );
	sc_delete->setKey( QKeySequence( Qt::CTRL | Qt::Key_Delete ) );
	connect( sc_delete, &QShortcut::activated,
	         this, &ReportsWidget::deleteFiles );
}
/**********************************************/
/**********************************************/
/**********************************************/
ReportsWidget::~ReportsWidget()
{
	delete ui;
}
/**********************************************/
/**********************************************/
/**********************************************/
void ReportsWidget::update()
{
	QString ext = _path.mid( _path.lastIndexOf(".") );
	if ( !_path.isEmpty() && QDir(_path).exists() )
	{
		ui->le_name->hide();
		ui->wid_cb->show();
		ui->le_name->setText( "--" );
		updatePlotSelector();
		sc_nextReport->blockSignals(false);
		sc_previousReport->blockSignals(false);
	}
	else if ( ext == ".pdf" || ext == ".tex" )
	{
		ui->le_name->show();
		ui->wid_cb->hide();
		QFileInfo info( _path );
		ui->le_name->setText( info.baseName() );
		showFile( _path );
		sc_nextReport->blockSignals(true);
		sc_previousReport->blockSignals(true);
	}
	else
	{
		ui->le_name->show();
		ui->wid_cb->hide();
		ui->le_name->setText( "--" );
		ui->te_file->setPlainText( "" );
		sc_nextReport->blockSignals(true);
		sc_previousReport->blockSignals(true);
	}
}
/**********************************************/
/**********************************************/
/**********************************************/
void ReportsWidget::updatePlotSelector()
{
	QStringList files = Files::ListReports( _path, true);
	ui->sel_file->clear();
	ui->sel_file->addItems(files);
}
/**********************************************/
/**********************************************/
/**********************************************/
void ReportsWidget::showFile( const QString& path )
{
	QString path2 = path;
	path2.replace( ".pdf", ".tex" );
	QString content = "";
	QFileInfo info( path2 );
	if ( info.exists() )
	{
		Files::read( path2, content );
		ui->te_file->setPlainText( content );
	}
}
/**********************************************/
/**********************************************/
/**********************************************/
void ReportsWidget::on_sel_file_currentIndexChanged( int index )
{
	QString arg1 = ui->sel_file->itemText( index );
	showFile( _path + "/" + arg1 );
}
/**********************************************/
/**********************************************/
/**********************************************/
void ReportsWidget::on_pb_new_released()
{
	QString filename = ui->le_name->text();
	if ( filename.isEmpty() || filename == "--" )
	{
		bool ok;
		QString text = QInputDialog::getText(this, tr("Entrer le nom du fichier !"),
		                                     tr("Nom du fichier :"), QLineEdit::Normal,
		                                     "Nouveau Rapport", &ok);
		if ( !ok )
			return;
		filename = text;
	}

	QString content;
	Files::read( Settings::getString(Keys::LaTeXDir) + "/reports/minirapport.tex", content );
	QString path = _path;
	if ( ui->le_name->isVisible() )
		path = QFileInfo(_path).absolutePath();
	Files::save( path + "/" + filename + ".tex", content );

	if ( ui->le_name->isVisible() )
	{
		_path = path + "/" + filename + ".tex";
		update();
	}
	else if ( ui->sel_file->isVisible() )
	{
		updatePlotSelector();
		int it = 0;
		for (; it < ui->sel_file->count(); ++it )
		{
			if ( ui->sel_file->itemText(it).contains(filename + ".tex") )
				break;
		}
		if ( it < ui->sel_file->count() )
			ui->sel_file->setCurrentIndex( it );
		else
			ui->sel_file->setCurrentIndex( 0 );
	}

}
/**********************************************/
/**********************************************/
/**********************************************/
void ReportsWidget::on_pb_externe_released()
{
	if ( ui->te_file->toPlainText().isEmpty() )
		return;
	QString path = _path;
	if ( ui->le_name->isVisible() )
	{
		path = _path;
	}
	else
	{
		path = _path + "/" + ui->sel_file->currentText();
	}
//	path.replace( ".tex", ".pdf" );
	QDesktopServices::openUrl( QUrl( "file://"+ path ) );
}
/**********************************************/
/**********************************************/
/**********************************************/
void ReportsWidget::on_pb_savecompile_released()
{
	if ( ui->te_file->toPlainText().isEmpty() )
		return;
	WaitingDialog* wdiag = new WaitingDialog( "", "Compilation en cours..." );
	wdiag->show();
	QString filename;
	QString path = _path;
	QFileInfo info(_path);
	if ( ui->le_name->isVisible() )
	{
		filename = ui->le_name->text();
		path = info.absolutePath();
	}
	else
		filename =  ui->sel_file->currentText();
	filename.replace( ".tex", "" );
	Files::save( path + "/" + filename + ".tex", ui->te_file->toPlainText() );
	QEventLoop loop;
	LaTeXThread thread = LaTeXThread( path, filename, false );
	connect( &thread, SIGNAL(finished()),
	         &loop, SLOT(quit()) );
	thread.start();
	loop.exec();
	wdiag->close();
	wdiag->deleteLater();
	if ( ui->le_name->isVisible() )
		emit sendPath( info.absolutePath() + "/" + filename + ".tex" );
	ui->sel_file->blockSignals(true);
	ui->sel_file->clearFocus();
	ui->sel_file->blockSignals(false);
	ui->le_name->blockSignals(true);
	ui->le_name->clearFocus();
	ui->le_name->blockSignals(false);
}
/**********************************************/
/**********************************************/
/**********************************************/
void ReportsWidget::nextReport()
{
	int newIndex = ui->sel_file->currentIndex() + 1;
	if ( newIndex > ui->sel_file->count() - 1 )
		newIndex = 0;
	ui->sel_file->setCurrentIndex( newIndex );
}
/**********************************************/
/**********************************************/
/**********************************************/
void ReportsWidget::previousReport()
{
	int newIndex = ui->sel_file->currentIndex() - 1;
	if ( newIndex < 0 )
		newIndex = ui->sel_file->count()-1;
	ui->sel_file->setCurrentIndex( newIndex );
}
/**********************************************/
/**********************************************/
/**********************************************/
void ReportsWidget::deleteFiles()
{
	QMessageBox confBox;
	QString filename;
	if ( ui->le_name->isVisible() )
		filename = ui->le_name->text();
	else if ( ui->sel_file->isVisible() )
		filename = ui->sel_file->currentText();
	confBox.setText( "Le graphique " + filename + " va être supprimer." );
	confBox.setInformativeText( "Confirmez-vous cette opération ?" );
	confBox.setIcon( QMessageBox::Warning );
	confBox.setStandardButtons( QMessageBox::Yes | QMessageBox::Cancel );
	confBox.setDefaultButton( QMessageBox::Cancel );
	int ret = confBox.exec();
	if ( ret == QMessageBox::Cancel )
		return;
	else if ( ret == QMessageBox::Yes )
	{
		QString path = _path;
		QFileInfo info(_path);
		if ( ui->le_name->isVisible() )
			path = info.absolutePath();
		info.setFile( path );
		filename.replace( ".tex", "" );

		QFileInfoList filestodelete = QDir( path ).entryInfoList( {filename+".*"}, QDir::Files | QDir::NoDotAndDotDot );
		filestodelete.append( QDir( path + "/LaTeX" ).entryInfoList( {filename+".*"}, QDir::Files | QDir::NoDotAndDotDot ) );

		for ( const QFileInfo &info : filestodelete )
			QFile::moveToTrash( info.absoluteFilePath() );

		if ( ui->le_name->isVisible() )
		{
			_path = "";
			update();
		}
		else if ( ui->sel_file->isVisible() )
		{
			int newIndex = ui->sel_file->currentIndex() - 1;
			if ( newIndex < 0 )
				newIndex = 0;
			updatePlotSelector();
			ui->sel_file->setCurrentIndex( newIndex );
		}
	}
}
/**********************************************/
/**********************************************/
/**********************************************/
