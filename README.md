# TraitementDonnees

Ce programme a été crée dans le but de traiter les données issues d'essais mécaniques.
Attention : Il n'est pas universel. Il nécessite une adaptation selon votre besoin.

## Program Info
Ce programme a été développé en c++ avec utilisation des librairies Qt.

## Pre-requirements
- LaTeX builder (LuaLaTeX)
- imagemagick
- ffmpeg

## License
CC-Zero ("No Rights Reserved")
